package com.hrsystem.svc;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import com.hrsystem.businessService.PerformanceReviewService;
import com.hrsystem.dao.PerformanceReviewDAO;
import com.hrsystem.vo.RequestPerformanceVO;
import com.hrsystem.vo.RequestTaskActivityVO;
import com.hrsystem.vo.RequestTaskAssignedVO;

public class PerformanceReviewBusinessSvc implements PerformanceReviewService{

	PerformanceReviewDAO CDAO =new PerformanceReviewDAO();
	public String PostServiceForAddTask(RequestTaskAssignedVO request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForAddTask(request);
		return returnJson;
	}

	
	public String PostServiceForPerformance(RequestPerformanceVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		String returnJson = "";
		returnJson= CDAO.PostServiceForPerformance(request);
		return returnJson;
	}


	public String PostServiceForTaskList(String request) throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForTaskList(request);
		return returnJson;
	}


	public String PostServiceForTaskActivity(RequestTaskActivityVO request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForTaskActivity(request);
		return returnJson;
	}


	public String PostServiceForTaskActivityList(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForTaskActivityList(request);
		return returnJson;
	}


	
	public String PostServiceForViewActivity(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForViewActivity(request);
		return returnJson;
	}


	
	public String PostServiceForTaskListAllEmp()
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForTaskListAllEmp();
		return returnJson;
	}


	public String PostServiceForActivityListAllEmp() throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForActivityListAllEmp();
		return returnJson;
	}



	public String PostServiceForReviewTaskGetActivity(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForReviewTaskGetActivity(request);
		return returnJson;
	}



	public String PostServiceForReviewTaskGetTask(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForReviewTaskGetTask(request);
		return returnJson;
	}



	public String PostServiceForSubmitWeight(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForSubmitWeight(request);
		return returnJson;
	}



	public String PostServiceForGetTaskByEmpId(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForGetTaskByEmpId(request);
		return returnJson;
	}


	
	public String PostServiceForgetReviewByEmpId(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForgetReviewByEmpId(request);
		return returnJson;
	}


	
	public String PostServiceForgetReviewByPerformanceId(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForgetReviewByPerformanceId(request);
		return returnJson;
	}


	
	public String DeleteServicePeroformanceReviewByPerformanceId(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.DeleteServicePeroformanceReviewByPerformanceId(request);
		return returnJson;
	}


	public String GetServiceEmpPayroll(String request) throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceEmpPayroll(request);
		return returnJson;
	}


	
	public String GetServiceReviewViewEmployee(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceReviewViewEmployee(request);
		return returnJson;
	}


	
	public String PostServiceSaveSignEmp(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceSaveSignEmp(request);
		return returnJson;
	}


	public String PostServiceSaveSignAdmin(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceSaveSignAdmin(request);
		return returnJson;
	}

}
