package com.hrsystem.svc;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.Date;

import com.hrsystem.businessService.LeaveService;
import com.hrsystem.dao.LeaveDAO;
import com.hrsystem.vo.RequestLeaveVO;

public class LeaveBusinessSvc implements LeaveService {

	LeaveDAO LDAO= new LeaveDAO();
	public String PostServiceForApplyLeave(RequestLeaveVO request) {
		String returnJson = "";
		returnJson= LDAO.PostServiceForApplyLeave(request);
		
		return returnJson;	
		}
	public String GetMethodForLeaveList() throws GeneralSecurityException,
			IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.GetMethodForLeaveList();
		
		return returnJson;	
		
	}
	public String PostMethodForLeaveListById(String request) {
		String returnJson = "";
		returnJson= LDAO.PostMethodForLeaveListById(request);
		
		return returnJson;	
	}
	public String PostMethodForViewLeaveId(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostMethodForViewLeaveId(request);
		
		return returnJson;
	}
	
	public String PostMethodForApproveLeaveId(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostMethodForApproveLeaveId(request);
		
		return returnJson;
	}
	public String PostMethodForRejectLeaveId(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostMethodForRejectLeaveId(request);
		
		return returnJson;
	
	}
	public String PostMethodForLeaveEmpId(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostMethodForLeaveEmpId(request);
		
		return returnJson;
		}
	public String PostMethodForLeaveStatus(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostMethodForLeaveStatus(request);
		
		return returnJson;
		}
	public String PostMethodForGetLeaveTotal(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostMethodForGetLeaveTotal(request);
		
		return returnJson;

	}
	public String PostServiceForApplyLeaveReschedule(RequestLeaveVO request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostServiceForApplyLeaveReschedule(request);
		
		return returnJson;	
	}
	public String PostMethodForfilterAllEmployeeStatus(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostServiceForfilterAllEmployeeStatus(request);
		
		return returnJson;	
	}
	public String PostMethodForCancelLeaveId(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostServiceForCancelLeaveId(request);
		
		return returnJson;	

	}
	public String PostMethodForLeavechangeStatus(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= LDAO.PostServiceForLeavechangeStatus(request);
		
		return returnJson;	
	}
	 
}
