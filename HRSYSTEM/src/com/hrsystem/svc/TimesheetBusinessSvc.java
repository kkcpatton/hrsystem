package com.hrsystem.svc;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;

import com.hrsystem.businessService.TimesheetService;
import com.hrsystem.dao.TimesheetDAO;
import com.hrsystem.vo.RequestTimeSheetNewVO;
import com.hrsystem.vo.RequestTimesheetVO;

public class TimesheetBusinessSvc implements TimesheetService {

	 TimesheetDAO CDAO = new TimesheetDAO();
	 
	public String GetServiceForEmployeeListById(String id)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForEmployeeListById(id);
		
		return returnJson;
	}


	
	public String PostServiceForNewTimeSheet(List<RequestTimeSheetNewVO> request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForNewTimeSheet(request);
		
		return returnJson;
	}


	public String GetServiceForGetTimeSheet(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForGetTimeSheet(request);
		
		return returnJson;
	}



	
	public String getTimeSheetServiceAdmin(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.getTimeSheetServiceAdmin(request);
		
		return returnJson;
	}



	public String getTimeSheetServiceEmpName(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.getTimeSheetServiceEmpName(request);
		
		return returnJson;
	}




	public String PostTimseSheetStatusAdmin(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostTimseSheetStatusAdmin(request);
		
		return returnJson;
	}

}
