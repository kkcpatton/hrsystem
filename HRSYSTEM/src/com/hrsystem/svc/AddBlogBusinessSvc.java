package com.hrsystem.svc;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import com.hrsystem.businessService.AddBlogService;

import com.hrsystem.dao.AddBlogDAO;
import com.hrsystem.vo.RequestBlogCreateVO;
import com.hrsystem.vo.RequestBlogPostVO;


public class AddBlogBusinessSvc implements AddBlogService{
	
	AddBlogDAO CDAO =new AddBlogDAO();
	
	
	public String PostServiceForAddBlog(RequestBlogCreateVO request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForAddBlog(request);
		
		return returnJson;
	}


	
	public String GetServiceForGetBlog() throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForGetBlog();
		
		return returnJson;
	}



	public String PostServiceForAddBlogPost(RequestBlogPostVO request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForGetBlogPost(request);
		
		return returnJson;
	}



	
	public String GetServiceForGetBlogPost() throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForGetBlogPostdisplay();
		
		return returnJson;
	}




	public String PostServiceForDeleteBlogPost(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForDeleteBlogPost(request);
		
		return returnJson;
	}

}
