package com.hrsystem.svc;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import com.hrsystem.businessService.AddEmployeeService;
import com.hrsystem.businessService.ExpenseService;
import com.hrsystem.dao.AddEmployeeDAO;
import com.hrsystem.dao.ExpenseDAO;
import com.hrsystem.vo.RequestEmployeeCreateVO;
import com.hrsystem.vo.RequestExpenseCreateVO;

public class ExpenseBusinessSvc implements ExpenseService {
	ExpenseDAO CDAO =new ExpenseDAO();
	
	public String GetServiceForExpenseEmpList()
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForExpenseEmpList();
		
		return returnJson;
	}
	
	public String PostServiceForAddExpense(RequestExpenseCreateVO request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForAddExpense(request);
		
		return returnJson;
	}

	
	public String GetServiceForExpenseList(String request) 
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForExpenseList(request);
		
		return returnJson;
		
		
	}
	public String GetServiceForExpenseListStatus(String[] request) 
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForExpenseListStatus(request);
		
		return returnJson;
		
		
	}
	public String GetServiceForExpenseList1(String request) 
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForExpenseList1(request);
		
		return returnJson;
		
		
	}
	
	public String GetServiceForExpenseListView(String request) 
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForExpenseListView(request);
		
		return returnJson;
		
		
	}
	
	public String PostServiceForExpenseUpdateController(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForExpenseUpdate(request);
		
		return returnJson;
		
	}

	
	public String PostServiceForExpenseStatusController(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForExpenseStatus(request);
		
		return returnJson;
	}

	
	public String GetServiceForExpenseListController()
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForExpenseListController();
		
		return returnJson;
	}

	
	public String GetServiceForGetNameController(String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		String returnJson = "";
		returnJson= CDAO.GetServiceForGetNameController(request);
		
		return returnJson;
	}

	
	public String GetServiceFoTotalExpense(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceFoTotalExpense(request);
		
		return returnJson;
	}

	
	public String PostServiceForDeleteExpenseController(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForDeleteExpense(request);
		
		return returnJson;
	}


	
}
