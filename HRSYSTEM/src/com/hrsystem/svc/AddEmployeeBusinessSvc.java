package com.hrsystem.svc;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import com.hrsystem.businessService.AddEmployeeService;
import com.hrsystem.dao.AddEmployeeDAO;
import com.hrsystem.vo.RequestEmployeeCreateVO;

public class AddEmployeeBusinessSvc implements AddEmployeeService{

	AddEmployeeDAO CDAO =new AddEmployeeDAO();
	public String PostServiceForAddEmployee(RequestEmployeeCreateVO request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForAddEmployee(request);
		
		return returnJson;
	}
	public String GetServiceForEmployeeList()
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForEmployeeList();
		
		return returnJson;
	}
	public String PostServiceForDeleteEmployee(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForDeleteEmployee(request);
		
		return returnJson;
	}
	
	public String GetServiceForEmployeeList1()
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForEmployeeList1();
		
		return returnJson;
	}
	
	public String PostServiceForviewEmployee(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForViewEmployee(request);
		
		return returnJson;
	}
	
	public String PostServiceForImageUpload(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForImageUpload(request);
		
		return returnJson;
	}

	public String PostServiceForImageUploadViewController(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForImageUploadViewController(request);
		
		return returnJson;
	}
	
	public String GetServiceForChartList() throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForChartList();
		
		return returnJson;
	}
	
	public String GetServiceForManagerList() throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForManagerList();
		
		return returnJson;
	}
	

}
