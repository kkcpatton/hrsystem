package com.hrsystem.svc;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;

import com.hrsystem.businessService.StatusReportService;
import com.hrsystem.dao.StatusReportDAO;
import com.hrsystem.vo.RequestStatusReportAcceptVO;
import com.hrsystem.vo.RequestStatusReportVO;

public class StatusReportBusinessSvc implements StatusReportService {
	StatusReportDAO CDAO =new StatusReportDAO();
	
	public String PostServiceForStatusReport(RequestStatusReportVO request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForStatusReport(request);
		
		return returnJson;
	}

	
	public String GetServiceForStatusList() throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForStatusList();
		
		return returnJson;
	}


	
	public String PostServiceForGetStatusByEmp(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForStatusListEmp(request);
		
		return returnJson;
	}


	
	public String PostServiceForStatusReportViewAdmin(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForStatusReportViewAdmin(request);
		
		return returnJson;
	}



	public String PostServiceForStatusReportAdminAccept(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForStatusReportAdminAccept(request);
		
		return returnJson;
	}


	public String GetServiceForStatusListAll() 
			throws GeneralSecurityException, IOException, ParseException {

		String returnJson = "";
		returnJson= CDAO.GetServiceForStatusListAll();
		
		return returnJson;
	}


	public String GetServiceForStatusListEmployeeId(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForStatusListEmployeeId(request);
		
		return returnJson;
	}


	
	public String GetServiceForStatusListBasedId(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForStatusListBasedId(request);
		
		return returnJson;
	}


	
	public String GetServiceForMonthlyStatusListBasedId(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForMonthlyStatusListBasedId(request);
		
		return returnJson;
	}


	
	public String GetServiceForEmployeeIdPayroll(String request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.GetServiceForEmployeeIdPayroll(request);
		
		return returnJson;
	}


	
	public String PostServiceForStatusReportNewAct(List<RequestStatusReportVO> request)
			throws GeneralSecurityException, IOException, ParseException {
		
		String returnJson = "";
		returnJson= CDAO.PostServiceForStatusReportNewAct(request);
		
		return returnJson;
	}


	
	public String PostServiceForDeleteStatusReport(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForDeleteStatusReport(request);
		
		return returnJson;
	}


	
	public String PostServiceForStatusReportByMonthEmployee(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForStatusReportByMonthEmployee(request);
		
		return returnJson;
	}


	
	public String PostServiceForStatusRecentUpdatesGet(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForStatusRecentUpdatesGet(request);
		
		return returnJson;
	}



	public String PostServiceForGetStatusReportReview(String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForGetStatusReportReview(request);
		
		return returnJson;
	}



	public String PostServiceForAcceptStatusAdmin(List<RequestStatusReportAcceptVO> request)
			throws GeneralSecurityException, IOException, ParseException {
		String returnJson = "";
		returnJson= CDAO.PostServiceForAcceptStatusAdmin(request);
		
		return returnJson;
	}


	
	

	
}
