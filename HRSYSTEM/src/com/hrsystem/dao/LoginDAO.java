package com.hrsystem.dao;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.hrsystem.db.DbServices;
import com.hrsystem.email.SimpleSendEmail;
import com.hrsystem.vo.RequestChangePasswordVO;
import com.hrsystem.vo.RequestLoginVO;
import com.hrsystem.vo.ResponseEmployeeLoginVO;
import com.hrsystem.vo.ResponseLoginVO;




@RestController
public class LoginDAO {

	public static String LoggedUser;
	public static String LoggedUserEmail;
	
	
	@Autowired
	private HttpServletRequest request;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceLogin(@RequestBody RequestLoginVO userLogin)
			throws GeneralSecurityException, IOException, InterruptedException, ParseException {
		
      

		List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();
		
		Connection conn = null;

		ResultSet rs = null;
		String returJson="";
		 String employeeId="";
		 String firstName="";
		 String lastName="";
		 String dateOfBirth="";
		 String phoneNumberPrimary="";
		 String phoneNumberSecondary="";
		 String emailPrimary="";
		 String emailSecondary="";
		 String location="";
		 String dateOfJoined="";
		 String clientName="";
		 String designation="";
		 String role="";
		 String employeePassword="";	
		 String employeeStatus="";
		  String name="";
		  String reportTo="";
		  String payroll="";
		  
		 String SELECT="";
		try {
			
			 SELECT = "SELECT EMPLOYEE_ID,FIRST_NAME,LAST_NAME,DATE_OF_BIRTH,PHONE_NUMBER_PRIMARY,PHONE_NUMBER_SECONDARY,EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL  FROM EMPLOYEE_DETAILS WHERE EMAIL_PRIMARY='"+userLogin.getUserName()+"' AND PASSWORD='"+userLogin.getPassword()+"'AND STATUS='Active' ";
			    conn = DbServices.getConnection();
            	PreparedStatement statement = conn.prepareStatement(SELECT);
				rs = statement.executeQuery();
				
				while (rs.next()) {

					if (rs.getString(1) != null) {
						employeeId = rs.getString(1);

					} else {
						employeeId = null;

					}
					if (rs.getString(2) != null) {
						firstName = rs.getString(2);
						LoggedUser=rs.getString(2);
						System.out.println("USER FIRST NAME -->"+firstName);
					
						HttpSession session = request.getSession();
					        session.setAttribute("name", LoggedUser);

					} else {
						firstName = null;

					}
					if (rs.getString(3) != null) {
						lastName = rs.getString(3);

					} else {
						lastName = null;

					}
			   		if (rs.getString(4) != null) {
			   			dateOfBirth = rs.getString(4);
			   			Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfBirth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfBirth = sdf.format(cal.getTime());
					} else {
						dateOfBirth = null;

					}
			   		if (rs.getString(5) != null) {
			   			phoneNumberPrimary = rs.getString(5);

					} else {
						phoneNumberPrimary = null;

					}
			   		if (rs.getString(6) != null) {
			   			phoneNumberSecondary = rs.getString(6);

					} else {
						phoneNumberSecondary = null;

					}
					if (rs.getString(7) != null) {
						emailPrimary = rs.getString(7);
						LoggedUserEmail= rs.getString(7);
						System.out.println("USER email -->"+emailPrimary);
						HttpSession session = request.getSession();
				        session.setAttribute("email", LoggedUserEmail);
						
					} else {
						emailPrimary = null;

					}
					if (rs.getString(8) != null) {
						emailSecondary = rs.getString(8);
						
					} else {
						emailSecondary = null;
						
					}
					if (rs.getString(9) != null) {
						location = rs.getString(9);
						
					} else {
						location = null;
						
					}
					if (rs.getString(10) != null) {
						dateOfJoined = rs.getString(10);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfJoined));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfJoined = sdf.format(cal.getTime());
					} else {
						dateOfJoined = null;
						
					}
					if (rs.getString(11) != null) {
						clientName = rs.getString(11);
						
					} else {
						clientName = null;
						
					}
					if (rs.getString(12) != null) {
						designation = rs.getString(12);
						
					} else {
						designation = null;
						
					}
					if (rs.getString(13) != null) {
						role = rs.getString(13);
						
					} else {
						role = null;
						
					}
					if (rs.getString(14) != null) {
						employeePassword = rs.getString(14);
						
					} else {
						employeePassword = null;
						
					}
					if (rs.getString(15) != null) {
						employeeStatus = rs.getString(15);
						
					} else {
						employeeStatus = null;
						
					}
					if (rs.getString(16) != null) {
						reportTo = rs.getString(16);
						
					} else {
						reportTo = null;
						
					}
					
					name=firstName+" "+lastName;
					if (rs.getString(17) != null) {
						payroll = rs.getString(17);
						
					} else {
						payroll = null;
						
					}
			 
					ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
							employeeId,  firstName,lastName,
							 dateOfBirth,phoneNumberPrimary,phoneNumberSecondary,emailPrimary,emailSecondary,location, dateOfJoined,clientName,
							 designation,  role,  employeePassword,employeeStatus,name,reportTo,payroll);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();
				}            
            conn.close();
            
         } catch (SQLException e) {
            e.printStackTrace();
        } 
				
 		return returJson;



	}
	@RequestMapping(value = "/forgot", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String postServiceforgotPassword(@RequestBody String email)  
			throws GeneralSecurityException, IOException, InterruptedException  {				
		System.out.println("email  .. "+email);
		Connection conn = null;
		PreparedStatement pstmt1=null; 
		PreparedStatement pstmt2=null; 
		PreparedStatement pstmt3=null; 
		ResultSet rs1 = null;
		ResultSet rs2 = null;	
		String returJson = "";
		String select="";
		Gson gs = new Gson(); 
		String internEmail = "";
		
		
		
		try {
			conn = DbServices.getConnection();
			select="SELECT EMAIL_PRIMARY FROM employee_details WHERE EMAIL_PRIMARY='"+email+"'";
		pstmt1 = conn.prepareStatement(select); 
	
		rs1 = pstmt1.executeQuery();
		while (rs1.next()) 
		{ 
 
			internEmail = rs1.getString(1); 
			if (email.equals(internEmail) ) 
				{ 
				char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
				StringBuilder sb = new StringBuilder();
				Random random = new Random();
				for (int i = 0; i < 12; i++) {
				    char c = chars[random.nextInt(chars.length)];
				    sb.append(c);
				}
				String output = sb.toString();
				
					String randompass =output;
					
					System.out.println("random = "+randompass);
					pstmt2 = conn.prepareStatement("UPDATE employee_details SET PASSWORD=?  WHERE  EMAIL_PRIMARY=?");
					pstmt2.setString(1, randompass);	
					pstmt2.setString(2, email);
					int v=0; 
					v = pstmt2.executeUpdate();
					if (v > 0)
					{ 
						pstmt3 = conn.prepareStatement("SELECT FIRST_NAME,LAST_NAME,PAYROLL  FROM employee_details WHERE EMAIL_PRIMARY=?"); 
					pstmt3.setString(1, email);	
					rs2 = pstmt3.executeQuery(); 
					while (rs2.next()) { 
						String firstName=""; 
						String lastName="";
						String text=""; 
						String payroll="";
						if (rs2.getString(1) != null) { 
							firstName = rs2.getString(1);
	} 
						else 
						{ 
							firstName = ""; 
						
						} 
						if (rs2.getString(2) != null) { 
							lastName = rs2.getString(2); 
							} 
						else { 
							lastName = ""; 
							} 
						if (rs2.getString(3) != null) { 
							payroll = rs2.getString(3); 
							} 
						else { 
							payroll = ""; 
							} 
						if(firstName!=null && lastName != null) 
							{ 
							if(payroll.equals("N2 Service")){
								text="Hello\t"+firstName+" "+lastName+"," +"\n\n\nYou are receiving this email because you requested to reset" +"\nyour www.n2sglobal.com  (HR-SYSTEM) password." +"\n\nYour temporary password is : "+randompass +"\n\nPlease visit www.n2sglobal.com and change your password in the" +"\nmember profile page." +"\n\nIf you have any questions, please contact us from our administrator." +"\n\nBest Regards" +"\n N2SGlobal Team" +"\n\n\nPlease visit www.n2sglobal.com for all your queries.";	
								
							}else{
								text="Hello\t"+firstName+" "+lastName+"," +"\n\n\nYou are receiving this email because you requested to reset" +"\nyour pattonlabs.com  (HR-SYSTEM) password." +"\n\nYour temporary password is : "+randompass +"\n\nPlease visit pattonlabs.com and change your password in the" +"\nmember profile page." +"\n\nIf you have any questions, please contact us from our administrator." +"\n\nBest Regards" +"\npatton labs Team" +"\n\n\nPlease visit pattonlabs.com for all your queries.";	
								
							}
						
							
								} 
						if(email!=null) 
								{ 
									String to=email;
									String subject="";
									if(payroll.equals("N2 Service")){
									 subject="Password Reset From N2SGlobal"; 
									}else{
										subject="Password Reset From Patton labs";
									}
									
									SimpleSendEmail ob = new SimpleSendEmail(); 
									ob.sendEmail(to, subject, text); 
									returJson = gs.toJson("Please check your email..").toString();
									} 
								} 
					} 
					} 
				} 
		if (internEmail == null ||internEmail == "" )
		{
			returJson = gs.toJson("Please enter registered email..").toString();
			} 
								} 
		catch (SQLException e) 
		{
				e.printStackTrace(); 
		
		}finally
		{ 
			try { 
				if (rs1 != null) {
				rs1.close(); 
				} if (conn != null) 
				{ 
					DbServices.closeDBConnection(conn); 
					}
				} catch (Exception e)
			{ 
					e.printStackTrace(); 
				
				} 
						} 
		return returJson;
			}
	@RequestMapping(value = "/changepwd", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String postServiceChangePassword(@RequestBody RequestChangePasswordVO change)
			throws GeneralSecurityException, IOException, InterruptedException {
		
	String email1=change.getEmailid();
	String pwd1=change.getCorrectpwd();
	String chg1=change.getChangepwd();
		
		Connection conn = null;
		PreparedStatement pstmt1=null; 
		PreparedStatement pstmt2=null; 
		PreparedStatement pstmt3=null; 
		ResultSet rs1 = null;
		ResultSet rs2 = null;	
		String returJson = "";
		String select="";
		Gson gs = new Gson(); 
		String internEmail = "";
		String internpwd = "";
		try {
			conn = DbServices.getConnection();
			select="SELECT EMAIL_PRIMARY,PASSWORD FROM employee_details WHERE EMAIL_PRIMARY='"+change.getEmailid()+"'";
		pstmt1 = conn.prepareStatement(select); 
	
		rs1 = pstmt1.executeQuery();
		while (rs1.next()) 
		{ 

			internEmail = rs1.getString(1);
			internpwd = rs1.getString(2); 
			
			if (email1.equals(internEmail) ){
				
				if(pwd1.equals(internpwd)){
					
					pstmt2 = conn.prepareStatement("UPDATE employee_details SET PASSWORD=?  WHERE  EMAIL_PRIMARY=?");
					pstmt2.setString(1, chg1);	
					pstmt2.setString(2, email1);
					int v=0; 
					v = pstmt2.executeUpdate();
					if(v>0){
						
						returJson = gs.toJson("your password is changed successfully").toString();	
						System.out.println("password changed successfully ");
					
					}		
					
				
				}
				else
				{
					returJson = gs.toJson("Please enter registered email and password ").toString();		
				}
						
			}
 	
				} 
		if (internEmail == null ||internEmail == "" )
		{
			returJson = gs.toJson("Please enter registered email..").toString();
			} 
								} 
		catch (SQLException e) 
		{
				e.printStackTrace(); 
		
		}finally
		{ 
			try { 
				if (rs1 != null) {
				rs1.close(); 
				} if (conn != null) 
				{ 
					DbServices.closeDBConnection(conn); 
					}
				} catch (Exception e)
			{ 
					e.printStackTrace(); 
				
				} 
			} 
		
		return returJson;
			}
}