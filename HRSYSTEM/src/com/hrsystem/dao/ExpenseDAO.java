package com.hrsystem.dao;

import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.google.gson.Gson;
import com.hrsystem.db.DbServices;
import com.hrsystem.email.SimpleSendEmail;
import com.hrsystem.vo.RequestExpenseCreateVO;
import com.hrsystem.vo.ResponseEmployeeExpenseVO;
import com.hrsystem.vo.ResponseEmployeeLoginVO;

public class ExpenseDAO {

	

	public String GetServiceForExpenseEmpList() {

List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
			    	String SELECT="SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL from employee_details WHERE EMPLOYEE_ROLE='USER' AND STATUS='Active'  ";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String employeeId;
						 String firstName;
						 String lastName;
						 String dateOfBirth;
						 String phoneNumberPrimary;
						 String phoneNumberSecondary;
						 String emailPrimary;
						 String emailSecondary;
						 String location;
						 String dateOfJoined;
						 String clientName;
						 String designation;
						 String role;
						 String employeePassword;
						 String employeeStatus;
						 String reportTo;
						 String payroll;
						if(rs.getString(1) != null){
							
							employeeId=rs.getString(1);
						}else {
							
							employeeId= null;
						}
						
						if(rs.getString(2) != null){
							
							firstName=rs.getString(2);
						}else {
							
							firstName= null;
						}
						if(rs.getString(3) != null){
							lastName= rs.getString(3);
	 
							
						}else {
							
							lastName= null;
						}
						if(rs.getString(4)!= null){
							
							dateOfBirth=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(dateOfBirth));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							dateOfBirth = sdf.format(cal.getTime());
						}else {
							
							dateOfBirth= null;
						}
						if(rs.getString(5)!= null){
							
							phoneNumberPrimary= rs.getString(5);
						}else {
							
							phoneNumberPrimary= null;
						}
						if(rs.getString(6)!= null){
							
							phoneNumberSecondary= rs.getString(6);
						}else {
							
							phoneNumberSecondary= null;
						}
						
						if(rs.getString(7)!= null){
							emailPrimary= rs.getString(7);
						}else {
							
							emailPrimary = null;
						}
						
						if(rs.getString(8)!= null){
							
							emailSecondary= rs.getString(8);
						}else {
							
							emailSecondary= null;
						}
						if(rs.getString(9)!= null){
							
							location= rs.getString(9);
						}else {
							
							location= null;
						}
						if(rs.getString(10)!= null){
							
							dateOfJoined= rs.getString(10);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(dateOfJoined));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							dateOfJoined = sdf.format(cal.getTime());
						}else {
							
							dateOfJoined= null;
						}
						if(rs.getString(11)!= null){
							
							clientName= rs.getString(11);
						}else {
							
							clientName= null;
						}
						if(rs.getString(12)!= null){
							
							designation= rs.getString(12);
						}else {
							
							designation= null;
						}

						if(rs.getString(13)!= null){
		
							role= rs.getString(13);
						}else {
		
							role= null;
						}
						if(rs.getString(14)!= null){
		
							employeePassword= null;
						}else {
		
							employeePassword= null;
						}
						if(rs.getString(15)!= null){
							
							employeeStatus= rs.getString(15);
						}else {
		
							employeeStatus= null;
						}
						if(rs.getString(16)!= null){
							
							reportTo= rs.getString(16);
						}else {
		
							reportTo= null;
						}
						if(rs.getString(17)!= null){
							
							payroll= rs.getString(17);
						}else {
		
							payroll= null;
						}
						String name=firstName+" "+lastName;
						ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
								employeeId,firstName,lastName,dateOfBirth,
								 phoneNumberPrimary,phoneNumberSecondary,emailPrimary,
								 emailSecondary,location,dateOfJoined,clientName,designation,
								 role,employeePassword,employeeStatus,name,reportTo,payroll);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
	}
	

	public String PostServiceForAddExpense(RequestExpenseCreateVO request) {
 List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	//	String useremail= LoginDAO.LoggedUser;
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		String classname=GetServiceForGetNameController1(request.getEmployeeId());
		System.out.println("amt1 : "+request.getExpenseAmt());
		//System.out.println("name1"+request.getSubmissionDate());
		try{
			
			if(request.getExpenseId() == null){
	 String INSERT="INSERT INTO expense_report (EMPLOYEE_ID, EXPENSE_ACCOUNT_CODE,EXPENSE_REPORTING_DATE, EXPENSE_SUBMISSION_DATE, AMOUNT_IN_DOLLAR, EXPENSE_MEMO,EXPENSE_CLASS,EXPENSE_RECEIPT,EXPENSE_TOTAL,EXPENSE_REPORT_STATUS,NOTE,RECEIPT_NAME,RECEIPT_TYPE,RECEIPT_SIZE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getEmployeeId());
            statement1.setString(2, request.getExpenseAccountCode());
            
            
            String reportingDate = request.getReportingDate();
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf1.parse(reportingDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlreportingDate = new java.sql.Date(date.getTime());
            
                     
            
            statement1.setDate(3, (java.sql.Date) sqlreportingDate);
            
            String submissionDate = request.getSubmissionDate();
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date1 = null;
			try {date1 = sdf2.parse(submissionDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlsubmissionDate = new java.sql.Date(date1.getTime());
            
            statement1.setDate(4,(java.sql.Date)sqlsubmissionDate );
            float ExpAmt = Float.parseFloat(request.getExpenseAmt());
            statement1.setFloat(5, ExpAmt);
            statement1.setString(6, request.getExpensememo());
            
            
            statement1.setString(7, classname);
            
            String receipt=request.getExpenseFile();
            byte[] b = receipt.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(8,blob);
            
            
            float TotalAmt = Float.parseFloat(request.getExpenseTotal());
            statement1.setFloat(9,TotalAmt);
            statement1.setString(10,request.getExpenseStatus());
            statement1.setString(11,request.getExpenseNote());
            statement1.setString(12,request.getFileName());
            statement1.setString(13,request.getFileType());
            statement1.setString(14,request.getFileSize());
            
            
            row = statement1.executeUpdate();
 		} 	else {
 			String UPDATE="UPDATE expense_report SET EMPLOYEE_ID=?, EXPENSE_ACCOUNT_CODE=?,EXPENSE_REPORTING_DATE=?, EXPENSE_SUBMISSION_DATE=?, AMOUNT_IN_DOLLAR=?, EXPENSE_MEMO=?,EXPENSE_CLASS=?,EXPENSE_RECEIPT=?,EXPENSE_TOTAL=?,EXPENSE_REPORT_STATUS=?,NOTE=?,RECEIPT_NAME=?,RECEIPT_TYPE=?,RECEIPT_SIZE=?  WHERE EXPENSE_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

            statement1.setString(1, request.getEmployeeId());
            statement1.setString(2, request.getExpenseAccountCode());
            
            
            String reportingDate = request.getReportingDate();
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf1.parse(reportingDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlreportingDate = new java.sql.Date(date.getTime());
            
                     
            
            statement1.setDate(3, (java.sql.Date) sqlreportingDate);
            
            String submissionDate = request.getReportingDate();
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date1 = null;
			try {date1 = sdf2.parse(submissionDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlsubmissionDate = new java.sql.Date(date1.getTime());
            
            statement1.setDate(4,(java.sql.Date)sqlsubmissionDate );
            statement1.setString(5, request.getExpenseAmt());
            statement1.setString(6, request.getExpensememo());
            statement1.setString(7, request.getExpenseEmpName());
            
            String receipt=request.getExpenseFile();
            byte[] b = receipt.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(8,blob);
            
            
            
            statement1.setString(9,request.getExpenseTotal());
            statement1.setString(10,request.getExpenseStatus());
            statement1.setString(11,request.getExpenseNote());
            statement1.setString(12,request.getFileName());
            statement1.setString(13,request.getFileType());
            statement1.setString(14,request.getFileSize());
            
            statement1.setString(15,request.getExpenseId());
            
            
            row = statement1.executeUpdate();

 		}
		if(row > 0){
			String name = getEmployeeName(request.getEmployeeId());
			SimpleSendEmail s1=new SimpleSendEmail();
			
			s1.HtmlEmail(request.getEmployeeId(),name,request);
		}
		
		if(row >0){
			

	    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EMPLOYEE_ID ='"+request.getEmployeeId()+"' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID ";
           	statement2 = conn.prepareStatement(SELECT);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String expenseId;
				 String employeeId;
				 String expenseAccountCode;
				 String reportingDate;
				 String submissionDate;
				 String expenseAmt;
				 String expensememo;
				 String expenseEmpName;
				 String expenseFile;
				 String expenseTotal;
				 String expenseStatus;
				 String expenseNote;
				 String fileType;
				 String fileName;
				 String fileSize;
				 String firstName;
				 String lastname;
				 
				if(rs.getString(1) != null){
					
					expenseId=rs.getString(1);
				}else {
					
					expenseId= null;
				}
				
				if(rs.getString(2) != null){
					
					employeeId=rs.getString(2);
				}else {
					
					employeeId= null;
				}
				if(rs.getString(3) != null){
					expenseAccountCode= rs.getString(3);

					
				}else {
					
					expenseAccountCode= null;
				}
				if(rs.getString(4)!= null){
					
					reportingDate=rs.getString(4);
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reportingDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reportingDate = sdf.format(cal.getTime());
					
				}else {
					
					reportingDate= null;
				}
				if(rs.getString(5)!= null){
					
					submissionDate= rs.getString(5);
									
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(submissionDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					submissionDate = sdf.format(cal.getTime());
					
					
				}else {
					
					submissionDate= null;
				}
				if(rs.getString(6)!= null){
					
					expenseAmt= Float.toString(rs.getFloat(6));
				}else {
					
					expenseAmt= null;
				}
				
				if(rs.getString(7)!= null){
					expensememo= rs.getString(7);
				}else {
					
					expensememo = null;
				}
				
				if(rs.getString(8)!= null){
					
					expenseEmpName= rs.getString(8);
				}else {
					
					expenseEmpName= null;
				}
				if(rs.getBlob(9)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(9);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					expenseFile=str;
				}else {
					
					expenseFile= null;
				}
				if(rs.getString(10)!= null){
					
					expenseTotal= Float.toString(rs.getFloat(10));
				}else {
					
					expenseTotal= null;
				}
				if(rs.getString(11)!= null){
					
					expenseStatus= rs.getString(11);
				}else {
					
					expenseStatus= null;
				}
				if(rs.getString(12)!= null){
					
					expenseNote= rs.getString(12);
				}else {
					
					expenseNote= null;
				}

				if(rs.getString(13)!= null){

					fileName= rs.getString(13);
				}else {

					fileName= null;
				}
				if(rs.getString(14)!= null){

					fileType= rs.getString(14);
				}else {

					fileType= null;
				}
				if(rs.getString(15)!= null){
					
					fileSize= rs.getString(15);
				}else {

					fileSize= null;
				}
				if(rs.getString(16)!= null){
					
					firstName= rs.getString(16);
				}else {

					firstName= null;
				}
				if(rs.getString(17)!= null){
	
					lastname= rs.getString(17);
				}else {

					lastname= null;
				}
				
				 String name=firstName +" "+lastname;
				 

				ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
						expenseId, employeeId,	 expenseAccountCode,			 reportingDate, submissionDate, expenseAmt, expensememo, name,
						expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
	    
			
		}
			//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			//statement2.close();
			//rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
	}
	
	
public String GetServiceForExpenseList(String request) {

List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
			System.out.println("as1"+request);
			conn = DbServices.getConnection();
	    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EMPLOYEE_ID ='"+request+"' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
			rs = statement2.executeQuery();
			System.out.println("as2"+request);
			while(rs.next()){
				
				 String expenseId;
				 String employeeId;
				 String expenseAccountCode;
				 String reportingDate;
				 String submissionDate;
				 String expenseAmt;
				 String expensememo;
				 String expenseEmpName;
				 String expenseFile;
				 String expenseTotal;
				 String expenseStatus;
				 String expenseNote;
				 String fileType;
				 String fileName;
				 String fileSize;
				 String firstName;
				 String lastname;
				 
				if(rs.getString(1) != null){
					
					expenseId=rs.getString(1);
				}else {
					
					expenseId= null;
				}
				
				if(rs.getString(2) != null){
					
					employeeId=rs.getString(2);
				}else {
					
					employeeId= null;
				}
				if(rs.getString(3) != null){
					expenseAccountCode= rs.getString(3);

					
				}else {
					
					expenseAccountCode= null;
				}
				if(rs.getString(4)!= null){
					
					reportingDate=rs.getString(4);
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reportingDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reportingDate = sdf.format(cal.getTime());
					
				}else {
					
					reportingDate= null;
				}
				if(rs.getString(5)!= null){
					
					submissionDate= rs.getString(5);
									
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(submissionDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					submissionDate = sdf.format(cal.getTime());
					
					
				}else {
					
					submissionDate= null;
				}
				if(rs.getString(6)!= null){
					
					expenseAmt= Float.toString(rs.getFloat(6));
				}else {
					
					expenseAmt= null;
				}
				
				if(rs.getString(7)!= null){
					expensememo= rs.getString(7);
				}else {
					
					expensememo = null;
				}
				
				if(rs.getString(8)!= null){
					
					expenseEmpName= rs.getString(8);
				}else {
					
					expenseEmpName= null;
				}
				if(rs.getBlob(9)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(9);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					expenseFile=str;
				}else {
					
					expenseFile= null;
				}
				if(rs.getString(10)!= null){
					
					expenseTotal= Float.toString(rs.getFloat(10));
				}else {
					
					expenseTotal= null;
				}
				if(rs.getString(11)!= null){
					
					expenseStatus= rs.getString(11);
				}else {
					
					expenseStatus= null;
				}
				if(rs.getString(12)!= null){
					
					expenseNote= rs.getString(12);
				}else {
					
					expenseNote= null;
				}

				if(rs.getString(13)!= null){

					fileName= rs.getString(13);
				}else {

					fileName= null;
				}
				if(rs.getString(14)!= null){

					fileType= rs.getString(14);
				}else {

					fileType= null;
				}
				if(rs.getString(15)!= null){
					
					fileSize= rs.getString(15);
				}else {

					fileSize= null;
				}
				if(rs.getString(16)!= null){
					
					firstName= rs.getString(16);
				}else {

					firstName= null;
				}
				if(rs.getString(17)!= null){
	
					lastname= rs.getString(17);
				}else {

					lastname= null;
				}
				
				 String name=firstName +" "+lastname;
				ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
						expenseId, employeeId,	 expenseAccountCode,reportingDate, submissionDate, expenseAmt, expensememo, name,
						expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
	    
			
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf123"+returJson);
			return returJson;
	}

public String GetServiceForExpenseListStatus(String[] request) {
	
	String empid="";
	String fromdate="";
	String todate="";
	String status="";
	
	empid=request[0];
	
	
	fromdate = request[1];
	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	java.util.Date date = null;
	try {date = sdf1.parse(fromdate);
	} catch (ParseException e) {
		e.printStackTrace();}
	java.sql.Date sqlfromdate = new java.sql.Date(date.getTime());
	
	
	
	
	todate=request[2];
	
	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
	java.util.Date date1 = null;
	try {date1 = sdf2.parse(todate);
	} catch (ParseException e) {
		e.printStackTrace();}
	java.sql.Date sqltodate = new java.sql.Date(date1.getTime());
	
	
	status=request[3];

List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
			System.out.println("as111"+sqlfromdate);
			System.out.println("as222"+sqltodate);
			System.out.println("as222"+status);
			conn = DbServices.getConnection();
	    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EXPENSE_SUBMISSION_DATE BETWEEN '"+sqlfromdate+"' AND '"+sqltodate+"' AND E.EMPLOYEE_ID ='"+empid+"' AND E.EXPENSE_REPORT_STATUS ='"+status+"' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID ";
           	statement2 = conn.prepareStatement(SELECT);
			rs = statement2.executeQuery();
			System.out.println("as2"+request);
			while(rs.next()){
				
				 String expenseId;
				 String employeeId;
				 String expenseAccountCode;
				 String reportingDate;
				 String submissionDate;
				 String expenseAmt;
				 String expensememo;
				 String expenseEmpName;
				 String expenseFile;
				 String expenseTotal;
				 String expenseStatus;
				 String expenseNote;
				 String fileType;
				 String fileName;
				 String fileSize;
				 String firstName;
				 String lastname;
				 
				if(rs.getString(1) != null){
					
					expenseId=rs.getString(1);
				}else {
					
					expenseId= null;
				}
				
				if(rs.getString(2) != null){
					
					employeeId=rs.getString(2);
				}else {
					
					employeeId= null;
				}
				if(rs.getString(3) != null){
					expenseAccountCode= rs.getString(3);

					
				}else {
					
					expenseAccountCode= null;
				}
				if(rs.getString(4)!= null){
					
					reportingDate=rs.getString(4);
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reportingDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reportingDate = sdf.format(cal.getTime());
					
				}else {
					
					reportingDate= null;
				}
				if(rs.getString(5)!= null){
					
					submissionDate= rs.getString(5);
									
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(submissionDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					submissionDate = sdf.format(cal.getTime());
					
					
				}else {
					
					submissionDate= null;
				}
				if(rs.getString(6)!= null){
					
					expenseAmt= Float.toString(rs.getFloat(6));
				}else {
					
					expenseAmt= null;
				}
				
				if(rs.getString(7)!= null){
					expensememo= rs.getString(7);
				}else {
					
					expensememo = null;
				}
				
				if(rs.getString(8)!= null){
					
					expenseEmpName= rs.getString(8);
				}else {
					
					expenseEmpName= null;
				}
				if(rs.getBlob(9)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(9);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					expenseFile=str;
				}else {
					
					expenseFile= null;
				}
				if(rs.getString(10)!= null){
					
					expenseTotal= Float.toString(rs.getFloat(10));
				}else {
					
					expenseTotal= null;
				}
				if(rs.getString(11)!= null){
					
					expenseStatus= rs.getString(11);
				}else {
					
					expenseStatus= null;
				}
				if(rs.getString(12)!= null){
					
					expenseNote= rs.getString(12);
				}else {
					
					expenseNote= null;
				}

				if(rs.getString(13)!= null){

					fileName= rs.getString(13);
				}else {

					fileName= null;
				}
				if(rs.getString(14)!= null){

					fileType= rs.getString(14);
				}else {

					fileType= null;
				}
				if(rs.getString(15)!= null){
					
					fileSize= rs.getString(15);
				}else {

					fileSize= null;
				}
				
if(rs.getString(16)!= null){
					
					firstName= rs.getString(16);
				}else {

					firstName= null;
				}
				if(rs.getString(17)!= null){
	
					lastname= rs.getString(17);
				}else {

					lastname= null;
				}
				
				 String name=firstName +" "+lastname;

				ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
						expenseId, employeeId,	 expenseAccountCode,reportingDate, submissionDate, expenseAmt, expensememo, name,
						expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
	    
			
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf123"+returJson);
			return returJson;
	
	
	

}

public String GetServiceForExpenseList1(String request) {

List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
			System.out.println("as1"+request);
			conn = DbServices.getConnection();
	    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EMPLOYEE_ID ='"+request+"' AND E.EXPENSE_REPORT_STATUS='Pending' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
			rs = statement2.executeQuery();
			System.out.println("as2"+request);
			while(rs.next()){
				
				 String expenseId;
				 String employeeId;
				 String expenseAccountCode;
				 String reportingDate;
				 String submissionDate;
				 String expenseAmt;
				 String expensememo;
				 String expenseEmpName;
				 String expenseFile;
				 String expenseTotal;
				 String expenseStatus;
				 String expenseNote;
				 String fileType;
				 String fileName;
				 String fileSize;

				 String firstName;
				 String lastname;
				 
				if(rs.getString(1) != null){
					
					expenseId=rs.getString(1);
				}else {
					
					expenseId= null;
				}
				
				if(rs.getString(2) != null){
					
					employeeId=rs.getString(2);
				}else {
					
					employeeId= null;
				}
				if(rs.getString(3) != null){
					expenseAccountCode= rs.getString(3);

					
				}else {
					
					expenseAccountCode= null;
				}
				if(rs.getString(4)!= null){
					
					reportingDate=rs.getString(4);
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reportingDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reportingDate = sdf.format(cal.getTime());
					
				}else {
					
					reportingDate= null;
				}
				if(rs.getString(5)!= null){
					
					submissionDate= rs.getString(5);
									
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(submissionDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					submissionDate = sdf.format(cal.getTime());
					
					
				}else {
					
					submissionDate= null;
				}
				if(rs.getString(6)!= null){
					
					expenseAmt= Float.toString(rs.getFloat(6));
				}else {
					
					expenseAmt= null;
				}
				
				if(rs.getString(7)!= null){
					expensememo= rs.getString(7);
				}else {
					
					expensememo = null;
				}
				
				if(rs.getString(8)!= null){
					
					expenseEmpName= rs.getString(8);
				}else {
					
					expenseEmpName= null;
				}
				if(rs.getBlob(9)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(9);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					expenseFile=str;
				}else {
					
					expenseFile= null;
				}
				if(rs.getString(10)!= null){
					
					expenseTotal= Float.toString(rs.getFloat(10));
				}else {
					
					expenseTotal= null;
				}
				if(rs.getString(11)!= null){
					
					expenseStatus= rs.getString(11);
				}else {
					
					expenseStatus= null;
				}
				if(rs.getString(12)!= null){
					
					expenseNote= rs.getString(12);
				}else {
					
					expenseNote= null;
				}

				if(rs.getString(13)!= null){

					fileName= rs.getString(13);
				}else {

					fileName= null;
				}
				if(rs.getString(14)!= null){

					fileType= rs.getString(14);
				}else {

					fileType= null;
				}
				if(rs.getString(15)!= null){
					
					fileSize= rs.getString(15);
				}else {

					fileSize= null;
				}
				
				if(rs.getString(16)!= null){
					
					firstName= rs.getString(16);
				}else {

					firstName= null;
				}
				if(rs.getString(17)!= null){
	
					lastname= rs.getString(17);
				}else {

					lastname= null;
				}
				
				 String name=firstName +" "+lastname;

				ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
						expenseId, employeeId,	 expenseAccountCode,reportingDate, submissionDate, expenseAmt, expensememo, name,
						expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
	    
			
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("emp admin exp"+returJson);
			return returJson;
	}

public String GetServiceForExpenseListView(String request) {
	List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson = "";
		
	PreparedStatement statement2 =null;
	
	
	try{
		System.out.println("as1"+request);
		conn = DbServices.getConnection();
    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EXPENSE_ID ='"+request+"' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		System.out.println("as2"+request);
		while(rs.next()){
			
			 String expenseId;
			 String employeeId;
			 String expenseAccountCode;
			 String reportingDate;
			 String submissionDate;
			 String expenseAmt;
			 String expensememo;
			 String expenseEmpName;
			 String expenseFile;
			 String expenseTotal;
			 String expenseStatus;
			 String expenseNote;
			 String fileType;
			 String fileName;
			 String fileSize;
			 String firstName;
			 String lastname;
			 
			if(rs.getString(1) != null){
				
				expenseId=rs.getString(1);
			}else {
				
				expenseId= null;
			}
			
			if(rs.getString(2) != null){
				
				employeeId=rs.getString(2);
			}else {
				
				employeeId= null;
			}
			if(rs.getString(3) != null){
				expenseAccountCode= rs.getString(3);

				
			}else {
				
				expenseAccountCode= null;
			}
			if(rs.getString(4)!= null){
				
				reportingDate=rs.getString(4);
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
						.parse(reportingDate));
				SimpleDateFormat sdf = new SimpleDateFormat(
						"MM/dd/yyyy");
				reportingDate = sdf.format(cal.getTime());
				
			}else {
				
				reportingDate= null;
			}
			if(rs.getString(5)!= null){
				
				submissionDate= rs.getString(5);
								
				Calendar cal = Calendar.getInstance();
				cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
						.parse(submissionDate));
				SimpleDateFormat sdf = new SimpleDateFormat(
						"MM/dd/yyyy");
				submissionDate = sdf.format(cal.getTime());
				
				
			}else {
				
				submissionDate= null;
			}
			if(rs.getString(6)!= null){
				
				expenseAmt= Float.toString(rs.getFloat(6));
			}else {
				
				expenseAmt= null;
			}
			
			if(rs.getString(7)!= null){
				expensememo= rs.getString(7);
			}else {
				
				expensememo = null;
			}
			
			if(rs.getString(8)!= null){
				
				expenseEmpName= rs.getString(8);
			}else {
				
				expenseEmpName= null;
			}
			if(rs.getBlob(9)!= null){
				
				
				java.sql.Blob ablob = rs.getBlob(9);
				String str = new String(ablob.getBytes(1l, (int) ablob.length()));
				expenseFile=str;
			}else {
				
				expenseFile= null;
			}
			if(rs.getString(10)!= null){
				
				expenseTotal= Float.toString(rs.getFloat(10));
			}else {
				
				expenseTotal= null;
			}
			if(rs.getString(11)!= null){
				
				expenseStatus= rs.getString(11);
			}else {
				
				expenseStatus= null;
			}
			if(rs.getString(12)!= null){
				
				expenseNote= rs.getString(12);
			}else {
				
				expenseNote= null;
			}
			
			if(rs.getString(13)!= null){

				fileName= rs.getString(13);
			}else {

				fileName= null;
			}
			if(rs.getString(14)!= null){

				fileType= rs.getString(14);
			}else {

				fileType= null;
			}
			
			if(rs.getString(15)!= null){
				
				fileSize= rs.getString(15);
			}else {

				fileSize= null;
			}

			if(rs.getString(16)!= null){
				
				firstName= rs.getString(16);
			}else {

				firstName= null;
			}
			if(rs.getString(17)!= null){

				lastname= rs.getString(17);
			}else {

				lastname= null;
			}
			
			 String name=firstName +" "+lastname;
			ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
					expenseId, employeeId,	 expenseAccountCode,reportingDate, submissionDate, expenseAmt, expensememo, name,
					expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}
    
		
	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("emp admin exp"+returJson);
		return returJson;
}






///////////////////








public String PostServiceForExpenseUpdate(String[] request) {

	 List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
			
			Connection conn = null;
	 		ResultSet rs = null;
	 		String returJson = "";
			int row = 0;
		//	String useremail= LoginDAO.LoggedUser;
			PreparedStatement statement1=null;
			PreparedStatement statement2 =null;
			
			
			//System.out.println("name1"+name);
			try{
				
				if(request[0] != null){
		 
	 			String UPDATE="UPDATE expense_report SET EXPENSE_REPORT_STATUS=?,NOTE=?  WHERE EXPENSE_ID=?";
	 			conn = DbServices.getConnection();
	            statement1 = conn.prepareStatement(UPDATE);

	            
	          
	            statement1.setString(1,request[2]);
	            statement1.setString(2,request[1]);
	          
	            
	            statement1.setString(3,request[0]);
	            
	            
	            row = statement1.executeUpdate();
	            
				}
	            if(row >0){
	    			

	    	    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EXPENSE_REPORT_STATUS='Pending' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID";
	               	statement2 = conn.prepareStatement(SELECT);
	    			rs = statement2.executeQuery();
	    			
	    			while(rs.next()){
	    				
	    				 String expenseId;
	    				 String employeeId;
	    				 String expenseAccountCode;
	    				 String reportingDate;
	    				 String submissionDate;
	    				 String expenseAmt;
	    				 String expensememo;
	    				 String expenseEmpName;
	    				 String expenseFile;
	    				 String expenseTotal;
	    				 String expenseStatus;
	    				 String expenseNote;
	    				 String fileType;
	    				 String fileName;
	    				 String fileSize;

	    				 String firstName;
	    				 String lastname;
	    				 
	    				if(rs.getString(1) != null){
	    					
	    					expenseId=rs.getString(1);
	    				}else {
	    					
	    					expenseId= null;
	    				}
	    				
	    				if(rs.getString(2) != null){
	    					
	    					employeeId=rs.getString(2);
	    				}else {
	    					
	    					employeeId= null;
	    				}
	    				if(rs.getString(3) != null){
	    					expenseAccountCode= rs.getString(3);

	    					
	    				}else {
	    					
	    					expenseAccountCode= null;
	    				}
	    				if(rs.getString(4)!= null){
	    					
	    					reportingDate=rs.getString(4);
	    					
	    					Calendar cal = Calendar.getInstance();
	    					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
	    							.parse(reportingDate));
	    					SimpleDateFormat sdf = new SimpleDateFormat(
	    							"MM/dd/yyyy");
	    					reportingDate = sdf.format(cal.getTime());
	    					
	    				}else {
	    					
	    					reportingDate= null;
	    				}
	    				if(rs.getString(5)!= null){
	    					
	    					submissionDate= rs.getString(5);
	    									
	    					Calendar cal = Calendar.getInstance();
	    					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
	    							.parse(submissionDate));
	    					SimpleDateFormat sdf = new SimpleDateFormat(
	    							"MM/dd/yyyy");
	    					submissionDate = sdf.format(cal.getTime());
	    					
	    					
	    				}else {
	    					
	    					submissionDate= null;
	    				}
	    				if(rs.getString(6)!= null){
	    					
	    					expenseAmt= Float.toString(rs.getFloat(6));
	    				}else {
	    					
	    					expenseAmt= null;
	    				}
	    				
	    				if(rs.getString(7)!= null){
	    					expensememo= rs.getString(7);
	    				}else {
	    					
	    					expensememo = null;
	    				}
	    				
	    				if(rs.getString(8)!= null){
	    					
	    					expenseEmpName= rs.getString(8);
	    				}else {
	    					
	    					expenseEmpName= null;
	    				}
	    				if(rs.getBlob(9)!= null){
	    					
	    					
	    					java.sql.Blob ablob = rs.getBlob(9);
	    					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
	    					expenseFile=str;
	    				}else {
	    					
	    					expenseFile= null;
	    				}
	    				if(rs.getString(10)!= null){
	    					
	    					expenseTotal= Float.toString(rs.getFloat(10));
	    				}else {
	    					
	    					expenseTotal= null;
	    				}
	    				if(rs.getString(11)!= null){
	    					
	    					expenseStatus= rs.getString(11);
	    				}else {
	    					
	    					expenseStatus= null;
	    				}
	    				if(rs.getString(12)!= null){
	    					
	    					expenseNote= rs.getString(12);
	    				}else {
	    					
	    					expenseNote= null;
	    				}

	    				if(rs.getString(13)!= null){

	    					fileName= rs.getString(13);
	    				}else {

	    					fileName= null;
	    				}
	    				if(rs.getString(14)!= null){

	    					fileType= rs.getString(14);
	    				}else {

	    					fileType= null;
	    				}
	    				if(rs.getString(15)!= null){
	    					
	    					fileSize= rs.getString(15);
	    				}else {

	    					fileSize= null;
	    				}

	    				if(rs.getString(16)!= null){
	    					
	    					firstName= rs.getString(16);
	    				}else {

	    					firstName= null;
	    				}
	    				if(rs.getString(17)!= null){
	    	
	    					lastname= rs.getString(17);
	    				}else {

	    					lastname= null;
	    				}
	    				
	    				 String name=firstName +" "+lastname;
	    				ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
	    						expenseId, employeeId,	 expenseAccountCode,			 reportingDate, submissionDate, expenseAmt, expensememo, name,
	    						expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
	    				finalList.add(mObj);
	    				Gson gs = new Gson();
	    				returJson = gs.toJson(finalList).toString();

	    			}
	    	    
	    			
	    		}
	    			

	    	}catch(Exception e){
	    		
	    		e.printStackTrace();
	    	}finally {
	    		try {
	    			statement1.close();
	    			
	    			conn.close();
	    		} catch (SQLException e) {
	    			e.printStackTrace();
	    		}
	    	}
	    		System.out.println("asfasdf"+returJson);
	    		return returJson;
	 		}

public String PostServiceForExpenseStatus(String[] request) {
List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson = "";
		
	PreparedStatement statement2 =null;
	
	
	try{
		System.out.println("as1"+request);
		conn = DbServices.getConnection();
    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EMPLOYEE_ID ='"+request[0]+"' AND E.EXPENSE_REPORT_STATUS ='"+request[1]+"' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		System.out.println("as2"+request);
		while(rs.next()){
			
			 String expenseId;
			 String employeeId;
			 String expenseAccountCode;
			 String reportingDate;
			 String submissionDate;
			 String expenseAmt;
			 String expensememo;
			 String expenseEmpName;
			 String expenseFile;
			 String expenseTotal;
			 String expenseStatus;
			 String expenseNote;
			 String fileType;
			 String fileName;
			 String fileSize;

			 	String firstName;
				 String lastname;
			 
			if(rs.getString(1) != null){
				
				expenseId=rs.getString(1);
			}else {
				
				expenseId= null;
			}
			
			if(rs.getString(2) != null){
				
				employeeId=rs.getString(2);
			}else {
				
				employeeId= null;
			}
			if(rs.getString(3) != null){
				expenseAccountCode= rs.getString(3);

				
			}else {
				
				expenseAccountCode= null;
			}
			if(rs.getString(4)!= null){
				
				reportingDate=rs.getString(4);
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
						.parse(reportingDate));
				SimpleDateFormat sdf = new SimpleDateFormat(
						"MM/dd/yyyy");
				reportingDate = sdf.format(cal.getTime());
				
			}else {
				
				reportingDate= null;
			}
			if(rs.getString(5)!= null){
				
				submissionDate= rs.getString(5);
								
				Calendar cal = Calendar.getInstance();
				cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
						.parse(submissionDate));
				SimpleDateFormat sdf = new SimpleDateFormat(
						"MM/dd/yyyy");
				submissionDate = sdf.format(cal.getTime());
				
				
			}else {
				
				submissionDate= null;
			}
			if(rs.getString(6)!= null){
				
				expenseAmt= Float.toString(rs.getFloat(6));
			}else {
				
				expenseAmt= null;
			}
			
			if(rs.getString(7)!= null){
				expensememo= rs.getString(7);
			}else {
				
				expensememo = null;
			}
			
			if(rs.getString(8)!= null){
				
				expenseEmpName= rs.getString(8);
			}else {
				
				expenseEmpName= null;
			}
			if(rs.getBlob(9)!= null){
				
				
				java.sql.Blob ablob = rs.getBlob(9);
				String str = new String(ablob.getBytes(1l, (int) ablob.length()));
				expenseFile=str;
			}else {
				
				expenseFile= null;
			}
			if(rs.getString(10)!= null){
				
				expenseTotal= Float.toString(rs.getFloat(10));
			}else {
				
				expenseTotal= null;
			}
			if(rs.getString(11)!= null){
				
				expenseStatus= rs.getString(11);
			}else {
				
				expenseStatus= null;
			}
			if(rs.getString(12)!= null){
				
				expenseNote= rs.getString(12);
			}else {
				
				expenseNote= null;
			}
			
			if(rs.getString(13)!= null){

				fileName= rs.getString(13);
			}else {

				fileName= null;
			}
			if(rs.getString(14)!= null){

				fileType= rs.getString(14);
			}else {

				fileType= null;
			}
			
			if(rs.getString(15)!= null){
				
				fileSize= rs.getString(15);
			}else {

				fileSize= null;
			}

			if(rs.getString(16)!= null){
				
				firstName= rs.getString(16);
			}else {

				firstName= null;
			}
			if(rs.getString(17)!= null){

				lastname= rs.getString(17);
			}else {

				lastname= null;
			}
			
			 String name=firstName +" "+lastname;
			ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
					expenseId, employeeId,	 expenseAccountCode,reportingDate, submissionDate, expenseAmt, expensememo, name,
					expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}
    
		
	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("emp admin exp"+returJson);
		return returJson;	
	
}

public String getEmployeeName(String id){
	String name="";
	 String SELECT_CANDIDATE="SELECT  FIRST_NAME from employee_details where EMPLOYEE_ID = ?";
	 Connection conn = null;
 		ResultSet rs = null;
 		PreparedStatement statement2=null;	
		
		 
		
		try {
			           
            conn = DbServices.getConnection();
        
        	 statement2 = conn.prepareStatement(SELECT_CANDIDATE);
         	statement2.setString(1, id);
 			rs = statement2.executeQuery();	
        	while (rs.next()) { 
        		 if (rs.getString(1) != null) {
 					name = rs.getString(1);
   
 				} else {
 					name = null;

 				}
        	} 

            
		}catch(Exception e){
			e.printStackTrace();
			
		}finally {
			try {
			
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		   
		System.out.println("name"+name);
        	
	return name;
}

public String GetServiceForGetNameController1(String request) {
	
	String FirstName="";
	String LastName="";
	String Name="";
	String returJson = "";
	 String SELECT_CANDIDATE="SELECT  FIRST_NAME,LAST_NAME from employee_details where EMPLOYEE_ID = ?";
	 Connection conn = null;
		ResultSet rs = null;
		PreparedStatement statement2=null;	
		
		 
		
		try {
			           
           conn = DbServices.getConnection();
       
       	 statement2 = conn.prepareStatement(SELECT_CANDIDATE);
        	statement2.setString(1, request);
			rs = statement2.executeQuery();	
       	while (rs.next()) { 
       		 if (rs.getString(1) != null) {
       			FirstName = rs.getString(1);
  
				} else {
					FirstName = null;

				}
       		if (rs.getString(2) != null) {
       			LastName = rs.getString(2);
  
				} else {
					LastName = null;

				}
       		Name=FirstName+" "+LastName;
     
    		returJson = Name;
       	} 
       	
		
           
		}catch(Exception e){
			e.printStackTrace();
			
		}finally {
			try {
			
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		   
		System.out.println("name"+FirstName+ LastName);
       	
	return returJson;
}
public String GetServiceForExpenseListController() {
	

List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson = "";
		
	PreparedStatement statement2 =null;
	
	
	try{
		
		conn = DbServices.getConnection();
    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T  WHERE E.EXPENSE_REPORT_STATUS='Pending' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			 String expenseId;
			 String employeeId;
			 String expenseAccountCode;
			 String reportingDate;
			 String submissionDate;
			 String expenseAmt;
			 String expensememo;
			 String expenseEmpName;
			 String expenseFile;
			 String expenseTotal;
			 String expenseStatus;
			 String expenseNote;
			 String fileType;
			 String fileName;
			 String fileSize;

			 		String firstName;
				 String lastname;
			 
			if(rs.getString(1) != null){
				
				expenseId=rs.getString(1);
			}else {
				
				expenseId= null;
			}
			
			if(rs.getString(2) != null){
				
				employeeId=rs.getString(2);
			}else {
				
				employeeId= null;
			}
			if(rs.getString(3) != null){
				expenseAccountCode= rs.getString(3);

				
			}else {
				
				expenseAccountCode= null;
			}
			if(rs.getString(4)!= null){
				
				reportingDate=rs.getString(4);
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
						.parse(reportingDate));
				SimpleDateFormat sdf = new SimpleDateFormat(
						"MM/dd/yyyy");
				reportingDate = sdf.format(cal.getTime());
				
			}else {
				
				reportingDate= null;
			}
			if(rs.getString(5)!= null){
				
				submissionDate= rs.getString(5);
								
				Calendar cal = Calendar.getInstance();
				cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
						.parse(submissionDate));
				SimpleDateFormat sdf = new SimpleDateFormat(
						"MM/dd/yyyy");
				submissionDate = sdf.format(cal.getTime());
				
				
			}else {
				
				submissionDate= null;
			}
			if(rs.getString(6)!= null){
				
				expenseAmt= Float.toString(rs.getFloat(6));
			}else {
				
				expenseAmt= null;
			}
			
			if(rs.getString(7)!= null){
				expensememo= rs.getString(7);
			}else {
				
				expensememo = null;
			}
			
			if(rs.getString(8)!= null){
				
				expenseEmpName= rs.getString(8);
			}else {
				
				expenseEmpName= null;
			}
			if(rs.getBlob(9)!= null){
				
				
				java.sql.Blob ablob = rs.getBlob(9);
				String str = new String(ablob.getBytes(1l, (int) ablob.length()));
				expenseFile=str;
			}else {
				
				expenseFile= null;
			}
			if(rs.getString(10)!= null){
				
				expenseTotal= Float.toString(rs.getFloat(10));
			}else {
				
				expenseTotal= null;
			}
			if(rs.getString(11)!= null){
				
				expenseStatus= rs.getString(11);
			}else {
				
				expenseStatus= null;
			}
			if(rs.getString(12)!= null){
				
				expenseNote= rs.getString(12);
			}else {
				
				expenseNote= null;
			}
			
			if(rs.getString(13)!= null){

				fileName= rs.getString(13);
			}else {

				fileName= null;
			}
			if(rs.getString(14)!= null){

				fileType= rs.getString(14);
			}else {

				fileType= null;
			}
			
			if(rs.getString(15)!= null){
				
				fileSize= rs.getString(15);
			}else {

				fileSize= null;
			}

			if(rs.getString(16)!= null){
				
				firstName= rs.getString(16);
			}else {

				firstName= null;
			}
			if(rs.getString(17)!= null){

				lastname= rs.getString(17);
			}else {

				lastname= null;
			}
			
			 String name=firstName +" "+lastname;
			
			ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
					expenseId, employeeId,	 expenseAccountCode,reportingDate, submissionDate, expenseAmt, expensememo, name,
					expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}
    
		
	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("emp admin exp"+returJson);
		return returJson;	
	

}

public String GetServiceForGetNameController(String request) {
	
	String FirstName="";
	String LastName="";
	String Name="";
	String returJson = "";
	 String SELECT_CANDIDATE="SELECT  FIRST_NAME,LAST_NAME from employee_details where EMPLOYEE_ID = ?";
	 Connection conn = null;
		ResultSet rs = null;
		PreparedStatement statement2=null;	
		
		 
		
		try {
			           
           conn = DbServices.getConnection();
       
       	 statement2 = conn.prepareStatement(SELECT_CANDIDATE);
        	statement2.setString(1, request);
			rs = statement2.executeQuery();	
       	while (rs.next()) { 
       		 if (rs.getString(1) != null) {
       			FirstName = rs.getString(1);
  
				} else {
					FirstName = null;

				}
       		if (rs.getString(2) != null) {
       			LastName = rs.getString(2);
  
				} else {
					LastName = null;

				}
       		Name=FirstName+" "+LastName;
           	Gson gs = new Gson();
    		returJson = gs.toJson(Name).toString();
       	} 
       	
		
           
		}catch(Exception e){
			e.printStackTrace();
			
		}finally {
			try {
			
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		   
		System.out.println("name"+FirstName+ LastName);
       	
	return returJson;
}

public String GetServiceFoTotalExpense(String[] request) {
	
	
	String fromdate="";
	String todate="";
	String status="";
	
	
	
	
	fromdate = request[0];
	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
	java.util.Date date = null;
	try {date = sdf1.parse(fromdate);
	} catch (ParseException e) {
		e.printStackTrace();}
	java.sql.Date sqlfromdate = new java.sql.Date(date.getTime());
	
	
	
	
	todate=request[1];
	
	SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
	java.util.Date date1 = null;
	try {date1 = sdf2.parse(todate);
	} catch (ParseException e) {
		e.printStackTrace();}
	java.sql.Date sqltodate = new java.sql.Date(date1.getTime());
	
	
	status=request[2];

List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
			System.out.println("as111"+sqlfromdate);
			System.out.println("as222"+sqltodate);
			System.out.println("as222"+status);
			conn = DbServices.getConnection();
	    	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EXPENSE_SUBMISSION_DATE BETWEEN '"+sqlfromdate+"' AND '"+sqltodate+"' AND E.EXPENSE_REPORT_STATUS ='"+status+"'AND E.EMPLOYEE_ID =T.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
			rs = statement2.executeQuery();
			System.out.println("as2"+request);
			while(rs.next()){
				
				 String expenseId;
				 String employeeId;
				 String expenseAccountCode;
				 String reportingDate;
				 String submissionDate;
				 String expenseAmt;
				 String expensememo;
				 String expenseEmpName;
				 String expenseFile;
				 String expenseTotal;
				 String expenseStatus;
				 String expenseNote;
				 String fileType;
				 String fileName;
				 String fileSize;


				 String firstName;
				 String lastname;
				 
				if(rs.getString(1) != null){
					
					expenseId=rs.getString(1);
				}else {
					
					expenseId= null;
				}
				
				if(rs.getString(2) != null){
					
					employeeId=rs.getString(2);
				}else {
					
					employeeId= null;
				}
				if(rs.getString(3) != null){
					expenseAccountCode= rs.getString(3);

					
				}else {
					
					expenseAccountCode= null;
				}
				if(rs.getString(4)!= null){
					
					reportingDate=rs.getString(4);
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reportingDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reportingDate = sdf.format(cal.getTime());
					
				}else {
					
					reportingDate= null;
				}
				if(rs.getString(5)!= null){
					
					submissionDate= rs.getString(5);
									
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(submissionDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					submissionDate = sdf.format(cal.getTime());
					
					
				}else {
					
					submissionDate= null;
				}
				if(rs.getString(6)!= null){
					
					expenseAmt= Float.toString(rs.getFloat(6));
				}else {
					
					expenseAmt= null;
				}
				
				if(rs.getString(7)!= null){
					expensememo= rs.getString(7);
				}else {
					
					expensememo = null;
				}
				
				if(rs.getString(8)!= null){
					
					expenseEmpName= rs.getString(8);
				}else {
					
					expenseEmpName= null;
				}
				if(rs.getBlob(9)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(9);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					expenseFile=str;
				}else {
					
					expenseFile= null;
				}
				if(rs.getString(10)!= null){
					
					expenseTotal= Float.toString(rs.getFloat(10));
				}else {
					
					expenseTotal= null;
				}
				if(rs.getString(11)!= null){
					
					expenseStatus= rs.getString(11);
				}else {
					
					expenseStatus= null;
				}
				if(rs.getString(12)!= null){
					
					expenseNote= rs.getString(12);
				}else {
					
					expenseNote= null;
				}

				if(rs.getString(13)!= null){

					fileName= rs.getString(13);
				}else {

					fileName= null;
				}
				if(rs.getString(14)!= null){

					fileType= rs.getString(14);
				}else {

					fileType= null;
				}
				if(rs.getString(15)!= null){
					
					fileSize= rs.getString(15);
				}else {

					fileSize= null;
				}



if(rs.getString(16)!= null){
					
					firstName= rs.getString(16);
				}else {

					firstName= null;
				}
				if(rs.getString(17)!= null){
	
					lastname= rs.getString(17);
				}else {

					lastname= null;
				}
				
				 String name=firstName +" "+lastname;
				ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
						expenseId, employeeId,	 expenseAccountCode,reportingDate, submissionDate, expenseAmt, expensememo, name,
						expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
	    
			
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf123"+returJson);
			return returJson;
}


public String PostServiceForDeleteExpense(String[] request) {

	List<ResponseEmployeeExpenseVO> finalList = new ArrayList<ResponseEmployeeExpenseVO>();
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson = "";
	int row = 0;
//	String useremail= LoginDAO.LoggedUser;
	PreparedStatement statement1=null;
	PreparedStatement statement2 =null;
	
	
	//System.out.println("name1"+name);
	try{
		
		if(request[1] != null){
 
			String DELETE_EXPENSE="delete from expense_report where EXPENSE_ID= ?";
            conn = DbServices.getConnection();
         statement1 = conn.prepareStatement(DELETE_EXPENSE);
        statement1.setString(1, request[1]);
          
         row = statement1.executeUpdate();
        
		}
        if(row >0){

	String SELECT="SELECT E.EXPENSE_ID, E.EMPLOYEE_ID, E.EXPENSE_ACCOUNT_CODE,E.EXPENSE_REPORTING_DATE, E.EXPENSE_SUBMISSION_DATE, E.AMOUNT_IN_DOLLAR, E.EXPENSE_MEMO,E.EXPENSE_CLASS,E.EXPENSE_RECEIPT,E.EXPENSE_TOTAL,E.EXPENSE_REPORT_STATUS,E.NOTE,E.RECEIPT_NAME,E.RECEIPT_TYPE,E.RECEIPT_SIZE,T.FIRST_NAME,T.LAST_NAME from expense_report E,employee_details T WHERE E.EMPLOYEE_ID ='"+request[0]+"' AND E.EMPLOYEE_ID =T.EMPLOYEE_ID ";
   	statement2 = conn.prepareStatement(SELECT);
	rs = statement2.executeQuery();
	
	while(rs.next()){
		
		 String expenseId;
		 String employeeId;
		 String expenseAccountCode;
		 String reportingDate;
		 String submissionDate;
		 String expenseAmt;
		 String expensememo;
		 String expenseEmpName;
		 String expenseFile;
		 String expenseTotal;
		 String expenseStatus;
		 String expenseNote;
		 String fileType;
		 String fileName;
		 String fileSize;
		 String firstName;
		 String lastname;
		 
		if(rs.getString(1) != null){
			
			expenseId=rs.getString(1);
		}else {
			
			expenseId= null;
		}
		
		if(rs.getString(2) != null){
			
			employeeId=rs.getString(2);
		}else {
			
			employeeId= null;
		}
		if(rs.getString(3) != null){
			expenseAccountCode= rs.getString(3);

			
		}else {
			
			expenseAccountCode= null;
		}
		if(rs.getString(4)!= null){
			
			reportingDate=rs.getString(4);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
					.parse(reportingDate));
			SimpleDateFormat sdf = new SimpleDateFormat(
					"MM/dd/yyyy");
			reportingDate = sdf.format(cal.getTime());
			
		}else {
			
			reportingDate= null;
		}
		if(rs.getString(5)!= null){
			
			submissionDate= rs.getString(5);
							
			Calendar cal = Calendar.getInstance();
			cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
					.parse(submissionDate));
			SimpleDateFormat sdf = new SimpleDateFormat(
					"MM/dd/yyyy");
			submissionDate = sdf.format(cal.getTime());
			
			
		}else {
			
			submissionDate= null;
		}
		if(rs.getString(6)!= null){
			
			expenseAmt= Float.toString(rs.getFloat(6));
		}else {
			
			expenseAmt= null;
		}
		
		if(rs.getString(7)!= null){
			expensememo= rs.getString(7);
		}else {
			
			expensememo = null;
		}
		
		if(rs.getString(8)!= null){
			
			expenseEmpName= rs.getString(8);
		}else {
			
			expenseEmpName= null;
		}
		if(rs.getBlob(9)!= null){
			
			
			java.sql.Blob ablob = rs.getBlob(9);
			String str = new String(ablob.getBytes(1l, (int) ablob.length()));
			expenseFile=str;
		}else {
			
			expenseFile= null;
		}
		if(rs.getString(10)!= null){
			
			expenseTotal= Float.toString(rs.getFloat(10));
		}else {
			
			expenseTotal= null;
		}
		if(rs.getString(11)!= null){
			
			expenseStatus= rs.getString(11);
		}else {
			
			expenseStatus= null;
		}
		if(rs.getString(12)!= null){
			
			expenseNote= rs.getString(12);
		}else {
			
			expenseNote= null;
		}

		if(rs.getString(13)!= null){

			fileName= rs.getString(13);
		}else {

			fileName= null;
		}
		if(rs.getString(14)!= null){

			fileType= rs.getString(14);
		}else {

			fileType= null;
		}
		if(rs.getString(15)!= null){
			
			fileSize= rs.getString(15);
		}else {

			fileSize= null;
		}
		if(rs.getString(16)!= null){
			
			firstName= rs.getString(16);
		}else {

			firstName= null;
		}
		if(rs.getString(17)!= null){

			lastname= rs.getString(17);
		}else {

			lastname= null;
		}
		
		 String name=firstName +" "+lastname;
		 

		ResponseEmployeeExpenseVO mObj = new ResponseEmployeeExpenseVO(
				expenseId, employeeId,	 expenseAccountCode,			 reportingDate, submissionDate, expenseAmt, expensememo, name,
				expenseFile, expenseTotal, expenseStatus, expenseNote, fileType,fileName, fileSize);
		finalList.add(mObj);
		Gson gs = new Gson();
		returJson = gs.toJson(finalList).toString();

	}
        }
	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
}


	

}
