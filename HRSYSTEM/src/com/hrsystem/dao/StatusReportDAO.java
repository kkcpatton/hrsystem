package com.hrsystem.dao;

import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.google.gson.Gson;
import com.hrsystem.db.DbServices;
import com.hrsystem.email.SimpleSendEmail;
import com.hrsystem.vo.RequestStatusReportAcceptVO;
import com.hrsystem.vo.RequestStatusReportVO;
import com.hrsystem.vo.ResponseEmployeeLoginVO;
import com.hrsystem.vo.ResponsePerformanceVO;
import com.hrsystem.vo.ResponseStatusReportVo;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class StatusReportDAO {

	public String PostServiceForStatusReport(RequestStatusReportVO request) {

		
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		


		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		
		
		try{
			
			
			if(request.getStatusId() == null){
				System.out.println("StartActualDate "+request.getStartPlannedDate());
				
			String INSERT="INSERT INTO status_report (ACTIVITY, START_PLANNED_DATE,START_ACTUAL_DATE,FINISH_PLANNED_DATE, FINISH_ESTIMATED_DATE, STATUS, CLIENT_DETAILS,CURRENT_PROJECT,MANAGER_NAME,CURRENT_MODULE,EMPLOYEE_ID,MANAGER_EMAIL,MANAGER_PHONE_NO,ACTION_STATUS,STATUS_MONTH,EMPLOYEE_SIGNATURE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getActivity());
            
            
            if(request.getStartPlannedDate()==null){
            	statement1.setDate(2, null);
            	
            }
            else{
            
            String plannedDate=request.getStartPlannedDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf.parse(plannedDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlplannedDate = new java.sql.Date(date.getTime());

			
			
			statement1.setDate(2, (java.sql.Date) sqlplannedDate);
            }
            
            if(request.getStartActualDate()==null){
            	statement1.setDate(3, null);
            }else{
            String actualdate= request.getStartActualDate();

            
            
        	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date1 = null;
			try {date1 = sdf1.parse(actualdate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlactualdate = new java.sql.Date(date1.getTime());

			statement1.setDate(3, (java.sql.Date) sqlactualdate);
			
            }
			
			
			if(request.getFinishPlannedDate()==null){
				
				statement1.setDate(4, null);
			}
			else{
			String finishplanneddate =request.getFinishPlannedDate();
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date2 = null;
			try {date2 = sdf2.parse(finishplanneddate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlfinishplanneddate = new java.sql.Date(date2.getTime());
          
			
			statement1.setDate(4, (java.sql.Date) sqlfinishplanneddate);
			}
			
			
			if(request.getFinishEstimatedDate()==null){
				
				statement1.setDate(5, null);
			}else{
			String finishestimateddate =request.getFinishEstimatedDate();
			SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date3 = null;
			try {date3 = sdf3.parse(finishestimateddate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlfinishestimateddate = new java.sql.Date(date3.getTime());
          
			
			statement1.setDate(5, (java.sql.Date) sqlfinishestimateddate);
			}
			
            statement1.setString(6, request.getStatus());
            statement1.setString(7, request.getClientDetails());
            statement1.setString(8,request.getCurrentProject() );
            
 
            statement1.setString(9,request.getManagerName());
            statement1.setString(10,request.getCurrentModule());
            statement1.setString(11,request.getEmployeeId());
            statement1.setString(12,request.getManagerEmail());
            statement1.setString(13,request.getManagerPhonenumber());
            statement1.setString(14,request.getAction_status());
            if(request.getStatusMonth() ==null){
				
				statement1.setDate(15, null);
			}else{
			String statusMonth =request.getStatusMonth();
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date4 = null;
			try {date4 = sdf4.parse(statusMonth);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlstatusMonth = new java.sql.Date(date4.getTime());
          
			
			statement1.setDate(15, (java.sql.Date) sqlstatusMonth);
			}
            
            String signature=request.getEmployeeSignature();
            byte[] b = signature.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(16,blob);
            
           
            
            
            
            row = statement1.executeUpdate();
 		} 	else {
 			   
 			String UPDATE="UPDATE status_report SET ACTIVITY=?, START_PLANNED_DATE=?,START_ACTUAL_DATE=?,FINISH_PLANNED_DATE=?, FINISH_ESTIMATED_DATE=?, STATUS=?, CLIENT_DETAILS=?,CURRENT_PROJECT=?,MANAGER_NAME=?,CURRENT_MODULE=?,EMPLOYEE_ID=?,MANAGER_EMAIL=?,MANAGER_PHONE_NO=?,ACTION_STATUS=?,STATUS_MONTH=?,EMPLOYEE_SIGNATURE=? WHERE STATUS_ID=?";
 	          conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

        statement1.setString(1, request.getActivity());
            
        if(request.getStartPlannedDate()==null){
        	statement1.setDate(2, null);
        	
        }
        else{
        
        String plannedDate=request.getStartPlannedDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = null;
		try {date = sdf.parse(plannedDate);
		} catch (ParseException e) {
			e.printStackTrace();}
		java.sql.Date sqlplannedDate = new java.sql.Date(date.getTime());

		
		
		statement1.setDate(2, (java.sql.Date) sqlplannedDate);
        }
        
        if(request.getStartActualDate()==null){
        	statement1.setDate(3, null);
        }else{
        String actualdate= request.getStartActualDate();

        
        
    	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date1 = null;
		try {date1 = sdf1.parse(actualdate);
		} catch (ParseException e) {
			e.printStackTrace();}
		java.sql.Date sqlactualdate = new java.sql.Date(date1.getTime());

		statement1.setDate(3, (java.sql.Date) sqlactualdate);
		
        }
		
		
		if(request.getFinishPlannedDate()==null){
			
			statement1.setDate(4, null);
		}
		else{
		String finishplanneddate =request.getFinishPlannedDate();
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date2 = null;
		try {date2 = sdf2.parse(finishplanneddate);
		} catch (ParseException e) {
			e.printStackTrace();}
		java.sql.Date sqlfinishplanneddate = new java.sql.Date(date2.getTime());
      
		
		statement1.setDate(4, (java.sql.Date) sqlfinishplanneddate);
		}
		
			
			if(request.getFinishEstimatedDate()==null){
				
				statement1.setDate(5, null);
			}	else{
			String finishestimateddate =request.getFinishEstimatedDate();
			
			
			SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date3 = null;
			try {date3 = sdf3.parse(finishestimateddate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlfinishestimateddate = new java.sql.Date(date3.getTime());
          
			
			statement1.setDate(5, (java.sql.Date) sqlfinishestimateddate);
			
			}
            statement1.setString(6, request.getStatus());
            statement1.setString(7, request.getClientDetails());
            statement1.setString(8,request.getCurrentProject() );
            
 
            statement1.setString(9,request.getManagerName());
            statement1.setString(10,request.getCurrentModule());
            statement1.setString(11,request.getEmployeeId());
            statement1.setString(12,request.getManagerEmail());
            statement1.setString(13,request.getManagerPhonenumber());
            statement1.setString(14,request.getAction_status());
            if(request.getStatusMonth() ==null){
				
				statement1.setDate(15, null);
			}else{
			String statusMonth =request.getStatusMonth();
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date4 = null;
			try {date4 = sdf4.parse(statusMonth);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlstatusMonth = new java.sql.Date(date4.getTime());
          
			
			statement1.setDate(15, (java.sql.Date) sqlstatusMonth);
			}
            
            String signature=request.getEmployeeSignature();
            byte[] b = signature.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(16,blob);
            
            statement1.setString(17,request.getStatusId());
           
            
            row = statement1.executeUpdate();

 		}
		
		    if (row > 0) {
		    	String statusMonth1 =request.getStatusMonth();
				SimpleDateFormat sd4 = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date dat4 = null;
				try {dat4 = sd4.parse(statusMonth1);
				} catch (ParseException e) {
					e.printStackTrace();}
				java.sql.Date sqlMonth = new java.sql.Date(dat4.getTime());
	          
		    	String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE S.EMPLOYEE_ID='"+ request.getEmployeeId()+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID ";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String statusId;
					 String employeeId;
					 String activity;
					 String startPlannedDate;
					 String startActualDate;
					 String finishPlannedDate;
					 String finishEstimatedDate;
					 String status;
					 String clientDetails;
					 String currentProject;
					 String managerName;
					 String currentModule;
					 String firstname;
					 String lastname;
					 String name;
					 String managerEmail;
					 String managerPhonenumber;
					 String action_status;
					 String statusMonth;
					 String employeeSignature;
					
					 String adminSignature;
					 String adminId;
					 String adminName;
					if(rs.getString(1) != null){
						
						statusId=rs.getString(1);
					}else {
						
						statusId= null;
					}
					
					if(rs.getString(2) != null){
						
						activity=rs.getString(2);
					}else {
						
						activity= null;
					}
					if(rs.getString(3) != null){
						startPlannedDate= rs.getString(3);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(startPlannedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						startPlannedDate = sdf.format(cal.getTime());
						
					}else {
						
						startPlannedDate= null;
					}
					if(rs.getString(4)!= null){
						
						startActualDate=rs.getString(4);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(startActualDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						startActualDate = sdf.format(cal.getTime());
					}else {
						
						startActualDate= null;
					}
					if(rs.getString(5)!= null){
						
						finishPlannedDate= rs.getString(5);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(finishPlannedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						finishPlannedDate = sdf.format(cal.getTime());
						
					}else {
						
						finishPlannedDate= null;
					}
					if(rs.getString(6)!= null){
						
						finishEstimatedDate= rs.getString(6);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(finishEstimatedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						finishEstimatedDate = sdf.format(cal.getTime());
						
					}else {
						
						finishEstimatedDate= null;
					}
					
					if(rs.getString(7)!= null){
						status= rs.getString(7);
					}else {
						
						status = null;
					}
					
					if(rs.getString(8)!= null){
						
						clientDetails= rs.getString(8);
					}else {
						
						clientDetails= null;
					}
					if(rs.getString(9)!= null){
						
						currentProject= rs.getString(9);
					}else {
						
						currentProject= null;
					}
					if(rs.getString(10)!= null){
						
						managerName= rs.getString(10);
						
					}else {
						
						managerName= null;
					}
					if(rs.getString(11)!= null){
						
						currentModule= rs.getString(11);
					}else {
						
						currentModule= null;
					}
					if(rs.getString(12)!= null){
						
						employeeId= rs.getString(12);
					}else {
						
						employeeId= null;
					}
					if(rs.getString(13)!= null){
						
						firstname= rs.getString(13);
					}else {
						
						firstname= null;
					}
					if(rs.getString(14)!= null){
	
						lastname= rs.getString(14);
					}else {
	
						lastname= null;
					}
					name=firstname+" "+lastname;
					
					if(rs.getString(15)!= null){
						
						managerEmail= rs.getString(15);
					}else {
	
						managerEmail= null;
					}
					if(rs.getString(16)!= null){
						
						managerPhonenumber= rs.getString(16);
					}else {
	
						managerPhonenumber= null;
					}
					
					if(rs.getString(17)!= null){
						
						action_status= rs.getString(17);
					}else {
	
						action_status= null;
					} 
					if(rs.getString(18) != null){
						statusMonth= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(statusMonth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						statusMonth = sdf.format(cal.getTime());
						
					}else {
						
						statusMonth= null;
					}
					 
					if(rs.getBlob(19)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(19);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						employeeSignature=str;
					}else {
						
						employeeSignature= null;
					}
					if(rs.getBlob(20)!= null){
						
						adminSignature="";
					}else {
						
						adminSignature= "";
					}
					if(rs.getString(21) != null){
						
						adminId=rs.getString(21);
						adminName="";
					}else {
						
						adminId= null;
						adminName="";
					}
					ResponseStatusReportVo mObj = new ResponseStatusReportVo(
							 statusId,employeeId, activity,startPlannedDate,  startActualDate,
							  finishPlannedDate,finishEstimatedDate, status,
							  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,
							  action_status,statusMonth,employeeSignature,adminSignature,adminName);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    }
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;

}

	
	public String GetServiceForName(String Id) {

	
				
				Connection conn = null;
		 		ResultSet rs = null;
		 		String returJson = "";
		 		String name="";
				PreparedStatement statement2 =null;
				
				
				try{
							conn = DbServices.getConnection();
							String SELECT="SELECT E.FIRST_NAME,E.LAST_NAME from employee_details E Where E.EMPLOYEE_ID='"+Id+"' ";
			               	statement2 = conn.prepareStatement(SELECT);
							rs = statement2.executeQuery();
							
							while(rs.next()){
								
								 String firstname;
								 String lastname;
								 
								
								 
								if(rs.getString(1) != null){
									
									firstname=rs.getString(1);
								}else {
									
									firstname= "";
								}
								
								if(rs.getString(2) != null){
									
									lastname=rs.getString(2);
								}else {
									
									lastname= "";
								}
								name=firstname+" "+lastname;
								
								

							}
					    
			

				}catch(Exception e){
					
					e.printStackTrace();
				}finally {
					try {
						
						statement2.close();
						rs.close();
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
					System.out.println("asfasdf"+name);
					return returJson;
			
			}
	
	
	public String GetServiceForStatusList() {
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E Where E.EMPLOYEE_ID=S.EMPLOYEE_ID AND S.ACTION_STATUS='Pending'";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;
						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;
						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 
						

						if(rs.getString(18) != null){
						statusMonth= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(statusMonth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						statusMonth = sdf.format(cal.getTime());
						
					}else {
						
						statusMonth= null;
					}
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						}
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
	}

	public String GetServiceForStatusListEmp(String request) {
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID  from status_report S,employee_details E WHERE S.EMPLOYEE_ID='"+ request+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;
						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;
						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 
						if(rs.getString(18) != null){
							statusMonth= rs.getString(18);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(statusMonth));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							statusMonth = sdf.format(cal.getTime());
							
						}else {
							
							statusMonth= null;
						}
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						} 
						if(rs.getBlob(20)!= null){
							
							
							
							adminSignature="";
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName="";
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,
								  managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
	}




	public String PostServiceForStatusReportViewAdmin(String request) {
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE S.STATUS_ID='"+ request+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;

						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;

						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 

						if(rs.getString(18) != null){
						statusMonth= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(statusMonth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						statusMonth = sdf.format(cal.getTime());
						
						}else {
						
						statusMonth= null;
						}
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						}
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("view status "+returJson);
			return returJson;
	}




	public String PostServiceForStatusReportAdminAccept(String[] request) {
		
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
	Connection conn = null;
	ResultSet rs = null;
	ResultSet rs1 = null;
	String returJson = "";
	int row = 0;

	PreparedStatement statement1=null;
	PreparedStatement statement2 =null;
	PreparedStatement statement3 =null;	
	try{
		
		
		if(request != null){
					
			String UPDATE="UPDATE status_report SET ACTION_STATUS=?,ADMIN_SIGNATURE=?,ADMIN_ID=? WHERE STATUS_ID='"+request[0]+"'";
	          conn = DbServices.getConnection();
	          statement1 = conn.prepareStatement(UPDATE);

	          statement1.setString(1, "Accepted");

	            String signature=request[1];
	            byte[] b = signature.getBytes(StandardCharsets.UTF_8);
	            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
	            
	            statement1.setBlob(2,blob);
	            statement1.setString(3, request[2]);
	            
	          row = statement1.executeUpdate();
		}
		if(row > 0){
			String EmailTo="";
			String Fullname="";
			String Activity2="";
			String SELECT1="SELECT S.STATUS_ID,S.ACTIVITY,E.FIRST_NAME,E.LAST_NAME,E.EMPLOYEE_ID,E.EMAIL_PRIMARY from status_report S,employee_details E WHERE S.STATUS_ID='"+request[0]+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID";
			statement3 = conn.prepareStatement(SELECT1);
			rs1 = statement3.executeQuery();
			while(rs1.next()){
					String statusId1;
					String activity1;
					String firstname1;
					String lastname1;
					String employeeId1;
					String employeeEmail1;
					
				 if(rs1.getString(1) != null){
						
						statusId1=rs1.getString(1);
					}else {
						
						statusId1= null;
					}
					
					if(rs1.getString(2) != null){
						
						activity1=rs1.getString(2);
						Activity2=activity1;
					}else {
						
						activity1= null;
					}
					
					if(rs1.getString(3)!= null){
						
						firstname1= rs1.getString(3);
					}else {
						
						firstname1= null;
					}
					if(rs1.getString(4)!= null){

						lastname1= rs1.getString(4);
					}else {

						lastname1= null;
					}
					String name1=firstname1+" "+lastname1;
					Fullname=name1;
					if(rs1.getString(5)!= null){
						
						employeeId1= rs1.getString(5);
					}else {
						
						employeeId1= null;
					}
					if(rs1.getString(6)!= null){
						
						employeeEmail1= rs1.getString(6);
						EmailTo=employeeEmail1;
					}else {
						
						employeeEmail1= null;
					}
			}
			
			SimpleSendEmail S1 = new SimpleSendEmail();
			S1.HtmlEmail1(EmailTo,Fullname,Activity2);
		}
		   if (row > 0) {
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE  S.EMPLOYEE_ID=E.EMPLOYEE_ID AND S.ACTION_STATUS='Pending' ";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;
						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;

						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 
						if(rs.getString(18) != null){
							statusMonth= rs.getString(18);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(statusMonth));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							statusMonth = sdf.format(cal.getTime());
							
						}else {
							
							statusMonth= null;
						}
						
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						}
						
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		   }

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				statement1.close();
				statement2.close();
				statement3.close();
				rs.close();
				rs1.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("Accept  "+returJson);
			return returJson;
	}




	public String GetServiceForStatusListAll() {

List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E Where E.EMPLOYEE_ID=S.EMPLOYEE_ID ";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;
						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;
						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 

						if(rs.getString(18) != null){
						statusMonth= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(statusMonth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						statusMonth = sdf.format(cal.getTime());
						
					}else {
						
						statusMonth= null;
					}
						
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						}
						
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
	
	}




	public String GetServiceForStatusListEmployeeId(String request) {
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E Where E.EMPLOYEE_ID=S.EMPLOYEE_ID AND S.EMPLOYEE_ID='"+request+"' AND S.ACTION_STATUS='Pending'";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;
						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;
						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 
						if(rs.getString(18) != null){
							statusMonth= rs.getString(18);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(statusMonth));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							statusMonth = sdf.format(cal.getTime());
							
						}else {
							
							statusMonth= null;
						}
						
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						}
						
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;	
	}




	public String GetServiceForStatusListBasedId(String[] request) {
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E Where E.EMPLOYEE_ID=S.EMPLOYEE_ID AND S.EMPLOYEE_ID='"+request[0]+"' AND S.ACTION_STATUS='"+request[1]+"'";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;
						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;
						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 
						if(rs.getString(18) != null){
							statusMonth= rs.getString(18);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(statusMonth));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							statusMonth = sdf.format(cal.getTime());
							
						}else {
							
							statusMonth= null;
						}
						
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						} 
						
						
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= "";
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
	}


	public String GetServiceForMonthlyStatusListBasedId(String[] request) {

List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E Where E.EMPLOYEE_ID=S.EMPLOYEE_ID AND S.EMPLOYEE_ID='"+request[0]+"' AND S.ACTION_STATUS='Accepted' AND S.STATUS_MONTH LIKE '"+request[2]+"-"+request[1]+"%'";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;
						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;
						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 
						if(rs.getString(18) != null){
							statusMonth= rs.getString(18);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(statusMonth));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							statusMonth = sdf.format(cal.getTime());
							
						}else {
							
							statusMonth= null;
						}
						
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						} 
						
						
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= "";
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
	
	}


	public String GetServiceForEmployeeIdPayroll(String request) {
	

		Connection conn = null;
		ResultSet rs = null;
		String returJson = "";

		PreparedStatement statement2 =null;

		try{
	
	
		conn = DbServices.getConnection();

    	String SELECT="SELECT PAYROLL from employee_details WHERE EMPLOYEE_ID='"+request+"'";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			String payroll;
			
			
			 
			if(rs.getString(1) != null){
				
				payroll=rs.getString(1);
			}else {
				
				payroll= "";
			}
			
		
			Gson gs = new Gson();
			returJson = gs.toJson(payroll).toString();

		}
    
		}catch(Exception e){
	
	e.printStackTrace();
		}finally {
			try {
		
		statement2.close();
		rs.close();
		conn.close();
			} catch (SQLException e) {
		e.printStackTrace();
			}
}
	System.out.println("payroll ... "+returJson);
	return returJson;


	
	
	}

////////////////////////////////////////	
	
	
// NEW ACTIVITY	
	
/////////////////////////////////	
	

@SuppressWarnings("deprecation")
public String PostServiceForStatusReportNewAct(List<RequestStatusReportVO> request1) {
		
		


		
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		


		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		
		
		try{
			for (int i = 0; i < request1.size(); i++) {
				RequestStatusReportVO request=request1.get(i);
				
				System.out.println("Activity;"+request.getActivity());
				System.out.println("currentProject;"+request.getCurrentProject());
				System.out.println("status Month "+request.getStatusMonth());
			
			if(request.getStatusId() == null){
				System.out.println("StartActualDate "+request.getStartPlannedDate());
				
			String INSERT="INSERT INTO status_report (ACTIVITY, START_PLANNED_DATE,START_ACTUAL_DATE,FINISH_PLANNED_DATE, FINISH_ESTIMATED_DATE, STATUS, CLIENT_DETAILS,CURRENT_PROJECT,MANAGER_NAME,CURRENT_MODULE,EMPLOYEE_ID,MANAGER_EMAIL,MANAGER_PHONE_NO,ACTION_STATUS,STATUS_MONTH,EMPLOYEE_SIGNATURE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getActivity());
            
            
            if(request.getStartPlannedDate()==null){
            	statement1.setDate(2, null);
            	
            }
            else{
            
            String plannedDate=request.getStartPlannedDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf.parse(plannedDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlplannedDate = new java.sql.Date(date.getTime());

			
			
			statement1.setDate(2, (java.sql.Date) sqlplannedDate);
            }
            
            if(request.getStartActualDate()==null){
            	statement1.setDate(3, null);
            }else{
            String actualdate= request.getStartActualDate();

            
            
        	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date1 = null;
			try {date1 = sdf1.parse(actualdate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlactualdate = new java.sql.Date(date1.getTime());

			statement1.setDate(3, (java.sql.Date) sqlactualdate);
			
            }
			
			
			if(request.getFinishPlannedDate()==null){
				
				statement1.setDate(4, null);
			}
			else{
			String finishplanneddate =request.getFinishPlannedDate();
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date2 = null;
			try {date2 = sdf2.parse(finishplanneddate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlfinishplanneddate = new java.sql.Date(date2.getTime());
          
			
			statement1.setDate(4, (java.sql.Date) sqlfinishplanneddate);
			}
			
			
			if(request.getFinishEstimatedDate()==null){
				
				statement1.setDate(5, null);
			}else{
			String finishestimateddate =request.getFinishEstimatedDate();
			SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date3 = null;
			try {date3 = sdf3.parse(finishestimateddate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlfinishestimateddate = new java.sql.Date(date3.getTime());
          
			
			statement1.setDate(5, (java.sql.Date) sqlfinishestimateddate);
			}
			
            statement1.setString(6, request.getStatus());
            statement1.setString(7, request.getClientDetails());
            statement1.setString(8,request.getCurrentProject() );
            
 
            statement1.setString(9,request.getManagerName());
            statement1.setString(10,request.getCurrentModule());
            statement1.setString(11,request.getEmployeeId());
            statement1.setString(12,request.getManagerEmail());
            statement1.setString(13,request.getManagerPhonenumber());
            statement1.setString(14,request.getAction_status());
            if(request.getStatusMonth() ==null){
				
				statement1.setDate(15, null);
			}else{
			String statusMonth =request.getStatusMonth();
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date4 = null;
			try {date4 = sdf4.parse(statusMonth);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlstatusMonth = new java.sql.Date(date4.getTime());
          
			
			statement1.setDate(15, (java.sql.Date) sqlstatusMonth);
			}
            
            String signature=request.getEmployeeSignature();
            byte[] b = signature.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(16,blob);
            
           
            
            
            
            row = statement1.executeUpdate();
 		} 	else {
 			   
 			String UPDATE="UPDATE status_report SET ACTIVITY=?, START_PLANNED_DATE=?,START_ACTUAL_DATE=?,FINISH_PLANNED_DATE=?, FINISH_ESTIMATED_DATE=?, STATUS=?, CLIENT_DETAILS=?,CURRENT_PROJECT=?,MANAGER_NAME=?,CURRENT_MODULE=?,EMPLOYEE_ID=?,MANAGER_EMAIL=?,MANAGER_PHONE_NO=?,ACTION_STATUS=?,STATUS_MONTH=?,EMPLOYEE_SIGNATURE=? WHERE STATUS_ID=?";
 	          conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

        statement1.setString(1, request.getActivity());
            
        if(request.getStartPlannedDate()==null){
        	statement1.setDate(2, null);
        	
        }
        else{
        
        String plannedDate=request.getStartPlannedDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = null;
		try {date = sdf.parse(plannedDate);
		} catch (ParseException e) {
			e.printStackTrace();}
		java.sql.Date sqlplannedDate = new java.sql.Date(date.getTime());

		
		
		statement1.setDate(2, (java.sql.Date) sqlplannedDate);
        }
        
        if(request.getStartActualDate()==null){
        	statement1.setDate(3, null);
        }else{
        String actualdate= request.getStartActualDate();

        
        
    	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date1 = null;
		try {date1 = sdf1.parse(actualdate);
		} catch (ParseException e) {
			e.printStackTrace();}
		java.sql.Date sqlactualdate = new java.sql.Date(date1.getTime());

		statement1.setDate(3, (java.sql.Date) sqlactualdate);
		
        }
		
		
		if(request.getFinishPlannedDate()==null){
			
			statement1.setDate(4, null);
		}
		else{
		String finishplanneddate =request.getFinishPlannedDate();
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date2 = null;
		try {date2 = sdf2.parse(finishplanneddate);
		} catch (ParseException e) {
			e.printStackTrace();}
		java.sql.Date sqlfinishplanneddate = new java.sql.Date(date2.getTime());
      
		
		statement1.setDate(4, (java.sql.Date) sqlfinishplanneddate);
		}
		
			
			if(request.getFinishEstimatedDate()==null){
				
				statement1.setDate(5, null);
			}	else{
			String finishestimateddate =request.getFinishEstimatedDate();
			
			
			SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date3 = null;
			try {date3 = sdf3.parse(finishestimateddate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlfinishestimateddate = new java.sql.Date(date3.getTime());
          
			
			statement1.setDate(5, (java.sql.Date) sqlfinishestimateddate);
			
			}
            statement1.setString(6, request.getStatus());
            statement1.setString(7, request.getClientDetails());
            statement1.setString(8,request.getCurrentProject() );
            
 
            statement1.setString(9,request.getManagerName());
            statement1.setString(10,request.getCurrentModule());
            statement1.setString(11,request.getEmployeeId());
            statement1.setString(12,request.getManagerEmail());
            statement1.setString(13,request.getManagerPhonenumber());
            statement1.setString(14,request.getAction_status());
            if(request.getStatusMonth() ==null){
				
				statement1.setDate(15, null);
			}else{
			String statusMonth =request.getStatusMonth();
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date4 = null;
			try {date4 = sdf4.parse(statusMonth);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlstatusMonth = new java.sql.Date(date4.getTime());
			
			
			statement1.setDate(15, (java.sql.Date) sqlstatusMonth);
			}
            
            String signature=request.getEmployeeSignature();
            byte[] b = signature.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(16,blob);
            
            statement1.setString(17,request.getStatusId());
           
            
            row = statement1.executeUpdate();

 		}
		
			}
			String EmpId="";
			String StatusMonthEmp="";
			java.sql.Date sqlStatusMonthEmp;
			String StatusDate="";
			String Statusmonth="";
			String StatusYear="";
			
	    	for (int i = 0; i < request1.size(); i++) {
				RequestStatusReportVO request=request1.get(i);
				EmpId=request.getEmployeeId();
				StatusMonthEmp=request.getStatusMonth();
				
			
				SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
				Date date4 = null;
				try {date4 = sdf4.parse(StatusMonthEmp);
				} catch (ParseException e) {
					e.printStackTrace();}
				sqlStatusMonthEmp = new java.sql.Date(date4.getTime()); 		
				
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				DateFormat df1 = new SimpleDateFormat("MM");
				DateFormat df2 = new SimpleDateFormat("yyyy");
				
				Statusmonth=df1.format(date4);
				StatusYear = df2.format(date4);
				 
	    	}
		    if (row > 0) {
		    	 
		    	String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE S.EMPLOYEE_ID='"+EmpId+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID AND S.STATUS_MONTH LIKE '"+StatusYear+"-"+Statusmonth+"%' ";
               	statement2 = conn.prepareStatement(SELECT);
              
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String statusId;
					 String employeeId;
					 String activity;
					 String startPlannedDate;
					 String startActualDate;
					 String finishPlannedDate;
					 String finishEstimatedDate;
					 String status;
					 String clientDetails;
					 String currentProject;
					 String managerName;
					 String currentModule;
					 String firstname;
					 String lastname;
					 String name;
					 String managerEmail;
					 String managerPhonenumber;
					 String action_status;
					 String statusMonth;
					 String employeeSignature;
					
					 String adminSignature;
					 String adminId;
					 String adminName;
					if(rs.getString(1) != null){
						
						statusId=rs.getString(1);
					}else {
						
						statusId= null;
					}
					
					if(rs.getString(2) != null){
						
						activity=rs.getString(2);
					}else {
						
						activity= null;
					}
					if(rs.getString(3) != null){
						startPlannedDate= rs.getString(3);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(startPlannedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						startPlannedDate = sdf.format(cal.getTime());
						
					}else {
						
						startPlannedDate= null;
					}
					if(rs.getString(4)!= null){
						
						startActualDate=rs.getString(4);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(startActualDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						startActualDate = sdf.format(cal.getTime());
					}else {
						
						startActualDate= null;
					}
					if(rs.getString(5)!= null){
						
						finishPlannedDate= rs.getString(5);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(finishPlannedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						finishPlannedDate = sdf.format(cal.getTime());
						
					}else {
						
						finishPlannedDate= null;
					}
					if(rs.getString(6)!= null){
						
						finishEstimatedDate= rs.getString(6);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(finishEstimatedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						finishEstimatedDate = sdf.format(cal.getTime());
						
					}else {
						
						finishEstimatedDate= null;
					}
					
					if(rs.getString(7)!= null){
						status= rs.getString(7);
					}else {
						
						status = null;
					}
					
					if(rs.getString(8)!= null){
						
						clientDetails= rs.getString(8);
					}else {
						
						clientDetails= null;
					}
					if(rs.getString(9)!= null){
						
						currentProject= rs.getString(9);
					}else {
						
						currentProject= null;
					}
					if(rs.getString(10)!= null){
						
						managerName= rs.getString(10);
						
					}else {
						
						managerName= null;
					}
					if(rs.getString(11)!= null){
						
						currentModule= rs.getString(11);
					}else {
						
						currentModule= null;
					}
					if(rs.getString(12)!= null){
						
						employeeId= rs.getString(12);
					}else {
						
						employeeId= null;
					}
					if(rs.getString(13)!= null){
						
						firstname= rs.getString(13);
					}else {
						
						firstname= null;
					}
					if(rs.getString(14)!= null){
	
						lastname= rs.getString(14);
					}else {
	
						lastname= null;
					}
					name=firstname+" "+lastname;
					
					if(rs.getString(15)!= null){
						
						managerEmail= rs.getString(15);
					}else {
	
						managerEmail= null;
					}
					if(rs.getString(16)!= null){
						
						managerPhonenumber= rs.getString(16);
					}else {
	
						managerPhonenumber= null;
					}
					
					if(rs.getString(17)!= null){
						
						action_status= rs.getString(17);
					}else {
	
						action_status= null;
					} 
					if(rs.getString(18) != null){
						statusMonth= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(statusMonth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						statusMonth = sdf.format(cal.getTime());
						
					}else {
						
						statusMonth= null;
					}
					 
					if(rs.getBlob(19)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(19);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						employeeSignature=str;
					}else {
						
						employeeSignature= null;
					}
					if(rs.getBlob(20)!= null){
						
						adminSignature="";
					}else {
						
						adminSignature= "";
					}
					if(rs.getString(21) != null){
						
						adminId=rs.getString(21);
						adminName="";
					}else {
						
						adminId= null;
						adminName="";
					}
					ResponseStatusReportVo mObj = new ResponseStatusReportVo(
							 statusId,employeeId, activity,startPlannedDate,  startActualDate,
							  finishPlannedDate,finishEstimatedDate, status,
							  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,
							  action_status,statusMonth,employeeSignature,adminSignature,adminName);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    }
	//	conn.close();
		
	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;


	}


public String PostServiceForDeleteStatusReport(String[] request) {


	


	List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson = "";
		
	System.out.println(" req  .. "+request);
	PreparedStatement statement1 =null;
	PreparedStatement statement2 =null;
	int row=0;
	try{
		conn = DbServices.getConnection();
		if(request[1] != null){
			 
			String DELETE_EXPENSE="delete from status_report where STATUS_ID = ?";
            conn = DbServices.getConnection();
         statement1 = conn.prepareStatement(DELETE_EXPENSE);
        statement1.setString(1, request[1]);
          
         row = statement1.executeUpdate();
        
		}
		
		Date dat=new SimpleDateFormat("MM/dd/yyyy").parse(request[2]);
		
		DateFormat df1 = new SimpleDateFormat("MM");
		DateFormat df2 = new SimpleDateFormat("yyyy");
		
		String Statusmonth=df1.format(dat);
		String StatusYear = df2.format(dat);
		if(row>0){
	    	 
	    	String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE S.EMPLOYEE_ID='"+ request[0]+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID AND S.STATUS_MONTH LIKE '"+StatusYear+"-"+Statusmonth+"%'";
          	statement2 = conn.prepareStatement(SELECT);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String statusId;
				 String employeeId;
				 String activity;
				 String startPlannedDate;
				 String startActualDate;
				 String finishPlannedDate;
				 String finishEstimatedDate;
				 String status;
				 String clientDetails;
				 String currentProject;
				 String managerName;
				 String currentModule;
				 String firstname;
				 String lastname;
				 String name;
				 String managerEmail;
				 String managerPhonenumber;
				 String action_status;
				 String statusMonth;
				 String employeeSignature;
				
				 String adminSignature;
				 String adminId;
				 String adminName;
				if(rs.getString(1) != null){
					
					statusId=rs.getString(1);
				}else {
					
					statusId= null;
				}
				
				if(rs.getString(2) != null){
					
					activity=rs.getString(2);
				}else {
					
					activity= null;
				}
				if(rs.getString(3) != null){
					startPlannedDate= rs.getString(3);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(startPlannedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					startPlannedDate = sdf.format(cal.getTime());
					
				}else {
					
					startPlannedDate= null;
				}
				if(rs.getString(4)!= null){
					
					startActualDate=rs.getString(4);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(startActualDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					startActualDate = sdf.format(cal.getTime());
				}else {
					
					startActualDate= null;
				}
				if(rs.getString(5)!= null){
					
					finishPlannedDate= rs.getString(5);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(finishPlannedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					finishPlannedDate = sdf.format(cal.getTime());
					
				}else {
					
					finishPlannedDate= null;
				}
				if(rs.getString(6)!= null){
					
					finishEstimatedDate= rs.getString(6);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(finishEstimatedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					finishEstimatedDate = sdf.format(cal.getTime());
					
				}else {
					
					finishEstimatedDate= null;
				}
				
				if(rs.getString(7)!= null){
					status= rs.getString(7);
				}else {
					
					status = null;
				}
				
				if(rs.getString(8)!= null){
					
					clientDetails= rs.getString(8);
				}else {
					
					clientDetails= null;
				}
				if(rs.getString(9)!= null){
					
					currentProject= rs.getString(9);
				}else {
					
					currentProject= null;
				}
				if(rs.getString(10)!= null){
					
					managerName= rs.getString(10);
					
				}else {
					
					managerName= null;
				}
				if(rs.getString(11)!= null){
					
					currentModule= rs.getString(11);
				}else {
					
					currentModule= null;
				}
				if(rs.getString(12)!= null){
					
					employeeId= rs.getString(12);
				}else {
					
					employeeId= null;
				}
				if(rs.getString(13)!= null){
					
					firstname= rs.getString(13);
				}else {
					
					firstname= null;
				}
				if(rs.getString(14)!= null){

					lastname= rs.getString(14);
				}else {

					lastname= null;
				}
				name=firstname+" "+lastname;
				
				if(rs.getString(15)!= null){
					
					managerEmail= rs.getString(15);
				}else {

					managerEmail= null;
				}
				if(rs.getString(16)!= null){
					
					managerPhonenumber= rs.getString(16);
				}else {

					managerPhonenumber= null;
				}
				
				if(rs.getString(17)!= null){
					
					action_status= rs.getString(17);
				}else {

					action_status= null;
				} 
				if(rs.getString(18) != null){
					statusMonth= rs.getString(18);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(statusMonth));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					statusMonth = sdf.format(cal.getTime());
					
				}else {
					
					statusMonth= null;
				}
				 
				if(rs.getBlob(19)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(19);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					employeeSignature=str;
				}else {
					
					employeeSignature= null;
				}
				if(rs.getBlob(20)!= null){
					
					adminSignature="";
				}else {
					
					adminSignature= "";
				}
				if(rs.getString(21) != null){
					
					adminId=rs.getString(21);
					adminName="";
				}else {
					
					adminId= null;
					adminName="";
				}
				ResponseStatusReportVo mObj = new ResponseStatusReportVo(
						 statusId,employeeId, activity,startPlannedDate,  startActualDate,
						  finishPlannedDate,finishEstimatedDate, status,
						  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,
						  action_status,statusMonth,employeeSignature,adminSignature,adminName);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
	    }
	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("Status id  .. "+returJson);
		return returJson;
	
	

	
	
	


}


public String PostServiceForStatusReportByMonthEmployee(String[] request) {

List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
 		
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE S.EMPLOYEE_ID='"+request[0]+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID AND S.STATUS_MONTH LIKE '"+request[2]+"-"+request[1]+"%'";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;

						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;

						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 

						if(rs.getString(18) != null){
						statusMonth= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(statusMonth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						statusMonth = sdf.format(cal.getTime());
						
						}else {
						
						statusMonth= null;
						}
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						}
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("view status "+returJson);
			return returJson;
	
}


public String PostServiceForStatusRecentUpdatesGet(String[] request) {
	


List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
List<ResponseStatusReportVo> finalList1 = new ArrayList<ResponseStatusReportVo>();		
		Connection conn = null;
 		ResultSet rs = null;
 		ResultSet rs1 = null;
 		String returJson = "";
 		String returJson1 = "";	
 		String returJson2 = "";	
 		
 		String previousMonth="";
 		String previousYear="";
 		int preMon=0;
 		int preYear=0;
		PreparedStatement statement2 =null;
	
		if(request[1]== "1"){
			previousMonth="12";
			 preYear = Integer.parseInt(request[2]);
				preYear=preYear-1;
			 previousYear =String.valueOf(preYear);  
		}else{
			preMon = Integer.parseInt(request[1]);
			preMon=preMon-1;
			if(preMon == 0){
				preMon= 12;
			}
			previousMonth =String.valueOf(preMon);  
			preYear = Integer.parseInt(request[2]);
			preYear=preYear-1;
			previousYear =String.valueOf(preYear); 
		}
		System.out.println("previousMonth "+previousMonth);
		System.out.println("previousYear "+previousYear);
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE S.EMPLOYEE_ID='"+request[0]+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID AND S.STATUS_MONTH LIKE '"+request[2]+"-"+request[1]+"%'";
					     
					
					statement2 = conn.prepareStatement(SELECT);
					
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;

						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;

						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 

						if(rs.getString(18) != null){
						statusMonth= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(statusMonth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						statusMonth = sdf.format(cal.getTime());
						
						}else {
						
						statusMonth= null;
						}
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						}
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson1 = gs.toJson(finalList).toString();
						
					}
					
					
					
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				
				rs.close();
				
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		if(finalList.size()>0){
			returJson=returJson1;
		}else{
			try{
				conn = DbServices.getConnection();
				String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE S.EMPLOYEE_ID='"+request[0]+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID AND S.STATUS_MONTH LIKE '"+previousYear+"-"+previousMonth+"%'";
				     
				
				statement2 = conn.prepareStatement(SELECT);
				
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String statusId;
					 String employeeId;
					 String activity;
					 String startPlannedDate;
					 String startActualDate;
					 String finishPlannedDate;
					 String finishEstimatedDate;
					 String status;
					 String clientDetails;
					 String currentProject;
					 String managerName;
					 String currentModule;
					 String firstname;
					 String lastname;
					 
					 String managerEmail;
					 String managerPhonenumber;
					 String action_status;

					 String statusMonth;
					 String employeeSignature;
					 
					 String adminSignature;
					 String adminId;
					 String adminName;
					 
					if(rs.getString(1) != null){
						
						statusId=rs.getString(1);
					}else {
						
						statusId= null;
					}
					
					if(rs.getString(2) != null){
						
						activity=rs.getString(2);
					}else {
						
						activity= null;
					}
					if(rs.getString(3) != null){
						startPlannedDate= rs.getString(3);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(startPlannedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						startPlannedDate = sdf.format(cal.getTime());
						
					}else {
						
						startPlannedDate= null;
					}
					if(rs.getString(4)!= null){
						
						startActualDate=rs.getString(4);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(startActualDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						startActualDate = sdf.format(cal.getTime());
					}else {
						
						startActualDate= null;
					}
					if(rs.getString(5)!= null){
						
						finishPlannedDate= rs.getString(5);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(finishPlannedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						finishPlannedDate = sdf.format(cal.getTime());
						
					}else {
						
						finishPlannedDate= null;
					}
					if(rs.getString(6)!= null){
						
						finishEstimatedDate= rs.getString(6);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(finishEstimatedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						finishEstimatedDate = sdf.format(cal.getTime());
						
					}else {
						
						finishEstimatedDate= null;
					}
					
					if(rs.getString(7)!= null){
						status= rs.getString(7);
					}else {
						
						status = null;
					}
					
					if(rs.getString(8)!= null){
						
						clientDetails= rs.getString(8);
					}else {
						
						clientDetails= null;
					}
					if(rs.getString(9)!= null){
						
						currentProject= rs.getString(9);
					}else {
						
						currentProject= null;
					}
					if(rs.getString(10)!= null){
						
						managerName= rs.getString(10);
						
					}else {
						
						managerName= null;
					}
					if(rs.getString(11)!= null){
						
						currentModule= rs.getString(11);
					}else {
						
						currentModule= null;
					}
					if(rs.getString(12)!= null){
						
						employeeId= rs.getString(12);
					}else {
						
						employeeId= null;
					}
					if(rs.getString(13)!= null){
						
						firstname= rs.getString(13);
					}else {
						
						firstname= null;
					}
					if(rs.getString(14)!= null){

						lastname= rs.getString(14);
					}else {

						lastname= null;
					}
					String name=firstname+" "+lastname;

					if(rs.getString(15)!= null){
						
						managerEmail= rs.getString(15);
					}else {
	
						managerEmail= null;
					}
					if(rs.getString(16)!= null){
						
						managerPhonenumber= rs.getString(16);
					}else {
	
						managerPhonenumber= null;
					}
					if(rs.getString(17)!= null){
						
						action_status= rs.getString(17);
					}else {
	
						action_status= null;
					} 

					if(rs.getString(18) != null){
					statusMonth= rs.getString(18);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(statusMonth));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					statusMonth = sdf.format(cal.getTime());
					
					}else {
					
					statusMonth= null;
					}
					if(rs.getBlob(19)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(19);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						employeeSignature=str;
					}else {
						
						employeeSignature= null;
					}
					if(rs.getBlob(20)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(20);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						adminSignature=str;
					}else {
						
						adminSignature= null;
					}
					if(rs.getString(21) != null){
						
						adminId=rs.getString(21);
						adminName=GetServiceForName(adminId);
					}else {
						
						adminId= null;
						adminName="";
					}
					ResponseStatusReportVo mObj = new ResponseStatusReportVo(
							 statusId,employeeId, activity,startPlannedDate,  startActualDate,
							  finishPlannedDate,finishEstimatedDate, status,
							  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
					finalList1.add(mObj);
					Gson gs = new Gson();
					returJson2 = gs.toJson(finalList1).toString();
					returJson=returJson2;
				}
				
				
				


	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			
			rs.close();
			
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
		}
		
		
		
		
		
		
		
			System.out.println("view status "+returJson);
			return returJson;
	

}


public String PostServiceForGetStatusReportReview(String[] request) {


List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
 		
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT S.STATUS_ID,S.ACTIVITY, S.START_PLANNED_DATE,S.START_ACTUAL_DATE,S.FINISH_PLANNED_DATE, S.FINISH_ESTIMATED_DATE, S.STATUS, S.CLIENT_DETAILS,S.CURRENT_PROJECT,S.MANAGER_NAME,S.CURRENT_MODULE,S.EMPLOYEE_ID,E.FIRST_NAME,E.LAST_NAME,S.MANAGER_EMAIL,S.MANAGER_PHONE_NO,S.ACTION_STATUS,S.STATUS_MONTH,S.EMPLOYEE_SIGNATURE,S.ADMIN_SIGNATURE,S.ADMIN_ID from status_report S,employee_details E WHERE S.EMPLOYEE_ID='"+request[0]+"' AND S.EMPLOYEE_ID=E.EMPLOYEE_ID AND S.ACTION_STATUS='"+request[1]+"' AND S.STATUS_MONTH LIKE '"+request[3]+"-"+request[2]+"%'";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String statusId;
						 String employeeId;
						 String activity;
						 String startPlannedDate;
						 String startActualDate;
						 String finishPlannedDate;
						 String finishEstimatedDate;
						 String status;
						 String clientDetails;
						 String currentProject;
						 String managerName;
						 String currentModule;
						 String firstname;
						 String lastname;
						 
						 String managerEmail;
						 String managerPhonenumber;
						 String action_status;

						 String statusMonth;
						 String employeeSignature;
						 
						 String adminSignature;
						 String adminId;
						 String adminName;
						 
						if(rs.getString(1) != null){
							
							statusId=rs.getString(1);
						}else {
							
							statusId= null;
						}
						
						if(rs.getString(2) != null){
							
							activity=rs.getString(2);
						}else {
							
							activity= null;
						}
						if(rs.getString(3) != null){
							startPlannedDate= rs.getString(3);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							startPlannedDate= null;
						}
						if(rs.getString(4)!= null){
							
							startActualDate=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(startActualDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							startActualDate = sdf.format(cal.getTime());
						}else {
							
							startActualDate= null;
						}
						if(rs.getString(5)!= null){
							
							finishPlannedDate= rs.getString(5);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishPlannedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishPlannedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishPlannedDate= null;
						}
						if(rs.getString(6)!= null){
							
							finishEstimatedDate= rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(finishEstimatedDate));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							finishEstimatedDate = sdf.format(cal.getTime());
							
						}else {
							
							finishEstimatedDate= null;
						}
						
						if(rs.getString(7)!= null){
							status= rs.getString(7);
						}else {
							
							status = null;
						}
						
						if(rs.getString(8)!= null){
							
							clientDetails= rs.getString(8);
						}else {
							
							clientDetails= null;
						}
						if(rs.getString(9)!= null){
							
							currentProject= rs.getString(9);
						}else {
							
							currentProject= null;
						}
						if(rs.getString(10)!= null){
							
							managerName= rs.getString(10);
							
						}else {
							
							managerName= null;
						}
						if(rs.getString(11)!= null){
							
							currentModule= rs.getString(11);
						}else {
							
							currentModule= null;
						}
						if(rs.getString(12)!= null){
							
							employeeId= rs.getString(12);
						}else {
							
							employeeId= null;
						}
						if(rs.getString(13)!= null){
							
							firstname= rs.getString(13);
						}else {
							
							firstname= null;
						}
						if(rs.getString(14)!= null){
	
							lastname= rs.getString(14);
						}else {
	
							lastname= null;
						}
						String name=firstname+" "+lastname;

						if(rs.getString(15)!= null){
							
							managerEmail= rs.getString(15);
						}else {
		
							managerEmail= null;
						}
						if(rs.getString(16)!= null){
							
							managerPhonenumber= rs.getString(16);
						}else {
		
							managerPhonenumber= null;
						}
						if(rs.getString(17)!= null){
							
							action_status= rs.getString(17);
						}else {
		
							action_status= null;
						} 

						if(rs.getString(18) != null){
						statusMonth= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(statusMonth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						statusMonth = sdf.format(cal.getTime());
						
						}else {
						
						statusMonth= null;
						}
						if(rs.getBlob(19)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(19);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							employeeSignature=str;
						}else {
							
							employeeSignature= null;
						}
						if(rs.getBlob(20)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(20);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							adminSignature=str;
						}else {
							
							adminSignature= null;
						}
						if(rs.getString(21) != null){
							
							adminId=rs.getString(21);
							adminName=GetServiceForName(adminId);
						}else {
							
							adminId= null;
							adminName="";
						}
						ResponseStatusReportVo mObj = new ResponseStatusReportVo(
								 statusId,employeeId, activity,startPlannedDate,  startActualDate,
								  finishPlannedDate,finishEstimatedDate, status,
								  clientDetails, currentProject, managerName, currentModule,name,managerEmail,managerPhonenumber,action_status,statusMonth,employeeSignature,adminSignature,adminName);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("view status "+returJson);
			return returJson;
	

}


public String PostServiceForAcceptStatusAdmin(List<RequestStatusReportAcceptVO> request1) {
	


	
List<ResponseStatusReportVo> finalList = new ArrayList<ResponseStatusReportVo>();
		


		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2=null;
		RequestStatusReportAcceptVO request;
		
		try{
			for(int i=0;i<request1.size();i++){
				
				request=request1.get(i);
			
			
 			   
 			String UPDATE="UPDATE status_report SET ADMIN_SIGNATURE=?,ADMIN_ID=?,ACTION_STATUS=? WHERE STATUS_ID=?";
 	          conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

       
            statement1.setString(1,request.getAdminSign());
            statement1.setString(2,request.getEmployeeId());
            statement1.setString(3,"Accepted");
            statement1.setString(4,request.getStatusId());
           
            
            row = statement1.executeUpdate();
		}
	
		
			
			
			
			
			
			
	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		
		Gson gs = new Gson();
		returJson = gs.toJson("return").toString();
		
		System.out.println("asfasdf"+returJson);
		return returJson;


	
}


	


	




}