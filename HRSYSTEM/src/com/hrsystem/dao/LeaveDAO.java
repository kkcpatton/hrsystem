package com.hrsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.google.gson.Gson;
import com.hrsystem.db.DbServices;
import com.hrsystem.email.SimpleSendEmail;
import com.hrsystem.vo.RequestLeaveVO;
import com.hrsystem.vo.ResponseEmployeeLoginVO;
import com.hrsystem.vo.ResponseLeaveVO;

public class LeaveDAO {

	public String PostServiceForApplyLeave(RequestLeaveVO request) {
		List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	//	String useremail= LoginDAO.LoggedUser;
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
	 try{
		 
//		 String INSERT="INSERT INTO Leave_details (Leave_FREQUENCY, NO_OF_WORKING_DAYS, BASIC_PAY, HOUSE_ALLOWANCE, MEDICAL_ALLOWANCE, TRANSPORT_ALLOWANCE, EDUCATION_ALLOWANCE, REIMBURSING_ALLOWANCE, BONUS_TYPE, BONUS_PAY, TOTAL_PAY, REMARKS, PAYMENT_DATE) values (?,?,?,?,?)";
		 String EmpEmail = "";
		 String INSERT="INSERT INTO leave_details (EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM, LEAVE_TO, DURATION, STATUS,DESCRIPTION)"
		 		+ " values (?,?,?,?,?,?,?,?,?)";
            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(INSERT);

//             System.out.println("ID--> "+ request.getEmployeeId());
//             System.out.println("name--> "+ request.getName());
//             System.out.println("Leave from -->"+request.getLeaveFrom());
//             System.out.println("Leave To --> "+ request.getLeaveTo());
             statement1.setString(1, request.getEmployeeId());
             statement1.setString(2, request.getName());
             statement1.setString(3, request.getDesignation());
	         statement1.setString(4, request.getLeaveType());

	          
	         
	         String FROM=request.getLeaveFrom();
	         
	         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date date1 = null;
				try {date1 = sdf.parse(FROM);
				} catch (ParseException e) {
					e.printStackTrace();}
				java.sql.Date sqldob = new java.sql.Date(date1.getTime());

				statement1.setDate(5, (java.sql.Date) sqldob);

				
			    String TO=request.getLeaveTo();
		         
		         SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
					java.util.Date date2 = null;
					try {date2 = sdf1.parse(TO);
					} catch (ParseException e) {
						e.printStackTrace();}
					java.sql.Date sqldob1 = new java.sql.Date(date2.getTime());

					statement1.setDate(6, (java.sql.Date) sqldob1);


	         
//	         statement1.setString(5, request.getLeaveTo());
	         statement1.setString(7, request.getDuration());
	         String Lstatus= "Pending";
	         statement1.setString(8, Lstatus);
	         statement1.setString(9, request.getDescription());
  
//	         System.out.println(request);
	          
	         row = statement1.executeUpdate();
            System.out.println("Inserted one row");
            
            
            if(row>0){
            	String Fullname = "";           	
           	String SELECT="SELECT L.LEAVE_ID, L.EMPLOYEE_ID, L.EMPLOYEE_NAME, E.EMAIL_PRIMARY from leave_details L, employee_details E "
           			+ "where  L.EMPLOYEE_ID = E.EMPLOYEE_ID AND  L.EMPLOYEE_ID="+request.getEmployeeId()+"  ";
              	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					String leaveId = null;
					 
					String employeeId = null;
					String name = null; 
					String designation  = null;
					String leaveType = null;
					String leaveFrom= null;
					String leaveTo = null;
					String duration= null;
					String status = null;
					String description = null;
					String description1 = null;

					String email = null;


					if(rs.getString(1) != null){
						
						leaveId = rs.getString(1);
					}else {
						
						leaveId =null;
					}
					
					
					if(rs.getString(2) != null){
						
						employeeId = rs.getString(2);
					}else {
						
						employeeId =null;
					}
					
					if(rs.getString(3)!= null){
						name= rs.getString(3);
						Fullname = name ; 
					}else{
						
						name = null;
					}
					 
					if(rs.getString(4)!= null){
						email = rs.getString(4);
						EmpEmail =email; 
					}else {
						
						email = null;
					}
			 	}
           
				System.out.println(EmpEmail +" email id to sent email");
				System.out.println(Fullname +"full name to sent email");
				SimpleSendEmail S1 = new SimpleSendEmail();
//				S1.HtmlEmail2(EmpEmail,Fullname);

            
            }
            
            
             if(row>0){
            	
            	String SELECT="SELECT LEAVE_ID, EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM,LEAVE_TO,"
            			+ "DURATION, STATUS, DESCRIPTION from leave_details   where EMPLOYEE_ID="+request.getEmployeeId()+" and STATUS != 'Cancelled'";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					String leaveId = null;
					 
					String employeeId = null;
					String name = null; 
					String designation  = null;
					String leaveType = null;
					String leaveFrom= null;
					String leaveTo = null;
					String duration= null;
					String status = null;
					String description = null;
					String description1 = null;

					String email = null;


					if(rs.getString(1) != null){
						
						leaveId = rs.getString(1);
					}else {
						
						leaveId =null;
					}
					
					
					if(rs.getString(2) != null){
						
						employeeId = rs.getString(2);
					}else {
						
						employeeId =null;
					}
					
					if(rs.getString(3)!= null){
						name= rs.getString(3);
						
					}else{
						
						name = null;
					}
					
					if(rs.getString(4)!= null){
						
						designation = rs.getString(4);
					}else{
						designation= null;
					}

					if(rs.getString(5)!= null){
						
						leaveType = rs.getString(5);
					}else{
						leaveType= null;
					}

					if(rs.getString(6)!= null){
						
						leaveFrom = rs.getString(6);
						
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(leaveFrom));
						SimpleDateFormat sdf2 = new SimpleDateFormat(
								"MM/dd/yyyy");
						leaveFrom = sdf2.format(cal.getTime());
					}else{
						leaveFrom= null;
					}

					if(rs.getString(7)!= null){
						
						leaveTo = rs.getString(7);
						
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(leaveTo));
						SimpleDateFormat sdf2 = new SimpleDateFormat(
								"MM/dd/yyyy");
						leaveTo = sdf2.format(cal.getTime());
						
					}else{
						leaveTo= null;
					}

					
					if(rs.getString(8)!= null){
						
						duration = rs.getString(8);
					}else{
						duration= null;
					}
					
					if(rs.getString(9)!= null){
						
						status = rs.getString(9);
					}else{
						status= null;
					}

					if(rs.getString(10)!= null){
						
						description = rs.getString(10);
					}else{
						description= null;
					}
 
					ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
							leaveType,leaveFrom,leaveTo,duration,status,description,description1,email);
					
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();
				}
            }
            
             

	 }catch(Exception e){
		 	e.printStackTrace();
	 }
		return returJson;
	}

	public String GetMethodForLeaveList() {
		List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		PreparedStatement statement3 =null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date date = new Date();
//        System.out.println("cureent date -->"+sdf.format(date));
        String SELECT2 = "";
		int row = 0; 
        try{
        	
			conn = DbServices.getConnection();
			SELECT2 =" UPDATE leave_details set STATUS = ? where STATUS = 'Approved' and LEAVE_TO <= '"+sdf.format(date)+"' ";
			statement3 = conn.prepareStatement(SELECT2);
			statement3.setString(1, "Consumed");
			row = statement3.executeUpdate();
        	if(row>0){
			System.out.println("updated consumed");
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
        

		
		try{
					conn = DbServices.getConnection();
		         	String SELECT="SELECT LEAVE_ID, EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM,LEAVE_TO,DURATION, STATUS, DESCRIPTION, DESCRIPTION_1 from Leave_details where STATUS != 'Cancelled'";
		         	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation  = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						String description1 = null;
						
						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							
							description = rs.getString(10);
						}else {
							description = null;
						}
						
						if(rs.getString(11)!= null){
							
							description1 = rs.getString(11);
						}else {
							
							 description1 = null;
						}
						String email = null;
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status,description, description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
	}

	public String PostMethodForLeaveListById(String request) {
		List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		PreparedStatement statement3 =null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date date = new Date();
//        System.out.println("cureent date -->"+sdf.format(date));
        String SELECT2 = "";
		int row = 0; 
        try{
        	
			conn = DbServices.getConnection();
			SELECT2 =" UPDATE leave_details set STATUS = ? where STATUS = 'Approved' and LEAVE_TO <= '"+sdf.format(date)+"' ";
			statement3 = conn.prepareStatement(SELECT2);
			statement3.setString(1, "Consumed");
			row = statement3.executeUpdate();
        	if(row>0){
			System.out.println("updated consumed");
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
        
         	
		try{
					conn = DbServices.getConnection();
//					SELECT2 =" SET SQL_SAFE_UPDATES=0; UPDATE leave_details set STATUS ='Consumed' where STATUS = 'Approved' and LEAVE_TO <= '"+sdf.format(date)+"' ";
					
		         	String SELECT="SELECT LEAVE_ID, EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM,LEAVE_TO,"
		         			+ "DURATION, STATUS, DESCRIPTION, DESCRIPTION_1 from Leave_details where EMPLOYEE_ID="+request+" and STATUS != 'Cancelled'";
		         	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation  = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description =null;
						String description1 =null;
						
						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
			
							
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							
							description = rs.getString(10);
						}else {
							description = null;
						}
						
						if(rs.getString(11)!= null){
							description1 = rs.getString(11);
						}else {
							description1 =null;
						}
						String email = null;
						
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status,description, description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
        
			System.out.println("asfasdf"+returJson);
			return returJson;
	}

	public String PostMethodForViewLeaveId(String request) {
		
		List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
		         	String SELECT="SELECT LEAVE_ID, EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM,LEAVE_TO,"
		         			+ "DURATION, STATUS, DESCRIPTION, DESCRIPTION_1 from Leave_details where LEAVE_ID="+request+" and STATUS!= 'Cancelled'";
		         	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation  = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						String description1 = null;
						

						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
			
							
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							
							description = rs.getString(10);
						}else {
							
							description = null;
						}

						if(rs.getString(11)!= null){
							description1 = rs.getString(11);

						}else {
							
						 description1 = null;
						}
						
						String email = null;
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status, description,description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("View Done By Leave id"+returJson);
			return returJson;
	}

	public String PostMethodForApproveLeaveId(String[] request) {
		List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
 		int row = 0;
		PreparedStatement statement1 =null;
		PreparedStatement statement2 =null;
		
		System.out.println("admin id --->"+ request[1]);
		try{
			
			if(request != null){
				
				String UPDATE="UPDATE leave_details SET STATUS=? WHERE LEAVE_ID='"+request[0]+"'";
		          conn = DbServices.getConnection();
		          statement1 = conn.prepareStatement(UPDATE);

		          statement1.setString(1, "Approved");

		          row = statement1.executeUpdate();
			}
			
			String employeeId1 = null;

			
			if(row>0){
            	String Fullname = "";   
            	String EmpEmail = " ";
            	
            	String a = "SELECT EMPLOYEE_ID, EMPLOYEE_NAME from leave_details where LEAVE_ID = '"+request[0]+"'";
             	PreparedStatement   statement3 = conn.prepareStatement(a);
               	ResultSet rs1 = statement3.executeQuery();
               	
				
				while(rs1.next()){
					
 					 
					String name = null; 
 
					if(rs1.getString(1) != null){
						
						employeeId1 = rs1.getString(1);
					}else {
						
						employeeId1 =null;
					}
					
					if(rs1.getString(2)!= null){
						name= rs1.getString(2);
						Fullname = name ; 
					}else{
						
						name = null;
					}
					
            	
            	
            	String b="SELECT EMAIL_PRIMARY from employee_details where EMPLOYEE_ID="+request[1]+" ";
            	
            	PreparedStatement   statement4 = conn.prepareStatement(b);
            	ResultSet rs2 = statement4.executeQuery();
				
				while(rs2.next()){
									 
					String email = null;

 					
					if(rs2.getString(1)!= null){
						email = rs2.getString(1);
						EmpEmail =email; 
					}else {
						
						email = null;
					}
					
 					 

			 	}
           
				System.out.println(EmpEmail +" --> admin email id to sent email");
				System.out.println(Fullname +" --> Employee full name to sent email");
			 	
				SimpleSendEmail S1 = new SimpleSendEmail();
				S1.HtmlEmail5(EmpEmail,Fullname);

            
            }

			}	
			
			if(row > 0){

//					conn = DbServices.getConnection();
		         	String SELECT="SELECT LEAVE_ID, EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM,LEAVE_TO,"
		         			+ "DURATION, STATUS, DESCRIPTION from Leave_details where STATUS != 'Cancelled' and EMPLOYEE_ID='"+employeeId1+"' ";
		         	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation  = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						

						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
			
							
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							
							description = rs.getString(10);
						}else {
							
							description =null;
						}
						
						String description1 = null;
						String email = null;
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status,description,description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("approve Done By Leave id"+returJson);
			return returJson;
	}

	public String PostMethodForRejectLeaveId(String[] request) {
		List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
 		int row = 0;
		PreparedStatement statement1 =null;
		PreparedStatement statement2 =null;
		
		
		try{
			
			if(request != null){
				
				String UPDATE="UPDATE leave_details SET STATUS=? WHERE LEAVE_ID='"+request[0]+"'";
		          conn = DbServices.getConnection();
		          statement1 = conn.prepareStatement(UPDATE);

		          statement1.setString(1, "Rejected");

		          row = statement1.executeUpdate();
			}
			
			
			String employeeId1 = null;
			
			if(row>0){
            	String Fullname = "";   
            	String EmpEmail = " ";
            	
            	String a = "SELECT EMPLOYEE_ID, EMPLOYEE_NAME from leave_details where LEAVE_ID = '"+request[0]+"'";
             	PreparedStatement   statement3 = conn.prepareStatement(a);
               	ResultSet rs1 = statement3.executeQuery();
               	
				
				while(rs1.next()){
					
 					 
					String name = null; 
 
					if(rs1.getString(1) != null){
						
						employeeId1 = rs1.getString(1);
					}else {
						
						employeeId1 =null;
					}
					
					if(rs1.getString(2)!= null){
						name= rs1.getString(2);
						Fullname = name ; 
					}else{
						
						name = null;
					}
					
            	String b="SELECT EMAIL_PRIMARY from employee_details where EMPLOYEE_ID="+request[1]+" ";
            	
            	PreparedStatement   statement4 = conn.prepareStatement(b);
            	ResultSet rs2 = statement4.executeQuery();
				
				while(rs2.next()){
									 
					String email = null;

 					
					if(rs2.getString(1)!= null){
						email = rs2.getString(1);
						EmpEmail =email; 
					}else {
						
						email = null;
					}
					
 					 

			 	}
           
				System.out.println(EmpEmail +" --> admin email id to sent email");
				System.out.println(Fullname +" --> Employee full name to sent email");
			 	
				SimpleSendEmail S1 = new SimpleSendEmail();
				S1.HtmlEmail6(EmpEmail,Fullname);

            
            }

			}	
			
			
			if(row > 0){

//					conn = DbServices.getConnection();
		         	String SELECT="SELECT LEAVE_ID, EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM,LEAVE_TO,"
		         			+ "DURATION, STATUS, DESCRIPTION from Leave_details where STATUS != 'Cancelled' and EMPLOYEE_ID = '"+employeeId1+"' ";
		         	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation  = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						

						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
			
							
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							
							description = rs.getString(10);
						}else {
							
							description = null;
						}
						
						String description1 = null;
						String email = null;
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status, description,description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("reject Done By Leave id"+returJson);
			return returJson;	
		}

	public String PostMethodForLeaveEmpId(String request) {
	List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
		         	String SELECT="SELECT L.LEAVE_ID, L.EMPLOYEE_ID, L.EMPLOYEE_NAME, L.DESIGNATION, L.LEAVE_TYPE, L.LEAVE_FROM,"
		         			+ "L.LEAVE_TO, L.DURATION, L.STATUS, L.DESCRIPTION, L.DESCRIPTION_1, E.EMPLOYEE_ID from leave_details L, employee_details E "
		         			+ "where E.EMPLOYEE_ID = L.EMPLOYEE_ID AND L.EMPLOYEE_ID='"+request+"'  ";
		         	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation  = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						String description1 = null;						

						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							description = rs.getString(10);
						}else {
							
							description = null;
						}
						
						if(rs.getString(11)!= null){
							 description1 = rs.getString(11);
						}else {
						description1 = null;
						}
						
						String email = null;
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status, description, description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("emp id list"+returJson);
			return returJson;
		}

	

	public String PostServiceForfilterAllEmployeeStatus(String request) {
	List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
		         	String SELECT="SELECT L.LEAVE_ID, L.EMPLOYEE_ID, L.EMPLOYEE_NAME, L.DESIGNATION, L.LEAVE_TYPE, L.LEAVE_FROM,"
		         			+ "L.LEAVE_TO, L.DURATION, L.STATUS, L.DESCRIPTION, L.DESCRIPTION_1, E.EMPLOYEE_ID from leave_details L, employee_details E "
		         			+ "where E.EMPLOYEE_ID = L.EMPLOYEE_ID AND L.STATUS='"+request+"'  ";
		         	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation  = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						String description1 = null;						

						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							description = rs.getString(10);
						}else {
							
							description = null;
						}
						
						if(rs.getString(11)!= null){
							 description1 = rs.getString(11);
						}else {
						description1 = null;
						}
						String email = null;
						
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status, description, description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("emp id list"+returJson);
			return returJson;
	}

	
	public String PostMethodForLeaveStatus(String request) {
	List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{ 
			
					conn = DbServices.getConnection();
		         	String SELECT="SELECT L.LEAVE_ID, L.EMPLOYEE_ID, L.EMPLOYEE_NAME, L.DESIGNATION, L.LEAVE_TYPE, L.LEAVE_FROM,"
		         			+ "L.LEAVE_TO, L.DURATION, L.STATUS,L.DESCRIPTION, L.DESCRIPTION_1, E.EMPLOYEE_ID from leave_details L, employee_details E "
		         			+ "where E.EMPLOYEE_ID = L.EMPLOYEE_ID AND L.EMPLOYEE_ID='"+request+"' AND L.STATUS != 'Cancelled' ";
		         	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation  = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						String description1 = null;
						
						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}
						if(rs.getString(10)!= null){
							
							description = rs.getString(10);
						}else {
							description = null;
						}

						if(rs.getString(11)!= null){
							description1 = rs.getString(11);
						}else {
						description1 = null;
						}
						
						String email = null;
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status, description, description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("emp id list"+returJson);
			return returJson;
	}

	public String PostMethodForGetLeaveTotal(String[] request) {
	 			return null;
	}

	public String PostServiceForApplyLeaveReschedule(RequestLeaveVO request) {
	List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		ResultSet rs1 = null;
 		String returJson = "";
 		int row = 0;
		PreparedStatement statement1 =null;
		PreparedStatement statement2 =null;			
		
		
		
		try{
			
			if(request != null){
				
//				System.out.println(request.getLeaveId()+"-----> leave id");
//				System.out.println(request.getEmployeeId()+"------> Employee id");
//				System.out.println("status -----> "+ request.getStatus());
//				System.out.println("duration is ---------> "+request.getDuration());
				
				
				String UPDATE="UPDATE leave_details SET LEAVE_TYPE = ?, LEAVE_FROM =? ,LEAVE_TO = ?, DURATION = ?, STATUS=? , DESCRIPTION = ? WHERE LEAVE_ID='"+request.getLeaveId()+"'";
		          conn = DbServices.getConnection();
		          statement1 = conn.prepareStatement(UPDATE);

		          statement1.setString(1,request.getLeaveType());
		          
		          
			         String FROM=request.getLeaveFrom();
			         
			         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						java.util.Date date1 = null;
						try {date1 = sdf.parse(FROM);
						} catch (ParseException e) {
							e.printStackTrace();}
						java.sql.Date sqldob = new java.sql.Date(date1.getTime());

				  statement1.setDate(2, (java.sql.Date) sqldob);

						
					    String TO=request.getLeaveTo();
				         
				         SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
							java.util.Date date2 = null;
							try {date2 = sdf1.parse(TO);
							} catch (ParseException e) {
								e.printStackTrace();}
							java.sql.Date sqldob1 = new java.sql.Date(date2.getTime());

				  statement1.setDate(3, (java.sql.Date) sqldob1);


		          
 		          statement1.setString(4, request.getDuration());
		          statement1.setString(5, "Pending");
		          statement1.setString(6, request.getDescription());

		          row = statement1.executeUpdate();
			}
			
			
			if(row>0){
            	String Fullname = "";   
            	String EmpEmail = " ";
            	
           	String SELECT="SELECT L.LEAVE_ID, L.EMPLOYEE_ID, L.EMPLOYEE_NAME, E.EMAIL_PRIMARY from leave_details L, employee_details E "
           			+ "where  L.EMPLOYEE_ID = E.EMPLOYEE_ID AND  L.EMPLOYEE_ID="+request.getEmployeeId()+"  ";
              	
           	PreparedStatement   statement3 = conn.prepareStatement(SELECT);
				rs1 = statement3.executeQuery();
				
				while(rs1.next()){
					
					String leaveId = null;
					 
					String employeeId = null;
					String name = null; 
					String designation  = null;
					String leaveType = null;
					String leaveFrom= null;
					String leaveTo = null;
					String duration= null;
					String status = null;
					String description = null;
					String description1 = null;

					String email = null;


					if(rs1.getString(1) != null){
						
						leaveId = rs1.getString(1);
					}else {
						
						leaveId =null;
					}
					
					
					if(rs1.getString(2) != null){
						
						employeeId = rs1.getString(2);
					}else {
						
						employeeId =null;
					}
					
					if(rs1.getString(3)!= null){
						name= rs1.getString(3);
						Fullname = name ; 
					}else{
						
						name = null;
					}
					 
					if(rs1.getString(4)!= null){
						email = rs1.getString(4);
						EmpEmail =email; 
					}else {
						
						email = null;
					}
			 	}
           
				System.out.println(EmpEmail +" email id to sent email");
				System.out.println(Fullname +"full name to sent email");
				SimpleSendEmail S1 = new SimpleSendEmail();
				S1.HtmlEmail3(EmpEmail,Fullname);

            
            }
            
			if(row > 0){


//					conn = DbServices.getConnection();
		         	String SELECT="SELECT LEAVE_ID, EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM,LEAVE_TO, DURATION, STATUS, DESCRIPTION, DESCRIPTION_1 from leave_details  where EMPLOYEE_ID = '"+request.getEmployeeId()+"' AND STATUS != 'Cancelled' ";
		         	statement2 = conn.prepareStatement(SELECT);
//		         	String empid=request.getEmployeeId();
//		         	statement2.setString(1, empid);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						String description1 = null;
						

						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
			
							
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							
							description = rs.getString(10);
						}else {
							
							description =null;
						}
						if(rs.getString(11)!= null){
							
							description1 = rs.getString(11);
						}else {
							
						 description1 = null;
						
						}
						String email = null;
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status,description,description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("EDITED Done By Leave id"+returJson);
			return returJson;
	}

	public String PostServiceForCancelLeaveId(String[] request) {
 		
		System.out.println("im cancel method");
		
	List<ResponseLeaveVO> finalList = new ArrayList<ResponseLeaveVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
 		int row = 0;
		PreparedStatement statement1 =null;
		PreparedStatement statement2 =null;
		
		
		try{
			
			if(request != null){
				
// 				 System.out.println("req 0"+request[0]);
// 				 System.out.println("req 1 "+ request[1]);
				String UPDATE="UPDATE leave_details SET STATUS=? WHERE LEAVE_ID='"+request[0]+"'";
		          conn = DbServices.getConnection();
		          statement1 = conn.prepareStatement(UPDATE);

		          statement1.setString(1, "Cancelled");

		          row = statement1.executeUpdate();
			}
			
			if(row>0){
            	String Fullname = "";   
            	String EmpEmail = " ";
            	
           	String SELECT="SELECT L.LEAVE_ID, L.EMPLOYEE_ID, L.EMPLOYEE_NAME, E.EMAIL_PRIMARY from leave_details L, employee_details E "
           			+ "where  L.EMPLOYEE_ID = E.EMPLOYEE_ID AND  L.EMPLOYEE_ID="+request[1]+"  ";
              	
           	PreparedStatement   statement3 = conn.prepareStatement(SELECT);
				ResultSet rs1 = statement3.executeQuery();
				
				while(rs1.next()){
					
					String leaveId = null;
					 
					String employeeId = null;
					String name = null; 
					String designation  = null;
					String leaveType = null;
					String leaveFrom= null;
					String leaveTo = null;
					String duration= null;
					String status = null;
					String description = null;
					String description1 = null;

					String email = null;


					if(rs1.getString(1) != null){
						
						leaveId = rs1.getString(1);
					}else {
						
						leaveId =null;
					}
					
					
					if(rs1.getString(2) != null){
						
						employeeId = rs1.getString(2);
					}else {
						
						employeeId =null;
					}
					
					if(rs1.getString(3)!= null){
						name= rs1.getString(3);
						Fullname = name ; 
					}else{
						
						name = null;
					}
					 
					if(rs1.getString(4)!= null){
						email = rs1.getString(4);
						EmpEmail =email; 
					}else {
						
						email = null;
					}
			 	}
           
				System.out.println(EmpEmail +" email id to sent email");
				System.out.println(Fullname +"full name to sent email");
				SimpleSendEmail S1 = new SimpleSendEmail();
				S1.HtmlEmail4(EmpEmail,Fullname);

            
            }

			
			if(row > 0){

//					conn = DbServices.getConnection();
		         	String SELECT="SELECT LEAVE_ID, EMPLOYEE_ID, EMPLOYEE_NAME, DESIGNATION, LEAVE_TYPE, LEAVE_FROM,LEAVE_TO, DURATION, STATUS, DESCRIPTION, DESCRIPTION_1 from leave_details  where EMPLOYEE_ID = '"+request[1]+"' AND STATUS != 'Cancelled' ";
		         	statement2 = conn.prepareStatement(SELECT);
//		         	String empid=request.getEmployeeId();
//		         	statement2.setString(1, empid);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						String leaveId = null;
						 
						String employeeId = null;
						String name = null; 
						String designation = null;
						String leaveType = null;
						String leaveFrom= null;
						String leaveTo = null;
						String duration= null;
						String status = null;
						String description = null;
						String description1 = null;
						

						if(rs.getString(1) != null){
							
							leaveId = rs.getString(1);
						}else {
							
							leaveId =null;
						}
						
						
						if(rs.getString(2) != null){
							
							employeeId = rs.getString(2);
						}else {
							
							employeeId =null;
						}
						
						if(rs.getString(3)!= null){
							name= rs.getString(3);
							
						}else{
							
							name = null;
						}
						
						if(rs.getString(4)!= null){
							
							designation = rs.getString(4);
						}else{
							designation= null;
						}

						if(rs.getString(5)!= null){
							
							leaveType = rs.getString(5);
						}else{
							leaveType= null;
						}

						if(rs.getString(6)!= null){
							
							leaveFrom = rs.getString(6);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveFrom));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveFrom = sdf2.format(cal.getTime());
			
							
						}else{
							leaveFrom= null;
						}

						if(rs.getString(7)!= null){
							
							leaveTo = rs.getString(7);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(leaveTo));
							SimpleDateFormat sdf2 = new SimpleDateFormat(
									"MM/dd/yyyy");
							leaveTo = sdf2.format(cal.getTime());
						
							
						}else{
							leaveTo= null;
						}

						
						if(rs.getString(8)!= null){
							
							duration = rs.getString(8);
						}else{
							duration= null;
						}
						
						if(rs.getString(9)!= null){
							
							status = rs.getString(9);
						}else{
							status= null;
						}

						if(rs.getString(10)!= null){
							
							description = rs.getString(10);
						}else {
							
							description =null;
						}
						if(rs.getString(11)!= null){
							
							description1 = rs.getString(11);
						}else {
							
						 description1 = null;
						
						}
						String email = null;
						ResponseLeaveVO mObj = new  ResponseLeaveVO(leaveId, employeeId,name,designation,
								leaveType,leaveFrom,leaveTo,duration,status,description,description1,email);
						
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("canceled Done By Leave id"+returJson);
			return returJson;

	}

	public String PostServiceForLeavechangeStatus(String request) {
		
		System.out.println("date "+request);
		
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date1 = null;
			try {date1 = sdf.parse(request);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqldob = new java.sql.Date(date1.getTime());

		System.out.println("format date -->"+sqldob);
		return null;
	}

	 
}
