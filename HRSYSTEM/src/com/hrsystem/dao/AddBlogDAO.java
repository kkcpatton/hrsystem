package com.hrsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.hrsystem.db.DbServices;
import com.hrsystem.vo.RequestBlogCreateVO;
import com.hrsystem.vo.RequestBlogPostVO;
import com.hrsystem.vo.ResponseBlogPostVO;
import com.hrsystem.vo.ResponseBlogVO;


public class AddBlogDAO {

	public String PostServiceForAddBlog(RequestBlogCreateVO request) {

		
List<ResponseBlogVO> finalList = new ArrayList<ResponseBlogVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		
		
		//System.out.println("name1"+name);
		try{
			
			if(request.getBlogId() == null){
	 String INSERT="INSERT INTO blog_table (BLOG_HEADING, BLOG_CONTENT,BLOG_DATE,BLOG_EMP_ID,BLOG_FLAG) values (?,?,?,?,?)";
            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getBlogHeading());
            statement1.setString(2, request.getBlogContent());
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            
            statement1.setString(3,dateFormat.format(date));
            statement1.setString(4, request.getBlogEmpId());
            
            statement1.setString(5,"Active");
           
         
            row = statement1.executeUpdate();
 		} 	else {
 			String UPDATE="UPDATE blog_table SET BLOG_HEADING =?, BLOG_CONTENT =?,BLOG_DATE =?, BLOG_EMP_ID =?, BLOG_FLAG =?  WHERE BLOG_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

            statement1.setString(1, request.getBlogHeading());
            statement1.setString(2, request.getBlogContent());
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            
            statement1.setString(3,dateFormat.format(date));
            statement1.setString(4, request.getBlogEmpId());
            
            statement1.setString(5,"Active");
            
            
            
            statement1.setString(6,request.getBlogId());
            
            
            row = statement1.executeUpdate();

 		}
		
		    if (row > 0) {
		    	String SELECT="SELECT BLOG_ID, BLOG_HEADING, BLOG_CONTENT,BLOG_DATE,BLOG_EMP_ID,BLOG_FLAG from blog_table Where BLOG_FLAG='Active' ORDER BY BLOG_ID DESC ";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String blogId;
					 String blogHeading;
					 String blogContent;
					 String blogDate;
					 String blogEmpId;
					 String blogFlag;
					 
					 
					if(rs.getString(1) != null){
						
						blogId=rs.getString(1);
					}else {
						
						blogId= null;
					}
					
					if(rs.getString(2) != null){
						
						blogHeading=rs.getString(2);
					}else {
						
						blogHeading= "";
					}
					if(rs.getString(3) != null){
						blogContent= rs.getString(3);
 
						
					}else {
						
						blogContent= "";
					}
					
					if(rs.getString(4) != null){
						blogDate= rs.getString(4);
 
						
					}else {
						
						blogDate= "";
					}
					if(rs.getString(5) != null){
						blogEmpId= rs.getString(5);
 
						
					}else {
						
						blogEmpId= "";
					}
					if(rs.getString(6) != null){
						blogFlag= rs.getString(6);
 
						
					}else {
						
						blogFlag= "";
					}
					
					
					ResponseBlogVO mObj = new ResponseBlogVO(blogId,blogHeading,blogContent,blogDate,blogEmpId,blogFlag);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();
					
				}
		    }
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
	
		
		
		
		
	}

	public String GetServiceForGetBlog() {

		
List<ResponseBlogVO> finalList = new ArrayList<ResponseBlogVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
		PreparedStatement statement2 =null;
		
		
		try{
				conn = DbServices.getConnection();
			
		    	String SELECT="SELECT B.BLOG_ID, B.BLOG_HEADING, B.BLOG_CONTENT,B.BLOG_DATE,B.BLOG_FLAG,E.FIRST_NAME,E.LAST_NAME from blog_table B,employee_details E WHERE (B.BLOG_EMP_ID=E.EMPLOYEE_ID AND BLOG_FLAG='Active') ORDER BY B.BLOG_ID DESC ";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				
				while(rs.next()){
					
					 String blogId;
					 String blogHeading;
					 String blogContent;
					 String blogDate;
					 String blogFlag;
					 String firstname="";
					 String lastname="";
					 
					if(rs.getString(1) != null){
						
						blogId=rs.getString(1);
					}else {
						
						blogId= null;
					}
					
					if(rs.getString(2) != null){
						
						blogHeading=rs.getString(2);
					}else {
						
						blogHeading= "";
					}
					if(rs.getString(3) != null){
						blogContent= rs.getString(3);
 
						
					}else {
						
						blogContent= "";
					}
					
					if(rs.getString(4) != null){
						blogDate= rs.getString(4);
 
						
					}else {
						
						blogDate= "";
					}
					
					if(rs.getString(5) != null){
						blogFlag= rs.getString(5);
 
						
					}else {
						
						blogFlag= "";
					}
					if(rs.getString(6) != null){
						firstname= rs.getString(6);
 
						
					}else {
						
						firstname= "";
					}
					if(rs.getString(7) != null){
						lastname= rs.getString(7);
 
						
					}else {
						
						lastname= "";
					}
					String name=firstname+" "+lastname;
					
					ResponseBlogVO mObj = new ResponseBlogVO(blogId,blogHeading,blogContent,blogDate,name,blogFlag);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		   
	

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
	
		
		
		
		
	}

	public String GetServiceForGetBlogPost(RequestBlogPostVO request) {


		
List<ResponseBlogPostVO> finalList = new ArrayList<ResponseBlogPostVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		
		
		//System.out.println("name1"+name);
		try{
			
			if(request.getPostId() == ""){
	 String INSERT="INSERT INTO post_table (BLOG_ID, POST_COMMENT,POST_EMP_ID,POST_DATE,POST_FLAG) values (?,?,?,?,?)";
            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getBlogId());
            statement1.setString(2, request.getPostComment());
            statement1.setString(3, request.getPostEmpId());
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            
            statement1.setString(4,dateFormat.format(date));
            
            
            statement1.setString(5,"Active");
           
         
            row = statement1.executeUpdate();
 		} 	else {
 			String UPDATE="UPDATE post_table SET BLOG_ID =?, POST_COMMENT =?,POST_EMP_ID =?, POST_DATE =?, POST_FLAG =?  WHERE POST_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

            statement1.setString(1, request.getBlogId());
            statement1.setString(2, request.getPostComment());
            statement1.setString(3, request.getPostEmpId());
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            
            statement1.setString(4,dateFormat.format(date));
            
            
            statement1.setString(5,"Active");
            
            
            
            statement1.setString(6,request.getPostId());
            
            
            row = statement1.executeUpdate();

 		}
		
		    if (row > 0) {
		    	String SELECT="SELECT P.POST_ID, P.BLOG_ID, P.POST_COMMENT,P.POST_EMP_ID,P.POST_DATE,E.EMP_PHOTO,P.POST_FLAG,E.FIRST_NAME,E.LAST_NAME from post_table P,employee_details E Where P.POST_EMP_ID=E.EMPLOYEE_ID AND POST_FLAG='Active'";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String postId;
					 String blogId;
					 String postComment;
					 String postEmpId;
					 String postEmpImage;
					 String postDate;
					 String postFlag;
					 String firstName;
					 String lastName;
					 
					if(rs.getString(1) != null){
						
						postId=rs.getString(1);
					}else {
						
						postId= null;
					}
					
					if(rs.getString(2) != null){
						
						blogId=rs.getString(2);
					}else {
						
						blogId= "";
					}
					if(rs.getString(3) != null){
						postComment= rs.getString(3);
 
						
					}else {
						
						postComment= "";
					}
					
					if(rs.getString(4) != null){
						postEmpId= rs.getString(4);
 
						
					}else {
						
						postEmpId= "";
					}
					if(rs.getString(5) != null){
						postDate= rs.getString(5);
 
						
					}else {
						
						postDate= "";
					}
					
					
					if(rs.getString(6) != null){
						postEmpImage= rs.getString(6);
 
						
					}else {
						
						postEmpImage= "";
					}
					if(rs.getString(7) != null){
						postFlag= rs.getString(7);
 
						
					}else {
						
						postFlag= "";
					}
					if(rs.getString(8) != null){
						firstName= rs.getString(8);
 
						
					}else {
						
						firstName= "";
					}
					if(rs.getString(9) != null){
						lastName= rs.getString(9);
 
						
					}else {
						
						lastName= "";
					}
					String name=firstName+" "+lastName;
					ResponseBlogPostVO mObj = new ResponseBlogPostVO(postId,blogId,postComment,name,postEmpImage,postDate,postFlag);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    }
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
	
		
		
		
		
	
	}

	public String GetServiceForGetBlogPostdisplay() {


		
List<ResponseBlogPostVO> finalList = new ArrayList<ResponseBlogPostVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
		PreparedStatement statement2 =null;
		
		
		//System.out.println("name1"+name);
		try{
				conn = DbServices.getConnection();
		    	String SELECT="SELECT P.POST_ID, P.BLOG_ID, P.POST_COMMENT,P.POST_EMP_ID,P.POST_DATE,E.EMP_PHOTO,P.POST_FLAG,E.FIRST_NAME,E.LAST_NAME from post_table P,employee_details E Where P.POST_EMP_ID=E.EMPLOYEE_ID AND POST_FLAG='Active'";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String postId;
					 String blogId;
					 String postComment;
					 String postEmpId;
					 String postEmpImage;
					 String postDate;
					 String postFlag;
					 String firstName;
					 String lastName;
					 
					if(rs.getString(1) != null){
						
						postId=rs.getString(1);
					}else {
						
						postId= null;
					}
					
					if(rs.getString(2) != null){
						
						blogId=rs.getString(2);
					}else {
						
						blogId= "";
					}
					if(rs.getString(3) != null){
						postComment= rs.getString(3);
 
						
					}else {
						
						postComment= "";
					}
					
					if(rs.getString(4) != null){
						postEmpId= rs.getString(4);
 
						
					}else {
						
						postEmpId= "";
					}
					if(rs.getString(5) != null){
						postDate= rs.getString(5);
 
						
					}else {
						
						postDate= "";
					}
					
					
					if(rs.getString(6) != null){
						postEmpImage= rs.getString(6);
 
						
					}else {
						
						postEmpImage= "";
					}
					if(rs.getString(7) != null){
						postFlag= rs.getString(7);
 
						
					}else {
						
						postFlag= "";
					}
					if(rs.getString(8) != null){
						firstName= rs.getString(8);
 
						
					}else {
						
						firstName= "";
					}
					if(rs.getString(9) != null){
						lastName= rs.getString(9);
 
						
					}else {
						
						lastName= "";
					}
					String name=firstName+" "+lastName;
					
					ResponseBlogPostVO mObj = new ResponseBlogPostVO(postId,blogId,postComment,name,postEmpImage,postDate,postFlag);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    
	
	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
	
		
		
		
		
	
	}

	public String PostServiceForDeleteBlogPost(String request) {


		
List<ResponseBlogVO> finalList = new ArrayList<ResponseBlogVO>();
		
Connection conn = null;
ResultSet rs = null;

String returJson = "";
PreparedStatement statement1=null;
PreparedStatement statement2 =null;
try{
String DELETE_BLOG="delete from blog_table where BLOG_ID= ?";
        conn = DbServices.getConnection();
     statement1 = conn.prepareStatement(DELETE_BLOG);
    statement1.setString(1, request);
      
    int row = statement1.executeUpdate();
  
    if (row > 0) {
			
		    	String SELECT="SELECT B.BLOG_ID, B.BLOG_HEADING, B.BLOG_CONTENT,B.BLOG_DATE,B.BLOG_FLAG,E.FIRST_NAME,E.LAST_NAME from blog_table B,employee_details E WHERE (B.BLOG_EMP_ID=E.EMPLOYEE_ID AND BLOG_FLAG='Active') ORDER BY B.BLOG_ID DESC ";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				
				while(rs.next()){
					
					 String blogId;
					 String blogHeading;
					 String blogContent;
					 String blogDate;
					 String blogFlag;
					 String firstname="";
					 String lastname="";
					 
					if(rs.getString(1) != null){
						
						blogId=rs.getString(1);
					}else {
						
						blogId= null;
					}
					
					if(rs.getString(2) != null){
						
						blogHeading=rs.getString(2);
					}else {
						
						blogHeading= "";
					}
					if(rs.getString(3) != null){
						blogContent= rs.getString(3);
 
						
					}else {
						
						blogContent= "";
					}
					
					if(rs.getString(4) != null){
						blogDate= rs.getString(4);
 
						
					}else {
						
						blogDate= "";
					}
					
					if(rs.getString(5) != null){
						blogFlag= rs.getString(5);
 
						
					}else {
						
						blogFlag= "";
					}
					if(rs.getString(6) != null){
						firstname= rs.getString(6);
 
						
					}else {
						
						firstname= "";
					}
					if(rs.getString(7) != null){
						lastname= rs.getString(7);
 
						
					}else {
						
						lastname= "";
					}
					String name=firstname+" "+lastname;
					
					ResponseBlogVO mObj = new ResponseBlogVO(blogId,blogHeading,blogContent,blogDate,name,blogFlag);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		   
    }

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
	
		
		
		
		
	
	}
	
	
	
	
	
}
