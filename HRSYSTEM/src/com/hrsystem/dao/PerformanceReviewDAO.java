package com.hrsystem.dao;

import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.google.gson.Gson;
import com.hrsystem.db.DbServices;
import com.hrsystem.vo.RequestPerformanceVO;
import com.hrsystem.vo.RequestTaskActivityVO;
import com.hrsystem.vo.RequestTaskAssignedVO;
import com.hrsystem.vo.ResponseEmployeeLoginVO;
import com.hrsystem.vo.ResponsePerformanceVO;
import com.hrsystem.vo.ResponseTaskActivityVO;
import com.hrsystem.vo.ResponseTaskAssignedVO;

public class PerformanceReviewDAO {

	public String PostServiceForAddTask(RequestTaskAssignedVO request) {
	
List<ResponseTaskAssignedVO> finalList = new ArrayList<ResponseTaskAssignedVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		
		
		//System.out.println("name1"+name);
		try{
			
			if(request.getTaskId() == null){
	 String INSERT="INSERT INTO task_assign_pr (EMPLOYEE_ID, TASK_ASSIGNED,TASK_WEIGHT) values (?,?,?)";
            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getEmployeeId());
            statement1.setString(2, request.getTaskAssigned());
            statement1.setString(3, request.getTaskWeight());
            
          
            row = statement1.executeUpdate();
 		} 	else {
 			String UPDATE="UPDATE task_assign_pr SET EMPLOYEE_ID =?, TASK_ASSIGNED =?,TASK_WEIGHT =?   WHERE TASK_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

            statement1.setString(1, request.getEmployeeId());
            statement1.setString(2, request.getTaskAssigned());
            statement1.setString(3, request.getTaskWeight());
            statement1.setString(4, request.getTaskId());
            
            
            
            row = statement1.executeUpdate();

 		}
		
		    if (row > 0) {
		    	String SELECT="SELECT T.TASK_ID, T.EMPLOYEE_ID, T.TASK_ASSIGNED,T.TASK_WEIGHT,E.FIRST_NAME,E.LAST_NAME,T.WEIGHT_ACHIEVED from task_assign_pr T,employee_details E WHERE T.EMPLOYEE_ID=E.EMPLOYEE_ID";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					String taskId;
					
					 String employeeId;
					 String taskAssigned;
					 String taskWeight;
					 String firstname;
					 String lastname;
					 String weightAchieved;
					 
					if(rs.getString(1) != null){
						
						taskId=rs.getString(1);
					}else {
						
						taskId= null;
					}
					
					if(rs.getString(2) != null){
						
						employeeId=rs.getString(2);
					}else {
						
						employeeId= "";
					}
					if(rs.getString(3) != null){
						taskAssigned= rs.getString(3);
 
						
					}else {
						
						taskAssigned= "";
					}
					if(rs.getString(4) != null){
						taskWeight= rs.getString(4);
 
						
					}else {
						
						taskWeight= "";
					}
					if(rs.getString(5) != null){
						firstname= rs.getString(5);
 
						
					}else {
						
						firstname= "";
					}
					if(rs.getString(6) != null){
						lastname= rs.getString(6);
 
						
					}else {
						
						lastname= "";
					}
					if(rs.getString(7) != null){
						weightAchieved= rs.getString(7);
 
						
					}else {
						
						weightAchieved= "";
					}
					String name=firstname+" "+lastname;
					ResponseTaskAssignedVO mObj = new ResponseTaskAssignedVO(
							taskId,employeeId,taskAssigned,taskWeight,name,weightAchieved);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    }
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("task ... "+returJson);
		return returJson;
	}
	
	

	public String PostServiceForPerformance(RequestPerformanceVO request) {
	
List<ResponsePerformanceVO> finalList = new ArrayList<ResponsePerformanceVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		
		
		
		try{
			
			if(request.getPerformanceId() == null){
	 String INSERT="INSERT INTO performance_review (JOB_SKILL,NEW_SKILL,RESOURSE_USES,ASSIGN_RESPONSIBILITIES,MEET_ATTENDANCE,LISTEN_TO_DIRECTION,RESPONSIBILITY,COMMITMENTS,PROBLEM_SOLVING,SUGGESTION_IMPROVEMENT,IDEA_AND_SOLUTION,MEET_CHALLENGES,INNOVATIVE_THINKING,EMPLOYEE_ID,REVIEWED_BY,REVIEWED_DATE,REVIEW_PERIOD,COMMENTS,REVIEW_PERIOD_TO,REVIEW_YEAR,REVIEW_QUARTER) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getJobskill());
            statement1.setString(2, request.getNewskill());
            statement1.setString(3, request.getResourseUses());
            statement1.setString(4, request.getAssignResponsibilities());
            
            statement1.setString(5, request.getMeetAttendance());
            statement1.setString(6, request.getListenToDirection());
            statement1.setString(7, request.getResponsibility());
            statement1.setString(8, request.getCommitments());
            
            statement1.setString(9, request.getProblemSolving());
            statement1.setString(10, request.getSuggestionImprovement());
            statement1.setString(11, request.getIdeaAndSolution());
            statement1.setString(12, request.getMeetChallenges());
            
            statement1.setString(13, request.getInnovativeThinking());
            statement1.setString(14, request.getEmployeeId());
            statement1.setString(15, request.getReviewedBy());
           
            
            
            if(request.getReviewedDate()==null){
            	statement1.setDate(16, null);
            	
            }
            else{
            
            String reviewDate=request.getReviewedDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf.parse(reviewDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlreviewDate = new java.sql.Date(date.getTime());

			
			
			statement1.setDate(16, (java.sql.Date) sqlreviewDate);
            }
            
            
            
            if(request.getReviewPeriod()==null){
            	statement1.setDate(17, null);
            }else{
            String reviewPeriod= request.getReviewPeriod();

            
            
        	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    		java.util.Date date1 = null;
    		try {date1 = sdf1.parse(reviewPeriod);
    		} catch (ParseException e) {
    			e.printStackTrace();}
    		java.sql.Date sqlreviewPeriod = new java.sql.Date(date1.getTime());

    		statement1.setDate(17, (java.sql.Date) sqlreviewPeriod);
    		
            }
            statement1.setString(18, request.getComments());
           
            
            if(request.getReviewPeriodTo()==null){
            	statement1.setDate(19, null);
            }else{
            String reviewPeriodTo= request.getReviewPeriodTo();

            
            
        	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    		java.util.Date date1 = null;
    		try {date1 = sdf1.parse(reviewPeriodTo);
    		} catch (ParseException e) {
    			e.printStackTrace();}
    		java.sql.Date sqlreviewPeriodTo = new java.sql.Date(date1.getTime());
            
    		statement1.setDate(19, (java.sql.Date) sqlreviewPeriodTo);
            }
    		statement1.setString(20, request.getYear());
    		statement1.setString(21, request.getQuarter());
            
            
           
            row = statement1.executeUpdate();
 		} 	else {
 			String UPDATE="UPDATE performance_review SET JOB_SKILL=?,NEW_SKILL=?,RESOURSE_USES=?,ASSIGN_RESPONSIBILITIES=?,MEET_ATTENDANCE=?,LISTEN_TO_DIRECTION=?,RESPONSIBILITY=?,COMMITMENTS=?,PROBLEM_SOLVING=?,SUGGESTION_IMPROVEMENT=?,IDEA_AND_SOLUTION=?,MEET_CHALLENGES=?,INNOVATIVE_THINKING=?,EMPLOYEE_ID=?,REVIEWED_BY=?,REVIEWED_DATE=?,REVIEW_PERIOD=?,COMMENTS=?,REVIEW_PERIOD_TO=?,REVIEW_YEAR=?,REVIEW_QUARTER=?  WHERE PERFORMANCE_ID =?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

            statement1.setString(1, request.getJobskill());
            statement1.setString(2, request.getNewskill());
            statement1.setString(3, request.getResourseUses());
            statement1.setString(4, request.getAssignResponsibilities());
            
            statement1.setString(5, request.getMeetAttendance());
            statement1.setString(6, request.getListenToDirection());
            statement1.setString(7, request.getResponsibility());
            statement1.setString(8, request.getCommitments());
            
            statement1.setString(9, request.getProblemSolving());
            statement1.setString(10, request.getSuggestionImprovement());
            statement1.setString(11, request.getIdeaAndSolution());
            statement1.setString(12, request.getMeetChallenges());
            
            statement1.setString(13, request.getInnovativeThinking());
            statement1.setString(14, request.getEmployeeId());
            statement1.setString(15, request.getReviewedBy());
           
            if(request.getReviewedDate()==null){
            	statement1.setDate(16, null);
            	
            }
            else{
            
            String reviewDate=request.getReviewedDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf.parse(reviewDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlreviewDate = new java.sql.Date(date.getTime());

			
			
			statement1.setDate(16, (java.sql.Date) sqlreviewDate);
            }
            
            
            if(request.getReviewPeriod()==null){
            	statement1.setDate(17, null);
            }else{
            String reviewPeriod= request.getReviewPeriod();

            
            
        	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    		java.util.Date date1 = null;
    		try {date1 = sdf1.parse(reviewPeriod);
    		} catch (ParseException e) {
    			e.printStackTrace();}
    		java.sql.Date sqlreviewPeriod = new java.sql.Date(date1.getTime());

    		statement1.setDate(17, (java.sql.Date) sqlreviewPeriod);
    		
            }
            
            statement1.setString(18, request.getComments());
            
            
            
            
            
            if(request.getReviewPeriodTo()==null){
            	statement1.setDate(19, null);
            }else{
            String reviewPeriodTo= request.getReviewPeriod();

            
            
        	SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    		java.util.Date date1 = null;
    		try {date1 = sdf1.parse(reviewPeriodTo);
    		} catch (ParseException e) {
    			e.printStackTrace();}
    		java.sql.Date sqlreviewPeriodTo = new java.sql.Date(date1.getTime());

    		statement1.setDate(19, (java.sql.Date) sqlreviewPeriodTo);
    		
            }
            statement1.setString(20, request.getYear());
    		statement1.setString(21, request.getQuarter());
            statement1.setString(22, request.getPerformanceId());
            row = statement1.executeUpdate();
            
 		}
		
		    if (row > 0) {
		    	String SELECT="SELECT P.PERFORMANCE_ID,P.JOB_SKILL,P.NEW_SKILL,P.RESOURSE_USES,P.ASSIGN_RESPONSIBILITIES,P.MEET_ATTENDANCE,P.LISTEN_TO_DIRECTION,P.RESPONSIBILITY,P.COMMITMENTS,P.PROBLEM_SOLVING,P.SUGGESTION_IMPROVEMENT,P.IDEA_AND_SOLUTION,P.MEET_CHALLENGES,P.INNOVATIVE_THINKING,P.EMPLOYEE_ID,P.REVIEWED_BY,P.REVIEWED_DATE,P.REVIEW_PERIOD,P.COMMENTS,E.FIRST_NAME,E.LAST_NAME,P.REVIEW_PERIOD_TO,P.EMPLOYEE_SIGNATURE,P.ADMIN_SIGNATURE,P.REVIEW_YEAR,P.REVIEW_QUARTER from performance_review P,employee_details E WHERE P.EMPLOYEE_ID=E.EMPLOYEE_ID AND P.EMPLOYEE_ID='"+request.getEmployeeId()+"'";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String performanceId;
					 
					 String jobskill;
					 String newskill;
					 String resourseUses;
					 
					 String assignResponsibilities;
					 String meetAttendance;
					 String listenToDirection;
					 String responsibility;
					 
					 
					 String commitments;
					 String problemSolving;
					 String suggestionImprovement;
					 String ideaAndSolution;
					 
					 String meetChallenges;
					 String innovativeThinking;
					 String employeeId;
					 String reviewedBy;
					 
					 String reviewedDate;
					 String reviewPeriod;
					 String comments;
					 String totalWeight = "";
					 String firstname="";
					 String lastname="";
					 String reviewPeriodTo;
					 String employeeSignature;
					 String adminSignature;
					 String review_year="";
					 String review_quarter="";
					 
					if(rs.getString(1) != null){
						
						performanceId=rs.getString(1);
					}else {
						
						performanceId= null;
					}
					
					if(rs.getString(2) != null){
						
						jobskill=rs.getString(2);
					}else {
						
						jobskill= "0";
					}
					if(rs.getString(3) != null){
						
						newskill=rs.getString(3);
					}else {
						
						newskill= "0";
					}
					
					if(rs.getString(4) != null){
						
						resourseUses=rs.getString(4);
					}else {
						
						resourseUses= "0";
					}
					
					
					if(rs.getString(5) != null){
						
						assignResponsibilities=rs.getString(5);
					}else {
						
						assignResponsibilities= "0";
					}
					
					if(rs.getString(6) != null){
						
						meetAttendance=rs.getString(6);
					}else {
						
						meetAttendance= "0";
					}
					if(rs.getString(7) != null){
						
						listenToDirection=rs.getString(7);
					}else {
						
						listenToDirection= "0";
					}
					
					if(rs.getString(8) != null){
						
						responsibility=rs.getString(8);
					}else {
						
						responsibility= "0";
					}
					
					
					
					
					if(rs.getString(9) != null){
						
						commitments=rs.getString(9);
					}else {
						
						commitments= "0";
					}
					
					if(rs.getString(10) != null){
						
						problemSolving=rs.getString(10);
					}else {
						
						problemSolving= "0";
					}
					if(rs.getString(11) != null){
						
						suggestionImprovement=rs.getString(11);
					}else {
						
						suggestionImprovement= "0";
					}
					
					if(rs.getString(12) != null){
						
						ideaAndSolution=rs.getString(12);
					}else {
						
						ideaAndSolution= "0";
					}
					
					
					

					if(rs.getString(13) != null){
						
						meetChallenges=rs.getString(13);
					}else {
						
						meetChallenges= "0";
					}
					
					if(rs.getString(14) != null){
						
						innovativeThinking=rs.getString(14);
					}else {
						
						innovativeThinking= "0";
					}
					if(rs.getString(15) != null){
						
						employeeId=rs.getString(15);
					}else {
						
						employeeId= null;
					}
					
					if(rs.getString(16) != null){
						
						reviewedBy=rs.getString(16);
					}else {
						
						reviewedBy= null;
					}
					
					
					if(rs.getString(17) != null){
						reviewedDate= rs.getString(17);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(reviewedDate));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						reviewedDate = sdf.format(cal.getTime());
						
					}else {
						
						reviewedDate= null;
					}
					if(rs.getString(18) != null){
						reviewPeriod= rs.getString(18);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(reviewPeriod));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						reviewPeriod = sdf.format(cal.getTime());
						
					}else {
						
						reviewPeriod= null;
					}
					
					
					
					if(rs.getString(19) != null){
						
						comments=rs.getString(19);
					}else {
						
						comments= null;
					}
					if(rs.getString(20) != null){
						
						firstname=rs.getString(20);
					}else {
						
						firstname= "";
					}
					if(rs.getString(21) != null){
						
						lastname=rs.getString(21);
					}else {
						
						lastname= "";
					}
					String name=firstname+" "+lastname;
					if(rs.getString(22) != null){
						reviewPeriodTo= rs.getString(22);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(reviewPeriodTo));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						reviewPeriodTo = sdf.format(cal.getTime());
						
					}else {
						
						reviewPeriodTo= null;
					}
					
					if(rs.getBlob(23)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(23);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						employeeSignature=str;
					}else {
						
						employeeSignature= null;
					}
					if(rs.getBlob(24)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(24);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						adminSignature=str;
					}else {
						
						adminSignature= null;
					}
					int total=0;
					 total =Integer.parseInt(jobskill)+Integer.parseInt(newskill)+
							Integer.parseInt(resourseUses)+Integer.parseInt(assignResponsibilities)+
							Integer.parseInt(meetAttendance)+Integer.parseInt(listenToDirection)+
							Integer.parseInt(responsibility)+Integer.parseInt(commitments)+
							
							Integer.parseInt(problemSolving)+Integer.parseInt(suggestionImprovement)+
							Integer.parseInt(ideaAndSolution)+Integer.parseInt(meetChallenges)+
							Integer.parseInt(innovativeThinking);
					
					System.out.println("total .. "+total);
					float tot1=0;
					float tot=0;
					tot1=total;
					tot= (tot1 / 65)*50;
				totalWeight=Float.toString(tot);  
					
					if(rs.getString(25) != null){
						
						review_year=rs.getString(25);
					}else {
						
						review_year= "";
					}
					if(rs.getString(26) != null){
						
						review_quarter=rs.getString(26);
					}else {
						
						review_quarter= "";
					}
					
					ResponsePerformanceVO mObj = new ResponsePerformanceVO(
							 performanceId,	  jobskill,	  newskill,	  resourseUses,				 
							  assignResponsibilities,  meetAttendance,  listenToDirection, responsibility,
							   commitments,  problemSolving, suggestionImprovement,  ideaAndSolution,
							  meetChallenges, innovativeThinking, employeeId,  reviewedBy,
							  reviewedDate, reviewPeriod, comments,totalWeight,name,reviewPeriodTo,employeeSignature,adminSignature,review_year,review_quarter);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    }
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("Performance  .. "+returJson);
		return returJson;
	}



	public String PostServiceForTaskList(String request) {
List<ResponseTaskAssignedVO> finalList = new ArrayList<ResponseTaskAssignedVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
	
		
		PreparedStatement statement2 =null;
		try{
				
		String SELECT="SELECT T.TASK_ID,T.EMPLOYEE_ID,T.TASK_ASSIGNED,T.TASK_WEIGHT,E.FIRST_NAME,E.LAST_NAME,T.WEIGHT_ACHIEVED from task_assign_pr T,employee_details E WHERE  T.EMPLOYEE_ID='"+ request+"' AND T.EMPLOYEE_ID=E.EMPLOYEE_ID";
		conn = DbServices.getConnection();
		statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			 String taskId;
			 
			 String employeeId;
			 String taskAssigned;
			 String taskWeight;
			 String firstname;
			 String lastname;
			 String weightAchieved;
			
			if(rs.getString(1) != null){
				
				taskId=rs.getString(1);
			}else {
				
				taskId= null;
			}
			
			if(rs.getString(2) != null){
				
				employeeId=rs.getString(2);
			}else {
				
				employeeId= null;
			}
			if(rs.getString(3) != null){
				
				taskAssigned=rs.getString(3);
			}else {
				
				taskAssigned= null;
			}
			
			if(rs.getString(4) != null){
				
				taskWeight=rs.getString(4);
			}else {
				
				taskWeight= null;
			}
			
			if(rs.getString(5) != null){
				firstname= rs.getString(5);

				
			}else {
				
				firstname= "";
			}
			if(rs.getString(6) != null){
				lastname= rs.getString(6);

				
			}else {
				
				lastname= "";
			}
			if(rs.getString(7) != null){
				weightAchieved= rs.getString(7);

				
			}else {
				
				weightAchieved= "";
			}
			String name=firstname+" "+lastname;
			
			
			ResponseTaskAssignedVO mObj = new ResponseTaskAssignedVO(
					taskId,	  employeeId,	  taskAssigned,	  taskWeight,name,weightAchieved				 
					 );
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}


		}catch(Exception e){

			e.printStackTrace();
		}finally {
			try {

				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("task  .. "+returJson);
		return returJson;
	}



	public String PostServiceForTaskActivity(RequestTaskActivityVO request) {
		

		
		List<ResponseTaskActivityVO> finalList = new ArrayList<ResponseTaskActivityVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		
		
		try{
			
			if(request.getActivityId() == null){
				String INSERT="INSERT INTO task_activity_list (TASK_ID,EMPLOYEE_ID,TASK_ASSIGNED,PROJECT_NAME,PROJECT_DESCRIPTION,TASK_COMPLETE,ISSUES_CLEARED) values (?,?,?,?,?,?,?)";
            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getTaskId());
            statement1.setString(2, request.getEmployeeId());
            statement1.setString(3, request.getTaskAssigned());
            statement1.setString(4, request.getProjectName());
            statement1.setString(5, request.getProjectDescription());
            statement1.setString(6, request.getTaskComplete());
            statement1.setString(7, request.getIssuesCleared());
            
            row = statement1.executeUpdate();
 		} 	else {
 			String UPDATE="UPDATE task_activity_list SET TASK_ID =? , EMPLOYEE_ID=?,TASK_ASSIGNED =?,PROJECT_NAME=?,PROJECT_DESCRIPTION=?,TASK_COMPLETE=?,ISSUES_CLEARED=?   WHERE TASK_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

            statement1.setString(1, request.getTaskId());
            statement1.setString(2, request.getEmployeeId());
            statement1.setString(3, request.getTaskAssigned());
            statement1.setString(4, request.getProjectName());
            statement1.setString(5, request.getProjectDescription());
            statement1.setString(6, request.getTaskComplete());
            statement1.setString(7, request.getIssuesCleared());
            
            
            
            row = statement1.executeUpdate();

 		}
		
		    if (row > 0) {
		    	String SELECT="SELECT ACTIVITY_ID,TASK_ID,EMPLOYEE_ID,TASK_ASSIGNED,PROJECT_NAME,PROJECT_DESCRIPTION,TASK_COMPLETE,ISSUES_CLEARED from task_activity_list";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					String activityId;
					 String taskId;
					 String employeeId;
					 String taskAssigned;
					 String projectName;
					 String projectDescription;
					 String taskComplete;
					 String issuesCleared;
					
					 
					if(rs.getString(1) != null){
						
						activityId=rs.getString(1);
					}else {
						
						activityId= null;
					}
					
					if(rs.getString(2) != null){
						
						taskId=rs.getString(2);
					}else {
						
						taskId= "";
					}
					if(rs.getString(3) != null){
						employeeId= rs.getString(3);
 
						
					}else {
						
						employeeId= "";
					}
					if(rs.getString(4) != null){
						taskAssigned= rs.getString(4);
 
						
					}else {
						
						taskAssigned= "";
					}
					if(rs.getString(5) != null){
						
						projectName=rs.getString(5);
					}else {
						
						projectName= null;
					}
					
					if(rs.getString(6) != null){
						
						projectDescription=rs.getString(6);
					}else {
						
						projectDescription= "";
					}
					if(rs.getString(7) != null){
						taskComplete= rs.getString(7);
 
						
					}else {
						
						taskComplete= "";
					}
					if(rs.getString(8) != null){
						issuesCleared= rs.getString(8);
 
						
					}else {
						
						issuesCleared= "";
					}
					
					
					ResponseTaskActivityVO mObj = new ResponseTaskActivityVO(
							activityId,taskId,employeeId,taskAssigned,
							projectName,projectDescription,taskComplete,issuesCleared);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    }
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("task Activity  ... "+returJson);
		return returJson;
	
	}



	public String PostServiceForTaskActivityList(String request) {
		
		List<ResponseTaskActivityVO> finalList = new ArrayList<ResponseTaskActivityVO>();

		Connection conn = null;
		ResultSet rs = null;
		String returJson = "";

		PreparedStatement statement2 =null;

		try{
	
	
		conn = DbServices.getConnection();

    	String SELECT="SELECT ACTIVITY_ID,TASK_ID,EMPLOYEE_ID,TASK_ASSIGNED,PROJECT_NAME,PROJECT_DESCRIPTION,TASK_COMPLETE,ISSUES_CLEARED from task_activity_list";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			String activityId;
			 String taskId;
			 String employeeId;
			 String taskAssigned;
			 String projectName;
			 String projectDescription;
			 String taskComplete;
			 String issuesCleared;
			
			 
			if(rs.getString(1) != null){
				
				activityId=rs.getString(1);
			}else {
				
				activityId= null;
			}
			
			if(rs.getString(2) != null){
				
				taskId=rs.getString(2);
			}else {
				
				taskId= "";
			}
			if(rs.getString(3) != null){
				employeeId= rs.getString(3);

				
			}else {
				
				employeeId= "";
			}
			if(rs.getString(4) != null){
				taskAssigned= rs.getString(4);

				
			}else {
				
				taskAssigned= "";
			}
			if(rs.getString(5) != null){
				
				projectName=rs.getString(5);
			}else {
				
				projectName= null;
			}
			
			if(rs.getString(6) != null){
				
				projectDescription=rs.getString(6);
			}else {
				
				projectDescription= "";
			}
			if(rs.getString(7) != null){
				taskComplete= rs.getString(7);

				
			}else {
				
				taskComplete= "";
			}
			if(rs.getString(8) != null){
				issuesCleared= rs.getString(8);

				
			}else {
				
				issuesCleared= "";
			}
			
			
			ResponseTaskActivityVO mObj = new ResponseTaskActivityVO(
					activityId,taskId,employeeId,taskAssigned,
					projectName,projectDescription,taskComplete,issuesCleared);
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}
    
		}catch(Exception e){
	
	e.printStackTrace();
		}finally {
			try {
		
		statement2.close();
		rs.close();
		conn.close();
			} catch (SQLException e) {
		e.printStackTrace();
			}
}
	System.out.println("task Activity List  ... "+returJson);
	return returJson;

}



	public String PostServiceForViewActivity(String request) {

		
		List<ResponseTaskActivityVO> finalList = new ArrayList<ResponseTaskActivityVO>();

		Connection conn = null;
		ResultSet rs = null;
		String returJson = "";

		PreparedStatement statement2 =null;

		try{
	
	
		conn = DbServices.getConnection();

    	String SELECT="SELECT ACTIVITY_ID,TASK_ID,EMPLOYEE_ID,TASK_ASSIGNED,PROJECT_NAME,PROJECT_DESCRIPTION,TASK_COMPLETE,ISSUES_CLEARED from task_activity_list WHERE ACTIVITY_ID='"+request+"'";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			String activityId;
			 String taskId;
			 String employeeId;
			 String taskAssigned;
			 String projectName;
			 String projectDescription;
			 String taskComplete;
			 String issuesCleared;
			
			 
			if(rs.getString(1) != null){
				
				activityId=rs.getString(1);
			}else {
				
				activityId= null;
			}
			
			if(rs.getString(2) != null){
				
				taskId=rs.getString(2);
			}else {
				
				taskId= "";
			}
			if(rs.getString(3) != null){
				employeeId= rs.getString(3);

				
			}else {
				
				employeeId= "";
			}
			if(rs.getString(4) != null){
				taskAssigned= rs.getString(4);

				
			}else {
				
				taskAssigned= "";
			}
			if(rs.getString(5) != null){
				
				projectName=rs.getString(5);
			}else {
				
				projectName= null;
			}
			
			if(rs.getString(6) != null){
				
				projectDescription=rs.getString(6);
			}else {
				
				projectDescription= "";
			}
			if(rs.getString(7) != null){
				taskComplete= rs.getString(7);

				
			}else {
				
				taskComplete= "";
			}
			if(rs.getString(8) != null){
				issuesCleared= rs.getString(8);

				
			}else {
				
				issuesCleared= "";
			}
			
			
			ResponseTaskActivityVO mObj = new ResponseTaskActivityVO(
					activityId,taskId,employeeId,taskAssigned,
					projectName,projectDescription,taskComplete,issuesCleared);
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}
    
		}catch(Exception e){
	
	e.printStackTrace();
		}finally {
			try {
		
		statement2.close();
		rs.close();
		conn.close();
			} catch (SQLException e) {
		e.printStackTrace();
			}
}
	System.out.println("task Activity List  ... "+returJson);
	return returJson;


	}



	public String PostServiceForTaskListAllEmp() {
		List<ResponseTaskAssignedVO> finalList = new ArrayList<ResponseTaskAssignedVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
	
		
		PreparedStatement statement2 =null;
		try{
				
		String SELECT="SELECT T.TASK_ID,T.EMPLOYEE_ID,T.TASK_ASSIGNED,T.TASK_WEIGHT,E.FIRST_NAME,E.LAST_NAME,T.WEIGHT_ACHIEVED from task_assign_pr T,employee_details E  WHERE T.EMPLOYEE_ID=E.EMPLOYEE_ID";
		conn = DbServices.getConnection();
		statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			 String taskId;
			 
			 String employeeId;
			 String taskAssigned;
			 String taskWeight;
			 String firstname;
			 String lastname;
			 String weightAchieved;
			
			if(rs.getString(1) != null){
				
				taskId=rs.getString(1);
			}else {
				
				taskId= null;
			}
			
			if(rs.getString(2) != null){
				
				employeeId=rs.getString(2);
			}else {
				
				employeeId= null;
			}
			if(rs.getString(3) != null){
				
				taskAssigned=rs.getString(3);
			}else {
				
				taskAssigned= null;
			}
			
			if(rs.getString(4) != null){
				
				taskWeight=rs.getString(4);
			}else {
				
				taskWeight= null;
			}
			if(rs.getString(5) != null){
				
				firstname=rs.getString(5);
			}else {
				
				firstname= "";
			}
			if(rs.getString(6) != null){
	
				lastname=rs.getString(6);
			}else {
	
				lastname= "";
			}
			if(rs.getString(7) != null){
				weightAchieved= rs.getString(7);

				
			}else {
				
				weightAchieved= "";
			}
			String name=firstname+" "+lastname;
			
			ResponseTaskAssignedVO mObj = new ResponseTaskAssignedVO(
					taskId,	  employeeId,	  taskAssigned,	  taskWeight,name,weightAchieved				 
					 );
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}


		}catch(Exception e){

			e.printStackTrace();
		}finally {
			try {

				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("task LIST ALL .. "+returJson);
		return returJson;
	}



	public String PostServiceForActivityListAllEmp() {

		
		List<ResponseTaskActivityVO> finalList = new ArrayList<ResponseTaskActivityVO>();

		Connection conn = null;
		ResultSet rs = null;
		String returJson = "";

		PreparedStatement statement2 =null;

		try{
	
	
		conn = DbServices.getConnection();

    	String SELECT="SELECT ACTIVITY_ID,TASK_ID,EMPLOYEE_ID,TASK_ASSIGNED,PROJECT_NAME,PROJECT_DESCRIPTION,TASK_COMPLETE,ISSUES_CLEARED from task_activity_list ";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			String activityId;
			 String taskId;
			 String employeeId;
			 String taskAssigned;
			 String projectName;
			 String projectDescription;
			 String taskComplete;
			 String issuesCleared;
			
			 
			if(rs.getString(1) != null){
				
				activityId=rs.getString(1);
			}else {
				
				activityId= null;
			}
			
			if(rs.getString(2) != null){
				
				taskId=rs.getString(2);
			}else {
				
				taskId= "";
			}
			if(rs.getString(3) != null){
				employeeId= rs.getString(3);

				
			}else {
				
				employeeId= "";
			}
			if(rs.getString(4) != null){
				taskAssigned= rs.getString(4);

				
			}else {
				
				taskAssigned= "";
			}
			if(rs.getString(5) != null){
				
				projectName=rs.getString(5);
			}else {
				
				projectName= null;
			}
			
			if(rs.getString(6) != null){
				
				projectDescription=rs.getString(6);
			}else {
				
				projectDescription= "";
			}
			if(rs.getString(7) != null){
				taskComplete= rs.getString(7);

				
			}else {
				
				taskComplete= "";
			}
			if(rs.getString(8) != null){
				issuesCleared= rs.getString(8);

				
			}else {
				
				issuesCleared= "";
			}
			
			
			ResponseTaskActivityVO mObj = new ResponseTaskActivityVO(
					activityId,taskId,employeeId,taskAssigned,
					projectName,projectDescription,taskComplete,issuesCleared);
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}
    
		}catch(Exception e){
	
	e.printStackTrace();
		}finally {
			try {
		
		statement2.close();
		rs.close();
		conn.close();
			} catch (SQLException e) {
		e.printStackTrace();
			}
}
	System.out.println("task Activity List ALL  ... "+returJson);
	return returJson;


	}



	public String PostServiceForReviewTaskGetActivity(String request) {
		


		
		List<ResponseTaskActivityVO> finalList = new ArrayList<ResponseTaskActivityVO>();

		Connection conn = null;
		ResultSet rs = null;
		String returJson = "";

		PreparedStatement statement2 =null;

		try{
	
	
		conn = DbServices.getConnection();

    	String SELECT="SELECT ACTIVITY_ID,TASK_ID,EMPLOYEE_ID,TASK_ASSIGNED,PROJECT_NAME,PROJECT_DESCRIPTION,TASK_COMPLETE,ISSUES_CLEARED from task_activity_list where TASK_ID='"+request+"' ";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			String activityId;
			 String taskId;
			 String employeeId;
			 String taskAssigned;
			 String projectName;
			 String projectDescription;
			 String taskComplete;
			 String issuesCleared;
			
			 
			if(rs.getString(1) != null){
				
				activityId=rs.getString(1);
			}else {
				
				activityId= null;
			}
			
			if(rs.getString(2) != null){
				
				taskId=rs.getString(2);
			}else {
				
				taskId= "";
			}
			if(rs.getString(3) != null){
				employeeId= rs.getString(3);

				
			}else {
				
				employeeId= "";
			}
			if(rs.getString(4) != null){
				taskAssigned= rs.getString(4);

				
			}else {
				
				taskAssigned= "";
			}
			if(rs.getString(5) != null){
				
				projectName=rs.getString(5);
			}else {
				
				projectName= null;
			}
			
			if(rs.getString(6) != null){
				
				projectDescription=rs.getString(6);
			}else {
				
				projectDescription= "";
			}
			if(rs.getString(7) != null){
				taskComplete= rs.getString(7);

				
			}else {
				
				taskComplete= "";
			}
			if(rs.getString(8) != null){
				issuesCleared= rs.getString(8);

				
			}else {
				
				issuesCleared= "";
			}
			
			
			ResponseTaskActivityVO mObj = new ResponseTaskActivityVO(
					activityId,taskId,employeeId,taskAssigned,
					projectName,projectDescription,taskComplete,issuesCleared);
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}
    
		}catch(Exception e){
	
	e.printStackTrace();
		}finally {
			try {
		
		statement2.close();
		rs.close();
		conn.close();
			} catch (SQLException e) {
		e.printStackTrace();
			}
}
	System.out.println("review Activity List for task  ... "+returJson);
	return returJson;


	
	}



	public String PostServiceForReviewTaskGetTask(String request) {
List<ResponseTaskAssignedVO> finalList = new ArrayList<ResponseTaskAssignedVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
	
		
		PreparedStatement statement2 =null;
		try{
				
		String SELECT="SELECT T.TASK_ID,T.EMPLOYEE_ID,T.TASK_ASSIGNED,T.TASK_WEIGHT,E.FIRST_NAME,E.LAST_NAME,T.WEIGHT_ACHIEVED from task_assign_pr T,employee_details E  WHERE T.TASK_ID='"+request+"' AND T.EMPLOYEE_ID=E.EMPLOYEE_ID";
		conn = DbServices.getConnection();
		statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			 String taskId;
			 
			 String employeeId;
			 String taskAssigned;
			 String taskWeight;
			 String firstname;
			 String lastname;
			 String weightAchieved;
			
			if(rs.getString(1) != null){
				
				taskId=rs.getString(1);
			}else {
				
				taskId= null;
			}
			
			if(rs.getString(2) != null){
				
				employeeId=rs.getString(2);
			}else {
				
				employeeId= null;
			}
			if(rs.getString(3) != null){
				
				taskAssigned=rs.getString(3);
			}else {
				
				taskAssigned= null;
			}
			
			if(rs.getString(4) != null){
				
				taskWeight=rs.getString(4);
			}else {
				
				taskWeight= null;
			}
			if(rs.getString(5) != null){
				
				firstname=rs.getString(5);
			}else {
				
				firstname= "";
			}
			if(rs.getString(6) != null){
	
				lastname=rs.getString(6);
			}else {
	
				lastname= "";
			}
			if(rs.getString(7) != null){
				weightAchieved= rs.getString(7);

				
			}else {
				
				weightAchieved= "";
			}
			String name=firstname+" "+lastname;
			
			ResponseTaskAssignedVO mObj = new ResponseTaskAssignedVO(
					taskId,	  employeeId,	  taskAssigned,	  taskWeight,name,weightAchieved				 
					 );
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}


		}catch(Exception e){

			e.printStackTrace();
		}finally {
			try {

				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("task LIST based on id .. "+returJson);
		return returJson;	
	}



	public String PostServiceForSubmitWeight(String[] request) {

List<ResponseTaskAssignedVO> finalList = new ArrayList<ResponseTaskAssignedVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
 		int row=0;

		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		try{
			String UPDATE="UPDATE task_assign_pr SET  WEIGHT_ACHIEVED =?   WHERE TASK_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

            statement1.setString(1, request[1]);
            statement1.setString(2, request[0]);
           
            row = statement1.executeUpdate();

 		
		
		    if (row > 0) {		
		    	String SELECT="SELECT T.TASK_ID,T.EMPLOYEE_ID,T.TASK_ASSIGNED,T.TASK_WEIGHT,E.FIRST_NAME,E.LAST_NAME,T.WEIGHT_ACHIEVED from task_assign_pr T,employee_details E  WHERE  T.EMPLOYEE_ID=E.EMPLOYEE_ID";
		
		    	statement2 = conn.prepareStatement(SELECT);
		    	rs = statement2.executeQuery();
		
		    	while(rs.next()){
			
		    		String taskId;
			 
		    		String employeeId;
		    		String taskAssigned;
		    		String taskWeight;
		    		String firstname;
		    		String lastname;
		    		String weightAchieved;
			
		    		if(rs.getString(1) != null){
				
		    			taskId=rs.getString(1);
		    		}else {
				
		    			taskId= null;
		    		}
			
		    		if(rs.getString(2) != null){
				
		    			employeeId=rs.getString(2);
		    		}else {
				
		    			employeeId= null;
		    			}
		    		if(rs.getString(3) != null){
				
		    			taskAssigned=rs.getString(3);
		    		}else {
				
		    			taskAssigned= null;
		    		}
			
		    		if(rs.getString(4) != null){
				
		    			taskWeight=rs.getString(4);
		    		}else {
				
		    			taskWeight= null;
		    		}
		    		if(rs.getString(5) != null){
				
		    			firstname=rs.getString(5);
		    		}else {
				
		    			firstname= "";
		    		}
		    		if(rs.getString(6) != null){
	
		    			lastname=rs.getString(6);
		    		}else {
	
		    			lastname= "";
		    		}
		    		if(rs.getString(7) != null){
		    			weightAchieved= rs.getString(7);

				
		    		}else {
				
		    			weightAchieved= "";
		    		}
		    		String name=firstname+" "+lastname;
			
			ResponseTaskAssignedVO mObj = new ResponseTaskAssignedVO(
					taskId,	  employeeId,	  taskAssigned,	  taskWeight,name,weightAchieved				 
					 );
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}
		    }

		}catch(Exception e){

			e.printStackTrace();
		}finally {
			try {

				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("UPDATE WEIGHT .. "+returJson);
		return returJson;	
	
	}



	public String PostServiceForGetTaskByEmpId(String request) {

List<ResponseTaskAssignedVO> finalList = new ArrayList<ResponseTaskAssignedVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		
	
		
		PreparedStatement statement2 =null;
		try{
				
		String SELECT="SELECT T.TASK_ID,T.EMPLOYEE_ID,T.TASK_ASSIGNED,T.TASK_WEIGHT,E.FIRST_NAME,E.LAST_NAME,T.WEIGHT_ACHIEVED from task_assign_pr T,employee_details E  WHERE T.EMPLOYEE_ID='"+request+"' AND T.EMPLOYEE_ID=E.EMPLOYEE_ID";
		conn = DbServices.getConnection();
		statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			 String taskId;
			 
			 String employeeId;
			 String taskAssigned;
			 String taskWeight;
			 String firstname;
			 String lastname;
			 String weightAchieved;
			
			if(rs.getString(1) != null){
				
				taskId=rs.getString(1);
			}else {
				
				taskId= null;
			}
			
			if(rs.getString(2) != null){
				
				employeeId=rs.getString(2);
			}else {
				
				employeeId= null;
			}
			if(rs.getString(3) != null){
				
				taskAssigned=rs.getString(3);
			}else {
				
				taskAssigned= null;
			}
			
			if(rs.getString(4) != null){
				
				taskWeight=rs.getString(4);
			}else {
				
				taskWeight= null;
			}
			if(rs.getString(5) != null){
				
				firstname=rs.getString(5);
			}else {
				
				firstname= "";
			}
			if(rs.getString(6) != null){
	
				lastname=rs.getString(6);
			}else {
	
				lastname= "";
			}
			if(rs.getString(7) != null){
				weightAchieved= rs.getString(7);

				
			}else {
				
				weightAchieved= "";
			}
			String name=firstname+" "+lastname;
			
			ResponseTaskAssignedVO mObj = new ResponseTaskAssignedVO(
					taskId,	  employeeId,	  taskAssigned,	  taskWeight,name,weightAchieved				 
					 );
			finalList.add(mObj);
			Gson gs = new Gson();
			returJson = gs.toJson(finalList).toString();

		}


		}catch(Exception e){

			e.printStackTrace();
		}finally {
			try {

				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("task LIST based on id .. "+returJson);
		return returJson;	
		
	}



	public String PostServiceForgetReviewByEmpId(String request) {
List<ResponsePerformanceVO> finalList = new ArrayList<ResponsePerformanceVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		System.out.println(" req  .. "+request);
		PreparedStatement statement2 =null;
		try{
			conn = DbServices.getConnection();

	    	String SELECT="SELECT P.PERFORMANCE_ID,P.JOB_SKILL,P.NEW_SKILL,P.RESOURSE_USES,P.ASSIGN_RESPONSIBILITIES,P.MEET_ATTENDANCE,P.LISTEN_TO_DIRECTION,P.RESPONSIBILITY,P.COMMITMENTS,P.PROBLEM_SOLVING,P.SUGGESTION_IMPROVEMENT,P.IDEA_AND_SOLUTION,P.MEET_CHALLENGES,P.INNOVATIVE_THINKING,P.EMPLOYEE_ID,P.REVIEWED_BY,P.REVIEWED_DATE,P.REVIEW_PERIOD,P.COMMENTS,E.FIRST_NAME,E.LAST_NAME,P.REVIEW_PERIOD_TO,P.EMPLOYEE_SIGNATURE,P.ADMIN_SIGNATURE,P.REVIEW_YEAR,P.REVIEW_QUARTER from performance_review P,employee_details E  WHERE P.EMPLOYEE_ID =? AND P.EMPLOYEE_ID=E.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
            statement2.setString(1, request);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String performanceId;
				 
				 String jobskill;
				 String newskill;
				 String resourseUses;
				 
				 String assignResponsibilities;
				 String meetAttendance;
				 String listenToDirection;
				 String responsibility;
				 
				 
				 String commitments;
				 String problemSolving;
				 String suggestionImprovement;
				 String ideaAndSolution;
				 
				 String meetChallenges;
				 String innovativeThinking;
				 String employeeId;
				 String reviewedBy;
				 
				 String reviewedDate;
				 String reviewPeriod;
				 String comments;
				 String totalWeight;
				 String firstname;
				 String lastname;
				 String reviewPeriodTo;
				 String employeeSignature;
				 String adminSignature;
				 
				 String review_year="";
				 String review_quarter="";
				 
				if(rs.getString(1) != null){
					
					performanceId=rs.getString(1);
				}else {
					
					performanceId= null;
				}
				
				if(rs.getString(2) != null){
					
					jobskill=rs.getString(2);
				}else {
					
					jobskill= "0";
				}
				if(rs.getString(3) != null){
					
					newskill=rs.getString(3);
				}else {
					
					newskill=  "0";
				}
				
				if(rs.getString(4) != null){
					
					resourseUses=rs.getString(4);
				}else {
					
					resourseUses=  "0";
				}
				
				
				if(rs.getString(5) != null){
					
					assignResponsibilities=rs.getString(5);
				}else {
					
					assignResponsibilities=  "0";
				}
				
				if(rs.getString(6) != null){
					
					meetAttendance=rs.getString(6);
				}else {
					
					meetAttendance=  "0";
				}
				if(rs.getString(7) != null){
					
					listenToDirection=rs.getString(7);
				}else {
					
					listenToDirection=  "0";
				}
				
				if(rs.getString(8) != null){
					
					responsibility=rs.getString(8);
				}else {
					
					responsibility=  "0";
				}
				
				
				
				
				if(rs.getString(9) != null){
					
					commitments=rs.getString(9);
				}else {
					
					commitments=  "0";
				}
				
				if(rs.getString(10) != null){
					
					problemSolving=rs.getString(10);
				}else {
					
					problemSolving=  "0";
				}
				if(rs.getString(11) != null){
					
					suggestionImprovement=rs.getString(11);
				}else {
					
					suggestionImprovement= "0";
				}
				
				if(rs.getString(12) != null){
					
					ideaAndSolution=rs.getString(12);
				}else {
					
					ideaAndSolution= "0";
				}
				
				
				

				if(rs.getString(13) != null){
					
					meetChallenges=rs.getString(13);
				}else {
					
					meetChallenges=  "0";
				}
				
				if(rs.getString(14) != null){
					
					innovativeThinking=rs.getString(14);
				}else {
					
					innovativeThinking= "0";
				}
				if(rs.getString(15) != null){
					
					employeeId=rs.getString(15);
				}else {
					
					employeeId= null;
				}
				
				if(rs.getString(16) != null){
					
					reviewedBy=rs.getString(16);
				}else {
					
					reviewedBy= null;
				}
				
				
				if(rs.getString(17) != null){
					reviewedDate= rs.getString(17);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewedDate = sdf.format(cal.getTime());
					
				}else {
					
					reviewedDate= null;
				}
				if(rs.getString(18) != null){
					reviewPeriod= rs.getString(18);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriod));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriod = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriod= null;
				}
				
				
				
				if(rs.getString(19) != null){
					
					comments=rs.getString(19);
				}else {
					
					comments= null;
				}
				
				if(rs.getString(20) != null){
					
					firstname=rs.getString(20);
				}else {
					
					firstname= "";
				}
				if(rs.getString(21) != null){
					
					lastname=rs.getString(21);
				}else {
					
					lastname= "";
				}
				String name=firstname+" "+lastname;
				
				if(rs.getString(22) != null){
					reviewPeriodTo= rs.getString(22);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriodTo));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriodTo = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriodTo= null;
				}
				if(rs.getBlob(23)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(23);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					employeeSignature=str;
				}else {
					
					employeeSignature= null;
				}
				if(rs.getBlob(24)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(24);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					adminSignature=str;
				}else {
					
					adminSignature= null;
				}
				int total=0;
				 total =Integer.parseInt(jobskill)+Integer.parseInt(newskill)+
						Integer.parseInt(resourseUses)+Integer.parseInt(assignResponsibilities)+
						Integer.parseInt(meetAttendance)+Integer.parseInt(listenToDirection)+
						Integer.parseInt(responsibility)+Integer.parseInt(commitments)+
						
						Integer.parseInt(problemSolving)+Integer.parseInt(suggestionImprovement)+
						Integer.parseInt(ideaAndSolution)+Integer.parseInt(meetChallenges)+
						Integer.parseInt(innovativeThinking);
				
				System.out.println("total .. "+total);
					float tot1=0;
					float tot=0;
					tot1=total;
					tot= (tot1 / 65)*50;
				totalWeight=Float.toString(tot);  
				
				if(rs.getString(25) != null){
					
					review_year=rs.getString(25);
				}else {
					
					review_year= "";
				}
				if(rs.getString(26) != null){
					
					review_quarter=rs.getString(26);
				}else {
					
					review_quarter= "";
				}
				 
				
				
				
				ResponsePerformanceVO mObj = new ResponsePerformanceVO(
						 performanceId,	  jobskill,	  newskill,	  resourseUses,				 
						  assignResponsibilities,  meetAttendance,  listenToDirection, responsibility,
						   commitments,  problemSolving, suggestionImprovement,  ideaAndSolution,
						  meetChallenges, innovativeThinking, employeeId,  reviewedBy,
						  reviewedDate, reviewPeriod, comments,totalWeight,name,reviewPeriodTo,employeeSignature,adminSignature,review_year,review_quarter);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("Performance  .. "+returJson);
			return returJson;
		}



	public String PostServiceForgetReviewByPerformanceId(String request) {
		List<ResponsePerformanceVO> finalList = new ArrayList<ResponsePerformanceVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		System.out.println(" req  .. "+request);
		PreparedStatement statement2 =null;
		try{
			conn = DbServices.getConnection();

	    	String SELECT="SELECT P.PERFORMANCE_ID,P.JOB_SKILL,P.NEW_SKILL,P.RESOURSE_USES,P.ASSIGN_RESPONSIBILITIES,P.MEET_ATTENDANCE,P.LISTEN_TO_DIRECTION,P.RESPONSIBILITY,P.COMMITMENTS,P.PROBLEM_SOLVING,P.SUGGESTION_IMPROVEMENT,P.IDEA_AND_SOLUTION,P.MEET_CHALLENGES,P.INNOVATIVE_THINKING,P.EMPLOYEE_ID,P.REVIEWED_BY,P.REVIEWED_DATE,P.REVIEW_PERIOD,P.COMMENTS,E.FIRST_NAME,E.LAST_NAME,P.REVIEW_PERIOD_TO,P.EMPLOYEE_SIGNATURE,P.ADMIN_SIGNATURE,P.REVIEW_YEAR,P.REVIEW_QUARTER from performance_review P,employee_details E  WHERE P.PERFORMANCE_ID =? AND P.EMPLOYEE_ID=E.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
            statement2.setString(1, request);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String performanceId;
				 
				 String jobskill;
				 String newskill;
				 String resourseUses;
				 
				 String assignResponsibilities;
				 String meetAttendance;
				 String listenToDirection;
				 String responsibility;
				 
				 
				 String commitments;
				 String problemSolving;
				 String suggestionImprovement;
				 String ideaAndSolution;
				 
				 String meetChallenges;
				 String innovativeThinking;
				 String employeeId;
				 String reviewedBy;
				 
				 String reviewedDate;
				 String reviewPeriod;
				 String comments;
				 String totalWeight;
				 String firstname;
				 String lastname;
				 String reviewPeriodTo;
				 String employeeSignature;
				 String adminSignature;
				 
				 String review_year="";
				 String review_quarter="";
				 
				if(rs.getString(1) != null){
					
					performanceId=rs.getString(1);
				}else {
					
					performanceId= null;
				}
				
				if(rs.getString(2) != null){
					
					jobskill=rs.getString(2);
				}else {
					
					jobskill=  "0";
				}
				if(rs.getString(3) != null){
					
					newskill=rs.getString(3);
				}else {
					
					newskill=  "0";
				}
				
				if(rs.getString(4) != null){
					
					resourseUses=rs.getString(4);
				}else {
					
					resourseUses=  "0";
				}
				
				
				if(rs.getString(5) != null){
					
					assignResponsibilities=rs.getString(5);
				}else {
					
					assignResponsibilities=  "0";
				}
				
				if(rs.getString(6) != null){
					
					meetAttendance=rs.getString(6);
				}else {
					
					meetAttendance=  "0";
				}
				if(rs.getString(7) != null){
					
					listenToDirection=rs.getString(7);
				}else {
					
					listenToDirection=  "0";
				}
				
				if(rs.getString(8) != null){
					
					responsibility=rs.getString(8);
				}else {
					
					responsibility=  "0";
				}
				
				
				
				
				if(rs.getString(9) != null){
					
					commitments=rs.getString(9);
				}else {
					
					commitments=  "0";
				}
				
				if(rs.getString(10) != null){
					
					problemSolving=rs.getString(10);
				}else {
					
					problemSolving=  "0";
				}
				if(rs.getString(11) != null){
					
					suggestionImprovement=rs.getString(11);
				}else {
					
					suggestionImprovement=  "0";
				}
				
				if(rs.getString(12) != null){
					
					ideaAndSolution=rs.getString(12);
				}else {
					
					ideaAndSolution=  "0";
				}
				
				
				

				if(rs.getString(13) != null){
					
					meetChallenges=rs.getString(13);
				}else {
					
					meetChallenges=  "0";
				}
				
				if(rs.getString(14) != null){
					
					innovativeThinking=rs.getString(14);
				}else {
					
					innovativeThinking= "0";
				}
				if(rs.getString(15) != null){
					
					employeeId=rs.getString(15);
				}else {
					
					employeeId= null;
				}
				
				if(rs.getString(16) != null){
					
					reviewedBy=rs.getString(16);
				}else {
					
					reviewedBy= null;
				}
				
				
				if(rs.getString(17) != null){
					reviewedDate= rs.getString(17);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewedDate = sdf.format(cal.getTime());
					
				}else {
					
					reviewedDate= null;
				}
				if(rs.getString(18) != null){
					reviewPeriod= rs.getString(18);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriod));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriod = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriod= null;
				}
				
				
				
				if(rs.getString(19) != null){
					
					comments=rs.getString(19);
				}else {
					
					comments= null;
				}
				
				if(rs.getString(20) != null){
					
					firstname=rs.getString(20);
				}else {
					
					firstname= "";
				}
				if(rs.getString(21) != null){
					
					lastname=rs.getString(21);
				}else {
					
					lastname= "";
				}
				String name=firstname+" "+lastname;
				if(rs.getString(22) != null){
					reviewPeriodTo= rs.getString(22);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriodTo));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriodTo = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriodTo= null;
				}
				
				if(rs.getBlob(23)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(23);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					employeeSignature=str;
				}else {
					
					employeeSignature= null;
				}
				if(rs.getBlob(24)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(24);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					adminSignature=str;
				}else {
					
					adminSignature= null;
				}
				int total=0;
				 total =Integer.parseInt(jobskill)+Integer.parseInt(newskill)+
						Integer.parseInt(resourseUses)+Integer.parseInt(assignResponsibilities)+
						Integer.parseInt(meetAttendance)+Integer.parseInt(listenToDirection)+
						Integer.parseInt(responsibility)+Integer.parseInt(commitments)+
						
						Integer.parseInt(problemSolving)+Integer.parseInt(suggestionImprovement)+
						Integer.parseInt(ideaAndSolution)+Integer.parseInt(meetChallenges)+
						Integer.parseInt(innovativeThinking);
				
				System.out.println("total .. "+total);
					float tot1=0;
					float tot=0;
					tot1=total;
					tot= (tot1 / 65)*50;
				totalWeight=Float.toString(tot);  
				
				if(rs.getString(25) != null){
					
					review_year=rs.getString(25);
				}else {
					
					review_year= "";
				}
				if(rs.getString(26) != null){
					
					review_quarter=rs.getString(26);
				}else {
					
					review_quarter= "";
				}
				 
				
				
				
				ResponsePerformanceVO mObj = new ResponsePerformanceVO(
						 performanceId,	  jobskill,	  newskill,	  resourseUses,				 
						  assignResponsibilities,  meetAttendance,  listenToDirection, responsibility,
						   commitments,  problemSolving, suggestionImprovement,  ideaAndSolution,
						  meetChallenges, innovativeThinking, employeeId,  reviewedBy,
						  reviewedDate, reviewPeriod, comments,totalWeight,name,reviewPeriodTo,employeeSignature,adminSignature,review_year,review_quarter);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("Performance id  .. "+returJson);
			return returJson;
		}



	public String DeleteServicePeroformanceReviewByPerformanceId(String[] request) {
	

		List<ResponsePerformanceVO> finalList = new ArrayList<ResponsePerformanceVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		System.out.println(" req  .. "+request[0] +request[1]);
		PreparedStatement statement2 =null;
		PreparedStatement statement1 =null;
		try{
			String DELETE_PERFORMANCE="delete from performance_review where PERFORMANCE_ID= ?";
            conn = DbServices.getConnection();
         statement1 = conn.prepareStatement(DELETE_PERFORMANCE);
        statement1.setString(1, request[0]);
          
        int row = statement1.executeUpdate();
      
        if (row > 0) {
			

	    	String SELECT="SELECT P.PERFORMANCE_ID,P.JOB_SKILL,P.NEW_SKILL,P.RESOURSE_USES,P.ASSIGN_RESPONSIBILITIES,P.MEET_ATTENDANCE,P.LISTEN_TO_DIRECTION,P.RESPONSIBILITY,P.COMMITMENTS,P.PROBLEM_SOLVING,P.SUGGESTION_IMPROVEMENT,P.IDEA_AND_SOLUTION,P.MEET_CHALLENGES,P.INNOVATIVE_THINKING,P.EMPLOYEE_ID,P.REVIEWED_BY,P.REVIEWED_DATE,P.REVIEW_PERIOD,P.COMMENTS,E.FIRST_NAME,E.LAST_NAME,P.REVIEW_PERIOD_TO,P.EMPLOYEE_SIGNATURE,P.ADMIN_SIGNATURE,P.REVIEW_YEAR,P.REVIEW_QUARTER from performance_review P,employee_details E  WHERE P.EMPLOYEE_ID =? AND P.EMPLOYEE_ID=E.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
            statement2.setString(1, request[1]);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String performanceId;
				 
				 String jobskill;
				 String newskill;
				 String resourseUses;
				 
				 String assignResponsibilities;
				 String meetAttendance;
				 String listenToDirection;
				 String responsibility;
				 
				 
				 String commitments;
				 String problemSolving;
				 String suggestionImprovement;
				 String ideaAndSolution;
				 
				 String meetChallenges;
				 String innovativeThinking;
				 String employeeId;
				 String reviewedBy;
				 
				 String reviewedDate;
				 String reviewPeriod;
				 String comments;
				 String totalWeight;
				 String firstname;
				 String lastname;
				 String reviewPeriodTo;
				 String employeeSignature;
				 String adminSignature;
				 
				 String review_year="";
				 String review_quarter="";
				 
				if(rs.getString(1) != null){
					
					performanceId=rs.getString(1);
				}else {
					
					performanceId= null;
				}
				
				if(rs.getString(2) != null){
					
					jobskill=rs.getString(2);
				}else {
					
					jobskill= "0";
				}
				if(rs.getString(3) != null){
					
					newskill=rs.getString(3);
				}else {
					
					newskill= "0";
				}
				
				if(rs.getString(4) != null){
					
					resourseUses=rs.getString(4);
				}else {
					
					resourseUses= "0";
				}
				
				
				if(rs.getString(5) != null){
					
					assignResponsibilities=rs.getString(5);
				}else {
					
					assignResponsibilities= "0";
				}
				
				if(rs.getString(6) != null){
					
					meetAttendance=rs.getString(6);
				}else {
					
					meetAttendance= "0";
				}
				if(rs.getString(7) != null){
					
					listenToDirection=rs.getString(7);
				}else {
					
					listenToDirection= "0";
				}
				
				if(rs.getString(8) != null){
					
					responsibility=rs.getString(8);
				}else {
					
					responsibility= "0";
				}
				
				
				
				
				if(rs.getString(9) != null){
					
					commitments=rs.getString(9);
				}else {
					
					commitments= "0";
				}
				
				if(rs.getString(10) != null){
					
					problemSolving=rs.getString(10);
				}else {
					
					problemSolving= "0";
				}
				if(rs.getString(11) != null){
					
					suggestionImprovement=rs.getString(11);
				}else {
					
					suggestionImprovement= "0";
				}
				
				if(rs.getString(12) != null){
					
					ideaAndSolution=rs.getString(12);
				}else {
					
					ideaAndSolution= "0";
				}
				
				
				

				if(rs.getString(13) != null){
					
					meetChallenges=rs.getString(13);
				}else {
					
					meetChallenges= "0";
				}
				
				if(rs.getString(14) != null){
					
					innovativeThinking=rs.getString(14);
				}else {
					
					innovativeThinking= "0";
				}
				if(rs.getString(15) != null){
					
					employeeId=rs.getString(15);
				}else {
					
					employeeId= null;
				}
				
				if(rs.getString(16) != null){
					
					reviewedBy=rs.getString(16);
				}else {
					
					reviewedBy= null;
				}
				
				
				if(rs.getString(17) != null){
					reviewedDate= rs.getString(17);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewedDate = sdf.format(cal.getTime());
					
				}else {
					
					reviewedDate= null;
				}
				if(rs.getString(18) != null){
					reviewPeriod= rs.getString(18);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriod));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriod = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriod= null;
				}
				
				
				
				if(rs.getString(19) != null){
					
					comments=rs.getString(19);
				}else {
					
					comments= null;
				}
				
				if(rs.getString(20) != null){
					
					firstname=rs.getString(20);
				}else {
					
					firstname= "";
				}
				if(rs.getString(21) != null){
					
					lastname=rs.getString(21);
				}else {
					
					lastname= "";
				}
				String name=firstname+" "+lastname;
				if(rs.getString(22) != null){
					reviewPeriodTo= rs.getString(22);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriodTo));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriodTo = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriodTo= null;
				}
				if(rs.getBlob(23)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(23);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					employeeSignature=str;
				}else {
					
					employeeSignature= null;
				}
				
				if(rs.getBlob(24)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(24);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					adminSignature=str;
				}else {
					
					adminSignature= null;
				}
				int total=0;
				 total =Integer.parseInt(jobskill)+Integer.parseInt(newskill)+
						Integer.parseInt(resourseUses)+Integer.parseInt(assignResponsibilities)+
						Integer.parseInt(meetAttendance)+Integer.parseInt(listenToDirection)+
						Integer.parseInt(responsibility)+Integer.parseInt(commitments)+
						
						Integer.parseInt(problemSolving)+Integer.parseInt(suggestionImprovement)+
						Integer.parseInt(ideaAndSolution)+Integer.parseInt(meetChallenges)+
						Integer.parseInt(innovativeThinking);
				
				System.out.println("total .. "+total);
					float tot1=0;
					float tot=0;
					tot1=total;
					tot= (tot1 / 65)*50;
				totalWeight=Float.toString(tot);  
				
				
				if(rs.getString(25) != null){
					
					review_year=rs.getString(25);
				}else {
					
					review_year= "";
				}
				if(rs.getString(26) != null){
					
					review_quarter=rs.getString(26);
				}else {
					
					review_quarter= "";
				} 
				
				
				
				ResponsePerformanceVO mObj = new ResponsePerformanceVO(
						 performanceId,	  jobskill,	  newskill,	  resourseUses,				 
						  assignResponsibilities,  meetAttendance,  listenToDirection, responsibility,
						   commitments,  problemSolving, suggestionImprovement,  ideaAndSolution,
						  meetChallenges, innovativeThinking, employeeId,  reviewedBy,
						  reviewedDate, reviewPeriod, comments,totalWeight,name,reviewPeriodTo,employeeSignature,adminSignature,review_year,review_quarter);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
        }
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("Performance id  .. "+returJson);
			return returJson;
		
	}



	public String GetServiceEmpPayroll(String request) {
		


		Connection conn = null;
		ResultSet rs = null;
		String returJson = "";

		PreparedStatement statement2 =null;

		try{
	
	
		conn = DbServices.getConnection();

    	String SELECT="SELECT PAYROLL from employee_details WHERE EMPLOYEE_ID='"+request+"'";
       	statement2 = conn.prepareStatement(SELECT);
		rs = statement2.executeQuery();
		
		while(rs.next()){
			
			String payroll;
			
			
			 
			if(rs.getString(1) != null){
				
				payroll=rs.getString(1);
			}else {
				
				payroll= "";
			}
			
		
			Gson gs = new Gson();
			returJson = gs.toJson(payroll).toString();

		}
    
		}catch(Exception e){
	
	e.printStackTrace();
		}finally {
			try {
		
		statement2.close();
		rs.close();
		conn.close();
			} catch (SQLException e) {
		e.printStackTrace();
			}
}
	System.out.println("payroll ... "+returJson);
	return returJson;


	
	}



	public String GetServiceReviewViewEmployee(String request) {

		List<ResponsePerformanceVO> finalList = new ArrayList<ResponsePerformanceVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		System.out.println(" req  .. "+request);
		PreparedStatement statement2 =null;
		try{
			conn = DbServices.getConnection();

	    	String SELECT="SELECT P.PERFORMANCE_ID,P.JOB_SKILL,P.NEW_SKILL,P.RESOURSE_USES,P.ASSIGN_RESPONSIBILITIES,P.MEET_ATTENDANCE,P.LISTEN_TO_DIRECTION,P.RESPONSIBILITY,P.COMMITMENTS,P.PROBLEM_SOLVING,P.SUGGESTION_IMPROVEMENT,P.IDEA_AND_SOLUTION,P.MEET_CHALLENGES,P.INNOVATIVE_THINKING,P.EMPLOYEE_ID,P.REVIEWED_BY,P.REVIEWED_DATE,P.REVIEW_PERIOD,P.COMMENTS,E.FIRST_NAME,E.LAST_NAME,P.REVIEW_PERIOD_TO,P.EMPLOYEE_SIGNATURE,P.ADMIN_SIGNATURE,P.REVIEW_YEAR,P.REVIEW_QUARTER from performance_review P,employee_details E  WHERE P.EMPLOYEE_ID =? AND P.EMPLOYEE_ID=E.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
            statement2.setString(1, request);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String performanceId;
				 
				 String jobskill;
				 String newskill;
				 String resourseUses;
				 
				 String assignResponsibilities;
				 String meetAttendance;
				 String listenToDirection;
				 String responsibility;
				 
				 
				 String commitments;
				 String problemSolving;
				 String suggestionImprovement;
				 String ideaAndSolution;
				 
				 String meetChallenges;
				 String innovativeThinking;
				 String employeeId;
				 String reviewedBy;
				 
				 String reviewedDate;
				 String reviewPeriod;
				 String comments;
				 String totalWeight;
				 String firstname;
				 String lastname;
				 String reviewPeriodTo;
				 
				 String employeeSignature;
				 
				 String adminSignature;
				 
				 String review_year="";
				 String review_quarter="";
				 
				if(rs.getString(1) != null){
					
					performanceId=rs.getString(1);
				}else {
					
					performanceId= null;
				}
				
				if(rs.getString(2) != null){
					
					jobskill=rs.getString(2);
				}else {
					
					jobskill=  "0";
				}
				if(rs.getString(3) != null){
					
					newskill=rs.getString(3);
				}else {
					
					newskill=  "0";
				}
				
				if(rs.getString(4) != null){
					
					resourseUses=rs.getString(4);
				}else {
					
					resourseUses=  "0";
				}
				
				
				if(rs.getString(5) != null){
					
					assignResponsibilities=rs.getString(5);
				}else {
					
					assignResponsibilities=  "0";
				}
				
				if(rs.getString(6) != null){
					
					meetAttendance=rs.getString(6);
				}else {
					
					meetAttendance=  "0";
				}
				if(rs.getString(7) != null){
					
					listenToDirection=rs.getString(7);
				}else {
					
					listenToDirection=  "0";
				}
				
				if(rs.getString(8) != null){
					
					responsibility=rs.getString(8);
				}else {
					
					responsibility=  "0";
				}
				
				
				
				
				if(rs.getString(9) != null){
					
					commitments=rs.getString(9);
				}else {
					
					commitments=  "0";
				}
				
				if(rs.getString(10) != null){
					
					problemSolving=rs.getString(10);
				}else {
					
					problemSolving=  "0";
				}
				if(rs.getString(11) != null){
					
					suggestionImprovement=rs.getString(11);
				}else {
					
					suggestionImprovement=  "0";
				}
				
				if(rs.getString(12) != null){
					
					ideaAndSolution=rs.getString(12);
				}else {
					
					ideaAndSolution=  "0";
				}
				
				
				

				if(rs.getString(13) != null){
					
					meetChallenges=rs.getString(13);
				}else {
					
					meetChallenges=  "0";
				}
				
				if(rs.getString(14) != null){
					
					innovativeThinking=rs.getString(14);
				}else {
					
					innovativeThinking= "0";
				}
				if(rs.getString(15) != null){
					
					employeeId=rs.getString(15);
				}else {
					
					employeeId= null;
				}
				
				if(rs.getString(16) != null){
					
					reviewedBy=rs.getString(16);
				}else {
					
					reviewedBy= null;
				}
				
				
				if(rs.getString(17) != null){
					reviewedDate= rs.getString(17);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewedDate = sdf.format(cal.getTime());
					
				}else {
					
					reviewedDate= null;
				}
				if(rs.getString(18) != null){
					reviewPeriod= rs.getString(18);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriod));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriod = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriod= null;
				}
				
				
				
				if(rs.getString(19) != null){
					
					comments=rs.getString(19);
				}else {
					
					comments= null;
				}
				
				if(rs.getString(20) != null){
					
					firstname=rs.getString(20);
				}else {
					
					firstname= "";
				}
				if(rs.getString(21) != null){
					
					lastname=rs.getString(21);
				}else {
					
					lastname= "";
				}
				String name=firstname+" "+lastname;
				if(rs.getString(22) != null){
					reviewPeriodTo= rs.getString(22);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriodTo));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriodTo = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriodTo= null;
				}
				
				if(rs.getBlob(23)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(23);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					employeeSignature=str;
				}else {
					
					employeeSignature= null;
				}
				
				if(rs.getBlob(24)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(24);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					adminSignature=str;
				}else {
					
					adminSignature= null;
				}
				int total=0;
				 total =Integer.parseInt(jobskill)+Integer.parseInt(newskill)+
						Integer.parseInt(resourseUses)+Integer.parseInt(assignResponsibilities)+
						Integer.parseInt(meetAttendance)+Integer.parseInt(listenToDirection)+
						Integer.parseInt(responsibility)+Integer.parseInt(commitments)+
						
						Integer.parseInt(problemSolving)+Integer.parseInt(suggestionImprovement)+
						Integer.parseInt(ideaAndSolution)+Integer.parseInt(meetChallenges)+
						Integer.parseInt(innovativeThinking);
				
				System.out.println("total .. "+total);
					float tot1=0;
					float tot=0;
					tot1=total;
					tot= (tot1 / 65)*50;
				totalWeight=Float.toString(tot);  
				
				
				if(rs.getString(25) != null){
					
					review_year=rs.getString(25);
				}else {
					
					review_year= "";
				}
				if(rs.getString(26) != null){
					
					review_quarter=rs.getString(26);
				}else {
					
					review_quarter= "";
				} 
				
				
				
				ResponsePerformanceVO mObj = new ResponsePerformanceVO(
						 performanceId,	  jobskill,	  newskill,	  resourseUses,				 
						  assignResponsibilities,  meetAttendance,  listenToDirection, responsibility,
						   commitments,  problemSolving, suggestionImprovement,  ideaAndSolution,
						  meetChallenges, innovativeThinking, employeeId,  reviewedBy,
						  reviewedDate, reviewPeriod, comments,totalWeight,name,reviewPeriodTo,employeeSignature,adminSignature,review_year,review_quarter);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("Performance id  .. "+returJson);
			return returJson;
		
		
	}



	public String PostServiceSaveSignEmp(String[] request) {
	


		List<ResponsePerformanceVO> finalList = new ArrayList<ResponsePerformanceVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		System.out.println(" req  .. "+request);
		PreparedStatement statement1 =null;
		PreparedStatement statement2 =null;
		int row=0;
		try{
			conn = DbServices.getConnection();

			String UPDATE="UPDATE performance_review SET  EMPLOYEE_SIGNATURE =?   WHERE PERFORMANCE_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);
           
            String signature=request[1];
            byte[] b = signature.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(1,blob);
            
            statement1.setString(2, request[0]);
           
            row = statement1.executeUpdate();
			
			if(row>0){
	    	String SELECT="SELECT P.PERFORMANCE_ID,P.JOB_SKILL,P.NEW_SKILL,P.RESOURSE_USES,P.ASSIGN_RESPONSIBILITIES,P.MEET_ATTENDANCE,P.LISTEN_TO_DIRECTION,P.RESPONSIBILITY,P.COMMITMENTS,P.PROBLEM_SOLVING,P.SUGGESTION_IMPROVEMENT,P.IDEA_AND_SOLUTION,P.MEET_CHALLENGES,P.INNOVATIVE_THINKING,P.EMPLOYEE_ID,P.REVIEWED_BY,P.REVIEWED_DATE,P.REVIEW_PERIOD,P.COMMENTS,E.FIRST_NAME,E.LAST_NAME,P.REVIEW_PERIOD_TO,P.EMPLOYEE_SIGNATURE,P.ADMIN_SIGNATURE,P.REVIEW_YEAR,P.REVIEW_QUARTER from performance_review P,employee_details E  WHERE P.EMPLOYEE_ID =? AND P.EMPLOYEE_ID=E.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
            statement2.setString(1, request[2]);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String performanceId;
				 
				 String jobskill;
				 String newskill;
				 String resourseUses;
				 
				 String assignResponsibilities;
				 String meetAttendance;
				 String listenToDirection;
				 String responsibility;
				 
				 
				 String commitments;
				 String problemSolving;
				 String suggestionImprovement;
				 String ideaAndSolution;
				 
				 String meetChallenges;
				 String innovativeThinking;
				 String employeeId;
				 String reviewedBy;
				 
				 String reviewedDate;
				 String reviewPeriod;
				 String comments;
				 String totalWeight;
				 String firstname;
				 String lastname;
				 String reviewPeriodTo;
				 String employeeSignature;
				 String adminSignature;
				 
				 String review_year="";
				 String review_quarter="";
				 
				if(rs.getString(1) != null){
					
					performanceId=rs.getString(1);
				}else {
					
					performanceId= null;
				}
				
				if(rs.getString(2) != null){
					
					jobskill=rs.getString(2);
				}else {
					
					jobskill=  "0";
				}
				if(rs.getString(3) != null){
					
					newskill=rs.getString(3);
				}else {
					
					newskill=  "0";
				}
				
				if(rs.getString(4) != null){
					
					resourseUses=rs.getString(4);
				}else {
					
					resourseUses=  "0";
				}
				
				
				if(rs.getString(5) != null){
					
					assignResponsibilities=rs.getString(5);
				}else {
					
					assignResponsibilities=  "0";
				}
				
				if(rs.getString(6) != null){
					
					meetAttendance=rs.getString(6);
				}else {
					
					meetAttendance=  "0";
				}
				if(rs.getString(7) != null){
					
					listenToDirection=rs.getString(7);
				}else {
					
					listenToDirection=  "0";
				}
				
				if(rs.getString(8) != null){
					
					responsibility=rs.getString(8);
				}else {
					
					responsibility=  "0";
				}
				
				
				
				
				if(rs.getString(9) != null){
					
					commitments=rs.getString(9);
				}else {
					
					commitments=  "0";
				}
				
				if(rs.getString(10) != null){
					
					problemSolving=rs.getString(10);
				}else {
					
					problemSolving=  "0";
				}
				if(rs.getString(11) != null){
					
					suggestionImprovement=rs.getString(11);
				}else {
					
					suggestionImprovement=  "0";
				}
				
				if(rs.getString(12) != null){
					
					ideaAndSolution=rs.getString(12);
				}else {
					
					ideaAndSolution=  "0";
				}
				
				
				

				if(rs.getString(13) != null){
					
					meetChallenges=rs.getString(13);
				}else {
					
					meetChallenges=  "0";
				}
				
				if(rs.getString(14) != null){
					
					innovativeThinking=rs.getString(14);
				}else {
					
					innovativeThinking= "0";
				}
				if(rs.getString(15) != null){
					
					employeeId=rs.getString(15);
				}else {
					
					employeeId= null;
				}
				
				if(rs.getString(16) != null){
					
					reviewedBy=rs.getString(16);
				}else {
					
					reviewedBy= null;
				}
				
				
				if(rs.getString(17) != null){
					reviewedDate= rs.getString(17);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewedDate = sdf.format(cal.getTime());
					
				}else {
					
					reviewedDate= null;
				}
				if(rs.getString(18) != null){
					reviewPeriod= rs.getString(18);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriod));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriod = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriod= null;
				}
				
				
				
				if(rs.getString(19) != null){
					
					comments=rs.getString(19);
				}else {
					
					comments= null;
				}
				
				if(rs.getString(20) != null){
					
					firstname=rs.getString(20);
				}else {
					
					firstname= "";
				}
				if(rs.getString(21) != null){
					
					lastname=rs.getString(21);
				}else {
					
					lastname= "";
				}
				String name=firstname+" "+lastname;
				if(rs.getString(22) != null){
					reviewPeriodTo= rs.getString(22);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriodTo));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriodTo = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriodTo= null;
				}
				
				if(rs.getBlob(23)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(23);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					employeeSignature=str;
				}else {
					
					employeeSignature= null;
				}
				
				if(rs.getBlob(24)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(24);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					adminSignature=str;
				}else {
					
					adminSignature= null;
				}
				
				int total=0;
				 total =Integer.parseInt(jobskill)+Integer.parseInt(newskill)+
						Integer.parseInt(resourseUses)+Integer.parseInt(assignResponsibilities)+
						Integer.parseInt(meetAttendance)+Integer.parseInt(listenToDirection)+
						Integer.parseInt(responsibility)+Integer.parseInt(commitments)+
						
						Integer.parseInt(problemSolving)+Integer.parseInt(suggestionImprovement)+
						Integer.parseInt(ideaAndSolution)+Integer.parseInt(meetChallenges)+
						Integer.parseInt(innovativeThinking);
				
				System.out.println("total .. "+total);
					float tot1=0;
					float tot=0;
					tot1=total;
					tot= (tot1 / 65)*50;
				totalWeight=Float.toString(tot);  
				
				
				 
				if(rs.getString(25) != null){
					
					review_year=rs.getString(25);
				}else {
					
					review_year= "";
				}
				if(rs.getString(26) != null){
					
					review_quarter=rs.getString(26);
				}else {
					
					review_quarter= "";
				}
				
				
				ResponsePerformanceVO mObj = new ResponsePerformanceVO(
						 performanceId,	  jobskill,	  newskill,	  resourseUses,				 
						  assignResponsibilities,  meetAttendance,  listenToDirection, responsibility,
						   commitments,  problemSolving, suggestionImprovement,  ideaAndSolution,
						  meetChallenges, innovativeThinking, employeeId,  reviewedBy,
						  reviewedDate, reviewPeriod, comments,totalWeight,name,reviewPeriodTo,employeeSignature,adminSignature,review_year,review_quarter);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("Performance id  .. "+returJson);
			return returJson;
		
		
	
		
		
		
	}



	public String PostServiceSaveSignAdmin(String[] request) {

		


		List<ResponsePerformanceVO> finalList = new ArrayList<ResponsePerformanceVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		System.out.println(" req  .. "+request);
		PreparedStatement statement1 =null;
		PreparedStatement statement2 =null;
		int row=0;
		try{
			conn = DbServices.getConnection();

			String UPDATE="UPDATE performance_review SET  ADMIN_SIGNATURE =?   WHERE PERFORMANCE_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);
           
            String signature=request[1];
            byte[] b = signature.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(1,blob);
            
            statement1.setString(2, request[0]);
           
            row = statement1.executeUpdate();
			
			if(row>0){
	    	String SELECT="SELECT P.PERFORMANCE_ID,P.JOB_SKILL,P.NEW_SKILL,P.RESOURSE_USES,P.ASSIGN_RESPONSIBILITIES,P.MEET_ATTENDANCE,P.LISTEN_TO_DIRECTION,P.RESPONSIBILITY,P.COMMITMENTS,P.PROBLEM_SOLVING,P.SUGGESTION_IMPROVEMENT,P.IDEA_AND_SOLUTION,P.MEET_CHALLENGES,P.INNOVATIVE_THINKING,P.EMPLOYEE_ID,P.REVIEWED_BY,P.REVIEWED_DATE,P.REVIEW_PERIOD,P.COMMENTS,E.FIRST_NAME,E.LAST_NAME,P.REVIEW_PERIOD_TO,P.EMPLOYEE_SIGNATURE,P.ADMIN_SIGNATURE,P.REVIEW_YEAR,P.REVIEW_QUARTER from performance_review P,employee_details E  WHERE P.PERFORMANCE_ID =? AND P.EMPLOYEE_ID=E.EMPLOYEE_ID";
           	statement2 = conn.prepareStatement(SELECT);
            statement2.setString(1, request[0]);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String performanceId;
				 
				 String jobskill;
				 String newskill;
				 String resourseUses;
				 
				 String assignResponsibilities;
				 String meetAttendance;
				 String listenToDirection;
				 String responsibility;
				 
				 
				 String commitments;
				 String problemSolving;
				 String suggestionImprovement;
				 String ideaAndSolution;
				 
				 String meetChallenges;
				 String innovativeThinking;
				 String employeeId;
				 String reviewedBy;
				 
				 String reviewedDate;
				 String reviewPeriod;
				 String comments;
				 String totalWeight;
				 String firstname;
				 String lastname;
				 String reviewPeriodTo;
				 String employeeSignature;
				 String adminSignature;
				 String review_year="";
				 String review_quarter="";
				if(rs.getString(1) != null){
					
					performanceId=rs.getString(1);
				}else {
					
					performanceId= null;
				}
				
				if(rs.getString(2) != null){
					
					jobskill=rs.getString(2);
				}else {
					
					jobskill=  "0";
				}
				if(rs.getString(3) != null){
					
					newskill=rs.getString(3);
				}else {
					
					newskill=  "0";
				}
				
				if(rs.getString(4) != null){
					
					resourseUses=rs.getString(4);
				}else {
					
					resourseUses=  "0";
				}
				
				
				if(rs.getString(5) != null){
					
					assignResponsibilities=rs.getString(5);
				}else {
					
					assignResponsibilities=  "0";
				}
				
				if(rs.getString(6) != null){
					
					meetAttendance=rs.getString(6);
				}else {
					
					meetAttendance=  "0";
				}
				if(rs.getString(7) != null){
					
					listenToDirection=rs.getString(7);
				}else {
					
					listenToDirection=  "0";
				}
				
				if(rs.getString(8) != null){
					
					responsibility=rs.getString(8);
				}else {
					
					responsibility=  "0";
				}
				
				
				
				
				if(rs.getString(9) != null){
					
					commitments=rs.getString(9);
				}else {
					
					commitments=  "0";
				}
				
				if(rs.getString(10) != null){
					
					problemSolving=rs.getString(10);
				}else {
					
					problemSolving=  "0";
				}
				if(rs.getString(11) != null){
					
					suggestionImprovement=rs.getString(11);
				}else {
					
					suggestionImprovement=  "0";
				}
				
				if(rs.getString(12) != null){
					
					ideaAndSolution=rs.getString(12);
				}else {
					
					ideaAndSolution=  "0";
				}
				
				
				

				if(rs.getString(13) != null){
					
					meetChallenges=rs.getString(13);
				}else {
					
					meetChallenges=  "0";
				}
				
				if(rs.getString(14) != null){
					
					innovativeThinking=rs.getString(14);
				}else {
					
					innovativeThinking= "0";
				}
				if(rs.getString(15) != null){
					
					employeeId=rs.getString(15);
				}else {
					
					employeeId= null;
				}
				
				if(rs.getString(16) != null){
					
					reviewedBy=rs.getString(16);
				}else {
					
					reviewedBy= null;
				}
				
				
				if(rs.getString(17) != null){
					reviewedDate= rs.getString(17);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewedDate));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewedDate = sdf.format(cal.getTime());
					
				}else {
					
					reviewedDate= null;
				}
				if(rs.getString(18) != null){
					reviewPeriod= rs.getString(18);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriod));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriod = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriod= null;
				}
				
				
				
				if(rs.getString(19) != null){
					
					comments=rs.getString(19);
				}else {
					
					comments= null;
				}
				
				if(rs.getString(20) != null){
					
					firstname=rs.getString(20);
				}else {
					
					firstname= "";
				}
				if(rs.getString(21) != null){
					
					lastname=rs.getString(21);
				}else {
					
					lastname= "";
				}
				String name=firstname+" "+lastname;
				if(rs.getString(22) != null){
					reviewPeriodTo= rs.getString(22);
					Calendar cal = Calendar.getInstance();
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(reviewPeriodTo));
					SimpleDateFormat sdf = new SimpleDateFormat(
							"MM/dd/yyyy");
					reviewPeriodTo = sdf.format(cal.getTime());
					
				}else {
					
					reviewPeriodTo= null;
				}
				
				if(rs.getBlob(23)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(23);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					employeeSignature=str;
				}else {
					
					employeeSignature= null;
				}
				
				if(rs.getBlob(24)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(24);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					adminSignature=str;
				}else {
					
					adminSignature= null;
				}
				int total=0;
				 total =Integer.parseInt(jobskill)+Integer.parseInt(newskill)+
						Integer.parseInt(resourseUses)+Integer.parseInt(assignResponsibilities)+
						Integer.parseInt(meetAttendance)+Integer.parseInt(listenToDirection)+
						Integer.parseInt(responsibility)+Integer.parseInt(commitments)+
						
						Integer.parseInt(problemSolving)+Integer.parseInt(suggestionImprovement)+
						Integer.parseInt(ideaAndSolution)+Integer.parseInt(meetChallenges)+
						Integer.parseInt(innovativeThinking);
				
				System.out.println("total .. "+total);
					float tot1=0;
					float tot=0;
					tot1=total;
					tot= (tot1 / 65)*50;
				totalWeight=Float.toString(tot);  
				
				
				 
				if(rs.getString(25) != null){
					
					review_year=rs.getString(25);
				}else {
					
					review_year= "";
				}
				if(rs.getString(26) != null){
					
					review_quarter=rs.getString(26);
				}else {
					
					review_quarter= "";
				}
				
				
				ResponsePerformanceVO mObj = new ResponsePerformanceVO(
						 performanceId,	  jobskill,	  newskill,	  resourseUses,				 
						  assignResponsibilities,  meetAttendance,  listenToDirection, responsibility,
						   commitments,  problemSolving, suggestionImprovement,  ideaAndSolution,
						  meetChallenges, innovativeThinking, employeeId,  reviewedBy,
						  reviewedDate, reviewPeriod, comments,totalWeight,name,reviewPeriodTo,employeeSignature,adminSignature,review_year,review_quarter);
				finalList.add(mObj);
				Gson gs = new Gson();
				returJson = gs.toJson(finalList).toString();

			}
			}
		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("Performance id  .. "+returJson);
			return returJson;
		
		
	
		
		
		
	
	}
	
	
	
	
	

}
