package com.hrsystem.dao;

import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import com.google.gson.Gson;
import com.hrsystem.db.DbServices;
import com.hrsystem.vo.RequestEmployeeCreateVO;
import com.hrsystem.vo.ResponseChartListVO;
import com.hrsystem.vo.ResponseEmployeeLoginVO;

public class AddEmployeeDAO {

	public String PostServiceForAddEmployee(RequestEmployeeCreateVO request) {
		
List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		
		
		//System.out.println("name1"+name);
		try{
			
			if(request.getEmployeeId() == null){
	 String INSERT="INSERT INTO employee_details (FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, request.getFirstName());
            statement1.setString(2, request.getLastName());
            
            String dob = request.getDateOfBirth();

        	SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf5.parse(dob);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqldob = new java.sql.Date(date.getTime());

			statement1.setDate(3, (java.sql.Date) sqldob);
          
            statement1.setString(4, request.getPhoneNumberPrimary());
            statement1.setString(5, request.getPhoneNumberSecondary());
            statement1.setString(6, request.getEmailPrimary());
            statement1.setString(7, request.getEmailSecondary());
            statement1.setString(8,request.getLocation() );
       
            
            String doj = request.getDateOfJoined();
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date1 = null;
			try {date1 = sdf4.parse(doj);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqldoj = new java.sql.Date(date1.getTime());

			statement1.setDate(9, (java.sql.Date) sqldoj);
		    
		  
            statement1.setString(10,request.getClientName() );
            statement1.setString(11,request.getDesignation());
            statement1.setString(12,request.getRole() );
            statement1.setString(13,request.getEmployeePassword());
            statement1.setString(14,request.getEmployeeStatus());
            statement1.setString(15,request.getReportTo());
            statement1.setString(16,request.getPayroll());
            row = statement1.executeUpdate();
 		} 	else {
 			String UPDATE="UPDATE employee_details SET FIRST_NAME =?, LAST_NAME =?,DATE_OF_BIRTH =?, PHONE_NUMBER_PRIMARY =?, PHONE_NUMBER_SECONDARY =?, EMAIL_PRIMARY =?,EMAIL_SECONDARY =?,LOCATION =?,DATE_OF_JOINED =?,CLIENT_NAME =?,DESIGNATION =?,EMPLOYEE_ROLE =?,PASSWORD =?,STATUS =?,REPORT_TO=?,PAYROLL=?  WHERE EMPLOYEE_ID=?";
 			conn = DbServices.getConnection();
            statement1 = conn.prepareStatement(UPDATE);

            statement1.setString(1, request.getFirstName());
            statement1.setString(2, request.getLastName());
            
            String dob = request.getDateOfBirth();
            
            SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf5.parse(dob);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqldob = new java.sql.Date(date.getTime());

			statement1.setDate(3, (java.sql.Date) sqldob);

            
            statement1.setString(4, request.getPhoneNumberPrimary());
            statement1.setString(5, request.getPhoneNumberSecondary());
            statement1.setString(6, request.getEmailPrimary());
            statement1.setString(7, request.getEmailSecondary());
            statement1.setString(8,request.getLocation() );
 
            String doj = request.getDateOfJoined();
			SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date1 = null;
			try {date1 = sdf4.parse(doj);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqldoj = new java.sql.Date(date1.getTime());

			statement1.setDate(9, (java.sql.Date) sqldoj);
			
            
            statement1.setString(10,request.getClientName() );
            statement1.setString(11,request.getDesignation());
            statement1.setString(12,request.getRole() );
            statement1.setString(13,request.getEmployeePassword());
            statement1.setString(14,request.getEmployeeStatus());
            statement1.setString(15,request.getReportTo());
            statement1.setString(16,request.getPayroll());
            statement1.setString(17,request.getEmployeeId());
            
            
            row = statement1.executeUpdate();

 		}
		
		    if (row > 0) {
		    	String SELECT="SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL from employee_details";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String employeeId;
					 String firstName;
					 String lastName;
					 String dateOfBirth;
					 String phoneNumberPrimary;
					 String phoneNumberSecondary;
					 String emailPrimary;
					 String emailSecondary;
					 String location;
					 String dateOfJoined;
					 String clientName;
					 String designation;
					 String role;
					 String employeePassword;
					 String employeeStatus;
					 String reportTo;
					 String payroll="";
					if(rs.getString(1) != null){
						
						employeeId=rs.getString(1);
					}else {
						
						employeeId= null;
					}
					
					if(rs.getString(2) != null){
						
						firstName=rs.getString(2);
					}else {
						
						firstName= "";
					}
					if(rs.getString(3) != null){
						lastName= rs.getString(3);
 
						
					}else {
						
						lastName= "";
					}
					if(rs.getString(4)!= null){
						
						dateOfBirth=rs.getString(4);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfBirth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfBirth = sdf.format(cal.getTime());
					}else {
						
						dateOfBirth= null;
					}
					if(rs.getString(5)!= null){
						
						phoneNumberPrimary= rs.getString(5);
					}else {
						
						phoneNumberPrimary= null;
					}
					if(rs.getString(6)!= null){
						
						phoneNumberSecondary= rs.getString(6);
					}else {
						
						phoneNumberSecondary= null;
					}
					
					if(rs.getString(7)!= null){
						emailPrimary= rs.getString(7);
					}else {
						
						emailPrimary = null;
					}
					
					if(rs.getString(8)!= null){
						
						emailSecondary= rs.getString(8);
					}else {
						
						emailSecondary= null;
					}
					if(rs.getString(9)!= null){
						
						location= rs.getString(9);
					}else {
						
						location= null;
					}
					if(rs.getString(10)!= null){
						
						dateOfJoined= rs.getString(10);
						
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfJoined));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfJoined = sdf.format(cal.getTime());
					}else {
						
						dateOfJoined= null;
					}
					if(rs.getString(11)!= null){
						
						clientName= rs.getString(11);
					}else {
						
						clientName= null;
					}
					if(rs.getString(12)!= null){
						
						designation= rs.getString(12);
					}else {
						
						designation= null;
					}

					if(rs.getString(13)!= null){
	
						role= rs.getString(13);
					}else {
	
						role= null;
					}
					if(rs.getString(14)!= null){
	
						employeePassword= rs.getString(14);
					}else {
	
						employeePassword= null;
					}
					if(rs.getString(15)!= null){
						
						employeeStatus= rs.getString(15);
					}else {
	
						employeeStatus= null;
					}
					String name= firstName+" "+lastName;
					if(rs.getString(16)!= null){
						
						reportTo= rs.getString(16);
					}else {
	
						reportTo= null;
					}
					if(rs.getString(17)!= null){
						
						payroll= rs.getString(17);
					}else {
	
						payroll= null;
					}
					ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
							employeeId,firstName,lastName,dateOfBirth,
							 phoneNumberPrimary,phoneNumberSecondary,emailPrimary,
							 emailSecondary,location,dateOfJoined,clientName,designation,
							 role,employeePassword,employeeStatus,name,reportTo,payroll);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    }
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
	}
	
	
	
	public String GetServiceForEmployeeList1() {
		List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();
				
				Connection conn = null;
		 		ResultSet rs = null;
		 		String returJson = "";
					
				PreparedStatement statement2 =null;
				
				
				try{
							conn = DbServices.getConnection();
					    	String SELECT="SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL from employee_details where EMPLOYEE_ROLE='USER'";
			               	statement2 = conn.prepareStatement(SELECT);
							rs = statement2.executeQuery();
							
							while(rs.next()){
								
								 String employeeId;
								 String firstName;
								 String lastName;
								 String dateOfBirth;
								 String phoneNumberPrimary;
								 String phoneNumberSecondary;
								 String emailPrimary;
								 String emailSecondary;
								 String location;
								 String dateOfJoined;
								 String clientName;
								 String designation;
								 String role;
								 String employeePassword;
								 String employeeStatus;
								 String reportTo;
								  String payroll="";
								if(rs.getString(1) != null){
									
									employeeId=rs.getString(1);
								}else {
									
									employeeId= null;
								}
								
								if(rs.getString(2) != null){
									
									firstName=rs.getString(2);
								}else {
									
									firstName= "";
								}
								if(rs.getString(3) != null){
									lastName= rs.getString(3);
			 
									
								}else {
									
									lastName= "";
								}
								if(rs.getString(4)!= null){
									
									dateOfBirth=rs.getString(4);
									Calendar cal = Calendar.getInstance();
									cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
											.parse(dateOfBirth));
									SimpleDateFormat sdf = new SimpleDateFormat(
											"MM/dd/yyyy");
									dateOfBirth = sdf.format(cal.getTime());
								}else {
									
									dateOfBirth= null;
								}
								if(rs.getString(5)!= null){
									
									phoneNumberPrimary= rs.getString(5);
								}else {
									
									phoneNumberPrimary= null;
								}
								if(rs.getString(6)!= null){
									
									phoneNumberSecondary= rs.getString(6);
								}else {
									
									phoneNumberSecondary= null;
								}
								
								if(rs.getString(7)!= null){
									emailPrimary= rs.getString(7);
								}else {
									
									emailPrimary = null;
								}
								
								if(rs.getString(8)!= null){
									
									emailSecondary= rs.getString(8);
								}else {
									
									emailSecondary= null;
								}
								if(rs.getString(9)!= null){
									
									location= rs.getString(9);
								}else {
									
									location= null;
								}
								if(rs.getString(10)!= null){
									
									dateOfJoined= rs.getString(10);
									Calendar cal = Calendar.getInstance();
									cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
											.parse(dateOfJoined));
									SimpleDateFormat sdf = new SimpleDateFormat(
											"MM/dd/yyyy");
									dateOfJoined = sdf.format(cal.getTime());
								}else {
									
									dateOfJoined= null;
								}
								if(rs.getString(11)!= null){
									
									clientName= rs.getString(11);
								}else {
									
									clientName= null;
								}
								if(rs.getString(12)!= null){
									
									designation= rs.getString(12);
								}else {
									
									designation= null;
								}

								if(rs.getString(13)!= null){
				
									role= rs.getString(13);
								}else {
				
									role= null;
								}
								if(rs.getString(14)!= null){
				
									employeePassword= rs.getString(14);
								}else {
				
									employeePassword= null;
								}
								if(rs.getString(15)!= null){
									
									employeeStatus= rs.getString(15);
								}else {
				
									employeeStatus= null;
								}
								if(rs.getString(16)!= null){
									
									reportTo= rs.getString(16);
								}else {
				
									reportTo= null;
								}
								String name= firstName+" "+lastName;
								
								if(rs.getString(17)!= null){
									
									payroll= rs.getString(17);
								}else {
				
									payroll= null;
								}

								ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
										employeeId,firstName,lastName,dateOfBirth,
										 phoneNumberPrimary,phoneNumberSecondary,emailPrimary,
										 emailSecondary,location,dateOfJoined,clientName,designation,
										 role,employeePassword,employeeStatus,name,reportTo,payroll);
								finalList.add(mObj);
								Gson gs = new Gson();
								returJson = gs.toJson(finalList).toString();

							}
					    
				//	conn.close();

				}catch(Exception e){
					
					e.printStackTrace();
				}finally {
					try {
						
						statement2.close();
						rs.close();
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
					System.out.println("asfasdf"+returJson);
					return returJson;
				
			}
	
	
	
public String GetServiceForEmployeeList() {
List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
			    	String SELECT="SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL from employee_details";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String employeeId;
						 String firstName;
						 String lastName;
						 String dateOfBirth;
						 String phoneNumberPrimary;
						 String phoneNumberSecondary;
						 String emailPrimary;
						 String emailSecondary;
						 String location;
						 String dateOfJoined;
						 String clientName;
						 String designation;
						 String role;
						 String employeePassword;
						 String employeeStatus;
						 String reportTo;
						  String payroll="";
						if(rs.getString(1) != null){
							
							employeeId=rs.getString(1);
						}else {
							
							employeeId= null;
						}
						
						if(rs.getString(2) != null){
							
							firstName=rs.getString(2);
						}else {
							
							firstName= "";
						}
						if(rs.getString(3) != null){
							lastName= rs.getString(3);
	 
							
						}else {
							
							lastName= "";
						}
						if(rs.getString(4)!= null){
							
							dateOfBirth=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(dateOfBirth));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							dateOfBirth = sdf.format(cal.getTime());
						}else {
							
							dateOfBirth= null;
						}
						if(rs.getString(5)!= null){
							
							phoneNumberPrimary= rs.getString(5);
						}else {
							
							phoneNumberPrimary= null;
						}
						if(rs.getString(6)!= null){
							
							phoneNumberSecondary= rs.getString(6);
						}else {
							
							phoneNumberSecondary= null;
						}
						
						if(rs.getString(7)!= null){
							emailPrimary= rs.getString(7);
						}else {
							
							emailPrimary = null;
						}
						
						if(rs.getString(8)!= null){
							
							emailSecondary= rs.getString(8);
						}else {
							
							emailSecondary= null;
						}
						if(rs.getString(9)!= null){
							
							location= rs.getString(9);
						}else {
							
							location= null;
						}
						if(rs.getString(10)!= null){
							
							dateOfJoined= rs.getString(10);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(dateOfJoined));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							dateOfJoined = sdf.format(cal.getTime());
						}else {
							
							dateOfJoined= null;
						}
						if(rs.getString(11)!= null){
							
							clientName= rs.getString(11);
						}else {
							
							clientName= null;
						}
						if(rs.getString(12)!= null){
							
							designation= rs.getString(12);
						}else {
							
							designation= null;
						}

						if(rs.getString(13)!= null){
		
							role= rs.getString(13);
						}else {
		
							role= null;
						}
						if(rs.getString(14)!= null){
		
							employeePassword= rs.getString(14);
						}else {
		
							employeePassword= null;
						}
						if(rs.getString(15)!= null){
							
							employeeStatus= rs.getString(15);
						}else {
		
							employeeStatus= null;
						}
						String name= firstName+" "+lastName;
						if(rs.getString(16)!= null){
							
							reportTo= rs.getString(16);
						}else {
		
							reportTo= null;
						}
						if(rs.getString(17)!= null){
							
							payroll= rs.getString(17);
						}else {
		
							payroll= null;
						}
						ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
								employeeId,firstName,lastName,dateOfBirth,
								 phoneNumberPrimary,phoneNumberSecondary,emailPrimary,
								 emailSecondary,location,dateOfJoined,clientName,designation,
								 role,employeePassword,employeeStatus,name,reportTo,payroll);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
		
	}

public String PostServiceForDeleteEmployee(String request) {


	List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();

	Connection conn = null;
		ResultSet rs = null;
	
	String returJson = "";
	PreparedStatement statement1=null;
	PreparedStatement statement2 =null;
	try{
		String DELETE_EMP="delete from employee_details where EMPLOYEE_ID= ?";
	            conn = DbServices.getConnection();
             statement1 = conn.prepareStatement(DELETE_EMP);
            statement1.setString(1, request);
              
            int row = statement1.executeUpdate();
          
            if (row > 0) {
		    	String SELECT="SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL from employee_details";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String employeeId;
					 String firstName;
					 String lastName;
					 String dateOfBirth;
					 String phoneNumberPrimary;
					 String phoneNumberSecondary;
					 String emailPrimary;
					 String emailSecondary;
					 String location;
					 String dateOfJoined;
					 String clientName;
					 String designation;
					 String role;
					 String employeePassword;
					 String employeeStatus;
					 String reportTo;
					  String payroll="";
					if(rs.getString(1) != null){
						
						employeeId=rs.getString(1);
					}else {
						
						employeeId= null;
					}
					
					if(rs.getString(2) != null){
						
						firstName=rs.getString(2);
					}else {
						
						firstName= "";
					}
					if(rs.getString(3) != null){
						lastName= rs.getString(3);
 
						
					}else {
						
						lastName= "";
					}
					if(rs.getString(4)!= null){
						
						dateOfBirth=rs.getString(4);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfBirth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfBirth = sdf.format(cal.getTime());
					}else {
						
						dateOfBirth= null;
					}
					if(rs.getString(5)!= null){
						
						phoneNumberPrimary= rs.getString(5);
					}else {
						
						phoneNumberPrimary= null;
					}
					if(rs.getString(6)!= null){
						
						phoneNumberSecondary= rs.getString(6);
					}else {
						
						phoneNumberSecondary= null;
					}
					
					if(rs.getString(7)!= null){
						emailPrimary= rs.getString(7);
					}else {
						
						emailPrimary = null;
					}
					
					if(rs.getString(8)!= null){
						
						emailSecondary= rs.getString(8);
					}else {
						
						emailSecondary= null;
					}
					if(rs.getString(9)!= null){
						
						location= rs.getString(9);
					}else {
						
						location= null;
					}
					if(rs.getString(10)!= null){
						
						dateOfJoined= rs.getString(10);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfJoined));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfJoined = sdf.format(cal.getTime());
					}else {
						
						dateOfJoined= null;
					}
					if(rs.getString(11)!= null){
						
						clientName= rs.getString(11);
					}else {
						
						clientName= null;
					}
					if(rs.getString(12)!= null){
						
						designation= rs.getString(12);
					}else {
						
						designation= null;
					}

					if(rs.getString(13)!= null){
	
						role= rs.getString(13);
					}else {
	
						role= null;
					}
					if(rs.getString(14)!= null){
	
						employeePassword= rs.getString(14);
					}else {
	
						employeePassword= null;
					}
					if(rs.getString(15)!= null){
						
						employeeStatus= rs.getString(15);
					}else {
	
						employeeStatus= null;
					}
					String name= firstName+" "+lastName;
					if(rs.getString(16)!= null){
						
						reportTo= rs.getString(16);
					}else {
	
						reportTo= null;
					}
					if(rs.getString(17)!= null){
						
						payroll= rs.getString(17);
					}else {
	
						payroll= null;
					}
					ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
							employeeId,firstName,lastName,dateOfBirth,
							 phoneNumberPrimary,phoneNumberSecondary,emailPrimary,
							 emailSecondary,location,dateOfJoined,clientName,designation,
							 role,employeePassword,employeeStatus,name,reportTo,payroll);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    }
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;

	

}



public String GetServiceForViewEmployee(String request) {
	
	List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson = "";
		
	PreparedStatement statement2 =null;
	
	
	try{
				conn = DbServices.getConnection();
		    	String SELECT="SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL from employee_details WHERE EMPLOYEE_ID='"+request+"'";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String employeeId;
					 String firstName;
					 String lastName;
					 String dateOfBirth;
					 String phoneNumberPrimary;
					 String phoneNumberSecondary;
					 String emailPrimary;
					 String emailSecondary;
					 String location;
					 String dateOfJoined;
					 String clientName;
					 String designation;
					 String role;
					 String employeePassword;
					 String employeeStatus;
					 
					 String reportTo;
					  String payroll="";
					if(rs.getString(1) != null){
						
						employeeId=rs.getString(1);
					}else {
						
						employeeId= null;
					}
					
					if(rs.getString(2) != null){
						
						firstName=rs.getString(2);
					}else {
						
						firstName= "";
					}
					if(rs.getString(3) != null){
						lastName= rs.getString(3);
 
						
					}else {
						
						lastName= "";
					}
					if(rs.getString(4)!= null){
						
						dateOfBirth=rs.getString(4);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfBirth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfBirth = sdf.format(cal.getTime());
						
					}else {
						
						dateOfBirth= null;
					}
					if(rs.getString(5)!= null){
						
						phoneNumberPrimary= rs.getString(5);
					}else {
						
						phoneNumberPrimary= null;
					}
					if(rs.getString(6)!= null){
						
						phoneNumberSecondary= rs.getString(6);
					}else {
						
						phoneNumberSecondary= null;
					}
					
					if(rs.getString(7)!= null){
						emailPrimary= rs.getString(7);
					}else {
						
						emailPrimary = null;
					}
					
					if(rs.getString(8)!= null){
						
						emailSecondary= rs.getString(8);
					}else {
						
						emailSecondary= null;
					}
					if(rs.getString(9)!= null){
						
						location= rs.getString(9);
					}else {
						
						location= null;
					}
					if(rs.getString(10)!= null){
						
						dateOfJoined= rs.getString(10);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfJoined));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfJoined = sdf.format(cal.getTime());
					}else {
						
						dateOfJoined= null;
					}
					if(rs.getString(11)!= null){
						
						clientName= rs.getString(11);
					}else {
						
						clientName= null;
					}
					if(rs.getString(12)!= null){
						
						designation= rs.getString(12);
					}else {
						
						designation= null;
					}

					if(rs.getString(13)!= null){
	
						role= rs.getString(13);
					}else {
	
						role= null;
					}
					if(rs.getString(14)!= null){
	
						employeePassword= rs.getString(14);
					}else {
	
						employeePassword= null;
					}
					if(rs.getString(15)!= null){
						
						employeeStatus= rs.getString(15);
					}else {
	
						employeeStatus= null;
					}
					if(rs.getString(16)!= null){
						
						reportTo= rs.getString(16);
					}else {
	
						reportTo= null;
					}
					String name= firstName+" "+lastName;
					if(rs.getString(17)!= null){
						
						payroll= rs.getString(17);
					}else {
	
						payroll= null;
					}
					ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
							employeeId,firstName,lastName,dateOfBirth,
							 phoneNumberPrimary,phoneNumberSecondary,emailPrimary,
							 emailSecondary,location,dateOfJoined,clientName,designation,
							 role,employeePassword,employeeStatus,name,reportTo,payroll);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("view emp "+returJson);
		return returJson;
}



public String PostServiceForImageUpload(String[] request) {
	Connection conn = null;
		ResultSet rs = null;
		String empImage="";
		String returJson="";
	int row = 0;

	PreparedStatement statement1=null;
	PreparedStatement statement2 =null;
	
	
	try{
		
		
		String UPDATE="UPDATE employee_details SET EMP_PHOTO =?  WHERE EMPLOYEE_ID=?";
		conn = DbServices.getConnection();
        statement1 = conn.prepareStatement(UPDATE);
        
        String image=request[1];
        byte[] b = image.getBytes(StandardCharsets.UTF_8);
        Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
        
        statement1.setBlob(1,blob);
        
        statement1.setString(2, request[0]);
        
        row = statement1.executeUpdate();
        
        if (row > 0) {
	    	String SELECT="SELECT EMP_PHOTO from employee_details where EMPLOYEE_ID='"+request[0]+"'";
           	statement2 = conn.prepareStatement(SELECT);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				 				 
				if(rs.getBlob(1) != null){
					
					empImage=rs.getString(1);
					
					java.sql.Blob ablob = rs.getBlob(1);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					empImage=str;
				}else {
					
					empImage= null;
				}
				
				Gson gs = new Gson();
				 returJson = gs.toJson(empImage).toString();
			}
	    }


}catch(Exception e){
	
	e.printStackTrace();
}finally {
	try {
		statement1.close();
		statement2.close();
		rs.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
	System.out.println("asfasdf"+returJson);
	return returJson;
				
	
}



public String PostServiceForImageUploadViewController(String request) {
	Connection conn = null;
	ResultSet rs = null;
	String empImage="";
	String returJson="";



PreparedStatement statement2 =null;


try{
	String SELECT="SELECT EMP_PHOTO from employee_details where EMPLOYEE_ID='"+request+"'";
	conn = DbServices.getConnection();
   	statement2 = conn.prepareStatement(SELECT);
	rs = statement2.executeQuery();
	
	while(rs.next()){
		 				 
		if(rs.getBlob(1) != null){
			
			empImage=rs.getString(1);
			
			java.sql.Blob ablob = rs.getBlob(1);
			String str = new String(ablob.getBytes(1l, (int) ablob.length()));
			empImage=str;
		}else {
			
			empImage= null;
		}
		
		Gson gs = new Gson();
		 returJson = gs.toJson(empImage).toString();
	}
}catch(Exception e){
	
	e.printStackTrace();
}finally {
	try {
		
		statement2.close();
		rs.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
	System.out.println("asfasdf"+returJson);
	return returJson;
				
}
public String PostServiceForgetEmpName(String request) {
	Connection conn = null;
	ResultSet rs = null;
	
	String returJson="";



PreparedStatement statement2 =null;


try{
	String SELECT="SELECT FIRST_NAME, LAST_NAME from employee_details Where EMPLOYEE_ID='"+request+"'";
	conn = DbServices.getConnection();
   	statement2 = conn.prepareStatement(SELECT);
	rs = statement2.executeQuery();
	
	while(rs.next()){
		 				 
		String firstName;
		 String lastName;
		
		 
		 
		if(rs.getString(1) != null){
			
			firstName=rs.getString(1);
		}else {
			
			firstName= null;
		}
		
		if(rs.getString(2) != null){
			
			lastName=rs.getString(2);
		}else {
			
			lastName= null;
		}
		String Name=firstName+" "+lastName;
		returJson=Name;
	}
}catch(Exception e){
	
	e.printStackTrace();
}finally {
	try {
		
		statement2.close();
		rs.close();
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
	System.out.println("asfasdf"+returJson);
	return returJson;
				
}


public String GetServiceForChartList() {
	

	
	List<ResponseChartListVO> finalList = new ArrayList<ResponseChartListVO>();
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson = "";
		
	PreparedStatement statement2 =null;
	
	
	try{
				conn = DbServices.getConnection();
		    	String SELECT="SELECT FIRST_NAME, LAST_NAME,REPORT_TO from employee_details Where STATUS='Active'";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String firstName;
					 String lastName;
					 String reportTo;
					 
					 
					if(rs.getString(1) != null){
						
						firstName=rs.getString(1);
					}else {
						
						firstName= "";
					}
					
					if(rs.getString(2) != null){
						
						lastName=rs.getString(2);
					}else {
						
						lastName= "";
					}
					if(rs.getString(3) != null){
						String reportTo1= rs.getString(3);
						if((reportTo1 == null)|| (reportTo1=="")){
							reportTo="";
						}else{
						 reportTo =PostServiceForgetEmpName(reportTo1);
						}
					}else {
						
						reportTo= "";
					}
					
					String name= firstName+" "+lastName;
					ResponseChartListVO mObj = new ResponseChartListVO(
							name,reportTo);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("view emp "+returJson);
		return returJson;


}



public String GetServiceForManagerList() {
	List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson = "";
		
	PreparedStatement statement2 =null;
	
	
	try{
				conn = DbServices.getConnection();
		    	String SELECT="SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL from employee_details where EMPLOYEE_ROLE='MANAGER' OR EMPLOYEE_ROLE='ADMIN'";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String employeeId;
					 String firstName;
					 String lastName;
					 String dateOfBirth;
					 String phoneNumberPrimary;
					 String phoneNumberSecondary;
					 String emailPrimary;
					 String emailSecondary;
					 String location;
					 String dateOfJoined;
					 String clientName;
					 String designation;
					 String role;
					 String employeePassword;
					 String employeeStatus;
					 String reportTo;
					  String payroll="";
					  
					if(rs.getString(1) != null){
						
						employeeId=rs.getString(1);
					}else {
						
						employeeId= null;
					}
					
					if(rs.getString(2) != null){
						
						firstName=rs.getString(2);
					}else {
						
						firstName= "";
					}
					if(rs.getString(3) != null){
						lastName= rs.getString(3);
 
						
					}else {
						
						lastName= "";
					}
					if(rs.getString(4)!= null){
						
						dateOfBirth=rs.getString(4);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfBirth));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfBirth = sdf.format(cal.getTime());
					}else {
						
						dateOfBirth= null;
					}
					if(rs.getString(5)!= null){
						
						phoneNumberPrimary= rs.getString(5);
					}else {
						
						phoneNumberPrimary= null;
					}
					if(rs.getString(6)!= null){
						
						phoneNumberSecondary= rs.getString(6);
					}else {
						
						phoneNumberSecondary= null;
					}
					
					if(rs.getString(7)!= null){
						emailPrimary= rs.getString(7);
					}else {
						
						emailPrimary = null;
					}
					
					if(rs.getString(8)!= null){
						
						emailSecondary= rs.getString(8);
					}else {
						
						emailSecondary= null;
					}
					if(rs.getString(9)!= null){
						
						location= rs.getString(9);
					}else {
						
						location= null;
					}
					if(rs.getString(10)!= null){
						
						dateOfJoined= rs.getString(10);
						Calendar cal = Calendar.getInstance();
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
								.parse(dateOfJoined));
						SimpleDateFormat sdf = new SimpleDateFormat(
								"MM/dd/yyyy");
						dateOfJoined = sdf.format(cal.getTime());
					}else {
						
						dateOfJoined= null;
					}
					if(rs.getString(11)!= null){
						
						clientName= rs.getString(11);
					}else {
						
						clientName= null;
					}
					if(rs.getString(12)!= null){
						
						designation= rs.getString(12);
					}else {
						
						designation= null;
					}

					if(rs.getString(13)!= null){
	
						role= rs.getString(13);
					}else {
	
						role= null;
					}
					if(rs.getString(14)!= null){
	
						employeePassword= rs.getString(14);
					}else {
	
						employeePassword= null;
					}
					if(rs.getString(15)!= null){
						
						employeeStatus= rs.getString(15);
					}else {
	
						employeeStatus= null;
					}
					if(rs.getString(16)!= null){
						
						reportTo= rs.getString(16);
					}else {
	
						reportTo= null;
					}
					String name= firstName+" "+lastName;
					if(rs.getString(17)!= null){
						
						payroll= rs.getString(17);
					}else {
	
						payroll= null;
					}
					ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
							employeeId,firstName,lastName,dateOfBirth,
							 phoneNumberPrimary,phoneNumberSecondary,emailPrimary,
							 emailSecondary,location,dateOfJoined,clientName,designation,
							 role,employeePassword,employeeStatus,name,reportTo,payroll);
					finalList.add(mObj);
					Gson gs = new Gson();
					returJson = gs.toJson(finalList).toString();

				}
		    
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		System.out.println("asfasdf"+returJson);
		return returJson;
	
}

}