package com.hrsystem.dao;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.hrsystem.db.DbServices;
import com.hrsystem.email.SimpleSendEmail;
import com.hrsystem.vo.RequestTimeSheetNewVO;
import com.hrsystem.vo.RequestTimesheetVO;
import com.hrsystem.vo.ResponseEmployeeLoginVO;
import com.hrsystem.vo.ResponseLeaveVO;
import com.hrsystem.vo.ResponseStatusReportVo;
import com.hrsystem.vo.ResponseTimesheetVO;

public class TimesheetDAO {

	public String GetServiceForEmployeeListById(String id) {

		List<ResponseEmployeeLoginVO> finalList = new ArrayList<ResponseEmployeeLoginVO>();
		
		Connection conn = null;
 		ResultSet rs = null;
 		String returJson = "";
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
			    	String SELECT="SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME,DATE_OF_BIRTH, PHONE_NUMBER_PRIMARY, PHONE_NUMBER_SECONDARY, EMAIL_PRIMARY,EMAIL_SECONDARY,LOCATION,DATE_OF_JOINED,CLIENT_NAME,DESIGNATION,EMPLOYEE_ROLE,PASSWORD,STATUS,REPORT_TO,PAYROLL from employee_details where EMPLOYEE_ROLE='USER' and EMPLOYEE_ID = '"+id+"' ";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 String employeeId;
						 String firstName;
						 String lastName;
						 String dateOfBirth;
						 String phoneNumberPrimary;
						 String phoneNumberSecondary;
						 String emailPrimary;
						 String emailSecondary;
						 String location;
						 String dateOfJoined;
						 String clientName;
						 String designation;
						 String role;
						 String employeePassword;
						 String employeeStatus;
						 String reportTo;
						 String payroll;
						 
						if(rs.getString(1) != null){
							
							employeeId=rs.getString(1);
						}else {
							
							employeeId= null;
						}
						
						if(rs.getString(2) != null){
							
							firstName=rs.getString(2);
						}else {
							
							firstName= "";
						}
						if(rs.getString(3) != null){
							lastName= rs.getString(3);
	 
							
						}else {
							
							lastName= "";
						}
						if(rs.getString(4)!= null){
							
							dateOfBirth=rs.getString(4);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(dateOfBirth));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							dateOfBirth = sdf.format(cal.getTime());
						}else {
							
							dateOfBirth= null;
						}
						if(rs.getString(5)!= null){
							
							phoneNumberPrimary= rs.getString(5);
						}else {
							
							phoneNumberPrimary= null;
						}
						if(rs.getString(6)!= null){
							
							phoneNumberSecondary= rs.getString(6);
						}else {
							
							phoneNumberSecondary= null;
						}
						
						if(rs.getString(7)!= null){
							emailPrimary= rs.getString(7);
						}else {
							
							emailPrimary = null;
						}
						
						if(rs.getString(8)!= null){
							
							emailSecondary= rs.getString(8);
						}else {
							
							emailSecondary= null;
						}
						if(rs.getString(9)!= null){
							
							location= rs.getString(9);
						}else {
							
							location= null;
						}
						if(rs.getString(10)!= null){
							
							dateOfJoined= rs.getString(10);
							Calendar cal = Calendar.getInstance();
							cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
									.parse(dateOfJoined));
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MM/dd/yyyy");
							dateOfJoined = sdf.format(cal.getTime());
						}else {
							
							dateOfJoined= null;
						}
						if(rs.getString(11)!= null){
							
							clientName= rs.getString(11);
						}else {
							
							clientName= null;
						}
						if(rs.getString(12)!= null){
							
							designation= rs.getString(12);
						}else {
							
							designation= null;
						}

						if(rs.getString(13)!= null){
		
							role= rs.getString(13);
						}else {
		
							role= null;
						}
						if(rs.getString(14)!= null){
		
							employeePassword= rs.getString(14);
						}else {
		
							employeePassword= null;
						}
						if(rs.getString(15)!= null){
							
							employeeStatus= rs.getString(15);
						}else {
		
							employeeStatus= null;
						}
						String name= firstName+" "+lastName;
						
						 if(rs.getString(16)!= null){
								
							 reportTo= rs.getString(16);
							}else {
			
								reportTo= "";
							}
						 if(rs.getString(17)!= null){
								
							 payroll= rs.getString(17);
							}else {
			
								payroll= "";
							}
						
						
						
						ResponseEmployeeLoginVO mObj = new ResponseEmployeeLoginVO(
								employeeId,firstName,lastName,dateOfBirth,
								 phoneNumberPrimary,phoneNumberSecondary,emailPrimary,
								 emailSecondary,location,dateOfJoined,clientName,designation,
								 role,employeePassword,employeeStatus,name,reportTo,payroll);
						finalList.add(mObj);
						Gson gs = new Gson();
						returJson = gs.toJson(finalList).toString();

					}
			    
		//	conn.close();

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
			System.out.println("asfasdf"+returJson);
			return returJson;
		
	}

	

	//XML INSERT/UPDATE/SELECT
	public String PostServiceForNewTimeSheetXML(List<RequestTimeSheetNewVO> request) throws JsonParseException, JsonMappingException, IOException {
		
		String EmpId="";
		String xmldate="";
		List<RequestTimeSheetNewVO> requestTest = new ArrayList<RequestTimeSheetNewVO>();
		for(int i=0;i<request.size();i++){
			
			RequestTimeSheetNewVO request1 = new RequestTimeSheetNewVO();
			RequestTimeSheetNewVO requestFinal = new RequestTimeSheetNewVO();
			request1 =request.get(i);
			requestFinal.setTaskName(request1.getTaskName());
			
			if(request1.getDay1() == null ){
				requestFinal.setDay1("0.00");
			}else{
				requestFinal.setDay1(request1.getDay1());
			}
			if(request1.getDay2() == null ){
				requestFinal.setDay2("0.00");
			}else{
				requestFinal.setDay2(request1.getDay2());
			}
			if(request1.getDay3() == null ){
				requestFinal.setDay3("0.00");
			}else{
				requestFinal.setDay3(request1.getDay3());
			}
			if(request1.getDay4() == null ){
				requestFinal.setDay4("0.00");
			}else{
				requestFinal.setDay4(request1.getDay4());
			}
			if(request1.getDay5() == null ){
				requestFinal.setDay5("0.00");
			}else{
				requestFinal.setDay5(request1.getDay5());
			}
			if(request1.getDay6() == null ){
				requestFinal.setDay6("0.00");
			}else{
				requestFinal.setDay6(request1.getDay6());
			}
			if(request1.getDay7() == null ){
				requestFinal.setDay7("0.00");
			}else{
				requestFinal.setDay7(request1.getDay7());
			}
			if(request1.getDay8() == null ){
				requestFinal.setDay8("0.00");
			}else{
				requestFinal.setDay8(request1.getDay8());
			}
			if(request1.getDay9() == null ){
				requestFinal.setDay9("0.00");
			}else{
				requestFinal.setDay9(request1.getDay9());
			}
			if(request1.getDay10() == null ){
				requestFinal.setDay10("0.00");
			}else{
				requestFinal.setDay10(request1.getDay10());
			}
			
			if(request1.getDay11() == null ){
				requestFinal.setDay11("0.00");
			}else{
				requestFinal.setDay11(request1.getDay11());
			}
			if(request1.getDay12() == null ){
				requestFinal.setDay12("0.00");
			}else{
				requestFinal.setDay12(request1.getDay12());
			}
			if(request1.getDay13() == null ){
				requestFinal.setDay13("0.00");
			}else{
				requestFinal.setDay13(request1.getDay13());
			}
			if(request1.getDay14() == null ){
				requestFinal.setDay14("0.00");
			}else{
				requestFinal.setDay14(request1.getDay14());
			}
			if(request1.getDay15() == null ){
				requestFinal.setDay15("0.00");
			}else{
				requestFinal.setDay15(request1.getDay15());
			}
			if(request1.getDay16() == null ){
				requestFinal.setDay16("0.00");
			}else{
				requestFinal.setDay16(request1.getDay16());
			}
			if(request1.getDay17() == null ){
				requestFinal.setDay17("0.00");
			}else{
				requestFinal.setDay17(request1.getDay17());
			}
			if(request1.getDay18() == null ){
				requestFinal.setDay18("0.00");
			}else{
				requestFinal.setDay18(request1.getDay18());
			}
			if(request1.getDay19() == null ){
				requestFinal.setDay19("0.00");
			}else{
				requestFinal.setDay19(request1.getDay19());
			}
			if(request1.getDay20() == null ){
				requestFinal.setDay20("0.00");
			}else{
				requestFinal.setDay20(request1.getDay20());
			}
			if(request1.getDay21() == null ){
				requestFinal.setDay21("0.00");
			}else{
				requestFinal.setDay21(request1.getDay21());
			}
			if(request1.getDay22() == null ){
				requestFinal.setDay22("0.00");
			}else{
				requestFinal.setDay22(request1.getDay22());
			}
			if(request1.getDay23() == null ){
				requestFinal.setDay23("0.00");
			}else{
				requestFinal.setDay23(request1.getDay23());
			}
			if(request1.getDay24() == null ){
				requestFinal.setDay24("0.00");
			}else{
				requestFinal.setDay24(request1.getDay24());
			}
			if(request1.getDay25() == null ){
				requestFinal.setDay25("0.00");
			}else{
				requestFinal.setDay25(request1.getDay25());
			}
			if(request1.getDay26() == null ){
				requestFinal.setDay26("0.00");
			}else{
				requestFinal.setDay26(request1.getDay26());
			}
			if(request1.getDay27() == null ){
				requestFinal.setDay27("0.00");
			}else{
				requestFinal.setDay27(request1.getDay27());
			}
			if(request1.getDay28() == null ){
				requestFinal.setDay28("0.00");
			}else{
				requestFinal.setDay28(request1.getDay28());
			}
			if(request1.getDay29() == null ){
				requestFinal.setDay29("0.00");
			}else{
				requestFinal.setDay29(request1.getDay29());
			}
			if(request1.getDay30() == null ){
				requestFinal.setDay30("0.00");
			}else{
				requestFinal.setDay30(request1.getDay30());
			}
			if(request1.getDay31() == null ){
				requestFinal.setDay31("0.00");
			}else{
				requestFinal.setDay31(request1.getDay31());
			}
			if(request1.getResourceComments() == null ){
				requestFinal.setResourceComments("");
			}else{
				requestFinal.setResourceComments(request1.getResourceComments());
			}
			if(request1.getProject() == null ){
				requestFinal.setProject("");
			}else{
				requestFinal.setProject(request1.getProject());
			}
			if(request1.getClient() == null ){
				requestFinal.setClient("");
			}else{
				requestFinal.setClient(request1.getClient());
			}
			if(request1.getClientManager() == null ){
				requestFinal.setClientManager("");
			}else{
				requestFinal.setClientManager(request1.getClientManager());
			}
			
			
			
			requestFinal.setEmployeeId(request1.getEmployeeId());
			requestFinal.setTimeSheetMonth(request1.getTimeSheetMonth());
			EmpId=request1.getEmployeeId();
			requestFinal.setStatus(request1.getStatus());
			
			requestTest.add(requestFinal);
			
			
			
			
			
			   // date and format use xml page
	    	
	    	String plannedDate=request1.getTimeSheetMonth();
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf.parse(plannedDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlplannedDate = new java.sql.Date(date.getTime());
		
			
			System.out.println("sqlplannedDate  "+sqlplannedDate );
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
			String strDate = dateFormat.format(sqlplannedDate);  
			
			System.out.println("strDate  "+sqlplannedDate );
			
			String[] splitDate = strDate.split("-");
			
			xmldate=splitDate[0]+"-"+splitDate[1];
			System.out.println("strDate  "+xmldate );
			
			
			
			
		}
		
		// arrraylist to json
		
		Gson gs1 = new Gson();
		String returJson1 = gs1.toJson(requestTest);
		
		System.out.println("returnjson 1"+returJson1 );
		
		
		
	
    	
    	
    	
    	
    	
		    
		 try {
			 
	            DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
	            DocumentBuilder build = dFact.newDocumentBuilder();
	            Document doc = build.newDocument();
	            Element root = doc.createElement("EmployeeDetail");

	            doc.appendChild(root);
	            Element memberList = doc.createElement("EmployeeTimesheet");
	            root.appendChild(memberList);


	        	Element member = doc.createElement("TimesheetDetail");
	        	
	        	 member.setAttribute("Data", returJson1);
	        	 memberList.appendChild(member);
	        	 
	            TransformerFactory tFact = TransformerFactory.newInstance();
	            Transformer trans = tFact.newTransformer();

	             DOMSource source = new DOMSource(doc);

	            StreamResult result = new StreamResult(new File("E:/SarathXML/"+"EMPID"+EmpId+"-"+xmldate+".xml"));
	            trans.transform(source, result);
	 
	            System.out.println("File saved!");

	        } catch (TransformerException ex) {
	            System.out.println("Error outputting document");
	        } catch (ParserConfigurationException ex) {
	            System.out.println("Error building document");
	        }
		
		
		 List readFromJson = null ;
		 try {

	        	File fXmlFile = new File("E:/SarathXML/"+"EMPID"+EmpId+"-"+xmldate+".xml");
	        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        	Document doc = dBuilder.parse(fXmlFile);

	        	//optional, but recommended
	        	//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
	        	doc.getDocumentElement().normalize();

	        	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

	        	NodeList nList = doc.getElementsByTagName("TimesheetDetail");

	        	String OutputXml="";
	        	
	        	for (int temp = 0; temp < nList.getLength(); temp++) {

	        		Element nNode = (Element) nList.item(temp);
	        		

	        		OutputXml =nNode.getAttribute("Data");
	        	
	        		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
	        		
	        		Type listType = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
	            	readFromJson = objGson.fromJson(OutputXml, listType);
	            	
	            	System.out.println("readFromJson 1"+readFromJson );
	            	
	            	System.out.println("return size "+readFromJson.size());
	            	
	            	for(int i = 0; i< readFromJson.size();i++){
	            	
	            	RequestTimeSheetNewVO ff = new RequestTimeSheetNewVO();
	            	ff=(RequestTimeSheetNewVO) readFromJson.get(i);
	            	
	            	System.out.println("----- Task name retived from xml "+ff.getTaskName());
	            	System.out.println("----  day 01 retived from xml "+ff.getDay1());
	            	System.out.println("------client manager retived from xml "+ff.getClientManager());
	            	}
	        	}
	            } catch (Exception e) {
	        	e.printStackTrace();
	            }
		

		
		
		Gson gs = new Gson();
		String returJson = gs.toJson(readFromJson).toString();
		return returJson;
	}

	
	//DB INSERT
public String PostServiceForNewTimeSheet(List<RequestTimeSheetNewVO> request) throws JsonParseException, JsonMappingException, IOException {
		
		String EmpId="";
		String xmldate="";
		String Db_SheetId="";
		String Admin_Status="";
		List<RequestTimeSheetNewVO> requestTest = new ArrayList<RequestTimeSheetNewVO>();
		for(int i=0;i<request.size();i++){
			
			RequestTimeSheetNewVO request1 = new RequestTimeSheetNewVO();
			RequestTimeSheetNewVO requestFinal = new RequestTimeSheetNewVO();
			request1 =request.get(i);
			requestFinal.setTaskName(request1.getTaskName());
			
			
			if(request1.getStatus() == null ){
				Admin_Status="";
			}else{
				Admin_Status=request1.getStatus();
			}
			
			if(request1.getDay1() == null ){
				requestFinal.setDay1("0.00");
			}else{
				requestFinal.setDay1(request1.getDay1());
			}
			if(request1.getDay2() == null ){
				requestFinal.setDay2("0.00");
			}else{
				requestFinal.setDay2(request1.getDay2());
			}
			if(request1.getDay3() == null ){
				requestFinal.setDay3("0.00");
			}else{
				requestFinal.setDay3(request1.getDay3());
			}
			if(request1.getDay4() == null ){
				requestFinal.setDay4("0.00");
			}else{
				requestFinal.setDay4(request1.getDay4());
			}
			if(request1.getDay5() == null ){
				requestFinal.setDay5("0.00");
			}else{
				requestFinal.setDay5(request1.getDay5());
			}
			if(request1.getDay6() == null ){
				requestFinal.setDay6("0.00");
			}else{
				requestFinal.setDay6(request1.getDay6());
			}
			if(request1.getDay7() == null ){
				requestFinal.setDay7("0.00");
			}else{
				requestFinal.setDay7(request1.getDay7());
			}
			if(request1.getDay8() == null ){
				requestFinal.setDay8("0.00");
			}else{
				requestFinal.setDay8(request1.getDay8());
			}
			if(request1.getDay9() == null ){
				requestFinal.setDay9("0.00");
			}else{
				requestFinal.setDay9(request1.getDay9());
			}
			if(request1.getDay10() == null ){
				requestFinal.setDay10("0.00");
			}else{
				requestFinal.setDay10(request1.getDay10());
			}
			
			if(request1.getDay11() == null ){
				requestFinal.setDay11("0.00");
			}else{
				requestFinal.setDay11(request1.getDay11());
			}
			if(request1.getDay12() == null ){
				requestFinal.setDay12("0.00");
			}else{
				requestFinal.setDay12(request1.getDay12());
			}
			if(request1.getDay13() == null ){
				requestFinal.setDay13("0.00");
			}else{
				requestFinal.setDay13(request1.getDay13());
			}
			if(request1.getDay14() == null ){
				requestFinal.setDay14("0.00");
			}else{
				requestFinal.setDay14(request1.getDay14());
			}
			if(request1.getDay15() == null ){
				requestFinal.setDay15("0.00");
			}else{
				requestFinal.setDay15(request1.getDay15());
			}
			if(request1.getDay16() == null ){
				requestFinal.setDay16("0.00");
			}else{
				requestFinal.setDay16(request1.getDay16());
			}
			if(request1.getDay17() == null ){
				requestFinal.setDay17("0.00");
			}else{
				requestFinal.setDay17(request1.getDay17());
			}
			if(request1.getDay18() == null ){
				requestFinal.setDay18("0.00");
			}else{
				requestFinal.setDay18(request1.getDay18());
			}
			if(request1.getDay19() == null ){
				requestFinal.setDay19("0.00");
			}else{
				requestFinal.setDay19(request1.getDay19());
			}
			if(request1.getDay20() == null ){
				requestFinal.setDay20("0.00");
			}else{
				requestFinal.setDay20(request1.getDay20());
			}
			if(request1.getDay21() == null ){
				requestFinal.setDay21("0.00");
			}else{
				requestFinal.setDay21(request1.getDay21());
			}
			if(request1.getDay22() == null ){
				requestFinal.setDay22("0.00");
			}else{
				requestFinal.setDay22(request1.getDay22());
			}
			if(request1.getDay23() == null ){
				requestFinal.setDay23("0.00");
			}else{
				requestFinal.setDay23(request1.getDay23());
			}
			if(request1.getDay24() == null ){
				requestFinal.setDay24("0.00");
			}else{
				requestFinal.setDay24(request1.getDay24());
			}
			if(request1.getDay25() == null ){
				requestFinal.setDay25("0.00");
			}else{
				requestFinal.setDay25(request1.getDay25());
			}
			if(request1.getDay26() == null ){
				requestFinal.setDay26("0.00");
			}else{
				requestFinal.setDay26(request1.getDay26());
			}
			if(request1.getDay27() == null ){
				requestFinal.setDay27("0.00");
			}else{
				requestFinal.setDay27(request1.getDay27());
			}
			if(request1.getDay28() == null ){
				requestFinal.setDay28("0.00");
			}else{
				requestFinal.setDay28(request1.getDay28());
			}
			if(request1.getDay29() == null ){
				requestFinal.setDay29("0.00");
			}else{
				requestFinal.setDay29(request1.getDay29());
			}
			if(request1.getDay30() == null ){
				requestFinal.setDay30("0.00");
			}else{
				requestFinal.setDay30(request1.getDay30());
			}
			if(request1.getDay31() == null ){
				requestFinal.setDay31("0.00");
			}else{
				requestFinal.setDay31(request1.getDay31());
			}
			if(request1.getResourceComments() == null ){
				requestFinal.setResourceComments("");
			}else{
				requestFinal.setResourceComments(request1.getResourceComments());
			}
			if(request1.getProject() == null ){
				requestFinal.setProject("");
			}else{
				requestFinal.setProject(request1.getProject());
			}
			if(request1.getClient() == null ){
				requestFinal.setClient("");
			}else{
				requestFinal.setClient(request1.getClient());
			}
			if(request1.getClientManager() == null ){
				requestFinal.setClientManager("");
			}else{
				requestFinal.setClientManager(request1.getClientManager());
			}
			
			
			
			requestFinal.setEmployeeId(request1.getEmployeeId());
			requestFinal.setTimeSheetMonth(request1.getTimeSheetMonth());
			requestFinal.setSelectMonth(request1.getSelectMonth());
			requestFinal.setSelectYear(request1.getSelectYear());
			EmpId=request1.getEmployeeId();
			requestFinal.setStatus(request1.getStatus());
			
			
			
			
			
			
			
			   // date and format use xml page
	    	
	    	String plannedDate=request1.getTimeSheetMonth();
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = null;
			try {date = sdf.parse(plannedDate);
			} catch (ParseException e) {
				e.printStackTrace();}
			java.sql.Date sqlplannedDate = new java.sql.Date(date.getTime());
		
			
			System.out.println("sqlplannedDate  "+sqlplannedDate );
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
			String strDate = dateFormat.format(sqlplannedDate);  
			
			System.out.println("strDate  "+sqlplannedDate );
			
			String[] splitDate = strDate.split("-");
			
			xmldate=splitDate[0]+"-"+splitDate[1];
			System.out.println("strDate  "+xmldate );
			
			if(request1.getSheetId() == null ){
				Db_SheetId="";
				 Db_SheetId="EMPID"+EmpId+"-"+xmldate;
				 System.out.println("Db_SheetId  "+Db_SheetId );
			}else{
				Db_SheetId=request1.getSheetId();

				 Db_SheetId="EMPID"+EmpId+"-"+xmldate;
				 System.out.println("Db_SheetId  "+Db_SheetId );
			}
			
			requestTest.add(requestFinal);
			System.out.println("Db_SheetId 1  "+Db_SheetId );
		}
		
		// arrraylist to json
		
		Gson gs1 = new Gson();
		String returJson1 = gs1.toJson(requestTest);
		
		System.out.println("returnjson 1"+returJson1 );
		
		
		
    	////////////////////
		
		
		Connection conn = null;
 		ResultSet rs = null;
 		ResultSet rs1 = null;
		int row = 0;
	
		PreparedStatement statement1=null;
		PreparedStatement statement2 =null;
		PreparedStatement statement3 =null;
		List readFromJsonDB = null ;
		List<String> sheetList = new ArrayList<String>();
		try{
			

	    	
			conn = DbServices.getConnection();
	    	String SELECT1="SELECT SHEET_ID from time_sheet  WHERE SHEET_ID='"+ Db_SheetId+"' ";
           	statement3 = conn.prepareStatement(SELECT1);
			rs1 = statement3.executeQuery();
			
			while(rs1.next()){
				
				 String Db_Sheet1;
			
				 
				if(rs1.getString(1) != null){
					
					Db_Sheet1=rs1.getString(1);
				}else {
					
					Db_Sheet1= null;
				}
				
				sheetList.add(Db_Sheet1);
			}
	    
			
			String DBValid="INSERT";
			
			for(int i=0;i<sheetList.size();i++){
				
				if(Db_SheetId.equals(sheetList.get(i))){
					DBValid="UPDATE";
					System.out.println(" update "+Db_SheetId);
					break;
				}else{
					DBValid="INSERT";
					System.out.println(" insert "+Db_SheetId);
				}
			}
			
			
			
			
			if(DBValid.equals("INSERT") ){
				System.out.println("Db_SheetId valid"+Db_SheetId);
				
			String INSERT="INSERT INTO time_sheet (SHEET_ID,TIMESHEET_DATA,STATUS) values (?,?,?)";
         
            statement1 = conn.prepareStatement(INSERT);

            
            statement1.setString(1, Db_SheetId);
            
            
            byte[] b = returJson1.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(2,blob);
           
            statement1.setString(3,Admin_Status);
            
            
            
            row = statement1.executeUpdate();
 		} 	else {
 			   
 			String UPDATE="UPDATE time_sheet SET TIMESHEET_DATA=?,STATUS=? WHERE SHEET_ID=?";
 	          
            statement1 = conn.prepareStatement(UPDATE);

            
            byte[] b = returJson1.getBytes(StandardCharsets.UTF_8);
            Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
            
            statement1.setBlob(1,blob);
           
            statement1.setString(2,Admin_Status);
            
            statement1.setString(3, Db_SheetId);
            
            row = statement1.executeUpdate();

 		}
			
		    if (row > 0) {
		    	
	          
		    	String SELECT="SELECT SHEET_ID,TIMESHEET_DATA,STATUS from time_sheet  WHERE SHEET_ID='"+ Db_SheetId+"' ";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 String Db_SheetId1;
					 String timesheetDate;
					 String status;
					 
					if(rs.getString(1) != null){
						
						Db_SheetId1=rs.getString(1);
					}else {
						
						Db_SheetId1= null;
					}
					
					if(rs.getBlob(2)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(2);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						timesheetDate=str;
					}else {
						
						timesheetDate= null;
					}
					if(rs.getString(3) != null){
						
						status=rs.getString(3);
					}else {
						
						status= null;
					}
					
				    Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		    		
		    		Type listType = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
		        	readFromJsonDB = objGson.fromJson(timesheetDate, listType);
					

				}
		    }
		    
		    
		    
		    
		    
	//	conn.close();

	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			statement1.close();
			statement2.close();
			statement3.close();
			rs.close();
			rs1.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		


		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		////////////////////
		
//		    
//		 try {
//			 
//	            DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
//	            DocumentBuilder build = dFact.newDocumentBuilder();
//	            Document doc = build.newDocument();
//	            Element root = doc.createElement("EmployeeDetail");
//
//	            doc.appendChild(root);
//	            Element memberList = doc.createElement("EmployeeTimesheet");
//	            root.appendChild(memberList);
//
//
//	        	Element member = doc.createElement("TimesheetDetail");
//	        	
//	        	 member.setAttribute("Data", returJson1);
//	        	 memberList.appendChild(member);
//	        	 
//	            TransformerFactory tFact = TransformerFactory.newInstance();
//	            Transformer trans = tFact.newTransformer();
//
//	             DOMSource source = new DOMSource(doc);
//
//	            StreamResult result = new StreamResult(new File("E:/SarathXML/"+"EMPID"+EmpId+"-"+xmldate+".xml"));
//	            trans.transform(source, result);
//	 
//	            System.out.println("File saved!");
//
//	        } catch (TransformerException ex) {
//	            System.out.println("Error outputting document");
//	        } catch (ParserConfigurationException ex) {
//	            System.out.println("Error building document");
//	        }
//		
//		
//		 List readFromJson = null ;
//		 try {
//
//	        	File fXmlFile = new File("E:/SarathXML/"+"EMPID"+EmpId+"-"+xmldate+".xml");
//	        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//	        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//	        	Document doc = dBuilder.parse(fXmlFile);
//
//	        	//optional, but recommended
//	        	//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
//	        	doc.getDocumentElement().normalize();
//
//	        	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
//
//	        	NodeList nList = doc.getElementsByTagName("TimesheetDetail");
//
//	        	String OutputXml="";
//	        	
//	        	for (int temp = 0; temp < nList.getLength(); temp++) {
//
//	        		Element nNode = (Element) nList.item(temp);
//	        		
//
//	        		OutputXml =nNode.getAttribute("Data");
//	        	
//	        		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
//	        		
//	        		Type listType = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
//	            	readFromJson = objGson.fromJson(OutputXml, listType);
//	            	
//	            	System.out.println("readFromJson 1"+readFromJson );
//	            	
//	            	System.out.println("return size "+readFromJson.size());
//	            	
//	            	for(int i = 0; i< readFromJson.size();i++){
//	            	
//	            	RequestTimeSheetNewVO ff = new RequestTimeSheetNewVO();
//	            	ff=(RequestTimeSheetNewVO) readFromJson.get(i);
//	            	
//	            	System.out.println("----- Task name retived from xml "+ff.getTaskName());
//	            	System.out.println("----  day 01 retived from xml "+ff.getDay1());
//	            	System.out.println("------client manager retived from xml "+ff.getClientManager());
//	            	}
//	        	}
//	            } catch (Exception e) {
//	        	e.printStackTrace();
//	            }
//		

		
		
		Gson gs = new Gson();
		
		String returJson = gs.toJson(readFromJsonDB).toString();
		return returJson;
	}

	
	
	
	
	public String GetServiceForGetTimeSheet1(String[] request) {
		
		String xmldate="";
		String EmpId="";
		EmpId=request[1];
		String plannedDate=request[0];
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = null;
		try {date = sdf.parse(plannedDate);
		} catch (ParseException e) {
			e.printStackTrace();}
		java.sql.Date sqlplannedDate = new java.sql.Date(date.getTime());
	
		
		System.out.println("sqlplannedDate  "+sqlplannedDate );
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
		String strDate = dateFormat.format(sqlplannedDate);  
		
		System.out.println("strDate  "+sqlplannedDate );
		
		String[] splitDate = strDate.split("-");
		
		xmldate=splitDate[0]+"-"+splitDate[1];
		System.out.println("strDate  "+xmldate );
		
		List readFromJson = null ;
		
		String returJson="";
		Gson gs1 = new Gson();
    	returJson = gs1.toJson("NOT-AVAILABLE").toString();
		
		
		
		File file = new File("E:/SarathXML/");
	 	String FinalFileName="";
        File[] files = file.listFiles();
        for(File f: files){
            System.out.println("--File Name ----"+f.getName());
            
            
            FinalFileName= "EMPID"+EmpId+"-"+xmldate+".xml";
            
            if(FinalFileName.equals(f.getName())){
            	
            	System.out.println("--Success ----"+f.getName());
            	
            	          	
            	
            	// xml retrival
            	
            	
            	
            	
       		 
            	try {

       	        	File fXmlFile = new File("E:/SarathXML/"+"EMPID"+EmpId+"-"+xmldate+".xml");
       	        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
       	        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
       	        	Document doc = dBuilder.parse(fXmlFile);

       	        	//optional, but recommended
       	        	//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
       	        	doc.getDocumentElement().normalize();

       	        	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

       	        	NodeList nList = doc.getElementsByTagName("TimesheetDetail");

       	        	String OutputXml="";
       	        	
       	        	for (int temp = 0; temp < nList.getLength(); temp++) {

       	        		Element nNode = (Element) nList.item(temp);
       	        		

       	        		OutputXml =nNode.getAttribute("Data");
       	        	
       	        		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
       	        		
       	        		Type listType = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
       	            	readFromJson = objGson.fromJson(OutputXml, listType);
       	            	
       	            	System.out.println("readFromJson 1"+readFromJson );
       	            	
       	            	System.out.println("return size "+readFromJson.size());
       	            	
       	            	for(int i = 0; i< readFromJson.size();i++){
       	            	
       	            	RequestTimeSheetNewVO ff = new RequestTimeSheetNewVO();
       	            	ff=(RequestTimeSheetNewVO) readFromJson.get(i);
       	            	
       	            	System.out.println("----- Task name retived from xml "+ff.getTaskName());
       	            	System.out.println("----  day 01 retived from xml "+ff.getDay1());
       	            	System.out.println("------client manager retived from xml "+ff.getClientManager());
       	            	}
       	        	}
       	            } catch (Exception e) {
       	        	e.printStackTrace();
       	            }	
            	
            	Gson gs = new Gson();
       		 returJson = gs.toJson(readFromJson).toString();
            	
            	break;
            }else{
            	System.out.println("--------No Match--------");
            	
            	
            	returJson = gs1.toJson("NOT-AVAILABLE").toString();
            }
        }
		
	
        
        
        
		
		
		
		return returJson;
	}
public String GetServiceForGetTimeSheet(String[] request) {
		
		String xmldate="";
		String EmpId="";
		EmpId=request[1];
		String plannedDate=request[0];
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = null;
		try {date = sdf.parse(plannedDate);
		} catch (ParseException e) {
			e.printStackTrace();}
		java.sql.Date sqlplannedDate = new java.sql.Date(date.getTime());
	
		
		System.out.println("sqlplannedDate  "+sqlplannedDate );
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
		String strDate = dateFormat.format(sqlplannedDate);  
		
		System.out.println("strDate  "+sqlplannedDate );
		
		String[] splitDate = strDate.split("-");
		
		xmldate=splitDate[0]+"-"+splitDate[1];
		System.out.println("strDate  "+xmldate );
		
		
		
		String returJson="NOT-AVAILABLE";
		Gson gs1 = new Gson();
    	returJson = gs1.toJson("NOT-AVAILABLE").toString();
		
    	String DBSheet_ID="EMPID"+EmpId+"-"+xmldate;

    	
    	List readFromJsonDB = null ;
		
		Connection conn = null;
 		ResultSet rs = null;
 		
			
		PreparedStatement statement2 =null;
		
		
		try{
					conn = DbServices.getConnection();
					String SELECT="SELECT SHEET_ID,TIMESHEET_DATA,STATUS  from time_sheet WHERE SHEET_ID='"+ DBSheet_ID+"' ";
	               	statement2 = conn.prepareStatement(SELECT);
					rs = statement2.executeQuery();
					
					while(rs.next()){
						
						 
						 String timesheetDate;
						 String status1;
						 
						
						
						if(rs.getBlob(2)!= null){
							
							
							java.sql.Blob ablob = rs.getBlob(2);
							String str = new String(ablob.getBytes(1l, (int) ablob.length()));
							timesheetDate=str;
						}else {
							
							timesheetDate= null;
						}
						if(rs.getString(3) != null){
							
							status1=rs.getString(3);
						}else {
							
							status1= null;
						}
						
					    Gson objGson = new GsonBuilder().setPrettyPrinting().create();
			    		
			    		Type listType = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
			        	readFromJsonDB = objGson.fromJson(timesheetDate, listType);
						

					}
			    
	

		}catch(Exception e){
			
			e.printStackTrace();
		}finally {
			try {
				
				statement2.close();
				rs.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	
    	if(readFromJsonDB != null){
    		Gson gs = new Gson();
   		 	returJson = gs.toJson(readFromJsonDB).toString();
    	}else{
    		returJson = gs1.toJson("NOT-AVAILABLE").toString();	
    	}
    	
    	
    	
    	System.out.println("returJson  "+returJson );
    	
    	
    	
    	
//		
//		File file = new File("E:/SarathXML/");
//	 	String FinalFileName="";
//        File[] files = file.listFiles();
//        for(File f: files){
//            System.out.println("--File Name ----"+f.getName());
//            
//            
//            FinalFileName= "EMPID"+EmpId+"-"+xmldate+".xml";
//            
//            if(FinalFileName.equals(f.getName())){
//            	
//            	System.out.println("--Success ----"+f.getName());
//            	
//            	          	
//            	
//            	// xml retrival
//            	
//            	
//            	
//            	
//       		 
//            	try {
//
//       	        	File fXmlFile = new File("E:/SarathXML/"+"EMPID"+EmpId+"-"+xmldate+".xml");
//       	        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//       	        	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//       	        	Document doc = dBuilder.parse(fXmlFile);
//
//       	        	//optional, but recommended
//       	        	//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
//       	        	doc.getDocumentElement().normalize();
//
//       	        	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
//
//       	        	NodeList nList = doc.getElementsByTagName("TimesheetDetail");
//
//       	        	String OutputXml="";
//       	        	
//       	        	for (int temp = 0; temp < nList.getLength(); temp++) {
//
//       	        		Element nNode = (Element) nList.item(temp);
//       	        		
//
//       	        		OutputXml =nNode.getAttribute("Data");
//       	        	
//       	        		Gson objGson = new GsonBuilder().setPrettyPrinting().create();
//       	        		
//       	        		Type listType = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
//       	            	readFromJson = objGson.fromJson(OutputXml, listType);
//       	            	
//       	            	System.out.println("readFromJson 1"+readFromJson );
//       	            	
//       	            	System.out.println("return size "+readFromJson.size());
//       	            	
//       	            	for(int i = 0; i< readFromJson.size();i++){
//       	            	
//       	            	RequestTimeSheetNewVO ff = new RequestTimeSheetNewVO();
//       	            	ff=(RequestTimeSheetNewVO) readFromJson.get(i);
//       	            	
//       	            	System.out.println("----- Task name retived from xml "+ff.getTaskName());
//       	            	System.out.println("----  day 01 retived from xml "+ff.getDay1());
//       	            	System.out.println("------client manager retived from xml "+ff.getClientManager());
//       	            	}
//       	        	}
//       	            } catch (Exception e) {
//       	        	e.printStackTrace();
//       	            }	
//            	
//            	Gson gs = new Gson();
//       		 returJson = gs.toJson(readFromJson).toString();
//            	
//            	break;
//            }else{
//            	System.out.println("--------No Match--------");
//            	
//            	
//            	returJson = gs1.toJson("NOT-AVAILABLE").toString();
//            }
//        }
//		
	
        
        
        
    	
    	
    	
		
		
		
		return returJson;
	}



public String getTimeSheetServiceAdmin(String request) {
	

	List readFromJsonDB = null ;
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson="NOT-AVAILABLE";
		Gson gs1 = new Gson();
    	returJson = gs1.toJson("NOT-AVAILABLE").toString();
    	String sheetIdSql = null;
    	String DBSheet_ID=request;
		
	PreparedStatement statement2 =null;
	
	
	try{
				conn = DbServices.getConnection();
				String SELECT="SELECT SHEET_ID,TIMESHEET_DATA,STATUS  from time_sheet WHERE SHEET_ID='"+ DBSheet_ID+"' ";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 
					 String timesheetDate;
					 String status1;
					 
					 if(rs.getString(1) != null){
							
						 sheetIdSql=rs.getString(1);
						}else {
							
							sheetIdSql= null;
						}
					
					if(rs.getBlob(2)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(2);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						timesheetDate=str;
					}else {
						
						timesheetDate= null;
					}
					if(rs.getString(3) != null){
						
						status1=rs.getString(3);
					}else {
						
						status1= null;
					}
					
				    Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		    		
		    		Type listType = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
		        	readFromJsonDB = objGson.fromJson(timesheetDate, listType);
					

				}
		    


	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	List<RequestTimeSheetNewVO> ListTimeSheet = new ArrayList<RequestTimeSheetNewVO>();
	if(readFromJsonDB != null){
		
		for(int i=0;i<readFromJsonDB.size();i++){
			
			
			RequestTimeSheetNewVO req=new RequestTimeSheetNewVO();
			req=(RequestTimeSheetNewVO) readFromJsonDB.get(i);
			req.setSheetId(sheetIdSql);
			ListTimeSheet.add(req);
		}
		
		
		
		Gson gs = new Gson();
		 	returJson = gs.toJson(ListTimeSheet).toString();
	}else{
		returJson = gs1.toJson("NOT-AVAILABLE").toString();	
	}
	
	
	
	System.out.println("returJson  "+returJson );
	
	return returJson;
}



public String getTimeSheetServiceEmpName(String request) {

	
	String name="";
	
	Connection conn = null;
		ResultSet rs = null;
		String returJson="";
		
    	
		
		
	PreparedStatement statement2 =null;
	
	
	try{
				conn = DbServices.getConnection();
				String SELECT="SELECT FIRST_NAME,LAST_NAME  from employee_details WHERE EMPLOYEE_ID='"+ request+"' ";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 
					 String firstName;
					 String lastName;
					 
					
					
					if(rs.getString(1)!= null){
						
						
						firstName = rs.getString(1);
						
					}else {
						
						firstName= "";
					}
					if(rs.getString(2) != null){
						
						lastName=rs.getString(2);
					}else {
						
						lastName= "";
					}
					
					name= firstName+" "+lastName;
					

				}
		    


	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

		Gson gs = new Gson();
		 	returJson = gs.toJson(name).toString();
	
	
	
	return returJson;

}



public String PostTimseSheetStatusAdmin(String[] request) {

	
	

	String returJson="";
	List readFromJsonDB = null ;
	List<RequestTimeSheetNewVO> ListTimeSheet = new ArrayList<RequestTimeSheetNewVO>();
	Connection conn = null;
		ResultSet rs = null;
		
		
	PreparedStatement statement2 =null;
	
	
	try{
				conn = DbServices.getConnection();
				String SELECT="SELECT SHEET_ID,TIMESHEET_DATA,STATUS  from time_sheet WHERE SHEET_ID='"+ request[0]+"' ";
               	statement2 = conn.prepareStatement(SELECT);
				rs = statement2.executeQuery();
				
				while(rs.next()){
					
					 
					 String timesheetDate;
					 String status1;
					 
					
					
					if(rs.getBlob(2)!= null){
						
						
						java.sql.Blob ablob = rs.getBlob(2);
						String str = new String(ablob.getBytes(1l, (int) ablob.length()));
						timesheetDate=str;
					}else {
						
						timesheetDate= null;
					}
					if(rs.getString(3) != null){
						
						status1=rs.getString(3);
					}else {
						
						status1= null;
					}
					
				    Gson objGson = new GsonBuilder().setPrettyPrinting().create();
		    		
		    		Type listType = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
		        	readFromJsonDB = objGson.fromJson(timesheetDate, listType);
					

				}
		    


	}catch(Exception e){
		
		e.printStackTrace();
	}finally {
		try {
			
			statement2.close();
			rs.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//ListTimeSheet=readFromJsonDB;
	for(int i=0;i<readFromJsonDB.size();i++){
		
		
		RequestTimeSheetNewVO req=new RequestTimeSheetNewVO();
		req=(RequestTimeSheetNewVO) readFromJsonDB.get(i);
		req.setStatus(request[1]);
		ListTimeSheet.add(req);
	}
//	System.out.println(" ===size===   "+ListTimeSheet.size());
//	for(int i=0;i<ListTimeSheet.size();i++){
//		
//		
//		RequestTimeSheetNewVO req=new RequestTimeSheetNewVO();
//		req=(RequestTimeSheetNewVO) ListTimeSheet.get(i);
//		System.out.println("getDay1  "+req.getDay1());
//		System.out.println("getTaskName  "+req.getTaskName());
//	}
	
	
	
	
	
	
	
	Gson gs1 = new Gson();
	String returJson1 = gs1.toJson(ListTimeSheet);
	
	
		ResultSet rs2 = null;
		ResultSet rs1 = null;
	int row = 0;

	PreparedStatement statement1=null;
	
	PreparedStatement statement3 =null;
	List readFromJsonDB1 = null ;
	List<String> sheetList = new ArrayList<String>();
	try{
		

    	
		conn = DbServices.getConnection();
    	   
		String UPDATE="UPDATE time_sheet SET TIMESHEET_DATA=?,STATUS=? WHERE SHEET_ID=?";
	          
        statement1 = conn.prepareStatement(UPDATE);

        
        byte[] b = returJson1.getBytes(StandardCharsets.UTF_8);
        Blob blob = new javax.sql.rowset.serial.SerialBlob(b);
        
        statement1.setBlob(1,blob);
       
        statement1.setString(2,request[1]);
        
        statement1.setString(3, request[0]);
        
        row = statement1.executeUpdate();

		
		
	    if (row > 0) {
	    	
          
	    	String SELECT="SELECT SHEET_ID,TIMESHEET_DATA,STATUS from time_sheet  WHERE SHEET_ID='"+ request[0]+"' ";
           	statement2 = conn.prepareStatement(SELECT);
			rs = statement2.executeQuery();
			
			while(rs.next()){
				
				 String Db_SheetId1;
				 String timesheetDate;
				 String status;
				 
				if(rs.getString(1) != null){
					
					Db_SheetId1=rs.getString(1);
				}else {
					
					Db_SheetId1= null;
				}
				
				if(rs.getBlob(2)!= null){
					
					
					java.sql.Blob ablob = rs.getBlob(2);
					String str = new String(ablob.getBytes(1l, (int) ablob.length()));
					timesheetDate=str;
				}else {
					
					timesheetDate= null;
				}
				if(rs.getString(3) != null){
					
					status=rs.getString(3);
				}else {
					
					status= null;
				}
				
			    Gson objGson1 = new GsonBuilder().setPrettyPrinting().create();
	    		
	    		Type listType1 = new TypeToken<ArrayList<RequestTimeSheetNewVO>>(){}.getType();
	        	readFromJsonDB1 = objGson1.fromJson(timesheetDate, listType1);
				

			}
	    }
	    
	    

}catch(Exception e){
	
	e.printStackTrace();
}finally {
	try {
		statement1.close();
		statement2.close();
		
		rs.close();
		
		conn.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
	


	
	
	
	
	
	
	
	
	
	
	
	Gson gs = new Gson();
		 returJson = gs.toJson(readFromJsonDB1).toString();
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	return returJson;

}

}
