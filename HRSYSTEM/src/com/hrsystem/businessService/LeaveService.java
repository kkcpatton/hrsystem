package com.hrsystem.businessService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.Date;

import com.hrsystem.vo.RequestLeaveVO;

public interface LeaveService {

	String PostServiceForApplyLeave(RequestLeaveVO request)
			throws GeneralSecurityException, IOException, ParseException;

	String GetMethodForLeaveList()
			throws GeneralSecurityException, IOException, ParseException;

 
	String PostMethodForLeaveListById(String request)
			throws GeneralSecurityException, IOException, ParseException;


	String PostMethodForViewLeaveId(String request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostMethodForApproveLeaveId(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostMethodForRejectLeaveId(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostMethodForLeaveEmpId(String request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostMethodForLeaveStatus(String request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostMethodForGetLeaveTotal(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostServiceForApplyLeaveReschedule(RequestLeaveVO request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostMethodForfilterAllEmployeeStatus(String request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostMethodForCancelLeaveId(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostMethodForLeavechangeStatus(String request)
			throws GeneralSecurityException, IOException, ParseException;

 
}
