package com.hrsystem.businessService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import com.hrsystem.vo.RequestEmployeeCreateVO;
import com.hrsystem.vo.RequestStatusReportVO;



public interface AddEmployeeService {

	
	
	public String PostServiceForAddEmployee(RequestEmployeeCreateVO request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForEmployeeList()
			throws GeneralSecurityException, IOException, ParseException;
	
	public String PostServiceForDeleteEmployee(String request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForEmployeeList1()
			throws GeneralSecurityException, IOException, ParseException;
	public String PostServiceForviewEmployee(String request)
			throws GeneralSecurityException, IOException, ParseException;
	public String PostServiceForImageUpload(String[] request)
			throws GeneralSecurityException, IOException, ParseException;
	public String PostServiceForImageUploadViewController(String request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForChartList()
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForManagerList()
			throws GeneralSecurityException, IOException, ParseException;
	
	
}
