package com.hrsystem.businessService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;

import com.hrsystem.vo.RequestTimeSheetNewVO;
import com.hrsystem.vo.RequestTimesheetVO;

public interface TimesheetService {

 
	String GetServiceForEmployeeListById(String id)
			throws GeneralSecurityException, IOException, ParseException;

	String PostServiceForNewTimeSheet(List<RequestTimeSheetNewVO> request)
		throws GeneralSecurityException, IOException, ParseException;

	String GetServiceForGetTimeSheet(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	String getTimeSheetServiceAdmin(String request)
			throws GeneralSecurityException, IOException, ParseException;

	String getTimeSheetServiceEmpName(String request)
			throws GeneralSecurityException, IOException, ParseException;

	String PostTimseSheetStatusAdmin(String[] request)
			throws GeneralSecurityException, IOException, ParseException;





	
}
