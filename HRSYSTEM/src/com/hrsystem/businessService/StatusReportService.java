package com.hrsystem.businessService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;

import com.hrsystem.vo.RequestStatusReportAcceptVO;
import com.hrsystem.vo.RequestStatusReportVO;

public interface StatusReportService {
	public String PostServiceForStatusReport(RequestStatusReportVO request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceForStatusList()
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForGetStatusByEmp(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForStatusReportViewAdmin(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForStatusReportAdminAccept(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceForStatusListAll()
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceForStatusListEmployeeId(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceForStatusListBasedId(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceForMonthlyStatusListBasedId(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceForEmployeeIdPayroll(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForStatusReportNewAct(List<RequestStatusReportVO> request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForDeleteStatusReport(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForStatusReportByMonthEmployee(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForStatusRecentUpdatesGet(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForGetStatusReportReview(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForAcceptStatusAdmin(List<RequestStatusReportAcceptVO> request)
			throws GeneralSecurityException, IOException, ParseException;




	
	}
