package com.hrsystem.businessService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import com.hrsystem.vo.RequestBlogCreateVO;
import com.hrsystem.vo.RequestBlogPostVO;
import com.hrsystem.vo.RequestEmployeeCreateVO;

public interface AddBlogService {
	public String PostServiceForAddBlog(RequestBlogCreateVO request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceForGetBlog()
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForAddBlogPost(RequestBlogPostVO request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceForGetBlogPost()
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForDeleteBlogPost(String request)
			throws GeneralSecurityException, IOException, ParseException;
}
