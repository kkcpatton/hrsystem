package com.hrsystem.businessService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import com.hrsystem.vo.RequestPerformanceVO;
import com.hrsystem.vo.RequestTaskActivityVO;
import com.hrsystem.vo.RequestTaskAssignedVO;

public interface PerformanceReviewService {

	
	public String PostServiceForAddTask(RequestTaskAssignedVO request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForPerformance(RequestPerformanceVO request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForTaskList(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForTaskActivity(RequestTaskActivityVO request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForTaskActivityList(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForViewActivity(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForTaskListAllEmp()
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForActivityListAllEmp()
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForReviewTaskGetActivity(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForReviewTaskGetTask(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForSubmitWeight(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForGetTaskByEmpId(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForgetReviewByEmpId(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceForgetReviewByPerformanceId(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String DeleteServicePeroformanceReviewByPerformanceId(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceEmpPayroll(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String GetServiceReviewViewEmployee(String request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceSaveSignEmp(String[] request)
			throws GeneralSecurityException, IOException, ParseException;

	public String PostServiceSaveSignAdmin(String[] request)
			throws GeneralSecurityException, IOException, ParseException;
}
