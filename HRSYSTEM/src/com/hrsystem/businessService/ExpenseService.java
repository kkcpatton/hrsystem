package com.hrsystem.businessService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import com.hrsystem.vo.RequestExpenseCreateVO;

public interface ExpenseService {

	public String GetServiceForExpenseEmpList()
			throws GeneralSecurityException, IOException, ParseException;
	public String PostServiceForAddExpense(RequestExpenseCreateVO request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForExpenseList(String request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForExpenseListStatus(String[] request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForExpenseList1(String request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForExpenseListView(String request)
			throws GeneralSecurityException, IOException, ParseException;
	public String PostServiceForExpenseUpdateController(String[] request)
			throws GeneralSecurityException, IOException, ParseException;
	public String PostServiceForExpenseStatusController(String[] request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForExpenseListController()
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceForGetNameController(String request)
			throws GeneralSecurityException, IOException, ParseException;
	public String GetServiceFoTotalExpense(String[] request)
			throws GeneralSecurityException, IOException, ParseException;
	public String PostServiceForDeleteExpenseController(String[] request)
			throws GeneralSecurityException, IOException, ParseException;
}
