package com.hrsystem.vo;

public class ResponsePerformanceVO {

	private String performanceId;
	
	private String jobskill;
	private String newskill;
	private String resourseUses;
	private String assignResponsibilities;
	private String meetAttendance;
	private String listenToDirection;
	private String responsibility;
	private String commitments;
	private String problemSolving;
	private String suggestionImprovement;
	private String ideaAndSolution;
	private String meetChallenges;
	private String innovativeThinking;
	
	private String employeeId;
	private String reviewedBy;
	private String reviewedDate;
	private String reviewPeriod;
	private String comments;
	private String totalWeight;
	private String employeeName;
	private String reviewPeriodTo;
	private String empSign;
	private String adminSign;
	private String quarter;
	private String year;
	
	
	public String getQuarter() {
		return quarter;
	}
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getAdminSign() {
		return adminSign;
	}
	public void setAdminSign(String adminSign) {
		this.adminSign = adminSign;
	}
	public String getEmpSign() {
		return empSign;
	}
	public void setEmpSign(String empSign) {
		this.empSign = empSign;
	}
	public String getReviewPeriodTo() {
		return reviewPeriodTo;
	}
	public void setReviewPeriodTo(String reviewPeriodTo) {
		this.reviewPeriodTo = reviewPeriodTo;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}
	public String getPerformanceId() {
		return performanceId;
	}
	public void setPerformanceId(String performanceId) {
		this.performanceId = performanceId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getReviewedBy() {
		return reviewedBy;
	}
	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}
	public String getReviewedDate() {
		return reviewedDate;
	}
	public void setReviewedDate(String reviewedDate) {
		this.reviewedDate = reviewedDate;
	}
	public String getReviewPeriod() {
		return reviewPeriod;
	}
	public void setReviewPeriod(String reviewPeriod) {
		this.reviewPeriod = reviewPeriod;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getJobskill() {
		return jobskill;
	}
	public void setJobskill(String jobskill) {
		this.jobskill = jobskill;
	}
	public String getNewskill() {
		return newskill;
	}
	public void setNewskill(String newskill) {
		this.newskill = newskill;
	}
	public String getResourseUses() {
		return resourseUses;
	}
	public void setResourseUses(String resourseUses) {
		this.resourseUses = resourseUses;
	}
	public String getAssignResponsibilities() {
		return assignResponsibilities;
	}
	public void setAssignResponsibilities(String assignResponsibilities) {
		this.assignResponsibilities = assignResponsibilities;
	}
	public String getMeetAttendance() {
		return meetAttendance;
	}
	public void setMeetAttendance(String meetAttendance) {
		this.meetAttendance = meetAttendance;
	}
	public String getListenToDirection() {
		return listenToDirection;
	}
	public void setListenToDirection(String listenToDirection) {
		this.listenToDirection = listenToDirection;
	}
	public String getResponsibility() {
		return responsibility;
	}
	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}
	public String getCommitments() {
		return commitments;
	}
	public void setCommitments(String commitments) {
		this.commitments = commitments;
	}
	public String getProblemSolving() {
		return problemSolving;
	}
	public void setProblemSolving(String problemSolving) {
		this.problemSolving = problemSolving;
	}
	public String getSuggestionImprovement() {
		return suggestionImprovement;
	}
	public void setSuggestionImprovement(String suggestionImprovement) {
		this.suggestionImprovement = suggestionImprovement;
	}
	public String getIdeaAndSolution() {
		return ideaAndSolution;
	}
	public void setIdeaAndSolution(String ideaAndSolution) {
		this.ideaAndSolution = ideaAndSolution;
	}
	public String getMeetChallenges() {
		return meetChallenges;
	}
	public void setMeetChallenges(String meetChallenges) {
		this.meetChallenges = meetChallenges;
	}
	public String getInnovativeThinking() {
		return innovativeThinking;
	}
	public void setInnovativeThinking(String innovativeThinking) {
		this.innovativeThinking = innovativeThinking;
	}
	

	public ResponsePerformanceVO(String performanceId, 
			String jobskill, String newskill, String resourseUses,
			String assignResponsibilities, String meetAttendance, String listenToDirection, String responsibility,
			String commitments, String problemSolving, String suggestionImprovement, String ideaAndSolution,
			String meetChallenges, String innovativeThinking,String employeeId, String reviewedBy, String reviewedDate,String reviewPeriod, String comments,String totalWeight ,String employeeName,String reviewPeriodTo,String empSign,String adminSign,String year,String quarter) {
		super();
		this.performanceId = performanceId;
		
		this.jobskill = jobskill;
		this.newskill = newskill;
		this.resourseUses = resourseUses;
		this.assignResponsibilities = assignResponsibilities;
		this.meetAttendance = meetAttendance;
		this.listenToDirection = listenToDirection;
		this.responsibility = responsibility;
		this.commitments = commitments;
		this.problemSolving = problemSolving;
		this.suggestionImprovement = suggestionImprovement;
		this.ideaAndSolution = ideaAndSolution;
		this.meetChallenges = meetChallenges;
		this.innovativeThinking = innovativeThinking;
		this.employeeId = employeeId;
		this.reviewedBy = reviewedBy;
		this.reviewedDate = reviewedDate;
		this.reviewPeriod = reviewPeriod;
		this.comments = comments;
		this.totalWeight= totalWeight;
		this.employeeName=employeeName;
		this.reviewPeriodTo=reviewPeriodTo;
		this.empSign=empSign;
		this.adminSign=adminSign;
		this.quarter=quarter;
		this.year=year;
		
	}
	
}
