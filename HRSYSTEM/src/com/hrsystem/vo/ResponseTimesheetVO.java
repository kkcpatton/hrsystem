package com.hrsystem.vo;

public class ResponseTimesheetVO {

	private String employeeId;
	private String timesheetId;
	private String date;
	private String day;
	private String hours;
	private String leaveDetails;
	private String timesheetStatus;
	
	private String clientName;
	private String location;
	private String deskNumber;
	private String supervisor;
	private String designation;
	private String companyName;
	private String firstName;
	
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getTimesheetId() {
		return timesheetId;
	}
	public void setTimesheetId(String timesheetId) {
		this.timesheetId = timesheetId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getLeaveDetails() {
		return leaveDetails;
	}
	public void setLeaveDetails(String leaveDetails) {
		this.leaveDetails = leaveDetails;
	}
 
	
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDeskNumber() {
		return deskNumber;
	}
	public void setDeskNumber(String deskNumber) {
		this.deskNumber = deskNumber;
	}
	public String getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getTimesheetStatus() {
		return timesheetStatus;
	}
	public void setTimesheetStatus(String timesheetStatus) {
		this.timesheetStatus = timesheetStatus;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public ResponseTimesheetVO(String timesheetId,String employeeId,String date, String day, String hours,
			String leaveDetails, String timesheetStatus, String clientName, String location,String deskNumber,
			String supervisor, String designation, String companyName, String firstName){
		super();
		
		this.timesheetId = timesheetId;
		this.employeeId = employeeId;
		this.date = date;
		this.day = day;
		this.hours = hours;
		this.leaveDetails = leaveDetails;
		this.timesheetStatus = timesheetStatus;
		this.clientName = clientName;
		this.location = location;
		this.deskNumber = deskNumber;
		this.supervisor = supervisor;
		this.designation = designation;
		this.companyName = companyName;
		this.firstName = firstName;
	}
}
