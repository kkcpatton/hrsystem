package com.hrsystem.vo;

public class RequestPerformanceVO {

	private String performanceId;
	private String employeeId;
	private String reviewedBy;
	private String reviewedDate;
	private String reviewPeriod;
	private String reviewPeriodTo;
	private String quarter;
	private String year;
	private String comments;
	private String jobskill;
	private String newskill;
	private String resourseUses;
	private String assignResponsibilities;
	private String meetAttendance;
	private String listenToDirection;
	private String responsibility;
	private String commitments;
	private String problemSolving;
	private String suggestionImprovement;
	private String ideaAndSolution;
	private String meetChallenges;
	private String innovativeThinking;
	private String totalWeight;
	
	public String getQuarter() {
		return quarter;
	}
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}
	
	
	public String getPerformanceId() {
		return performanceId;
	}
	public void setPerformanceId(String performanceId) {
		this.performanceId = performanceId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getReviewedBy() {
		return reviewedBy;
	}
	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}
	public String getReviewedDate() {
		return reviewedDate;
	}
	public void setReviewedDate(String reviewedDate) {
		this.reviewedDate = reviewedDate;
	}
	public String getReviewPeriod() {
		return reviewPeriod;
	}
	public void setReviewPeriod(String reviewPeriod) {
		this.reviewPeriod = reviewPeriod;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getJobskill() {
		return jobskill;
	}
	public void setJobskill(String jobskill) {
		this.jobskill = jobskill;
	}
	public String getNewskill() {
		return newskill;
	}
	public void setNewskill(String newskill) {
		this.newskill = newskill;
	}
	public String getResourseUses() {
		return resourseUses;
	}
	public void setResourseUses(String resourseUses) {
		this.resourseUses = resourseUses;
	}
	public String getAssignResponsibilities() {
		return assignResponsibilities;
	}
	public void setAssignResponsibilities(String assignResponsibilities) {
		this.assignResponsibilities = assignResponsibilities;
	}
	public String getMeetAttendance() {
		return meetAttendance;
	}
	public void setMeetAttendance(String meetAttendance) {
		this.meetAttendance = meetAttendance;
	}
	public String getListenToDirection() {
		return listenToDirection;
	}
	public void setListenToDirection(String listenToDirection) {
		this.listenToDirection = listenToDirection;
	}
	public String getResponsibility() {
		return responsibility;
	}
	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}
	public String getCommitments() {
		return commitments;
	}
	public void setCommitments(String commitments) {
		this.commitments = commitments;
	}
	public String getProblemSolving() {
		return problemSolving;
	}
	public void setProblemSolving(String problemSolving) {
		this.problemSolving = problemSolving;
	}
	public String getSuggestionImprovement() {
		return suggestionImprovement;
	}
	public void setSuggestionImprovement(String suggestionImprovement) {
		this.suggestionImprovement = suggestionImprovement;
	}
	public String getIdeaAndSolution() {
		return ideaAndSolution;
	}
	public void setIdeaAndSolution(String ideaAndSolution) {
		this.ideaAndSolution = ideaAndSolution;
	}
	public String getMeetChallenges() {
		return meetChallenges;
	}
	public void setMeetChallenges(String meetChallenges) {
		this.meetChallenges = meetChallenges;
	}
	public String getInnovativeThinking() {
		return innovativeThinking;
	}
	public void setInnovativeThinking(String innovativeThinking) {
		this.innovativeThinking = innovativeThinking;
	}
	
	public String getReviewPeriodTo() {
		return reviewPeriodTo;
	}
	public void setReviewPeriodTo(String reviewPeriodTo) {
		this.reviewPeriodTo = reviewPeriodTo;
	}
	
	
}
