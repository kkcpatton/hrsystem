package com.hrsystem.vo;

public class RequestTaskActivityVO {

	
	private String activityId;
	private String taskId;
	private String employeeId;
	private String taskAssigned;
	private String projectName;
	private String projectDescription;
	private String taskComplete;
	private String issuesCleared;
	
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getTaskAssigned() {
		return taskAssigned;
	}
	public void setTaskAssigned(String taskAssigned) {
		this.taskAssigned = taskAssigned;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public String getTaskComplete() {
		return taskComplete;
	}
	public void setTaskComplete(String taskComplete) {
		this.taskComplete = taskComplete;
	}
	public String getIssuesCleared() {
		return issuesCleared;
	}
	public void setIssuesCleared(String issuesCleared) {
		this.issuesCleared = issuesCleared;
	}
}
