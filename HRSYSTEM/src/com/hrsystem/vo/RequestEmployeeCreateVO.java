package com.hrsystem.vo;

public class RequestEmployeeCreateVO {
	private String employeeId;
	private String firstName;
	private String lastName;
	private String dateOfBirth;
	private String phoneNumberPrimary;
	private String phoneNumberSecondary;
	private String emailPrimary;
	private String emailSecondary;
	private String location;
	private String dateOfJoined;
	private String clientName;
	private String designation;
	private String role;
	private String employeePassword;
	private String name;
	private String reportTo;
	private String payroll;
	
	
	public String getPayroll() {
		return payroll;
	}
	public void setPayroll(String payroll) {
		this.payroll = payroll;
	}
	public String getReportTo() {
		return reportTo;
	}
	public void setReportTo(String reportTo) {
		this.reportTo = reportTo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private String employeeStatus;
	
	public String getEmployeeStatus() {
		return employeeStatus;
	}
	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getPhoneNumberPrimary() {
		return phoneNumberPrimary;
	}
	public void setPhoneNumberPrimary(String phoneNumberPrimary) {
		this.phoneNumberPrimary = phoneNumberPrimary;
	}
	public String getPhoneNumberSecondary() {
		return phoneNumberSecondary;
	}
	public void setPhoneNumberSecondary(String phoneNumberSecondary) {
		this.phoneNumberSecondary = phoneNumberSecondary;
	}
	public String getEmailPrimary() {
		return emailPrimary;
	}
	public void setEmailPrimary(String emailPrimary) {
		this.emailPrimary = emailPrimary;
	}
	public String getEmailSecondary() {
		return emailSecondary;
	}
	public void setEmailSecondary(String emailSecondary) {
		this.emailSecondary = emailSecondary;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDateOfJoined() {
		return dateOfJoined;
	}
	public void setDateOfJoined(String dateOfJoined) {
		this.dateOfJoined = dateOfJoined;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEmployeePassword() {
		return employeePassword;
	}
	public void setEmployeePassword(String employeePassword) {
		this.employeePassword = employeePassword;
	}
}
