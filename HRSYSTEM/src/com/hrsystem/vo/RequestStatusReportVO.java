package com.hrsystem.vo;

public class RequestStatusReportVO {

	
	private String statusId;
	private String employeeId;
	
	
	private String activity;
	private String startPlannedDate;
	private String startActualDate;
	private String finishPlannedDate;
	private String finishEstimatedDate;
	private String status;
	private String clientDetails;
	private String currentProject;
	private String managerName;
	private String currentModule;

	
	private String managerEmail;
	private String managerPhonenumber;
	private String action_status;
	private String statusMonth;
	private String employeeSignature;
	
	
	public String getEmployeeSignature() {
		return employeeSignature;
	}
	public void setEmployeeSignature(String employeeSignature) {
		this.employeeSignature = employeeSignature;
	}
	public String getStatusMonth() {
		return statusMonth;
	}
	public void setStatusMonth(String statusMonth) {
		this.statusMonth = statusMonth;
	}
	public String getAction_status() {
		return action_status;
	}
	public void setAction_status(String action_status) {
		this.action_status = action_status;
	}
	public String getManagerEmail() {
		return managerEmail;
	}
	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}
	
	
	public String getManagerPhonenumber() {
		return managerPhonenumber;
	}
	public void setManagerPhonenumber(String managerPhonenumber) {
		this.managerPhonenumber = managerPhonenumber;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getStartPlannedDate() {
		return startPlannedDate;
	}
	public void setStartPlannedDate(String startPlannedDate) {
		this.startPlannedDate = startPlannedDate;
	}
	public String getStartActualDate() {
		return startActualDate;
	}
	public void setStartActualDate(String startActualDate) {
		this.startActualDate = startActualDate;
	}
	public String getFinishPlannedDate() {
		return finishPlannedDate;
	}
	public void setFinishPlannedDate(String finishPlannedDate) {
		this.finishPlannedDate = finishPlannedDate;
	}
	public String getFinishEstimatedDate() {
		return finishEstimatedDate;
	}
	public void setFinishEstimatedDate(String finishEstimatedDate) {
		this.finishEstimatedDate = finishEstimatedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getClientDetails() {
		return clientDetails;
	}
	public void setClientDetails(String clientDetails) {
		this.clientDetails = clientDetails;
	}
	public String getCurrentProject() {
		return currentProject;
	}
	public void setCurrentProject(String currentProject) {
		this.currentProject = currentProject;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getCurrentModule() {
		return currentModule;
	}
	public void setCurrentModule(String currentModule) {
		this.currentModule = currentModule;
	} 
	
	
	
	
}
