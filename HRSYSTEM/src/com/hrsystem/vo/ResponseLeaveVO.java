package com.hrsystem.vo;

public class ResponseLeaveVO {

	private String leaveId;
	private String employeeId;
	private String name;
	private String designation;
	private String leaveType;
	private String leaveFrom;
	private String leaveTo;
	private String duration;
	private String status;
 
	private String description;
	private String description1;
	private String email;
	
	
	public String getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(String leaveId) {
		this.leaveId = leaveId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public String getLeaveFrom() {
		return leaveFrom;
	}
	public void setLeaveFrom(String leaveFrom) {
		this.leaveFrom = leaveFrom;
	}
	public String getLeaveTo() {
		return leaveTo;
	}
	public void setLeaveTo(String leaveTo) {
		this.leaveTo = leaveTo;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	public String getDescription1() {
		return description1;
	}
	public void setDescription1(String description1) {
		this.description1 = description1;
	}
	 
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ResponseLeaveVO (String leaveId, String employeeId,
	 String name,String designation,String leaveType, String leaveFrom, String leaveTo, String duration,
	 String status,String description,String description1,String email){
		
		super();
		this.leaveId = leaveId;
		this.employeeId = employeeId;
		this.name = name;
		this.designation = designation;
		this.leaveType = leaveType;
		this.leaveFrom = leaveFrom;
		this.leaveTo  = leaveTo;
		this.duration = duration;
		this.status = status;
		this.description  =description;
		this.description1 = description;
		this.email = email;
	}
}
