package com.hrsystem.vo;

import java.util.List;

public class RequestTimesheetListVO {

	
	private List<RequestTimesheetVO> timesheet;
	
	private List<String> TList;
	
	

	public List<String> getTList() {
		return TList;
	}

	public void setTList(List<String> tList) {
		TList = tList;
	}

	public List<RequestTimesheetVO> getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(List<RequestTimesheetVO> timesheet) {
		this.timesheet = timesheet;
	}
	
	
//	@Override
//	public String toString() {
//		return "RequestTimesheetListVO [timesheet=" + timesheet + "]";
//	}
	
}
