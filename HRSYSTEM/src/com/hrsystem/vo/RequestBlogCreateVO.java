package com.hrsystem.vo;

public class RequestBlogCreateVO {
	private String blogId;
	private String blogHeading;
	private String blogContent;
	private String blogEmpId;
	private String blogFlag;
	
	public String getBlogFlag() {
		return blogFlag;
	}
	public void setBlogFlag(String blogFlag) {
		this.blogFlag = blogFlag;
	}
	public String getBlogEmpId() {
		return blogEmpId;
	}
	public void setBlogEmpId(String blogEmpId) {
		this.blogEmpId = blogEmpId;
	}
	public String getBlogId() {
		return blogId;
	}
	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}
	public String getBlogHeading() {
		return blogHeading;
	}
	public void setBlogHeading(String blogHeading) {
		this.blogHeading = blogHeading;
	}
	public String getBlogContent() {
		return blogContent;
	}
	public void setBlogContent(String blogContent) {
		this.blogContent = blogContent;
	}
}
