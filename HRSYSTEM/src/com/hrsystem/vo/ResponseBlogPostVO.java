package com.hrsystem.vo;

public class ResponseBlogPostVO {
	private String postId;
	private String blogId;
	private String postComment;
	private String postEmpId;
	private String postEmpImage;
	private String postDate;
	private String postFlag;
	
	
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getBlogId() {
		return blogId;
	}
	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}
	public String getPostComment() {
		return postComment;
	}
	public void setPostComment(String postComment) {
		this.postComment = postComment;
	}
	public String getPostEmpId() {
		return postEmpId;
	}
	public void setPostEmpId(String postEmpId) {
		this.postEmpId = postEmpId;
	}
	public String getPostEmpImage() {
		return postEmpImage;
	}
	public void setPostEmpImage(String postEmpImage) {
		this.postEmpImage = postEmpImage;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public String getPostFlag() {
		return postFlag;
	}
	public void setPostFlag(String postFlag) {
		this.postFlag = postFlag;
	}
	public ResponseBlogPostVO(String postId, String blogId, String postComment, String postEmpId, String postEmpImage,
			String postDate, String postFlag) {
		super();
		this.postId = postId;
		this.blogId = blogId;
		this.postComment = postComment;
		this.postEmpId = postEmpId;
		this.postEmpImage = postEmpImage;
		this.postDate = postDate;
		this.postFlag = postFlag;
	}
}
