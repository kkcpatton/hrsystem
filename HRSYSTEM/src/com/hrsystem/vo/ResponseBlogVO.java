package com.hrsystem.vo;

public class ResponseBlogVO {

	private String blogId;
	private String blogHeading;
	private String blogContent;
	private String blogDate;
	private String blogEmpId;
	private String blogFlag;
	
	public String getBlogId() {
		return blogId;
	}
	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}
	public String getBlogHeading() {
		return blogHeading;
	}
	public void setBlogHeading(String blogHeading) {
		this.blogHeading = blogHeading;
	}
	public String getBlogContent() {
		return blogContent;
	}
	public void setBlogContent(String blogContent) {
		this.blogContent = blogContent;
	}
	public String getBlogDate() {
		return blogDate;
	}
	public void setBlogDate(String blogDate) {
		this.blogDate = blogDate;
	}
	public String getBlogEmpId() {
		return blogEmpId;
	}
	public void setBlogEmpId(String blogEmpId) {
		this.blogEmpId = blogEmpId;
	}
	public String getBlogFlag() {
		return blogFlag;
	}
	public void setBlogFlag(String blogFlag) {
		this.blogFlag = blogFlag;
	}
	public ResponseBlogVO(String blogId, String blogHeading, String blogContent, String blogDate, String blogEmpId,
			String blogFlag) {
		super();
		this.blogId = blogId;
		this.blogHeading = blogHeading;
		this.blogContent = blogContent;
		this.blogDate = blogDate;
		this.blogEmpId = blogEmpId;
		this.blogFlag = blogFlag;
	}
	
	
}
