package com.hrsystem.vo;

public class RequestExpenseCreateVO {

	private String expenseId;
	private String employeeId;
	private String expenseAccountCode;
	private String reportingDate;
	private String submissionDate;
	private String expenseAmt;
	private String expensememo;
	private String expenseEmpName;
	private String expenseFile;
	private String expenseTotal;
	private String expenseStatus;
	private String expenseNote;
	private String fileType;
	private String fileName;
	private String fileSize;
	
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getExpenseNote() {
		return expenseNote;
	}
	public void setExpenseNote(String expenseNote) {
		this.expenseNote = expenseNote;
	}
	public String getExpenseStatus() {
		return expenseStatus;
	}
	public void setExpenseStatus(String expenseStatus) {
		this.expenseStatus = expenseStatus;
	}
	public String getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(String expenseId) {
		this.expenseId = expenseId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getExpenseAccountCode() {
		return expenseAccountCode;
	}
	public void setExpenseAccountCode(String expenseAccountCode) {
		this.expenseAccountCode = expenseAccountCode;
	}
	public String getReportingDate() {
		return reportingDate;
	}
	public void setReportingDate(String reportingDate) {
		this.reportingDate = reportingDate;
	}
	public String getSubmissionDate() {
		return submissionDate;
	}
	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}
	public String getExpenseAmt() {
		return expenseAmt;
	}
	public void setExpenseAmt(String expenseAmt) {
		this.expenseAmt = expenseAmt;
	}
	public String getExpensememo() {
		return expensememo;
	}
	public void setExpensememo(String expensememo) {
		this.expensememo = expensememo;
	}
	public String getExpenseEmpName() {
		return expenseEmpName;
	}
	public void setExpenseEmpName(String expenseEmpName) {
		this.expenseEmpName = expenseEmpName;
	}
	public String getExpenseFile() {
		return expenseFile;
	}
	public void setExpenseFile(String expenseFile) {
		this.expenseFile = expenseFile;
	}
	public String getExpenseTotal() {
		return expenseTotal;
	}
	public void setExpenseTotal(String expenseTotal) {
		this.expenseTotal = expenseTotal;
	}
	
	
	
	
}
