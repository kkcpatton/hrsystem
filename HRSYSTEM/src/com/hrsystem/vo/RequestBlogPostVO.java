package com.hrsystem.vo;

public class RequestBlogPostVO {

	private String postId;
	private String blogId;
	private String postComment;
	private String postEmpId;
	private String postFlag;
	
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getBlogId() {
		return blogId;
	}
	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}
	public String getPostComment() {
		return postComment;
	}
	public void setPostComment(String postComment) {
		this.postComment = postComment;
	}
	public String getPostEmpId() {
		return postEmpId;
	}
	public void setPostEmpId(String postEmpId) {
		this.postEmpId = postEmpId;
	}
	public String getPostFlag() {
		return postFlag;
	}
	public void setPostFlag(String postFlag) {
		this.postFlag = postFlag;
	}
	
	
}
