package com.hrsystem.vo;

public class RequestStatusReportAcceptVO {
	private String statusId;
	private String adminSign;
	private String employeeId;
	
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getAdminSign() {
		return adminSign;
	}
	public void setAdminSign(String adminSign) {
		this.adminSign = adminSign;
	}
	
}
