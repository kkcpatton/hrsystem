package com.hrsystem.vo;

public class ResponseChartListVO {

	private String userName;
	private String reportingTo;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getReportingTo() {
		return reportingTo;
	}
	public void setReportingTo(String reportingTo) {
		this.reportingTo = reportingTo;
	}
	
	public ResponseChartListVO(String userName, String reportingTo) {
		super();
		this.userName = userName;
		this.reportingTo = reportingTo;
	}
	
}
