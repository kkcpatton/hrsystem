package com.hrsystem.vo;

public class RequestTaskAssignedVO {
	
	private String taskId;
	private String employeeId;
	private String taskAssigned;
	private String taskWeight;
	private String weightAchieved;
	
	public String getWeightAchieved() {
		return weightAchieved;
	}
	public void setWeightAchieved(String weightAchieved) {
		this.weightAchieved = weightAchieved;
	}
	public String getTaskWeight() {
		return taskWeight;
	}
	public void setTaskWeight(String taskWeight) {
		this.taskWeight = taskWeight;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getTaskAssigned() {
		return taskAssigned;
	}
	public void setTaskAssigned(String taskAssigned) {
		this.taskAssigned = taskAssigned;
	}
}
