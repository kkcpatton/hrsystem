package com.hrsystem.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hrsystem.businessService.AddEmployeeService;
import com.hrsystem.businessService.ExpenseService;
import com.hrsystem.svc.AddEmployeeBusinessSvc;
import com.hrsystem.svc.ExpenseBusinessSvc;
import com.hrsystem.vo.RequestEmployeeCreateVO;
import com.hrsystem.vo.RequestExpenseCreateVO;

@RestController
public class ExpenseController {


	@RequestMapping(value = "/expenseemplist", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForExpenseEmpListController()
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.GetServiceForExpenseEmpList();
	
		return returnJson;
	 
	}	
	@RequestMapping(value = "/addexpense", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForAddExpenseController(@RequestBody RequestExpenseCreateVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.PostServiceForAddExpense(request);
	System.out.println("amt : "+request.getExpenseAmt());
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/expenselist", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForExpenseListController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.GetServiceForExpenseList(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/expenselistId", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForExpenseListController1(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.GetServiceForExpenseList1(request);
	
		return returnJson;
	 
	}
	
	
	
	@RequestMapping(value = "/filterstatus", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForExpenseListController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.GetServiceForExpenseListStatus(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/expenseIdView", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForExpenseListControllerView(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.GetServiceForExpenseListView(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/submitview", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForExpenseUpdateController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.PostServiceForExpenseUpdateController(request);
	
		return returnJson;
	 
	}
	
	
	
	@RequestMapping(value = "/filterstatusAdmin", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForExpenseStatusController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.PostServiceForExpenseStatusController(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/expenselistadmin", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForExpenseListController()
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.GetServiceForExpenseListController();
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/getName", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForGetNameController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.GetServiceForGetNameController(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/totalExpensefilter", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceFoTotalExpenseController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.GetServiceFoTotalExpense(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/deleteExpense", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForDeleteExpenseController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		ExpenseService CDAO = new ExpenseBusinessSvc();
		String returnJson = CDAO.PostServiceForDeleteExpenseController(request);
	
		return returnJson;
	 
	}
	
}
