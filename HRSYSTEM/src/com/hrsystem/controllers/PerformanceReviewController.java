package com.hrsystem.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hrsystem.businessService.ExpenseService;
import com.hrsystem.businessService.PerformanceReviewService;
import com.hrsystem.svc.ExpenseBusinessSvc;
import com.hrsystem.svc.PerformanceReviewBusinessSvc;
import com.hrsystem.vo.RequestPerformanceVO;
import com.hrsystem.vo.RequestTaskActivityVO;
import com.hrsystem.vo.RequestTaskAssignedVO;

@RestController
public class PerformanceReviewController {

	
	
	@RequestMapping(value = "/addTask", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForAddExpenseController(@RequestBody RequestTaskAssignedVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForAddTask(request);
	
		return returnJson;
	 
	}
	
	
	
	@RequestMapping(value = "/addperformanceEmp", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForAddExpenseController(@RequestBody RequestPerformanceVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForPerformance(request);
	System.out.println("amt : "+request.getIdeaAndSolution());
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/taskList", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForTaskListController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForTaskList(request);
	
		return returnJson;
	 
	}	
	
	@RequestMapping(value = "/taskActivity", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForTaskActivityController(@RequestBody RequestTaskActivityVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForTaskActivity(request);
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/taskActivityList", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForTaskActivityListController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForTaskActivityList(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/viewActivity", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForViewActivityController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForViewActivity(request);
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/taskListAllEmp", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForTaskListAllEmpController()
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForTaskListAllEmp();
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/activityListAllEmp", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForActivityListAllEmpController()
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForActivityListAllEmp();
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/ReviewTask", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForReviewTaskGetActivityController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForReviewTaskGetActivity(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/ReviewTask1", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForReviewTaskGetTaskController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForReviewTaskGetTask(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/submitweight", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForReviewTaskGetTaskController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForSubmitWeight(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/getTaskByEmpId", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForgetTaskByEmpIdController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForGetTaskByEmpId(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/getReviewByEmpId", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForgetReviewByEmpIdController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForgetReviewByEmpId(request);
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/getReviewByPerformanceId", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForgetReviewByPerformanceIdController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceForgetReviewByPerformanceId(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/DeleteReviewById", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String DeleteServicePeroformanceReviewByPerformanceId(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.DeleteServicePeroformanceReviewByPerformanceId(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/getEmpPayroll", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceEmpPayrollController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.GetServiceEmpPayroll(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/GetReviewViewEmp", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceReviewViewEmployeeController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.GetServiceReviewViewEmployee(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/saveSignEmp", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServicesaveSignEmpController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceSaveSignEmp(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/saveSignAdmin", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServicesaveSignAdminController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		PerformanceReviewService CDAO = new PerformanceReviewBusinessSvc();
		String returnJson = CDAO.PostServiceSaveSignAdmin(request);
	
		return returnJson;
	 
	}
	
}
