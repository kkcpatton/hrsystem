package com.hrsystem.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.hrsystem.businessService.AddEmployeeService;
import com.hrsystem.businessService.StatusReportService;
import com.hrsystem.svc.AddEmployeeBusinessSvc;
import com.hrsystem.svc.StatusReportBusinessSvc;
import com.hrsystem.vo.RequestStatusReportAcceptVO;
import com.hrsystem.vo.RequestStatusReportVO;
import com.hrsystem.vo.ResponseStatusReportVo;


@RestController
public class StatusReportController {
	@Autowired
	private HttpServletRequest request;
	
	@RequestMapping(value = "/statusreport", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForStatusReportController(@RequestBody RequestStatusReportVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForStatusReport(request);
	
		return returnJson;
	 
	}
	
	
	
	
	@RequestMapping(value = "/statuslist", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForStatusListController()
			throws GeneralSecurityException, IOException, ParseException, InterruptedException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.GetServiceForStatusList();
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/statuslistEmp", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForStatusReportController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForGetStatusByEmp(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/statusviewAdmin", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForStatusReportViewAdminController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForStatusReportViewAdmin(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/statusAdminAccept", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForStatusReportAdminAcceptController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		System.out.println("Accept  "+request);
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForStatusReportAdminAccept(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/viewAllList", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForStatusListAllController()
			throws GeneralSecurityException, IOException, ParseException, InterruptedException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.GetServiceForStatusListAll();
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/statusListEmpId", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForStatusListEmployeeId(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.GetServiceForStatusListEmployeeId(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/statusfilter", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForStatusListBasedId(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.GetServiceForStatusListBasedId(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/monthlyStatusReport", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForMonthlyStatusListBasedId(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.GetServiceForMonthlyStatusListBasedId(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/getEmpPayrollStatus", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForEmployeeIdPayroll(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.GetServiceForEmployeeIdPayroll(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/StatusReportNew", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForStatusReportNewAct(@RequestBody List<RequestStatusReportVO> request)
			throws GeneralSecurityException, IOException, ParseException {
		
		
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForStatusReportNewAct(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/DeleteStatusReport", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForDeleteStatusReport(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForDeleteStatusReport(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/getStatusReportByMonthEmp", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForStatusReportByMonthEmployee(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForStatusReportByMonthEmployee(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/statusRecentUpdatesGet", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForStatusRecentUpdatesGet(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForStatusRecentUpdatesGet(request);
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/getStatusReportReview", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForGetStatusReportReview(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForGetStatusReportReview(request);
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/AcceptStatusAdmin", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForAcceptStatusAdmin(@RequestBody List<RequestStatusReportAcceptVO> request)
			throws GeneralSecurityException, IOException, ParseException {
//		RequestStatusReportAcceptVO request1;
//		for(int i=0;i<request.size();i++){
//			
//			request1=request.get(i);
//			System.out.println("admin sign "+request1.getAdminSign());
//			System.out.println("admin sign "+request1.getStatusId());
//		}
		StatusReportService CDAO = new StatusReportBusinessSvc();
		String returnJson = CDAO.PostServiceForAcceptStatusAdmin(request);
		//String returnJson="";
		return returnJson;
	 
	}
}
