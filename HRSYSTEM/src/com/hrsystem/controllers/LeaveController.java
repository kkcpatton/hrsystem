package com.hrsystem.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hrsystem.businessService.LeaveService;

import com.hrsystem.svc.LeaveBusinessSvc;

import com.hrsystem.vo.RequestLeaveVO;


@RestController

public class LeaveController {


	 private HttpServletRequest request;
		
		@RequestMapping(value = "/applyLeave", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostServiceForApplyLeave(@RequestBody RequestLeaveVO request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostServiceForApplyLeave(request);
		
			return returnJson;
		 
		}
		

		@RequestMapping(value = "/rescheduleLeave", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostServiceForApplyLeaveReschedule(@RequestBody RequestLeaveVO request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostServiceForApplyLeaveReschedule(request);
		
			return returnJson;
		 
		}
	 	
		
		@RequestMapping(value = "/leaveList", method = RequestMethod.GET)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String GetMethodForLeaveList()
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.GetMethodForLeaveList();
		
			return returnJson;
		 
		}
		
		 
		@RequestMapping(value = "/getListById", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForLeaveListById(@RequestBody String request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForLeaveListById(request);
		
			return returnJson;
		 
		}
	
		@RequestMapping(value = "/changeStatus", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForLeavechangeStatus(@RequestBody String request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForLeavechangeStatus(request);
		
			return returnJson;
		 
		}
	
		
		@RequestMapping(value = "/viewLeave", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForViewLeaveId(@RequestBody String request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForViewLeaveId(request);
		
			return returnJson;
		 
		}
		
		
		@RequestMapping(value = "/approveLeave", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForApproveLeave(@RequestBody String[] request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForApproveLeaveId(request);
		
			return returnJson;
		 
		}
	
		
		@RequestMapping(value = "/rejectLeave", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForRejectLeave(@RequestBody String[] request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForRejectLeaveId(request);
		
			return returnJson;
		 
		}

		
		@RequestMapping(value = "/cancel", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForCancelLeave(@RequestBody String[] request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForCancelLeaveId(request);
		
			return returnJson;
		 
		}

		
		@RequestMapping(value = "/filterEmpId", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForLeaveEmpId(@RequestBody String request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForLeaveEmpId(request);
		
			return returnJson;
		 
		}
	
		
		@RequestMapping(value = "/filterAllEmployeeStatus", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForfilterAllEmployeeStatus(@RequestBody String request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForfilterAllEmployeeStatus(request);
		
			return returnJson;
		 
		}
	
		
		@RequestMapping(value = "/filterStatus", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForLeaveStatus(@RequestBody String request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForLeaveStatus(request);
		
			return returnJson;
		 
		}
	
		
		@RequestMapping(value = "/getTotal", method = RequestMethod.POST)
		@ResponseStatus(value = HttpStatus.OK)
		public @ResponseBody
		String PostMethodForGetLeaveTotal(@RequestBody String[] request)
				throws GeneralSecurityException, IOException, ParseException {
			
			LeaveService CDAO = new LeaveBusinessSvc();
			String returnJson = CDAO.PostMethodForGetLeaveTotal(request);
		
			return returnJson;
		 
		}

		
}
