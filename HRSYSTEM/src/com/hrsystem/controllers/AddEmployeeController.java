package com.hrsystem.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hrsystem.businessService.AddEmployeeService;
import com.hrsystem.svc.AddEmployeeBusinessSvc;
import com.hrsystem.vo.RequestEmployeeCreateVO;



@RestController
public class AddEmployeeController {

	
	private HttpServletRequest request;
	
	@RequestMapping(value = "/addemployee", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForAddEmployeeController(@RequestBody RequestEmployeeCreateVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.PostServiceForAddEmployee(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/employeelist", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForEmployeeListController()
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.GetServiceForEmployeeList();
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/employeeDelete", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForClientsToHandleController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.PostServiceForDeleteEmployee(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/employeelist1", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForEmployeeListController1()
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.GetServiceForEmployeeList1();
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/employeeView", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForViewEmployeeController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.PostServiceForviewEmployee(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/imageUpload", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForImageUploadController(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.PostServiceForImageUpload(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/viewProfileImage", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForImageUploadViewController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.PostServiceForImageUploadViewController(request);
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/chartlist", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForChartListController()
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.GetServiceForChartList();
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/managerlist", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForManagerListController()
			throws GeneralSecurityException, IOException, ParseException {
		
		AddEmployeeService CDAO = new AddEmployeeBusinessSvc();
		String returnJson = CDAO.GetServiceForManagerList();
	
		return returnJson;
	 
	}
	
}
