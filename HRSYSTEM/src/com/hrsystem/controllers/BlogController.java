package com.hrsystem.controllers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;



import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hrsystem.businessService.AddBlogService;

import com.hrsystem.svc.AddBlogBusinessSvc;

import com.hrsystem.vo.RequestBlogCreateVO;
import com.hrsystem.vo.RequestBlogPostVO;



@RestController
public class BlogController {

	
	@RequestMapping(value = "/addBlog", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForAddBlogController(@RequestBody RequestBlogCreateVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		AddBlogService CDAO =  new AddBlogBusinessSvc();
		String returnJson = CDAO.PostServiceForAddBlog(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/bloglist", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForBlogListController()
			throws GeneralSecurityException, IOException, ParseException {
		
		AddBlogService CDAO =  new AddBlogBusinessSvc();
		String returnJson = CDAO.GetServiceForGetBlog();
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/addPostComment", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForAddBlogPostController(@RequestBody RequestBlogPostVO request)
			throws GeneralSecurityException, IOException, ParseException {
		
		AddBlogService CDAO =  new AddBlogBusinessSvc();
		String returnJson = CDAO.PostServiceForAddBlogPost(request);
	
		return returnJson;
	 
	}
	
	@RequestMapping(value = "/blogPostList", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForBlogPostListController()
			throws GeneralSecurityException, IOException, ParseException {
		
		AddBlogService CDAO =  new AddBlogBusinessSvc();
		String returnJson = CDAO.GetServiceForGetBlogPost();
	
		return returnJson;
	 
	}
	@RequestMapping(value = "/postDelete", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForAddBlogPostController(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {
		
		AddBlogService CDAO =  new AddBlogBusinessSvc();
		String returnJson = CDAO.PostServiceForDeleteBlogPost(request);
	
		return returnJson;
	 
	}
}
