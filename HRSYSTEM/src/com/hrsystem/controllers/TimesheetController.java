package com.hrsystem.controllers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import com.google.gson.Gson;
import com.hrsystem.businessService.TimesheetService;
import com.hrsystem.svc.TimesheetBusinessSvc;
import com.hrsystem.vo.RequestTimeSheetNewVO;
import com.hrsystem.vo.RequestTimesheetListVO;
import com.hrsystem.vo.RequestTimesheetVO;
import com.hrsystem.vo.ResponseTimesheetVO;

@RestController
public class TimesheetController {

	
	@RequestMapping(value = "/getEmpDetailsById", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String GetServiceForEmployeeListByIdController1(@RequestBody String id)
			throws GeneralSecurityException, IOException, ParseException {
//		System.out.println("method inside");
		TimesheetService CDAO = new TimesheetBusinessSvc();
		String returnJson = CDAO.GetServiceForEmployeeListById(id);
	
		return returnJson;
	 
	}

	@RequestMapping(value = "/newTimeSheet", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForNewTimeSheet(@RequestBody List<RequestTimeSheetNewVO> request)
			throws GeneralSecurityException, IOException, ParseException {

		TimesheetService CDAO = new TimesheetBusinessSvc();
		String returnJson = CDAO.PostServiceForNewTimeSheet(request);
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/getTimeSheet", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostServiceForGetTimeSheet(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {

		TimesheetService CDAO = new TimesheetBusinessSvc();
		String returnJson = CDAO.GetServiceForGetTimeSheet(request);
	
		return returnJson;
	 
	}
	
	
	@RequestMapping(value = "/getTimeSheetAdmin", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String getTimseSheetServiceAdmin(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {

		TimesheetService CDAO = new TimesheetBusinessSvc();
		String returnJson = CDAO.getTimeSheetServiceAdmin(request);
	
		return returnJson;
	 
	}
	
	
	

	@RequestMapping(value = "/postTimeSheetStatus", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String PostTimseSheetStatusAdmin(@RequestBody String[] request)
			throws GeneralSecurityException, IOException, ParseException {

		TimesheetService CDAO = new TimesheetBusinessSvc();
		String returnJson = CDAO.PostTimseSheetStatusAdmin(request);
	
		return returnJson;
	 
	}
	
	
	
	
	
	@RequestMapping(value = "/getTimeSheetEmpName", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody
	String getTimeSheetServiceEmpName(@RequestBody String request)
			throws GeneralSecurityException, IOException, ParseException {

		TimesheetService CDAO = new TimesheetBusinessSvc();
		String returnJson = CDAO.getTimeSheetServiceEmpName(request);
	
		return returnJson;
	 
	}
}
