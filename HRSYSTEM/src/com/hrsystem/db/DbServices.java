package com.hrsystem.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class DbServices {
	static Connection dbConnection=null;
	public static Connection getConnection()
	{
		try {

			Class.forName("com.mysql.jdbc.Driver");			
				
				dbConnection = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/hrpatton","root","root");
			
		} catch (Exception exc_conn) {
			System.out.println("Entity Manager:Get Connection method : Exception Occured in Open statement :"
							+ exc_conn);
		}
		return dbConnection;

	}

	public static final void closeDBConnection(Connection dbConnection) {
		try {
			// Close the connection only if the object is not null
			if (dbConnection != null) {
				dbConnection.close();
			}
		} catch (Exception exc_close) {
			System.out.println("DBService : Exception Occured in close statement :"
					+ exc_close);
		} finally {
			dbConnection = null;
		}
	}
	public static final void closeResultSet(ResultSet resultSet) {
		try {
			// Close the connection only if the object is not null
			if (resultSet!= null) {
				resultSet.close();
			}
		} catch (Exception exc_close) {
			System.out.println("DBService : Exception Occured in close statement :"
					+ exc_close);
		} finally {
			resultSet = null;
		}
	}

}
