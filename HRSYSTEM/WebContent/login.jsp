<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="appModule">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HR System</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://rawgit.com/dalelotts/angular-bootstrap-datetimepicker/master/src/css/datetimepicker.css" />
<!-- <link rel="stylesheet" href="StyleSheet/stylesheet.css" type="text/css"> -->
<style type="text/css">
[ng\:cloak], [ng-cloak], .ng-cloak {
	display: none !important;
}

 #font {
 
	 font-family: "Trebuchet MS", Helvetica, sans-serif;
	 
/* "Trebuchet MS", Helvetica, sans-serif	 Tahoma, Geneva, sans-serif */
/* 	 Verdana, Geneva, sans-serif */
/* 	 "Lucida Sans Unicode", "Lucida Grande", sans-serif */
/* 	 "Comic Sans MS", cursive, sans-serif */
/* 	 Arial, Helvetica, sans-serif */
 }
 
</style>
</head>
<body id="font" ng-cloak class="ng-cloak" background="style/loginimage1.jpg" style="background-size: cover;min-width:100%;width: 100%;
height: 100%;
min-width: 1000px;
min-height: 1000px;
max-height:100%;" >

<form name="from1" ng-controller="loginCtrl">
	
		<div class="form-group container"  style="width: 30%;margin-top:20px;">
		<br/><br/>
		<br/><br/>
<!-- 		<p>  -->
<!--  		<img alt="image1" src="style/images4.png" width="230px" height="180px" style=" position: relative; left: 20px; ">  -->
<!-- 		</p>  -->
 		<br/><br/>
		<div style="font-size: 22px;color: black;float:left;margin-left:65px;text-shadow: 2px 2px #B1B3FF"><center>{{value}}</center></div>
		<br/><br/>
			<div class="col-sm-5 login" >
				<!-- 			<h3><label>N2 Services Inc</label></h3> -->
<!-- 				<h4><center> Please login</center></h4> -->
					<br/>
					<p style="font-size: 17px;color: red;">Sign in</p>
				<input name="Email" class="form-control" placeholder="Email"  style="width: 200%;"
					autofocus="" required="" type="text" ng-model="user.userName"><br>
				<input name="password" class="form-control" placeholder="Password" style="width: 200%;"
					autofocus="" required="" type="password" ng-model="user.password">
				<br>
				<!-- 				<div class="form-group"> -->
				<div class="col-sm-offset" >
					<!-- 						<div class="checkbox"> -->
					<label style="color: black;"> <input type="checkbox">
						Remember me
					</label> 
					<p><a href=# style="color: red; text-align:right;width: 250px;" ng-click="forgotPassword()">Forgot password</a></p>
					
					<!-- 						</div> -->

				</div>
				
				
				
				
				<!-- 	captcha setup	start		</div> -->
<!-- 				<div> -->
<!-- 					<label style="color: black;"> -->
<!-- 					<input type="checkbox" ng-model="captcha.check" ng-click="captchaSelection()"> I'm not a robot -->
<!-- 					</label> -->
<!-- 					</br> -->
						 
<!-- 						<div ng-hide="captcha1"style="float:left;margin-left:50px;"> -->
<!-- 							<div> -->
<!-- 								<div style="float:left;margin-left:120px;height:20px; "> -->
<!-- 									<button style="background-color: white;border-style: solid; -->
<!--    									 border-color: white white white;height:35px;width:25px;">  -->
<!-- 									<img  style="margin-left:-7.5px;" src="style/refresh1.jpg" width="23px" height="30px" ng-click="refreshCaptcha()"> -->
<!-- 									</button> -->
<!-- 								</div> -->
<!-- 								<div style=" margin-bottom:15px; " > -->
<!-- 									<div style="padding: 10px; margin: 15px; width: 90px; height: 40px; border: 2px solid grey; border-radius: 5px;background: gray;"> -->
<!-- 									<div style="width: 23%; float: left; margin-left: 10px;margin-top: -10px; position: relative; top: 4px;">{{randoms1}}</div> -->
								
<!-- 									</div> -->
<!-- 								</div> -->
<!-- 							</div> -->
<!-- 							<p style="width: 300px;margin-left:10px; ">Please enter the above text.</p> -->
<!-- 							<div style="float:left;margin-left:10px;width: 90%;"> -->
<!-- 									<input name="test" class="form-control" style="width: 40%;" -->
<!-- 									type="text" ng-model="captchatext"><br> -->
									
<!-- 							</div> -->
<!-- 						</div> -->




<!-- 				</div> -->
				
				
				
				
				
				
				
				 <!-- captcha setup  finish  -->
				
				
				
				
				
				
				<input class="btn btn-lg btn-primary btn-block" type="button"
					style="width: 200%;margin-top:10px;" value="Login" ng-click="login(user)" />
			</div>
		</div>
		
	</form>


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script> 
  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-route.min.js"></script>
  	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-animate.js"></script>
    <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.13.3.js"></script>
<!--   	<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.0.js"></script> -->
  	

	<script src="scriptFile/app.js"></script>
	<script src="scriptFile/controller_file/login_controller.js"></script>
	
	<script src="scriptFile/service_file/login_service.js"></script>


<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/moment.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js"></script>    
    <script src="http://code.angularjs.org/1.2.18/angular.min.js"></script>        
    <script src="https://rawgit.com/dalelotts/angular-bootstrap-datetimepicker/master/src/js/datetimepicker.js"></script>
    <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>   
	
</body>
</html>