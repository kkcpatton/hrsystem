<div  ng-controller="timesheetCtrl" style="width: 85%;">

						
						<h4 style="margin-bottom:100px;"> Monthly Resource Time Sheet </h4>
							
						<select ng-model="adminSheet.empId" name="priority"
							ng-options="x.employeeId as x.name  for x in sheetEmplist"
							
							style="width: 200px;float: left;margin-left: 220px;" class="form-control">
							<option label="-Select Employee-"></option>
						</select>
						
						
						
						<select ng-model="adminSheet.selectMonth" name="Status" ng-hide="hideSelectMonth"
							ng-options="x for x in selectMonth" 
							style="float:left;width: 140px; margin-left: 20px;" class="form-control">
							<option label="-Select Month-"></option>
						</select>
						<select ng-model="adminSheet.selectYear" name="Status2" ng-hide="hideSelectYear"
							ng-options="x for x in selectYear"
							style="float:left;width: 130px; margin-left: 20px;" class="form-control">
							<option label="-Select Year-"></option>
						</select>

						<input  name="Candidate Name"  class="btn btn-info" autofocus=""  ng-hide="hideGoButton" type="button" 
						style="width: 60px;float:left; margin-left: 15px;background-color:#1a8cff;margin-bottom:50px;" value="Go >>"
						ng-click="getTimeSheetData(adminSheet.empId,adminSheet.selectMonth,adminSheet.selectYear)" />


	  
 <div ng-hide="hideTableAdmin" style="float:left;margin-top:45px;width:1080px; border: 2px solid gray;
    padding: 5px;
    margin: 5px; ">  

<p style="font-size: 15px; color:#032A50;"><b>Monthly Resource Time Sheet Approval</b> </p>
 <div style="margin-top:25px; "> 
 <div style="float:left;"> 
 <p > <b style="color:blue; "> Project :      </b> {{sheet.project}}  </p>
 
 </div>
 </br></br>
 <div style="float:left;"> 
 <p style="margin-left:0px;"> <b style="color:blue; "> client : </b> {{sheet.client}}</p>
 </div>
 
 
 <div style="float:left;"> 
 <p style="margin-top:0px;margin-left:653px;"> <b style="color:blue; ">Consultant :</b> {{consultantName}}  </p>
   
 </div>

  </div>
  </br></br>
 <center> <b> {{sheet.TimeSheetMonth |date:'MMMM , y'}} </b></center>

 
 
 
 <table class="table table-bordered"  style=" font-size: 11.5px; "  >
						<thead style="background-color: #dbdbdb">
						
							<tr>
							<th style=" width: 220px;">Task Name </th>
							<th style=" width: 20px; "> 01</th>
							<th style=" width: 20px; "> 02</th>
							<th style=" width: 20px;; ">03</th>
							<th style=" width: 20px; ">04</th>
							<th style=" width: 20px; ">05</th>
							<th style=" width: 20px; "> 06</th>
							<th style=" width: 20px;; ">07</th>
							<th style=" width: 20px; ">08</th>
							<th style=" width: 20px; ">09</th>
							<th style=" width: 20px; ">10</th>
							<th style=" width: 20px; "> 11</th>
							<th style=" width: 20px; "> 12</th>
							<th style=" width: 20px; ">13</th>
							<th style=" width: 20px; ">14</th>
							<th style=" width: 20px;">15</th>
							<th style=" width: 20px;"> 16</th>
							<th style=" width: 20px;"> 17</th>
							<th style=" width: 20px;">18</th>
							<th style=" width: 20px; ">19</th>
							<th style=" width: 20px; ">20</th>
							<th style=" width: 20px;"> 21</th>
							<th style=" width: 20px; "> 22</th>
							<th style=" width: 20px; ">23</th>
							<th style=" width: 20px; ">24</th>
							<th style=" width: 20px;">25</th>
							<th style=" width: 20px; "> 26</th>
							<th style=" width: 20px; "> 27</th>
							<th ng-hide="HideDay28"  style=" width: 20px; ">28</th>
							<th ng-hide="HideDay29"  style=" width: 20px; ">29</th>
							<th ng-hide="HideDay30"  style=" width: 20px;">30</th>
							<th ng-hide="HideDay31"  style=" width: 20px;">31</th>
							<th  style=" width: 50px;"><b>Total</b> </th>
			
						</tr>
						</thead>
							<tfoot style="background-color: #dbdbdb;">
					<tr>
					
					<th style=" width: 120px;"> Total </th>
							<th style=" width: 20px; ">{{totalDay1 | number : 2}} </th>
							<th style=" width: 20px; ">{{totalDay2 | number : 2}} </th>
							<th style=" width: 20px; ">{{totalDay3 | number : 2}}</th>
							<th style=" width: 20px; "> {{totalDay4 | number : 2}}</th>
							<th style=" width: 20px; "> {{totalDay5 | number : 2}}</th>
							<th style=" width: 20px; ">{{ totalDay6 | number : 2}} </th>
							<th style=" width: 20px;; ">{{totalDay7  | number : 2}} </th>
							<th style=" width: 20px; "> {{totalDay8  | number : 2}}</th>
							<th style=" width: 20px; ">{{ totalDay9 | number : 2}} </th>
							<th style=" width: 20px; "> {{totalDay10  | number : 2}}</th>
							<th style=" width: 20px; ">{{totalDay11  | number : 2}} </th>
							<th style=" width: 20px; ">{{totalDay12  | number : 2}} </th>
							<th style=" width: 20px; ">{{totalDay13  | number : 2}} </th>
							<th style=" width: 20px; ">{{totalDay14  | number : 2}} </th>
							<th style=" width: 20px;">{{totalDay15 | number : 2 }} </th>
							<th style=" width: 20px;"> {{totalDay16  | number : 2}} </th>
							<th style=" width: 20px;">{{totalDay17 | number : 2}} </th>
							<th style=" width: 20px;"> {{totalDay18  | number : 2}}</th>
							<th style=" width: 20px; ">{{totalDay19  | number : 2}} </th>
							<th style=" width: 20px; "> {{totalDay20  | number : 2}}</th>
							<th style=" width: 20px;"> {{totalDay21  | number : 2}}</th>
							<th style=" width: 20px; "> {{totalDay22 | number : 2 }}</th>
							<th style=" width: 20px; ">{{totalDay23  | number : 2}} </th>
							<th style=" width: 20px; ">{{ totalDay24 | number : 2}} </th>
							<th style=" width: 20px;"> {{totalDay25  | number : 2}}</th>
							<th style=" width: 20px; "> {{totalDay26 | number : 2 }} </th>
							<th style=" width: 20px; ">{{totalDay27  | number : 2}} </th>
							<th ng-hide="HideDay28"  style=" width: 20px; ">{{totalDay28  | number : 2}} </th>
							<th ng-hide="HideDay29"  style=" width: 20px; "> {{totalDay29  | number : 2}}</th>
							<th ng-hide="HideDay30"  style=" width: 20px;"> {{totalDay30  | number : 2}}</th>
							<th ng-hide="HideDay31"  style=" width: 20px;">{{ totalDay31  | number : 2}} </th>
							<th  style=" width: 50px;background-color: green;">{{totaltask | number : 2}} </th>
					
					
 					 
					 
				</tr>		
			</tfoot>
						
						<tbody   >

						<tr  ng-repeat="sheet in SheetTaskRepeatAdimn" >
							<td style="padding: 0 1px;width: 120px;"> 
							<span  style="width: 120px;" ng-disabled="DisableTaskName"  >{{sheet.taskName}} </span>
							   </td>
							
							<td ng-show="DisableDay01=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay01=== true" > {{}}</span>
							<span  ng-show="DisableDay01=== false" > {{sheet.day1}}</span>
							
							</td>
							<td ng-show="DisableDay01=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay01=== true" > {{}} </span>
							<span  ng-show="DisableDay01=== false" > {{sheet.day1}}</span>
							
							</td>
							
							<td ng-show="DisableDay02=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay02=== true" > {{}}</span>
							<span  ng-show="DisableDay02=== false" > {{sheet.day2}}</span>
							
							</td>
							<td ng-show="DisableDay02=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay02=== true" > {{}} </span>
							<span  ng-show="DisableDay02=== false" > {{sheet.day2}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay03=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay03=== true" > {{}}</span>
							<span  ng-show="DisableDay03=== false" > {{sheet.day3}}</span>
							
							</td>
							<td ng-show="DisableDay03=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay03=== true" > {{}} </span>
							<span  ng-show="DisableDay03=== false" > {{sheet.day3}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay04=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay04=== true" > {{}}</span>
							<span  ng-show="DisableDay04=== false" > {{sheet.day4}}</span>
							
							</td>
							<td ng-show="DisableDay04=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay04=== true" > {{}} </span>
							<span  ng-show="DisableDay04=== false" > {{sheet.day4}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay05=== true" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay05=== true" > {{}}</span>
							<span  ng-show="DisableDay05=== false" > {{sheet.day5}}</span>
							
							</td>
							<td ng-show="DisableDay05=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay05=== true" > {{}} </span>
							<span  ng-show="DisableDay05=== false" > {{sheet.day5}}</span>
							
							</td>
							
							
							
							
							
							<td ng-show="DisableDay06=== true" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay06=== true" > {{}}</span>
							<span  ng-show="DisableDay06=== false" > {{sheet.day6}}</span>
							
							</td>
							
							<td ng-show="DisableDay06 === false" style="padding: 0 1px;background-color: white;">
							<span  ng-show="DisableDay06=== true" > </span>
							<span  ng-show="DisableDay06=== false" > {{sheet.day6}}</span>
													
							
							</td>
							
							
							
							
							<td ng-show="DisableDay07 === true" style="padding: 0 1px;background-color: white;">
<!-- 							<span ng-disabled="DisableDay07" > {{sheet.day7}}</span> -->
							<span  ng-show="DisableDay07=== true" > {{ }}</span>
							<span  ng-show="DisableDay07=== false" > {{sheet.day7}}</span>
												
							</td>
							
							<td ng-show="DisableDay07 === false" style="padding: 0 1px;background-color: white;">
							<span  ng-show="DisableDay07=== true" > {{ }}</span>
							<span  ng-show="DisableDay07=== false" > {{sheet.day7}}</span>
							
						
							
							</td>
							
							
							
							
							
							<td ng-show="DisableDay08=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay08=== true" > {{}}</span>
							<span  ng-show="DisableDay08=== false" > {{sheet.day8}}</span>
							
							</td>
							<td ng-show="DisableDay08=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay08=== true" > {{}} </span>
							<span  ng-show="DisableDay08=== false" > {{sheet.day8}}</span>
							
							</td>
							
							
							
							
							<td ng-show="DisableDay09=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay09=== true" > {{}}</span>
							<span  ng-show="DisableDay09=== false" > {{sheet.day9}}</span>
							
							</td>
							<td ng-show="DisableDay09=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay09=== true" > {{}} </span>
							<span  ng-show="DisableDay09=== false" > {{sheet.day9}}</span>
							
							</td>
							
							
							
							
							
							<td ng-show="DisableDay10=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay10=== true" > {{}}</span>
							<span  ng-show="DisableDay10=== false" > {{sheet.day10}}</span>
							
							</td>
							<td ng-show="DisableDay10=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay10=== true" > {{}} </span>
							<span  ng-show="DisableDay10=== false" > {{sheet.day10}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay11=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay11=== true" > {{}}</span>
							<span  ng-show="DisableDay11=== false" > {{sheet.day11}}</span>
							
							</td>
							<td ng-show="DisableDay11=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay11=== true" > {{}} </span>
							<span  ng-show="DisableDay11=== false" > {{sheet.day11}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay12=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay12=== true" > {{}}</span>
							<span  ng-show="DisableDay12=== false" > {{sheet.day12}}</span>
							
							</td>
							<td ng-show="DisableDay12=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay12=== true" > {{}} </span>
							<span  ng-show="DisableDay12=== false" > {{sheet.day12}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay13=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay13=== true" > {{}}</span>
							<span  ng-show="DisableDay13=== false" > {{sheet.day13}}</span>
							
							</td>
							<td ng-show="DisableDay13=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay13=== true" > {{}} </span>
							<span  ng-show="DisableDay13=== false" > {{sheet.day13}}</span>
							
							</td>
							
							
							<td ng-show="DisableDay14=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay14=== true" > {{}}</span>
							<span  ng-show="DisableDay14=== false" > {{sheet.day14}}</span>
							
							</td>
							<td ng-show="DisableDay14=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay14=== true" > {{}} </span>
							<span  ng-show="DisableDay14=== false" > {{sheet.day14}}</span>
							
							</td>
							
							
							<td ng-show="DisableDay15=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay15=== true" > {{}}</span>
							<span  ng-show="DisableDay15=== false" > {{sheet.day15}}</span>
							
							</td>
							<td ng-show="DisableDay15=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay15=== true" > {{}} </span>
							<span  ng-show="DisableDay15=== false" > {{sheet.day15}}</span>
							
							</td>
							
							
							<td ng-show="DisableDay16=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay16=== true" > {{}}</span>
							<span  ng-show="DisableDay16=== false" > {{sheet.day16}}</span>
							
							</td>
							<td ng-show="DisableDay16=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay16=== true" > {{}} </span>
							<span  ng-show="DisableDay16=== false" > {{sheet.day16}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay17=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay17=== true" > {{}}</span>
							<span  ng-show="DisableDay17=== false" > {{sheet.day17}}</span>
							
							</td>
							<td ng-show="DisableDay17=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay17=== true" > {{}} </span>
							<span  ng-show="DisableDay17=== false" > {{sheet.day17}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay18=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay18=== true" > {{}}</span>
							<span  ng-show="DisableDay18=== false" > {{sheet.day18}}</span>
							
							</td>
							<td ng-show="DisableDay18=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay18=== true" > {{}} </span>
							<span  ng-show="DisableDay18=== false" > {{sheet.day18}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay19=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay19=== true" > {{}}</span>
							<span  ng-show="DisableDay19=== false" > {{sheet.day19}}</span>
							
							</td>
							<td ng-show="DisableDay19=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay19=== true" > {{}} </span>
							<span  ng-show="DisableDay19=== false" > {{sheet.day19}}</span>
							
							</td>
							
							
							
							
							<td ng-show="DisableDay20=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay20=== true" > {{}}</span>
							<span  ng-show="DisableDay20=== false" > {{sheet.day20}}</span>
							
							</td>
							<td ng-show="DisableDay20=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay20=== true" > {{}} </span>
							<span  ng-show="DisableDay20=== false" > {{sheet.day20}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay21=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay21=== true" > {{}}</span>
							<span  ng-show="DisableDay21=== false" > {{sheet.day21}}</span>
							
							</td>
							<td ng-show="DisableDay21=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay21=== true" > {{}} </span>
							<span  ng-show="DisableDay21=== false" > {{sheet.day21}}</span>
							
							</td>
							
							
							<td ng-show="DisableDay22=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay22=== true" > {{}}</span>
							<span  ng-show="DisableDay22=== false" > {{sheet.day22}}</span>
							
							</td>
							<td ng-show="DisableDay22=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay22=== true" > {{}} </span>
							<span  ng-show="DisableDay22=== false" > {{sheet.day22}}</span>
							
							</td>
							
							
							<td ng-show="DisableDay23=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay23=== true" > {{}}</span>
							<span  ng-show="DisableDay23=== false" > {{sheet.day23}}</span>
							
							</td>
							<td ng-show="DisableDay23=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay23=== true" > {{}} </span>
							<span  ng-show="DisableDay23=== false" > {{sheet.day23}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay24=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay24=== true" > {{}}</span>
							<span  ng-show="DisableDay24=== false" > {{sheet.day24}}</span>
							
							</td>
							<td ng-show="DisableDay24=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay24=== true" > {{}} </span>
							<span  ng-show="DisableDay24=== false" > {{sheet.day24}}</span>
							
							</td>
							
							
							
							<td ng-show="DisableDay25=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay25=== true" > {{}}</span>
							<span  ng-show="DisableDay25=== false" > {{sheet.day25}}</span>
							
							</td>
							<td ng-show="DisableDay25=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay25=== true" > {{}} </span>
							<span  ng-show="DisableDay25=== false" > {{sheet.day25}}</span>
							
							</td>
							
							
							<td ng-show="DisableDay26=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay26=== true" > {{}}</span>
							<span  ng-show="DisableDay26=== false" > {{sheet.day26}}</span>
							
							</td>
							<td ng-show="DisableDay26=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay26=== true" > {{}} </span>
							<span  ng-show="DisableDay26=== false" > {{sheet.day26}}</span>
							
							</td>
							
							<td ng-show="DisableDay27=== true" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay27=== true" > {{}}</span>
							<span  ng-show="DisableDay27=== false" > {{sheet.day27}}</span>
							
							</td>
							<td ng-show="DisableDay27=== false" style="padding: 0 1px;background-color: white;">

							<span   ng-show="DisableDay27=== true" > {{}} </span>
							<span  ng-show="DisableDay27=== false" > {{sheet.day27}}</span>
							
							</td>
							
							<td ng-hide="HideDay28"  style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay28=== true" > {{}}</span>
							<span  ng-show="DisableDay28=== false" > {{sheet.day28}}</span>
							
							</td>
							
							
							<td ng-hide="HideDay29"  style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay29=== true" > {{}}</span>
							<span  ng-show="DisableDay29=== false" > {{sheet.day29}}</span>
							
							</td>
							
							
							
							<td ng-hide="HideDay30"  style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay30=== true" > {{}}</span>
							<span  ng-show="DisableDay30=== false" > {{sheet.day30}}</span>
							
							</td>
							
							
							<td ng-hide="HideDay31" style="padding: 0 1px;background-color: white;">
							<span   ng-show="DisableDay31=== true" > {{}}</span>
							<span  ng-show="DisableDay31=== false" > {{sheet.day31}}</span>
							
							</td>
							
							
							<td style="padding: 0 1px;background-color: yellow;" >
							<span  > {{sheet.total | number : 2}}</span>
					
							
							</td>
							</tr>
							
							
							
						
						</tbody>
 			
						</table>
						 
						 <div style="float:left;">
						 <p style="float:left;"><b> Total Billable Hours : {{totaltask | number : 2}}</b> </p>
						 <br/>
						 <p style="float:left;"><b> Total Non-Billable Hours : </b> </p>
 						</div>
 						
						<div  style="width: 100%;float: left; text-align: right;margin-top:20px;">
						<div style="width: 33%; float: left;  position: relative; top: 4px;">Resource Comments :</div>
 						<textarea name="clientDetails" ng-disabled="DisableResourceComments" 
							style="width: 250px; height:42px;float: left; margin-left:  20px; position: relative; bottom: 10px;" class="form-control"
							autofocus=""  type="text" ng-model="sheet.resourceComments"></textarea>

						</div>	
						

						
						<div  style="width: 100%;float: left; text-align: right;margin-top:10px;">
						<div style="width: 33%; float: left;  position: relative; top: 4px;">Client Manager  :</div>
 						<input name="Candidate Name" ng-disabled="DisableManager"
							style="width: 250px;float: left;margin-left: 20px;" class="form-control"
							autofocus=""  type="text" 
							ng-model="sheet.clientManager">
						
						</div>
						
						<div ng-hide="HideSheetTable"  style="width: 100%;float: left; text-align: right;margin-top:20px;">
						<div style="width: 33%; float: left;  position: relative; ">Status  :</div>
 						<p style="float: left;margin-left:20px;color: red;"><i>{{statusAdminApprove}}</i></p>
						
						</div>
						
						<input ng-disabled="DisableAccept" name="Candidate Name"  class="btn btn-info" autofocus=""  type="button"   
						style="width: 70px;float:left; margin-left: 375px;margin-top:40px;background-color:#1a8cff;" value="Accept"
						ng-click="AcceptAdmin(sheet.sheetId)" />
						
						<input  ng-disabled="DisableReject" name="Candidate Name"  class="btn btn-info" autofocus=""  type="button"   
						style="width: 70px;float:left; margin-left: 20px;margin-top:40px;background-color:red;" value="Reject"
						ng-click="RejectAdmin(sheet.sheetId)" />
						
 						<input  name="Candidate Name"  class="btn btn-info" autofocus=""  type="button"   
						style="width: 70px;float:left; margin-left: 20px;margin-top:40px;margin-bottom:30px;background-color:#1a8cff;" value="Cancel"
						ng-click="CancelAdmin()" />

 </div>
 

  
   
  
</div>
			
