var app1 = angular.module('appModule', ['ngRoute']);

app1.config(function($routeProvider, $httpProvider) {
	console.log("config");
	$routeProvider
	.when('/', {
		templateUrl : 'login.html',
		controller : 'loginCtrl'
	}).otherwise({
		redirectTo : '/login.html'
	});
//	 $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
	$httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
