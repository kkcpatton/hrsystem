'use strict';

app.controller('BlogCtrl', function($scope, $window,blogService, $http,$rootScope) {
 

	var sess = sessionStorage.getItem("key1");
	if (sess) {
		
		$scope.viewBlog=false;
		$scope.blog={
                postHeading:"",
                postContent:""
        };

		$scope.submit = function(data) {
			
			var empId = sessionStorage.getItem("idemp");
			data= {
					
					blogId:$scope.blog.blogId,
					blogHeading:$scope.blog.postHeading,
					blogContent:$scope.blog.postContent,
					blogEmpId:empId,
					blogFlag:""
				  
			};
			

			if(($scope.blog.postHeading == "")||($scope.blog.postHeading == null)|| ($scope.blog.postContent == null)||($scope.blog.postContent == "")){
                alert("Please fill all the fields");
            }else{
                
                blogService.submit(data,$scope);
                
                
            }


		};

		$scope.hidePost=true;
		$scope.AddPost=function(){
			$scope.hidePost=false;	
			$scope.viewBlog=true;
			
		};
		$scope.cancel=function(blog){
			$scope.blog={
	                postHeading:"",
	                postContent:""
	        };

			$scope.hidePost=true;
			$scope.viewBlog=false;
			$scope.blogList();
		};
		
		
		$scope.blogList = [];
		$scope.blogList = function() {
			
			
			var response;
			 response = $http.get('bloglist');
			
			 response.success(function(data, status, headers, config) {
				 console.log(data);
					
					$scope.contacts = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://localhost:8086/HRSYSTEM/bloglist',
//				data : {}
//			}).success(function(result) {
//				console.log(result);
//				
//				$scope.contacts = result;
//			
//				
//			});
		};
		$scope.blogList();
		
		
		$scope.viewEmployee= function(){
			var empId = sessionStorage.getItem("idemp");
			blogService.viewImage(empId,$scope);
	    	
	    };
		
	    $scope.viewEmployee();
	    
	    $scope.sendComment=function(blogId,comment){
	    	
	    	var empId = sessionStorage.getItem("idemp");
	    	var data;
	    	data= {
					
					postId:"",
	    			blogId:blogId,
					postComment:comment,
					postEmpId:empId,
					postFlag:""
				  
			};
			if((comment == null) ||(comment == "")){
				alert("Please Enter Comments");
			}else{
			blogService.submitComment(data,$scope);
			$scope.commentClear();
			}
	    };
	    $scope.commentClear=function(){
	    	$scope.comment="";
	    	$scope.blogList();
	    };
	    
	    
	    $scope.blogPostList = [];
		$scope.blogPostList = function() {
			
			
			var response;
			 response = $http.get('blogPostList');
			
			 response.success(function(data, status, headers, config) {
				 console.log(data);
					
				 $scope.contactsPost = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://localhost:8086/HRSYSTEM/blogPostList',
//				data : {}
//			}).success(function(result) {
//				console.log(result);
//				
//				$scope.contactsPost = result;
//			
//				
//			});
		};
		$scope.blogPostList();
		
		
		$scope.DeletePostSelect=function(blogIdSelect){
			
			var result= confirm("Are you sure to delete ?");
			 
			if(result == true){
			blogService.DeletePost(blogIdSelect,$scope);
			}
			$scope.hideDeletePost=true;
			$scope.hideSelectPost=true;
			$scope.hideAddPost=false;
		};
		$scope.hideDeletePost=true;
		$scope.hideSelectPost=true;
		$scope.hideAddPost=false;
		
		$scope.DeletePost=function(){
			$scope.hideDeletePost=false;
			$scope.hideSelectPost=false;
			$scope.hideAddPost=true;
		};
		var EmpRole = sessionStorage.getItem("EmpRole");
		if(EmpRole == 'ADMIN'){
			$scope.hideAddPost=false;
		}else{
			$scope.hideAddPost=true;
		}
		
	}
	else {
		$window.location.href = 'login.jsp';

	}

	 });
