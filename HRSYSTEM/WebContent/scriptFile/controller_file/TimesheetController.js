'use strict';


 
app.controller('timesheetCtrl', function($scope, $window, $http, $rootScope,timesheetService,$filter,$sce) {
 
	var sess = sessionStorage.getItem("key1");
	if (sess) {
		
		
		$scope.HideDay=[];
		 
		$scope.employeeList = [];
		$scope.employeeList = function() {
			
			var response;
			 response = $http.get('employeelist1');
			
			 response.success(function(data, status, headers, config) {
				 console.log(data);
				 $scope.sheetEmplist = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://localhost:8086/HRSYSTEM/employeelist1',
//				data : {}
//			}).success(function(result) {
//				
//				
//				$scope.sheetEmplist = result;
//				
//			});
		};
		$scope.employeeList();
		
		$scope.SheetTaskRepeat = [{}];
		
		$scope.selectMonth =["January","February","March","April","May","June","July","August","September","October","November","December"];
	  	$scope.selectYear = ["2017","2018","2019","2020","2021","2022","2023","2024"]; 
		$scope.HideSheetTable=true;
		$scope.DisableSubmit=false;
		
		
		$scope.hideSelectMonth=false;
		$scope.hideSelectYear=false;
		$scope.hideGoButton=false;
		
		$scope.DisableDay01=false;
		$scope.DisableDay02=false;					
		$scope.DisableDay03=false;
		$scope.DisableDay04=false;
		$scope.DisableDay05=false;
		$scope.DisableDay06=false;
		$scope.DisableDay07=false;					
		$scope.DisableDay08=false;
		$scope.DisableDay09=false;
		$scope.DisableDay10=false;
		$scope.DisableDay11=false;
		$scope.DisableDay12=false;					
		$scope.DisableDay13=false;
		$scope.DisableDay14=false;
		$scope.DisableDay15=false;
		$scope.DisableDay16=false;
		$scope.DisableDay17=false;					
		$scope.DisableDay18=false;
		$scope.DisableDay19=false;
		$scope.DisableDay20=false;
		$scope.DisableDay21=false;
		$scope.DisableDay22=false;					
		$scope.DisableDay23=false;
		$scope.DisableDay24=false;
		$scope.DisableDay25=false;
		$scope.DisableDay26=false;
		$scope.DisableDay27=false;					
		$scope.DisableDay28=false;
		$scope.DisableDay29=false;
		$scope.DisableDay30=false;
		$scope.DisableDay31=false;
		$scope.HideDay28=false;
		$scope.HideDay29=false;
		$scope.HideDay30=false;
		$scope.HideDay31=false;
		$scope.DisableEdit=false;
		$scope.DisableSubmit=false;
		$scope.DisableClear=false;
		$scope.DisableTaskName=false;
		$scope.RemoveSheetImage=false;	
		$scope.AddSheetButton=false;
		
		$scope.DisableManager=false;
		$scope.DisableClient=false;
		$scope.DisableProject=false;
		$scope.DisableResourceComments=false;
		
		$scope.timeSheetDate=function(sheetMonth,sheetYear){
			
			if( (sheetMonth == null) || (sheetMonth == "") || (sheetMonth == "undefined") || (sheetYear == null )|| (sheetYear == "") || (sheetYear == "undefined"))
			{
				
				alert("Please Select TimeSheet Month and year");
			}
				else{
				
			$scope.hideSelectMonth=true;
			$scope.hideSelectYear=true;
			$scope.hideGoButton=true;
			
			var AssignMonth1="";
			var AssignYear="";
			AssignYear=sheetYear;
			if(sheetMonth == "January"){
		  		
		  		AssignMonth1="01";
		  	}else if(sheetMonth == "February"){
		  		
		  		AssignMonth1="02";
		  	}else if(sheetMonth == "March"){
		  		
		  		AssignMonth1="03";
		  	}else if(sheetMonth == "April"){
		  		
		  		AssignMonth1="04";
		  	}else if(sheetMonth == "May"){
		  		
		  		AssignMonth1="03";
		  	}else if(sheetMonth == "June"){
		  		
		  		AssignMonth1="06";
		  	}else if(sheetMonth == "July"){
		  		
		  		AssignMonth1="07";
		  	}else if(sheetMonth == "August"){
		  		
		  		AssignMonth1="08";
		  	}else if(sheetMonth == "September"){
		  
		  		AssignMonth1="09";
		  	}else if(sheetMonth == "October"){
		  	
		  		AssignMonth1="10";
		  	}else if(sheetMonth == "November"){
		  		
		  		AssignMonth1="11";
		  	}else if(sheetMonth == "December"){
		  		
		  		AssignMonth1="12";
		  	}
			
			
			
			var dateEdit=AssignYear+"-"+AssignMonth1+"-05";
			
			
			var date=new Date(dateEdit);
			 $scope.viewDate=new Date(dateEdit);
			 
			 
			
//			alert("DATE "+date);
			// get the data by date
			 var idemp = sessionStorage.getItem("idemp");
			 $scope.getSheetByMonth=[];
			 $scope.getSheetByMonth=[date,idemp];
			timesheetService.getEmpTimeSheetByMonth($scope.getSheetByMonth,$scope);
			
			
			
			$scope.DisableSubmit=false;
			$scope.HideSheetTable=false;
			
			$scope.DisableDay01=false;
			$scope.DisableDay02=false;					
			$scope.DisableDay03=false;
			$scope.DisableDay04=false;
			$scope.DisableDay05=false;
			$scope.DisableDay06=false;
			$scope.DisableDay07=false;					
			$scope.DisableDay08=false;
			$scope.DisableDay09=false;
			$scope.DisableDay10=false;
			$scope.DisableDay11=false;
			$scope.DisableDay12=false;					
			$scope.DisableDay13=false;
			$scope.DisableDay14=false;
			$scope.DisableDay15=false;
			$scope.DisableDay16=false;
			$scope.DisableDay17=false;					
			$scope.DisableDay18=false;
			$scope.DisableDay19=false;
			$scope.DisableDay20=false;
			$scope.DisableDay21=false;
			$scope.DisableDay22=false;					
			$scope.DisableDay23=false;
			$scope.DisableDay24=false;
			$scope.DisableDay25=false;
			$scope.DisableDay26=false;
			$scope.DisableDay27=false;					
			$scope.DisableDay28=false;
			$scope.DisableDay29=false;
			$scope.DisableDay30=false;
			$scope.DisableDay31=false;
			$scope.HideDay28=false;
			$scope.HideDay29=false;
			$scope.HideDay30=false;
			$scope.HideDay31=false;
			$scope.DisableEdit=false;
			$scope.DisableSubmit=false;
			$scope.DisableClear=false;
			$scope.DisableTaskName=false;
			$scope.RemoveSheetImage=false;
			$scope.AddSheetButton=false;
			
		$scope.lastMonthDate= new Date(date.getFullYear(), date.getMonth()+1, 0, 23, 59, 59).getDate();
		
		
		var d = new Date(date);
		
	    var weekday = new Array(7);
	    weekday[0] = "Sunday";
	    weekday[1] = "Monday";
	    weekday[2] = "Tuesday";
	    weekday[3] = "Wednesday";
	    weekday[4] = "Thursday";
	    weekday[5] = "Friday";
	    weekday[6] = "Saturday";
	    $scope.HideDay=[];
	    for(var i=1;i<=$scope.lastMonthDate;i++){
	    	var y=d.getFullYear();
		    var m=date.getMonth()+1;
		    var dd=i;
		    
		   // 'October 10, 2021 23:15:30'
		    var monthname="";
		    if(m=="1"){
		    	monthname="January";
		    }else if(m=="2"){
		    	monthname="February";
		    }else if(m=="3"){
		    	monthname="March"
		    }else if(m=="4"){
		    	monthname="April"
		    }else if(m=="5"){
		    	monthname="May"
		    }else if(m=="6"){
		    	monthname="June"
		    }else if(m=="7"){
		    	monthname="July"
		    }else if(m=="8"){
		    	monthname="August"
		    }else if(m=="9"){
		    	monthname="September"
		    }else if(m=="10"){
		    	monthname="October"
		    }else if(m=="11"){
		    	monthname="November"
		    }else if(m=="12"){
		    	monthname="December"
		    }
		    
		    
		    var EditDate1=''+monthname+' '+i+', '+y+' 23:15:30';
		    var EditDate=y+'-'+m+'-'+dd;
		    console.log(EditDate1);
		    var d2 = new Date(EditDate1);
		    console.log(EditDate1);
	    var n = weekday[d2.getDay()];
		console.log(n);
		
		if(n == "Sunday" || n == "Saturday" ){
			
			$scope.HideDay.push(i);
			console.log($scope.HideDay);
		}
		
	    }
	   if($scope.lastMonthDate < "31"){
		   $scope.HideDay31=true;
	   }
	   if($scope.lastMonthDate < "30"){
		   $scope.HideDay30=true;
	   }
	   if($scope.lastMonthDate < "29"){
		   $scope.HideDay29=true;
	   }
	   if($scope.lastMonthDate < "28"){
		   $scope.HideDay28=true;
	   }
	    
	    
	    var data=[];
	    data=$scope.HideDay;
	    for(var i=0;i<=data.length;i++){
	    	if(data[i]== "01"){
	    		$scope.DisableDay01=true;
	    	}
	    	if(data[i]== "02"){
	    		$scope.DisableDay02=true;
	    	}
	    	if(data[i]== "03"){
	    		$scope.DisableDay03=true;
	    	}
	    	
	    	if(data[i]== "04"){
	    		$scope.DisableDay04=true;
	    	}
	    	if(data[i]== "05"){
	    		$scope.DisableDay05=true;
	    	}
	    	
	    	if(data[i]== "06"){
	    		$scope.DisableDay06=true;
	    	}
	    	if(data[i]== "07"){
	    		$scope.DisableDay07=true;
	    	}
	    	if(data[i]== "08"){
	    		$scope.DisableDay08=true;
	    	}
	    	if(data[i]== "09"){
	    		$scope.DisableDay09=true;
	    	}
	    	if(data[i]== "10"){
	    		$scope.DisableDay10=true;
	    	}
	    	if(data[i]== "11"){
	    		$scope.DisableDay11=true;
	    	}
	    	if(data[i]== "12"){
	    		$scope.DisableDay12=true;
	    	}
	    	if(data[i]== "13"){
	    		$scope.DisableDay13=true;
	    	}
	    	
	    	if(data[i]== "14"){
	    		$scope.DisableDay14=true;
	    	}
	    	if(data[i]== "15"){
	    		$scope.DisableDay15=true;
	    	}
	    	
	    	if(data[i]== "16"){
	    		$scope.DisableDay16=true;
	    	}
	    	if(data[i]== "17"){
	    		$scope.DisableDay17=true;
	    	}
	    	if(data[i]== "18"){
	    		$scope.DisableDay18=true;
	    	}
	    	if(data[i]== "19"){
	    		$scope.DisableDay19=true;
	    	}
	    	if(data[i]== "20"){
	    		$scope.DisableDay20=true;
	    	}
	    	if(data[i]== "21"){
	    		$scope.DisableDay21=true;
	    	}
	    	if(data[i]== "22"){
	    		$scope.DisableDay22=true;
	    	}
	    	if(data[i]== "23"){
	    		$scope.DisableDay23=true;
	    	}
	    	
	    	if(data[i]== "24"){
	    		$scope.DisableDay24=true;
	    	}
	    	if(data[i]== "25"){
	    		$scope.DisableDay25=true;
	    	}
	    	
	    	if(data[i]== "26"){
	    		$scope.DisableDay26=true;
	    	}
	    	if(data[i]== "27"){
	    		$scope.DisableDay27=true;
	    	}
	    	if(data[i]== "28"){
	    		$scope.DisableDay28=true;
	    	}
	    	if(data[i]== "29"){
	    		$scope.DisableDay29=true;
	    	}
	    	if(data[i]== "30"){
	    		$scope.DisableDay30=true;
	    	}
	    	if(data[i]== "31"){
	    		$scope.DisableDay31=true;
	    	}
	    }
		console.log($scope.HideDay);
		
		
		
		
		
		
		}
		
		
		
		
		
		
		
		};
		
		  $scope.AddSheet = function(data) { 
			  var data1=[];
			  data1=$scope.SheetTaskRepeat;
			  

				var ptaskName1="";
				var EmptyTaskName="";
				angular.forEach(data1, function (value, key) {
					ptaskName1 = data1[key].taskName;
					
					if(ptaskName1 =="undefined" || ptaskName1 == null || ptaskName1 ==""){
						EmptyTaskName="Empty";
						
					}
				 });
				
				if(ptaskName1 =="undefined" || ptaskName1 == null || ptaskName1 =="" || EmptyTaskName=="Empty"){
					alert("Please Check Task Name should not be Empty");
					
				}else{
					
					 $scope.SheetTaskRepeat.push(
						     {} 
						     );
				}
			
			  
//			     $scope.SheetTaskRepeat.push(
//			     {} 
//			     );
		
			     
			     
	  };
	  
 $scope.RemoveSheet= function(index) { 
		  
		  var remove=true;
		  
		  angular.element(window).keydown(function () {
		        if(event.keyCode == 13) {
		        		//alert("index=="+index);
		                 event.preventDefault();
		                 return false;
		        }else{
		        	  remove=false;		        	  
		        }
		    });	
		  if(remove){
			  var data2;
    		  data2=$scope.SheetTaskRepeat;
    		  $scope.counter=0;
    		  angular.forEach(data2, function (value, key) {
    			  $scope.counter=$scope.counter+1;	
    				
    			 });
    		  if($scope.counter >1){
    		  	$scope.SheetTaskRepeat.splice(index,1); 
    		  }else{
    			  alert("Atleast one Task is Must");
    		  }
		  }
	  };
	 
//	  $scope.RemoveSheet= function(index) { 
//		  var data2;
//		  data2=$scope.SheetTaskRepeat;
//		  $scope.counter=0;
//		  angular.forEach(data2, function (value, key) {
//			  $scope.counter=$scope.counter+1;	
//				
//			 });
//		  if($scope.counter >1){
//		  	$scope.SheetTaskRepeat.splice(index,1); 
//		  }else{
//			  alert("Atleast one Task is Must");
//		  }
//	  };
	  
	  
	  
	  
	  $scope.ClearSheet= function(data){
			$scope.dataFinal=[];
			 $scope.SheetTaskRepeat=[{}];
			 $scope.sheet = {
	        			
					
						
	        	};
				$scope.hideSelectMonth=false;
				$scope.hideSelectYear=false;
				$scope.hideGoButton=false;
				
			 	$scope.DisableDay01=false;
				$scope.DisableDay02=false;					
				$scope.DisableDay03=false;
				$scope.DisableDay04=false;
				$scope.DisableDay05=false;
				$scope.DisableDay06=false;
				$scope.DisableDay07=false;					
				$scope.DisableDay08=false;
				$scope.DisableDay09=false;
				$scope.DisableDay10=false;
				$scope.DisableDay11=false;
				$scope.DisableDay12=false;					
				$scope.DisableDay13=false;
				$scope.DisableDay14=false;
				$scope.DisableDay15=false;
				$scope.DisableDay16=false;
				$scope.DisableDay17=false;					
				$scope.DisableDay18=false;
				$scope.DisableDay19=false;
				$scope.DisableDay20=false;
				$scope.DisableDay21=false;
				$scope.DisableDay22=false;					
				$scope.DisableDay23=false;
				$scope.DisableDay24=false;
				$scope.DisableDay25=false;
				$scope.DisableDay26=false;
				$scope.DisableDay27=false;					
				$scope.DisableDay28=false;
				$scope.DisableDay29=false;
				$scope.DisableDay30=false;
				$scope.DisableDay31=false;
				$scope.DisableTaskName=false;
				$scope.RemoveSheetImage=false;
				$scope.AddSheetButton=false;
				$scope.DisableSubmit=false;
				
				$scope.DisableManager=false;
				$scope.DisableClient=false;
				$scope.DisableProject=false;
				$scope.DisableResourceComments=false;
				$scope.HideSheetTable=true;
				 $scope.viewDate="";
	  };
	  
	  $scope.SubmitSheet = function(data) { 
		  
		  
		  
		  
		  
		  
		  
		  var sheetMonth="";
		  sheetMonth= $scope.sheet.selectMonth;
			var AssignMonth1="";
			var AssignYear="";
			AssignYear= $scope.sheet.selectYear;
			if(sheetMonth == "January"){
		  		
		  		AssignMonth1="01";
		  	}else if(sheetMonth == "February"){
		  		
		  		AssignMonth1="02";
		  	}else if(sheetMonth == "March"){
		  		
		  		AssignMonth1="03";
		  	}else if(sheetMonth == "April"){
		  		
		  		AssignMonth1="04";
		  	}else if(sheetMonth == "May"){
		  		
		  		AssignMonth1="03";
		  	}else if(sheetMonth == "June"){
		  		
		  		AssignMonth1="06";
		  	}else if(sheetMonth == "July"){
		  		
		  		AssignMonth1="07";
		  	}else if(sheetMonth == "August"){
		  		
		  		AssignMonth1="08";
		  	}else if(sheetMonth == "September"){
		  
		  		AssignMonth1="09";
		  	}else if(sheetMonth == "October"){
		  	
		  		AssignMonth1="10";
		  	}else if(sheetMonth == "November"){
		  		
		  		AssignMonth1="11";
		  	}else if(sheetMonth == "December"){
		  		
		  		AssignMonth1="12";
		  	}
			
			
			
			var dateEdit=AssignYear+"-"+AssignMonth1+"-05";
			
			
			var SheetMonthDate=new Date(dateEdit);
		  
		  
		  
		  
		  
		  
		  
		  var idemp = sessionStorage.getItem("idemp");
		  	var SheetTaskRepeatFinal=[];
		  	var data=[];
			$scope.dataFinal=[];
			data=$scope.SheetTaskRepeat;
			var countDay1=0;
			var countDay2=0;
			var countDay3=0;
			var countDay4=0;
			var countDay5=0;
			var countDay6=0;
			var countDay7=0;
			var countDay8=0;
			var countDay9=0;
			var countDay10=0;
			var countDay11=0;
			var countDay12=0;
			var countDay13=0;
			var countDay14=0;
			var countDay15=0;
			var countDay16=0;
			var countDay17=0;
			var countDay18=0;
			var countDay19=0;
			var countDay20=0;
			var countDay21=0;
			var countDay22=0;
			var countDay23=0;
			var countDay24=0;
			var countDay25=0;
			var countDay26=0;
			var countDay27=0;
			var countDay28=0;
			var countDay29=0;
			var countDay30=0;
			var countDay31=0;
			
			angular.forEach(data, function (value, key) {
			
				$scope.tday1=0;	
				$scope.tday2=0;
				$scope.tday3=0;
				$scope.tday4=0;
				$scope.tday5=0;
				$scope.tday6=0;
				$scope.tday7=0;
				$scope.tday8=0;
				$scope.tday9=0;
				$scope.tday10=0;
				$scope.tday11=0;
				$scope.tday12=0;
				$scope.tday13=0;
				$scope.tday14=0;
				$scope.tday15=0;
				$scope.tday16=0;
				$scope.tday17=0;
				$scope.tday18=0;
				$scope.tday19=0;
				$scope.tday20=0;
				$scope.tday21=0;
				$scope.tday22=0;
				$scope.tday23=0;
				$scope.tday24=0;
				$scope.tday25=0;
				$scope.tday26=0;
				$scope.tday27=0;
				$scope.tday28=0;
				$scope.tday29=0;
				$scope.tday30=0;
				$scope.tday31=0;
			
				 var pday1="";
				 var pday2="";
				 var pday3="";
				 var pday4="";
				 var pday5="";
				 var pday6="";
				 var pday7="";
				 var pday8="";
				 var pday9="";
				 var pday10="";
				 var pday11="";
				 var pday12="";
				 var pday13="";
				 var pday14="";
				 var pday15="";
				 var pday16="";
				 var pday17="";
				 var pday18="";
				 var pday19="";
				 var pday20="";
				 var pday21="";
				 var pday22="";
				 var pday23="";
				 var pday24="";
				 var pday25="";
				 var pday26="";
				 var pday27="";
				 var pday28="";
				 var pday29="";
				 var pday30="";
				 var pday31="";
			

				pday1= data[key].day1;
				pday2= data[key].day2;
				pday3= data[key].day3;
				pday4= data[key].day4;
				pday5= data[key].day5;
				pday6= data[key].day6;
				pday7= data[key].day7;
				pday8= data[key].day8;
				pday9= data[key].day9;
				pday10= data[key].day10;
				pday11= data[key].day11;
				pday12= data[key].day12;
				pday13= data[key].day13;
				pday14= data[key].day14;
				pday15= data[key].day15;
				pday16= data[key].day16;
				pday17= data[key].day17;
				pday18= data[key].day18;
				pday19= data[key].day19;
				pday20= data[key].day20;
				pday21= data[key].day21;
				pday22= data[key].day22;
				pday23= data[key].day23;
				pday24= data[key].day24;
				pday25= data[key].day25;
				pday26= data[key].day26;
				pday27= data[key].day27;
				pday28= data[key].day28;
				pday29= data[key].day29;
				pday30= data[key].day30;
				pday31= data[key].day31;
			
				
				
				
				countDay1=countDay1+parseFloat(pday1);
				$scope.tday1=countDay1;
				
				 countDay2=countDay2+parseFloat(pday2);
				 $scope.tday2=countDay2;
				 
				 countDay3=countDay3+parseFloat(pday3);
				 $scope.tday3=countDay3;
				 
				 countDay4=countDay4+parseFloat(pday4);
				 $scope.tday4=countDay4;
				 
				 countDay5=countDay5+parseFloat(pday5);
				 $scope.tday5=countDay5;
				 
				 countDay6=countDay6+parseFloat(pday6);
				 $scope.tday6=countDay6;
				 
				 countDay7=countDay7+parseFloat(pday7);
				 $scope.tday7=countDay7;
				 
				 countDay8=countDay8+parseFloat(pday8);
				 $scope.tday8=countDay8;
				 
				 countDay9=countDay9+parseFloat(pday9);
				 $scope.tday9=countDay9;
				 
				 countDay10=countDay10+parseFloat(pday10);
				 $scope.tday10=countDay10;
				 
				 countDay11=countDay11+parseFloat(pday11);
				 $scope.tday11=countDay11;
				 
				 countDay12=countDay12+parseFloat(pday12);
				 $scope.tday12=countDay12;
				 
				 countDay13=countDay13+parseFloat(pday13);
				 $scope.tday13=countDay13;
				 
				 countDay14=countDay14+parseFloat(pday14);
				 $scope.tday14=countDay14;
				 
				 countDay15=countDay15+parseFloat(pday15);
				 $scope.tday15=countDay15;
				 
				 countDay16=countDay16+parseFloat(pday16);
				 $scope.tday16=countDay16;
				 
				 countDay17=countDay17+parseFloat(pday17);
				 $scope.tday17=countDay17;
				 
				 countDay18=countDay18+parseFloat(pday18);
				 $scope.tday18=countDay18;
				 
				 countDay19=countDay19+parseFloat(pday19);
				 $scope.tday19=countDay19;
				 
				 countDay20=countDay20+parseFloat(pday20);
				 $scope.tday20=countDay20;
				 
				 countDay21=countDay21+parseFloat(pday21);
				 $scope.tday21=countDay21;
				 
				 countDay22=countDay22+parseFloat(pday21);
				 $scope.tday22=countDay22;
				 
				 countDay23=countDay23+parseFloat(pday23);
				 $scope.tday23=countDay23;
				 
				 countDay24=countDay24+parseFloat(pday24);
				 $scope.tday24=countDay24;
				 
				 countDay25=countDay25+parseFloat(pday25);
				 $scope.tday25=countDay25;
				 
				 countDay26=countDay26+parseFloat(pday26);
				 $scope.tday26=countDay26;
				 
				 countDay27=countDay27+parseFloat(pday27);
				 $scope.tday27=countDay27;
				 
				 countDay28=countDay28+parseFloat(pday28);
				 $scope.tday28=countDay28;
				 
				 countDay29=countDay29+parseFloat(pday29);
				 $scope.tday29=countDay29;
				 
				 countDay30=countDay30+parseFloat(pday30);
				 $scope.tday30=countDay30;
				 
				 countDay31=countDay31+parseFloat(pday31);
				 $scope.tday31=countDay31;
			

			 });
			
			if($scope.tday1 > 8.00 ){
//				$scope.valday1 =true;
				alert("Please check day-01 total which exceeds 08 Hours")
			}else if($scope.tday2 > 8.00){
				alert("Please check day-02 total which exceeds 08 Hours")
			}else if($scope.tday3 > 8.00){
				alert("Please check day-03 total which exceeds 08 Hours")
			}else if($scope.tday4 > 8.00){
				alert("Please check day-04 total which exceeds 08 Hours")
			}else if($scope.tday5 > 8.00){
				alert("Please check day-05 total which exceeds 08 Hours")
			}else if($scope.tday6 > 8.00){
				alert("Please check day-06 total which exceeds 08 Hours")
			}else if($scope.tday7 > 8.00){
				alert("Please check day-07 total which exceeds 08 Hours")
			}else if($scope.tday8 > 8.00){
				alert("Please check day-08 total which exceeds 08 Hours")
			}else if($scope.tday9 > 8.00){
				alert("Please check day-09 total which exceeds 08 Hours")
			}else if($scope.tday10 > 8.00){
				alert("Please check day-10 total which exceeds 08 Hours")
			}else if($scope.tday11 > 8.00){
				alert("Please check day-11 total which exceeds 08 Hours")
			}else if($scope.tday12 > 8.00){
				alert("Please check day-12 total which exceeds 08 Hours")
			}else if($scope.tday13 > 8.00){
				alert("Please check day-13 total which exceeds 08 Hours")
			}else if($scope.tday14 > 8.00){
				alert("Please check day-14 total which exceeds 08 Hours")
			}else if($scope.tday15 > 8.00){
				alert("Please check day-15 total which exceeds 08 Hours")
			}else if($scope.tday16 > 8.00){
				alert("Please check day-16 total which exceeds 08 Hours")
			}else if($scope.tday17 > 8.00){
				alert("Please check day-17 total which exceeds 08 Hours")
			}else if($scope.tday18 > 8.00){
				alert("Please check day-18 total which exceeds 08 Hours")
			}else if($scope.tday19 > 8.00){
				alert("Please check day-19 total which exceeds 08 Hours")
			}else if($scope.tday20 > 8.00){
				alert("Please check day-20 total which exceeds 08 Hours")
			}else if($scope.tday21 > 8.00){
				alert("Please check day-21 total which exceeds 08 Hours")
			}else if($scope.tday22 > 8.00){
				alert("Please check day-22 total which exceeds 08 Hours")
			}else if($scope.tday23 > 8.00){
				alert("Please check day-23 total which exceeds 08 Hours")
			}else if($scope.tday24 > 8.00){
				alert("Please check day-24 total which exceeds 08 Hours")
			}else if($scope.tday25 > 8.00){
				alert("Please check day-25 total which exceeds 08 Hours")
			}else if($scope.tday26 > 8.00){
				alert("Please check day-26 total which exceeds 08 Hours")
			}else if($scope.tday27 > 8.00){
				alert("Please check day-27 total which exceeds 08 Hours")
			}else if($scope.tday28 > 8.00){
				alert("Please check day-28 total which exceeds 08 Hours")
			}else if($scope.tday29 > 8.00){
				alert("Please check day-29 total which exceeds 08 Hours")
			}else if($scope.tday30 > 8.00){
				alert("Please check day-30 total which exceeds 08 Hours")
			}else if($scope.tday31 > 8.00){
				alert("Please check day-31 total which exceeds 08 Hours")
			}else{
//				$scope.valday1 =false;
			angular.forEach(data, function (value, key) {
				
				 var ptaskName="";
				 var psheetId=null;
				 var pTimeSheetMonth="";
				 var pday1="";
				 var pday2="";
				 var pday3="";
				 var pday4="";
				 var pday5="";
				 var pday6="";
				 var pday7="";
				 var pday8="";
				 var pday9="";
				 var pday10="";
				 var pday11="";
				 var pday12="";
				 var pday13="";
				 var pday14="";
				 var pday15="";
				 var pday16="";
				 var pday17="";
				 var pday18="";
				 var pday19="";
				 var pday20="";
				 var pday21="";
				 var pday22="";
				 var pday23="";
				 var pday24="";
				 var pday25="";
				 var pday26="";
				 var pday27="";
				 var pday28="";
				 var pday29="";
				 var pday30="";
				 var pday31="";
				 var presourceComments="";
				 var pproject="";
				 var pclient="";
				 var pclientManager="";
				 var pselectMonth="";
				 var pselectYear="";
				
				psheetId = data[key].sheetId;
				ptaskName = data[key].taskName;
				pday1= data[key].day1;
				pday2= data[key].day2;
				pday3= data[key].day3;
				pday4= data[key].day4;
				pday5= data[key].day5;
				pday6= data[key].day6;
				pday7= data[key].day7;
				pday8= data[key].day8;
				pday9= data[key].day9;
				pday10= data[key].day10;
				pday11= data[key].day11;
				pday12= data[key].day12;
				pday13= data[key].day13;
				pday14= data[key].day14;
				pday15= data[key].day15;
				pday16= data[key].day16;
				pday17= data[key].day17;
				pday18= data[key].day18;
				pday19= data[key].day19;
				pday20= data[key].day20;
				pday21= data[key].day21;
				pday22= data[key].day22;
				pday23= data[key].day23;
				pday24= data[key].day24;
				pday25= data[key].day25;
				pday26= data[key].day26;
				pday27= data[key].day27;
				pday28= data[key].day28;
				pday29= data[key].day29;
				pday30= data[key].day30;
				pday31= data[key].day31;
				presourceComments= data[key].resourceComments;
				pproject=data[key].project;
				pclient=data[key].client;
				pclientManager=data[key].clientManager;
				
				pselectMonth=data[key].selectMonth;
				pselectYear=data[key].selectYear;
				
				var data_r;
				if(ptaskName ==null || ptaskName =="undefined" || $scope.tday1 > 8.00 || $scope.tday2 > 8.00
						
				
				
				
				
				){
					
				}else{
					
					
					
					
				 data_r= {
							
							sheetId:psheetId,
							employeeId:idemp,
							timeSheetMonth:SheetMonthDate,
							selectMonth:$scope.sheet.selectMonth,
							selectYear:$scope.sheet.selectYear,
							taskName:ptaskName,							
							day1:pday1,
							day2:pday2,
							day3:pday3,
							day4:pday4,
							day5:pday5,
							day6:pday6,
							day7:pday7,
							day8:pday8,
							day9:pday9,
							day10:pday10,
							day11:pday11,
							day12:pday12,
							day13:pday13,
							day14:pday14,
							day15:pday15,
							day16:pday16,
							day17:pday17,
							day18:pday18,
							day19:pday19,
							day20:pday20,
							day21:pday21,
							day22:pday22,
							day23:pday23,
							day24:pday24,
							day25:pday25,
							day26:pday26,
							day27:pday27,
							day28:pday28,
							day29:pday29,
							day30:pday30,
							day31:pday31,
							resourceComments:$scope.sheet.resourceComments,
							project:$scope.sheet.project,
							client:$scope.sheet.client,
							clientManager:$scope.sheet.clientManager,
							status:"Pending"
					};
				 
				 $scope.dataFinal.push(data_r);
//				alert("$scope.dataFinal"+$scope.dataFinal);
				}
				
				 });
			
			}
			
			if($scope.tday1 > 8.00 ){
				
				
			}else if($scope.tday2 > 8.00){
				
			}else if($scope.tday3 > 8.00){
				
			}else if($scope.tday4 > 8.00){
			
			}else if($scope.tday5 > 8.00){
				
			}else if($scope.tday6 > 8.00){
				
			}else if($scope.tday7 > 8.00){
				
			}else if($scope.tday8 > 8.00){
				
			}else if($scope.tday9 > 8.00){
				
			}else if($scope.tday10 > 8.00){
				
			}else if($scope.tday11 > 8.00){
				
			}else if($scope.tday12 > 8.00){
				
			}else if($scope.tday13 > 8.00){
				
			}else if($scope.tday14 > 8.00){
				
			}else if($scope.tday15 > 8.00){
				
			}else if($scope.tday16 > 8.00){
				
			}else if($scope.tday17 > 8.00){
			
			}else if($scope.tday18 > 8.00){
				
			}else if($scope.tday19 > 8.00){
				
			}else if($scope.tday20 > 8.00){
				
			}else if($scope.tday21 > 8.00){
				
			}else if($scope.tday22 > 8.00){
				
			}else if($scope.tday23 > 8.00){
				
			}else if($scope.tday24 > 8.00){
				
			}else if($scope.tday25 > 8.00){
				
			}else if($scope.tday26 > 8.00){
				
			}else if($scope.tday27 > 8.00){
				
			}else if($scope.tday28 > 8.00){
				
			}else if($scope.tday29 > 8.00){
				
			}else if($scope.tday30 > 8.00){
				
			}else if($scope.tday31 > 8.00){
				
			}else {
				var ptaskName1="";
				var EmptyTaskName="";
				angular.forEach(data, function (value, key) {
					ptaskName1 = data[key].taskName;
					
					if(ptaskName1 =="undefined" || ptaskName1 == null || ptaskName1 ==""){
						EmptyTaskName="Empty";
						
					}
				 });
				
				if(ptaskName1 =="undefined" || ptaskName1 == null || ptaskName1 =="" || EmptyTaskName=="Empty"){
					alert("Please Check Task Name should not be Empty");
					
				}else{
					
					timesheetService.submitNewSheet( $scope.dataFinal,$scope);
				}
			}
	  };
	  
	 
	  $scope.EditSheet= function(date){
		  
		  
		  
			$scope.DisableDay01=false;
			$scope.DisableDay02=false;					
			$scope.DisableDay03=false;
			$scope.DisableDay04=false;
			$scope.DisableDay05=false;
			$scope.DisableDay06=false;
			$scope.DisableDay07=false;					
			$scope.DisableDay08=false;
			$scope.DisableDay09=false;
			$scope.DisableDay10=false;
			$scope.DisableDay11=false;
			$scope.DisableDay12=false;					
			$scope.DisableDay13=false;
			$scope.DisableDay14=false;
			$scope.DisableDay15=false;
			$scope.DisableDay16=false;
			$scope.DisableDay17=false;					
			$scope.DisableDay18=false;
			$scope.DisableDay19=false;
			$scope.DisableDay20=false;
			$scope.DisableDay21=false;
			$scope.DisableDay22=false;					
			$scope.DisableDay23=false;
			$scope.DisableDay24=false;
			$scope.DisableDay25=false;
			$scope.DisableDay26=false;
			$scope.DisableDay27=false;					
			$scope.DisableDay28=false;
			$scope.DisableDay29=false;
			$scope.DisableDay30=false;
			$scope.DisableDay31=false;
			
			$scope.DisableManager=false;
			$scope.DisableClient=false;
			$scope.DisableProject=false;
			$scope.DisableResourceComments=false;
			
			$scope.DisableSubmit=false;
			$scope.DisableTaskName=false;
			$scope.RemoveSheetImage=false;
			$scope.AddSheetButton=false;
			$scope.DisableClear=false;
			
			
			
		
			$scope.lastMonthDate1= new Date(date.getFullYear(), date.getMonth()+1, 0, 23, 59, 59).getDate();
			
			
			var d = new Date(date);
			
		    var weekday = new Array(7);
		    weekday[0] = "Sunday";
		    weekday[1] = "Monday";
		    weekday[2] = "Tuesday";
		    weekday[3] = "Wednesday";
		    weekday[4] = "Thursday";
		    weekday[5] = "Friday";
		    weekday[6] = "Saturday";
		    $scope.HideDay=[];
		    for(var i=1;i<=$scope.lastMonthDate1;i++){
		    	var y=d.getFullYear();
			    var m=date.getMonth()+1;
			    var dd=i;
			    
			 // 'October 10, 2021 23:15:30'
			    var monthname="";
			    if(m=="1"){
			    	monthname="January";
			    }else if(m=="2"){
			    	monthname="February";
			    }else if(m=="3"){
			    	monthname="March"
			    }else if(m=="4"){
			    	monthname="April"
			    }else if(m=="5"){
			    	monthname="May"
			    }else if(m=="6"){
			    	monthname="June"
			    }else if(m=="7"){
			    	monthname="July"
			    }else if(m=="8"){
			    	monthname="August"
			    }else if(m=="9"){
			    	monthname="September"
			    }else if(m=="10"){
			    	monthname="October"
			    }else if(m=="11"){
			    	monthname="November"
			    }else if(m=="12"){
			    	monthname="December"
			    }
			    
			    
			    var EditDate1=''+monthname+' '+i+', '+y+' 23:15:30';
			    
			    console.log(EditDate1);
			    var d2 = new Date(EditDate1);
			    
			    
			    var EditDate=y+'-'+m+'-'+dd;
			    
		    var n = weekday[d2.getDay()];
			console.log(n);
			
			if(n == "Sunday" || n == "Saturday" ){
				
				$scope.HideDay.push(i);
				
			}
			
		    }
		   if($scope.lastMonthDate1 < "31"){
			   $scope.HideDay31=true;
		   }
		   if($scope.lastMonthDate1 < "30"){
			   $scope.HideDay30=true;
		   }
		   if($scope.lastMonthDate1 < "29"){
			   $scope.HideDay29=true;
		   }
		   if($scope.lastMonthDate1 < "28"){
			   $scope.HideDay28=true;
		   }
		    
		    
		    var data=[];
		    data=$scope.HideDay;
		    for(var i=0;i<=data.length;i++){
		    	if(data[i]== "01"){
		    		$scope.DisableDay01=true;
		    	}
		    	if(data[i]== "02"){
		    		$scope.DisableDay02=true;
		    	}
		    	if(data[i]== "03"){
		    		$scope.DisableDay03=true;
		    	}
		    	
		    	if(data[i]== "04"){
		    		$scope.DisableDay04=true;
		    	}
		    	if(data[i]== "05"){
		    		$scope.DisableDay05=true;
		    	}
		    	
		    	if(data[i]== "06"){
		    		$scope.DisableDay06=true;
		    	}
		    	if(data[i]== "07"){
		    		$scope.DisableDay07=true;
		    	}
		    	if(data[i]== "08"){
		    		$scope.DisableDay08=true;
		    	}
		    	if(data[i]== "09"){
		    		$scope.DisableDay09=true;
		    	}
		    	if(data[i]== "10"){
		    		$scope.DisableDay10=true;
		    	}
		    	if(data[i]== "11"){
		    		$scope.DisableDay11=true;
		    	}
		    	if(data[i]== "12"){
		    		$scope.DisableDay12=true;
		    	}
		    	if(data[i]== "13"){
		    		$scope.DisableDay13=true;
		    	}
		    	
		    	if(data[i]== "14"){
		    		$scope.DisableDay14=true;
		    	}
		    	if(data[i]== "15"){
		    		$scope.DisableDay15=true;
		    	}
		    	
		    	if(data[i]== "16"){
		    		$scope.DisableDay16=true;
		    	}
		    	if(data[i]== "17"){
		    		$scope.DisableDay17=true;
		    	}
		    	if(data[i]== "18"){
		    		$scope.DisableDay18=true;
		    	}
		    	if(data[i]== "19"){
		    		$scope.DisableDay19=true;
		    	}
		    	if(data[i]== "20"){
		    		$scope.DisableDay20=true;
		    	}
		    	if(data[i]== "21"){
		    		$scope.DisableDay21=true;
		    	}
		    	if(data[i]== "22"){
		    		$scope.DisableDay22=true;
		    	}
		    	if(data[i]== "23"){
		    		$scope.DisableDay23=true;
		    	}
		    	
		    	if(data[i]== "24"){
		    		$scope.DisableDay24=true;
		    	}
		    	if(data[i]== "25"){
		    		$scope.DisableDay25=true;
		    	}
		    	
		    	if(data[i]== "26"){
		    		$scope.DisableDay26=true;
		    	}
		    	if(data[i]== "27"){
		    		$scope.DisableDay27=true;
		    	}
		    	if(data[i]== "28"){
		    		$scope.DisableDay28=true;
		    	}
		    	if(data[i]== "29"){
		    		$scope.DisableDay29=true;
		    	}
		    	if(data[i]== "30"){
		    		$scope.DisableDay30=true;
		    	}
		    	if(data[i]== "31"){
		    		$scope.DisableDay31=true;
		    	}
		    }
			
			
			
			
			
			
			
	  };
	  
	  
	  // admin
	  $scope.hideTableAdmin=true;
	  $scope.DisableAccept=false;
	  $scope.DisableReject=false;
		
	  $scope.CancelAdmin=function(){
		  $scope.adminSheet={};
		  $scope.hideTableAdmin=true;  
	  };
	  $scope.AcceptAdmin=function(sheetId){
		 
		  var StatusAccept="";
		  StatusAccept="Accepted";
		  $scope.PostStatusAccept=[sheetId,StatusAccept];
		  
		  
		  timesheetService.PostTimeSheetStatus($scope.PostStatusAccept,$scope ); 
		    
		  
	  };
	  $scope.RejectAdmin=function(sheetId){
			 
		  var StatusReject="";
		  StatusReject="Rejected";
		  $scope.PostStatusReject=[sheetId,StatusReject];
		  		  
		  timesheetService.PostTimeSheetStatus($scope.PostStatusReject,$scope ); 
		    
		  
		  };
	  
	  
	  $scope.getTimeSheetData=function(empId,sheetMonth,selectYear){
		  
		  $scope.DisableAccept=false;
		  $scope.DisableReject=false;
		  
		  $scope.SheetTaskRepeatAdimn=[];
		  $scope.timesheetId="";
		  var AssignMonth1="";
		  var AssignYear="";
		  AssignYear=selectYear;
		
		  
		  if(sheetMonth == "January"){
		  		
		  		AssignMonth1="01";
		  	}else if(sheetMonth == "February"){
		  		
		  		AssignMonth1="02";
		  	}else if(sheetMonth == "March"){
		  		
		  		AssignMonth1="03";
		  	}else if(sheetMonth == "April"){
		  		
		  		AssignMonth1="04";
		  	}else if(sheetMonth == "May"){
		  		
		  		AssignMonth1="03";
		  	}else if(sheetMonth == "June"){
		  		
		  		AssignMonth1="06";
		  	}else if(sheetMonth == "July"){
		  		
		  		AssignMonth1="07";
		  	}else if(sheetMonth == "August"){
		  		
		  		AssignMonth1="08";
		  	}else if(sheetMonth == "September"){
		  
		  		AssignMonth1="09";
		  	}else if(sheetMonth == "October"){
		  	
		  		AssignMonth1="10";
		  	}else if(sheetMonth == "November"){
		  		
		  		AssignMonth1="11";
		  	}else if(sheetMonth == "December"){
		  		
		  		AssignMonth1="12";
		  	}
		  
		  
		  
		  var dateEdit=AssignYear+"-"+AssignMonth1+"-05";
			
			
			var date=new Date(dateEdit);
			
		


			$scope.DisableSubmit=false;
			$scope.HideSheetTable=false;
			
			$scope.DisableDay01=false;
			$scope.DisableDay02=false;					
			$scope.DisableDay03=false;
			$scope.DisableDay04=false;
			$scope.DisableDay05=false;
			$scope.DisableDay06=false;
			$scope.DisableDay07=false;					
			$scope.DisableDay08=false;
			$scope.DisableDay09=false;
			$scope.DisableDay10=false;
			$scope.DisableDay11=false;
			$scope.DisableDay12=false;					
			$scope.DisableDay13=false;
			$scope.DisableDay14=false;
			$scope.DisableDay15=false;
			$scope.DisableDay16=false;
			$scope.DisableDay17=false;					
			$scope.DisableDay18=false;
			$scope.DisableDay19=false;
			$scope.DisableDay20=false;
			$scope.DisableDay21=false;
			$scope.DisableDay22=false;					
			$scope.DisableDay23=false;
			$scope.DisableDay24=false;
			$scope.DisableDay25=false;
			$scope.DisableDay26=false;
			$scope.DisableDay27=false;					
			$scope.DisableDay28=false;
			$scope.DisableDay29=false;
			$scope.DisableDay30=false;
			$scope.DisableDay31=false;
			$scope.HideDay28=false;
			$scope.HideDay29=false;
			$scope.HideDay30=false;
			$scope.HideDay31=false;
			$scope.DisableEdit=false;
			$scope.DisableSubmit=false;
			$scope.DisableClear=false;
			$scope.DisableTaskName=false;
			$scope.AddSheetButton=false;
			$scope.RemoveSheetImage=false;

$scope.lastMonthDate2= new Date(date.getFullYear(), date.getMonth()+1, 0, 23, 59, 59).getDate();
		
		
		var d = new Date(date);
		
	    var weekday = new Array(7);
	    weekday[0] = "Sunday";
	    weekday[1] = "Monday";
	    weekday[2] = "Tuesday";
	    weekday[3] = "Wednesday";
	    weekday[4] = "Thursday";
	    weekday[5] = "Friday";
	    weekday[6] = "Saturday";
	    $scope.HideDay=[];
	    for(var i=1;i<=$scope.lastMonthDate2;i++){
	    	var y=d.getFullYear();
		    var m=date.getMonth()+1;
		    var dd=i;
		    var EditDate=y+'-'+m+'-'+dd;
		    var d2 = new Date(EditDate);
	    var n = weekday[d2.getDay()];
		console.log(n);
		
		if(n == "Sunday" || n == "Saturday" ){
			
			$scope.HideDay.push(i);
			
		}
		
	    }
	   if($scope.lastMonthDate2 < "31"){
		   $scope.HideDay31=true;
	   }
	   if($scope.lastMonthDate2 < "30"){
		   $scope.HideDay30=true;
	   }
	   if($scope.lastMonthDate2 < "29"){
		   $scope.HideDay29=true;
	   }
	   if($scope.lastMonthDate2 < "28"){
		   $scope.HideDay28=true;
	   }
	    
	    
	    var data=[];
	    data=$scope.HideDay;
	    for(var i=0;i<=data.length;i++){
	    	if(data[i]== "01"){
	    		$scope.DisableDay01=true;
	    	}
	    	if(data[i]== "02"){
	    		$scope.DisableDay02=true;
	    	}
	    	if(data[i]== "03"){
	    		$scope.DisableDay03=true;
	    	}
	    	
	    	if(data[i]== "04"){
	    		$scope.DisableDay04=true;
	    	}
	    	if(data[i]== "05"){
	    		$scope.DisableDay05=true;
	    	}
	    	
	    	if(data[i]== "06"){
	    		$scope.DisableDay06=true;
	    	}
	    	if(data[i]== "07"){
	    		$scope.DisableDay07=true;
	    	}
	    	if(data[i]== "08"){
	    		$scope.DisableDay08=true;
	    	}
	    	if(data[i]== "09"){
	    		$scope.DisableDay09=true;
	    	}
	    	if(data[i]== "10"){
	    		$scope.DisableDay10=true;
	    	}
	    	if(data[i]== "11"){
	    		$scope.DisableDay11=true;
	    	}
	    	if(data[i]== "12"){
	    		$scope.DisableDay12=true;
	    	}
	    	if(data[i]== "13"){
	    		$scope.DisableDay13=true;
	    	}
	    	
	    	if(data[i]== "14"){
	    		$scope.DisableDay14=true;
	    	}
	    	if(data[i]== "15"){
	    		$scope.DisableDay15=true;
	    	}
	    	
	    	if(data[i]== "16"){
	    		$scope.DisableDay16=true;
	    	}
	    	if(data[i]== "17"){
	    		$scope.DisableDay17=true;
	    	}
	    	if(data[i]== "18"){
	    		$scope.DisableDay18=true;
	    	}
	    	if(data[i]== "19"){
	    		$scope.DisableDay19=true;
	    	}
	    	if(data[i]== "20"){
	    		$scope.DisableDay20=true;
	    	}
	    	if(data[i]== "21"){
	    		$scope.DisableDay21=true;
	    	}
	    	if(data[i]== "22"){
	    		$scope.DisableDay22=true;
	    	}
	    	if(data[i]== "23"){
	    		$scope.DisableDay23=true;
	    	}
	    	
	    	if(data[i]== "24"){
	    		$scope.DisableDay24=true;
	    	}
	    	if(data[i]== "25"){
	    		$scope.DisableDay25=true;
	    	}
	    	
	    	if(data[i]== "26"){
	    		$scope.DisableDay26=true;
	    	}
	    	if(data[i]== "27"){
	    		$scope.DisableDay27=true;
	    	}
	    	if(data[i]== "28"){
	    		$scope.DisableDay28=true;
	    	}
	    	if(data[i]== "29"){
	    		$scope.DisableDay29=true;
	    	}
	    	if(data[i]== "30"){
	    		$scope.DisableDay30=true;
	    	}
	    	if(data[i]== "31"){
	    		$scope.DisableDay31=true;
	    	}
	    } 
		  
		  
		  
		 
		  
		  
		  
		  
		  
		  if((empId==null) ||(empId =="") ||(sheetMonth==null)||(sheetMonth="")||(selectYear==null)||(selectYear="")){
			  alert("Please Choose Employee  Month and Year");
		  }else{
			  var timesheetId1="";
			   timesheetId1 ="EMPID"+empId+"-"+AssignYear+"-"+AssignMonth1;
			  $scope.timesheetId=timesheetId1;
			 
			  timesheetService.getTimeSheetAdmin($scope.timesheetId,$scope ); 
			  timesheetService.getTimeSheetEmpName(empId,$scope ); 
			  
		  }
		  
		  
		  
		  
		  
		  
		  
	  };
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	}
	else {
		$window.location.href = 'login.jsp';

	}

	 });




app.directive('validNumber', function() {
    return {
      require: '?ngModel',
      link: function(scope, element, attrs, ngModelCtrl) {
        if(!ngModelCtrl) {
          return; 
        }

        ngModelCtrl.$parsers.push(function(val) {
          if (angular.isUndefined(val)) {
              var val = '';
          }
          
          var clean = val.replace(/[^-0-9\.]/g, '');
          var negativeCheck = clean.split('-');
			var decimalCheck = clean.split('.');
          if(!angular.isUndefined(negativeCheck[1])) {
              negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
              clean =negativeCheck[0] + '-' + negativeCheck[1];
              if(negativeCheck[0].length > 0) {
              	clean =negativeCheck[0];
              }
              
          }
            
          if(!angular.isUndefined(decimalCheck[1])) {
              decimalCheck[1] = decimalCheck[1].slice(0,2);
              clean =decimalCheck[0] + '.' + decimalCheck[1];
          }

          if (val !== clean) {
            ngModelCtrl.$setViewValue(clean);
            ngModelCtrl.$render();
          }
          return clean;
        });

        element.bind('keypress', function(event) {
          if(event.keyCode === 32) {
            event.preventDefault();
          }
        });
      }
    };
  });
