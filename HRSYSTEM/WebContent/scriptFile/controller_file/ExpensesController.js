'use strict';

app.controller('ExpensesCtrl', function($scope, $window,ExpensesService, $http, $rootScope) {
 

	var sess = sessionStorage.getItem("key1");
	if (sess) {
	
		$scope.iddisableReportDate=true;
		$scope.hideexpenseform=true;
		$scope.hidestatus=false;
		$scope.hideback1=true;
		$scope.Showexpense=function(){
			$scope.viewEmp=true;
			$scope.hideexpenseform=false;
			$scope.hideexpensetable=true;
			$scope.hideback1=true;
			$scope.hidestatus=true;
			$scope.resumenameui="";
		};
		$scope.Backpage1=function(){
			
			$scope.viewEmp=true;
			$scope.hideexpenseform=true;
			$scope.hideexpensetable=false;
			$scope.hideback1=true;
		};
		$scope.expensecode = ["1410-Payroll Advance",
		                      "1500-Equipment", 
		                      "6120-Bank Service Charges",
		                      "6160-Dues and Subscriptions",
		                      "6180-Insurance",
		                      "6230-Licenses and Permits",
		                      "6240-Miscellaneous",
		                      "6390-Training",
		                      "6525-Office Furniture",
		                      "6550-Office Supplies",
		                      "6250-Postage and Delivery",
		                      "6260-Printing and Reproduction",
		                      "6340-Telephone",
		                      "6350-Travel & Ent:6360 - Entertainment",
		                      "6350-Travel & Ent:6365 - Lodging",
		                      "6350-Travel & Ent:6370 - Meals",
		                      "6350-Travel & Ent:6380 - Travel",
		                      "6350-Travel & Ent:6385 - Per diem",
		                      "6999-Uncategorized Expenses"];

		$scope.Statuscode = ["Pending","Accepted","Rejected"];
		
		$scope.submit = function(data) {
		
			
			var PdfCheck=["",""];
			var resumename2;
			
			if($scope.contacts != null || $scope.contacts !="" || $scope.contacts !="undefined"){
				
				if($scope.expense.file == "undefined" || $scope.expense.file == null || $scope.expense.file == ""){
					resumename2="test.pdf1";
					PdfCheck[1] = resumename2.split('.')[1];
					
				}
				else{
					resumename2=$scope.expense.file.filename;
					PdfCheck[1] = resumename2.split('.')[1];
					
				}
				
			}else{
				
				if($scope.expense.file == "undefined" || $scope.expense.file == null || $scope.expense.file == ""){
					resumename2="test.pdf1";
					PdfCheck[1] = resumename2.split('.')[1];
					
				}
				else{
					resumename2=$scope.expense.file.filename;
					PdfCheck[1] = resumename2.split('.')[1];
					
				}
				
			}
			
			
			
			
			var j;
			for(j in $scope.contacts) {
				
	            if($scope.contacts[j].expenseId == $scope.expense.expenseId  ) {
	            	
	            	if($scope.expense.file == null){
            			resumename2=$scope.contacts[j].fileName;
            			PdfCheck[1] = resumename2.split('.')[1];
            		}
            		else{
            			resumename2=$scope.expense.file.filename;
            			PdfCheck[1] = resumename2.split('.')[1];
            		}
	            }
			}
			
			
			

			 
			
			if($scope.expense.amt == null||$scope.expense.amt==0.00){
				alert("Please Enter the Expense Amount");
				//$scope.expense.amt=0.00;
			}else if($scope.expense.expenseAccountCode == null){
				alert("Please select the Account Code ");
			}else if(PdfCheck[1] != "pdf" ){
			
//			if($scope.expense.amt == null || $scope.expense.expenseAccountCode == null || PdfCheck[1] != "pdf" ){
					
				
				alert("Please attach Expense receipt in PDF format");
			}else{
			
			var empId = sessionStorage.getItem("idemp");
			
			if($scope.expense.expenseId == null){
			data= {
					
					expenseId:$scope.expense.expenseId,
					employeeId:empId,
					expenseAccountCode:$scope.expense.expenseAccountCode,
					reportingDate:$scope.expense.reportingdate,
					submissionDate:$scope.expense.submissiondate,
					expenseAmt:$scope.expense.amt.toString(),
					expensememo:$scope.expense.memo,
					expenseEmpName:"",
					expenseFile:$scope.expense.file.base64,
					fileName:$scope.expense.file.filename,
					fileType:$scope.expense.file.filetype,
					fileSize:$scope.expense.file.filesize,
					expenseTotal:"0",
					expenseStatus:"Pending",
					expenseNote:"Total expenses to be reimbursed from N2"
					
					
					
	 				  
			};
			}else{
				var i;
		        for(i in $scope.contacts) {
		            if($scope.contacts[i].expenseId == $scope.expense.expenseId  ) {
		            	
		            	var resumename1;
		            	var resume1;
		            	var contenttype1;
		            	var filesize1;
//		            	alert("$scope.localbench.file  "+$scope.localbench.file);
		            		if($scope.expense.file == null){
		            			resumename1=$scope.contacts[i].fileName;
		            			resume1=$scope.contacts[i].expenseFile;
		            			contenttype1=$scope.contacts[i].fileType;
		            			filesize1=$scope.contacts[i].fileSize;
		            		}
		            		else{
		            			resumename1=$scope.expense.file.filename;
		            			resume1=$scope.expense.file.base64;
		            			contenttype1=$scope.expense.file.filetype;
		            			filesize1=$scope.expense.file.filesize;
		            		}
		            		data= {
		        					
		        					expenseId:$scope.expense.expenseId,
		        					employeeId:empId,
		        					expenseAccountCode:$scope.expense.expenseAccountCode,
		        					reportingDate:$scope.expense.reportingdate,
		        					submissionDate:$scope.expense.submissiondate,
		        					expenseAmt:$scope.expense.amt.toString(),
		        					expensememo:$scope.expense.memo,
		        					expenseEmpName:"",
		        					expenseFile:resume1,
		        					fileName:resumename1,
		        					fileType:contenttype1,
		        					fileSize:filesize1,
		        					expenseTotal:"0",
		        					expenseStatus:"Pending",
		        					expenseNote:"Total expenses to be reimbursed from N2"
		        					
		        					
		        					
		        	 				  
		        			};
			}
		  }
		}
			$scope.expense={};
			ExpensesService.submit(data,$scope);
			
			}
	};
	
	$scope.EditExpense=function(expenseId){
		//alert("inside edit");
		$scope.hidestatus=true;
		$scope.hideexpenseform=false;
		var i;
        for(i in $scope.contacts) {
            if($scope.contacts[i].expenseId == expenseId) {
            	
            	$scope.resumenameui=$scope.contacts[i].fileName;
            	$scope.expense={
            			
            			expenseId:$scope.contacts[i].expenseId,
            			employeeId:$scope.contacts[i].employeeId,
            			expenseAccountCode:$scope.contacts[i].expenseAccountCode,
            			reportingdate:new Date($scope.contacts[i].reportingDate),
            			submissiondate:new Date($scope.contacts[i].submissionDate),
            			amt:$scope.contacts[i].expenseAmt,
            			memo:$scope.contacts[i].expensememo,
            			expenseEmpName:$scope.contacts[i].expenseEmpName,
            			expenseTotal:$scope.contacts[i].expenseTotal,
            			expenseStatus:$scope.contacts[i].expenseStatus,
            			expenseNote:$scope.contacts[i].expenseNote	
	            			
	            			
            	};
            }
        }
	};
	
	
	
	
	$scope.DeleteExpense=function(expenseId){
		var deleteUser = $window.confirm('Do you want to delete this record?');
		if(deleteUser){
			var empId = sessionStorage.getItem("idemp");
			var DeleteExp=[];
			DeleteExp=[empId,expenseId];
			ExpensesService.DeleteExpense(DeleteExp,$scope);
		 }
	};

	
	$scope.employeeList1 = [];
	$scope.employeeList1 = function() {
		var doj = sessionStorage.getItem("jod");
		$scope.expense = {
							reportingdate:doj=new Date(doj),
							submissiondate:new Date()
							
				};
		
		var response;
		 response = $http.get('expenseemplist');
		
		 response.success(function(data, status, headers, config) {
			 console.log(data);
				$scope.contactsemp = data;
		 });
			response.error(function(data, status, headers, config) {
				alert("Error in connection");
			});
		
		
//		
//		$http({
//			method : 'GET',
//			url : 'http://localhost:8086/HRSYSTEM/expenseemplist',
//			data : {}
//		}).success(function(result) {
//			
//			console.log(result);
//			$scope.contactsemp = result;
//			 
//			
//			
//		});
			
	};
	$scope.employeeList1();
	
	$scope.employeeList2 = [];
	$scope.employeeList2 = function() {
		
		
		var response;
		 response = $http.get('employeelist1');
		
		 response.success(function(data, status, headers, config) {
			 console.log(data);
			 $scope.contactsemplist = data;
		 });
			response.error(function(data, status, headers, config) {
				alert("Error in connection");
			});
		
		
		
//		$http({
//			method : 'GET',
//			url : 'http://localhost:8086/HRSYSTEM/employeelist1',
//			data : {}
//		}).success(function(result) {
//			
//			
//			$scope.contactsemplist = result;
//			
//			
//			
//		});
	};
	$scope.employeeList2();
	
	$scope.expenseList2 = [];
	$scope.expenseList2 = function() {
		
		
		var response;
		 response = $http.get('expenselistadmin');
		
		 response.success(function(data, status, headers, config) {
			 console.log(data);
			 $scope.contactsAdEmp = data;
				$scope.total = function(data) {
					 $scope.amt1=0;
					 angular.forEach(data, function (value, key) {
					      	
						 var amt2 = data[key].expenseAmt;
						 $scope.amt1=parseFloat($scope.amt1)+parseFloat(amt2);	
			              	
			              
			              
			          });
						}
				 $scope.total(data);
				
		 });
			response.error(function(data, status, headers, config) {
				alert("Error in connection");
			});
		
		
		
		
//		$http({
//			method : 'GET',
//			url : 'http://localhost:8086/HRSYSTEM/expenselistadmin',
//			data : {}
//		}).success(function(result) {
//			
//			
//			$scope.contactsAdEmp = result;
//			$scope.total = function(result) {
//				 $scope.amt1=0;
//				 angular.forEach(result, function (value, key) {
//				      	
//					 var amt2 = result[key].expenseAmt;
//					 $scope.amt1=parseFloat($scope.amt1)+parseFloat(amt2);	
//		              	
//		              
//		              
//		          });
//					}
//			 $scope.total(result);
//			
//			
//		});
	};
	$scope.expenseList2();
	
	$scope.cancel = function (expense){
		
		$scope.expense = {};     			
		var doj = sessionStorage.getItem("jod");
		$scope.expense = {
							reportingdate:doj=new Date(doj),
							submissiondate:new Date()
							
				};
	
		$scope.hideexpenseform=true;
		$scope.hideexpensetable=false;
		$scope.hidestatus=false;
		$scope.resumenameui="";
	}
	
	$scope.filterexpenseempAd = function(expenseempAd){
		$scope.contactsAdEmp = {};
		if(expenseempAd == null){
			$scope.expenseList2();
		}
		else{
		ExpensesService.filterexpenseempAd(expenseempAd,$scope);
		}
		
	}
	
	$scope.filterexpenseempAdStatus= function(empId,status){
		$scope.contactsAdEmp = {};
		var filterstatus = [];
		filterstatus = [empId,status];
		ExpensesService.filterexpenseempAdStatus(filterstatus,$scope);
	}
	
	
	
$scope.View = function(expenseId){
	$scope.casthide=true;
		ExpensesService.View(expenseId,$scope);
		$scope.hideviewexpense=false;
		$scope.hidefilterAdmin=true;
		$scope.hidechooseEmployee=true;
	};
	
	
	
	$scope.CancelView = function(){
		$scope.hideviewexpense=true;
		$scope.hidenote=true;
		$scope.hidechooseEmployee=false;
	};
	
	$scope.viewEmp=true;
	$scope.ViewEmp = function(expenseId){
		
		ExpensesService.View(expenseId,$scope);
		$scope.viewEmp=false;
		$scope.hideback1=false;
	};
	
	
	
	
	
	$scope.expenseList = [];
	$scope.expenseList = function() {
		var empId = sessionStorage.getItem("idemp");
		ExpensesService.expenseget(empId, $scope);
		}
	
	$scope.expenseList();
	
	$scope.go = function (filtertable){
		var empId = sessionStorage.getItem("idemp");
		var filtertable1 = [];
		filtertable1 = [empId,$scope.filtertable.fromdate,$scope.filtertable.todate,$scope.filtertable.status];
		ExpensesService.go(filtertable1,$scope);
		
	}
	
	$scope.go1 = function (filtertable1){
		
		var filtertable1 = [];
		if(($scope.expenseempAd1.empId == null) || ($scope.expenseempAd1.empId == "")){
			alert("Please Select Employee");
		}else{
		filtertable1 = [$scope.expenseempAd1.empId,$scope.filtertable1.fromdate,$scope.filtertable1.todate,$scope.filtertable1.status];
		ExpensesService.go1(filtertable1,$scope);
		}
	}
	
	
	$scope.go2 = function (filtertable2){
		
		var filtertable2 = [];
		
		filtertable2 = [$scope.filtertable2.fromdate,$scope.filtertable2.todate,$scope.filtertable2.status];
		ExpensesService.go2(filtertable2,$scope);
		
	}
	
	
	$scope.hideexpensetable=false;
	$scope.hideexpensetableView=true;
	$scope.hideback=true;
	$scope.hideexpenseform=true;
	$scope.hidestatus=false;
	
	$scope.Showstatus= function(){
		$scope.hideexpensetable=true;
		$scope.hideexpensetableView=false;
		$scope.hideback=false;
		$scope.hideexpenseform=true;
		$scope.hidestatus=true;
	};
	$scope.Backpage= function(){
		$scope.hideexpensetable=false;
		$scope.hideexpensetableView=true;
		$scope.hideback=true;
		$scope.hideexpenseform=true;
		$scope.hidestatus=false;
		$scope.expenseList();
	};
	
	
	
	
	
	
	$scope.hideviewexpense=true;
	
	
	
	$scope.SubmitView=function(expenseId,expenseNote,status,employeeId){
		
		var tableview = [];
		tableview = [expenseId,expenseNote,status,employeeId];
		//alert("tableview"+tableview);
		if((expenseId==null) || (status == null) || (employeeId == null)){
			alert("Plese Select status");
		}else{
		ExpensesService.submitview(tableview,$scope);
		
		}
		$scope.expenseList2();
		$scope.hidefilterAdmin=true;
		$scope.expenseempAd={};
	};
	
	
	
	$scope.Accept=function(expenseId,expenseNote,expenseStatus,employeeId){
		var tableview = [];
		tableview = [expenseId,expenseNote,"Accepted",employeeId];
//		alert("tableview"+tableview);
		if((expenseId==null) || (status == null) || (employeeId == null)){
			
		}else{
		ExpensesService.submitview(tableview,$scope);
	
		}
		$scope.expenseList2();
		$scope.hidefilterAdmin=true;
		$scope.expenseempAd={};
	};
	$scope.Reject=function(expenseId,expenseNote,expenseStatus,employeeId){
		var tableview = [];
		tableview = [expenseId,expenseNote,"Rejected",employeeId];
//		alert("tableview"+tableview);
		if((expenseId==null) || (status == null) || (employeeId == null)){
			
		}else{
		ExpensesService.submitview(tableview,$scope);
		
		}
		$scope.expenseList2();
		$scope.hidefilterAdmin=true;
		$scope.expenseempAd={};
	};
	
	
	
	
	
	
	$scope.labelName=false;
	$scope.casthide=true;
	
	$scope.hoverInName= function(){
		$scope.hoverViewName=true;
	};
	$scope.hoverOutName= function(){
		$scope.hoverViewName=false;
	};
	
	$scope.getNamebyId= function(employeeId){
//		alert("emp ID "+employeeId);
		$scope.name="sadfasd";
		ExpensesService.getNamebyId(employeeId,$scope);
		$scope.casthide =false;
	};
	$scope.hidenote=true;
	$scope.AddNote= function(){
		$scope.hidenote=!$scope.hidenote;	
	};
	
	$scope.hidefilterAdmin=true;
	$scope.hideTotalfilterAdmin=true;
	$scope.hidechooseEmployee=false;
	$scope.SingleIdhide=false;
	$scope.ShowTotalExpense= function(){
		$scope.SingleIdhide=true;
		$scope.hidefilterAdmin=true;
		$scope.hideTotalfilterAdmin=false;
		
		$scope.hidechooseEmployee=true;
		
	};
	

	
	
	$scope.Showdatefilter = function(){
		$scope.SingleIdhide=true;
		$scope.hidefilterAdmin=false;
		$scope.hideTotalfilterAdmin=true;
		
		$scope.hidechooseEmployee=true;
	};
	
	$scope.Cancelfilteradmin= function(){
		$scope.hidechooseEmployee=false;
		$scope.hideTotalfilterAdmin=true;
		$scope.hidefilterAdmin=true;
		$scope.SingleIdhide=false;
		
		$scope.filtertable1={};
		$scope.expenseempAd1={};
	};
	$scope.download1 = function (candidateResume,filename,filetype,filesize){
		  
		  var a11= candidateResume
	    
	    	
	    	var byteCharacters = atob(a11);
	    	
	    	var byteNumbers = new Array(byteCharacters.length);
	    	for (var i = 0; i < byteCharacters.length; i++) {
	    	    byteNumbers[i] = byteCharacters.charCodeAt(i);
	    	}
	    	
	    	    	
	    	var byteArray = new Uint8Array(byteNumbers);
	    	var blob = new Blob([byteArray], {type: filetype});
	    	
	    	console.log(blob);
	    	
	    	function b64toBlob(b64Data, contentType, sliceSize) {
	    		  contentType = contentType || '';
	    		  sliceSize = sliceSize || 512;

	    		  var byteCharacters = atob(b64Data);
	    		  var byteArrays = [];

	    		  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
	    		    var slice = byteCharacters.slice(offset, offset + sliceSize);

	    		    var byteNumbers = new Array(slice.length);
	    		    for (var i = 0; i < slice.length; i++) {
	    		      byteNumbers[i] = slice.charCodeAt(i);
	    		    }

	    		    var byteArray = new Uint8Array(byteNumbers);

	    		    byteArrays.push(byteArray);
	    		  }

	    		  var blob = new Blob(byteArrays, {type: contentType});
	    		  return blob;
	    		}
	    	
	    	
	    	var blob = b64toBlob(a11, filetype,filesize);
	    	var blobUrl = URL.createObjectURL(blob);

	    	//window.location = blobUrl;
	    	saveAs(blob, filename);
		  
	  };
	
	//pagination
	$scope.predicate = 'name';  
	   $scope.reverse = true;  
	   $scope.currentPage = 1;  
	   
	$scope.numPerPage = 15;  
 
	   $scope.paginate = function (value) {  
	     var begin, end, index;  
	     begin = ($scope.currentPage - 1) * $scope.numPerPage;  
	     end = begin + $scope.numPerPage;  
	     index = $scope.contacts.indexOf(value);  
	     return (begin <= index && index < end);  
	   }; 
	   
	   
	   $scope.getListOfPage = function(chooseTeam){
	 	  
	 	  if($scope.choose1.page == "100"){
	 		  	  $scope.numPerPage = 100;  	 
	 	  }
	 	  if($scope.choose1.page == "20"){
	 		   $scope.numPerPage = 20;  	   
	 	  }
	 	  if($scope.choose1.page == "30"){
	 		  
	 		  $scope.numPerPage = 30;  
	 	  }
	 	  if($scope.choose1.page == "40"){
	 		  $scope.numPerPage = 40;  
	 	  }
	 	  if($scope.choose1.page == ""){
	 		  $scope.numPerPage = 15;  
	 	  }
	 	  if($scope.choose1.page == "200"){
	 		  $scope.numPerPage = 200;  
	 	  }
	 	  
	   }
	
	
	}
	
	
	else {
		$window.location.href = 'login.jsp';

	}

	 });