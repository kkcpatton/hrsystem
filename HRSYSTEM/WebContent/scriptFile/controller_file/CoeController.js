app.controller('CoeCtrl', function($scope, $window, $http, $rootScope) {
 

	var sess = sessionStorage.getItem("key1");
	if (sess) {
		
		 $scope.hideHomeCoe=false;
		 $scope.hideAi=true;
		 $scope.hideMobility=true;
		 $scope.hideBi=true;
		 $scope.hideSoa=true;
		 $scope.hideJ2ee=true;
		 
		 $scope.AIFn= function(){
			 $scope.hideHomeCoe=true;
			 $scope.hideAi=false;
			 $scope.hideMobility=true; 
			 $scope.hideBi=true;
			 $scope.hideSoa=true;
			 $scope.hideJ2ee=true;
		 };
		 
		 $scope.mobIos=function(){
			 $scope.hideIos=false;
			 $scope.hideAnroid=true;
			 $scope.hideKony=true;
			 $scope.hidePhonegap=true;
			 $scope.hideCordova=true;
		 };
		 $scope.mobAnroid=function(){
			 $scope.hideIos=true;
			 $scope.hideAnroid=false;
			 $scope.hideKony=true;
			 $scope.hidePhonegap=true;
			 $scope.hideCordova=true;
		 };
		 $scope.mobKony=function(){
			 $scope.hideIos=true;
			 $scope.hideAnroid=true;
			 $scope.hideKony=false;
			 $scope.hidePhonegap=true;
			 $scope.hideCordova=true; 
			 };
		 $scope.mobPhonegap=function(){
			 $scope.hideIos=true;
			 $scope.hideAnroid=true;
			 $scope.hideKony=true;
			 $scope.hidePhonegap=false;
			 $scope.hideCordova=true; 
			 };
		 $scope.mobCordova=function(){
			 $scope.hideIos=true;
			 $scope.hideAnroid=true;
			 $scope.hideKony=true;
			 $scope.hidePhonegap=true;
			 $scope.hideCordova=false;
			 };
			 
		 $scope.MobilityFn=function(){
			 
			 $scope.hideIos=false;
			 $scope.hideAnroid=true;
			 $scope.hideKony=true;
			 $scope.hidePhonegap=true;
			 $scope.hideCordova=true;
			 
			 
			 $scope.hideHomeCoe=true;
			 $scope.hideAi=true;
			 $scope.hideMobility=false; 
			 $scope.hideBi=true;
			 $scope.hideSoa=true;
			 $scope.hideJ2ee=true;
		 };
		 $scope.BIFn=function(){
			 $scope.hideHomeCoe=true;
			 $scope.hideAi=true;
			 $scope.hideMobility=true; 
			 $scope.hideBi=false;
			 $scope.hideSoa=true;
			 $scope.hideJ2ee=true;
		 };
		 
		 $scope.SoaFn=function(){
			 $scope.hideHomeCoe=true;
			 $scope.hideAi=true;
			 $scope.hideMobility=true; 
			 $scope.hideBi=true;
			 $scope.hideSoa=false;
			 $scope.hideJ2ee=true; 
		 };
		 
		 $scope.J2EEFn=function(){
			 $scope.hideHomeCoe=true;
			 $scope.hideAi=true;
			 $scope.hideMobility=true; 
			 $scope.hideBi=true;
			 $scope.hideSoa=true;
			 $scope.hideJ2ee=false; 
		 };
	}
	else {
		$window.location.href = 'login.jsp';

	}

	 });
