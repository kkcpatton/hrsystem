'use strict';

app.controller('PerformanceReviewCtrl', function($scope, $window,PerformanceReviewService, $http, $rootScope) {
 

	var sess = sessionStorage.getItem("key1");
	if (sess) {
	

		
		$scope.employeeList3 = [];
		$scope.employeeList3 = function() {
			
			var response;
			 response = $http.get('employeelist1');
			
			 response.success(function(data, status, headers, config) {

					$scope.statusEmplist = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://localhost:8086/HRSYSTEM/employeelist1',
//				data : {}
//			}).success(function(result) {
//				
//				
//				$scope.statusEmplist = result;
//				
//			});
		};
		$scope.employeeList3();
		
		$scope.PerformanceYear = ["2017","2018","2019","2020","2021","2022","2023","2024"]; 
		
		//performance Review button
		
		$scope.hideperformanceReview=true;
		$scope.hideAssignTaskForm=true;
		$scope.hidePerformancebutton=false;
		
		$scope.PerformanceReview=function(){
			$scope.hideperformanceReview=false;
			$scope.hideAssignTaskForm=true;
			$scope.hidePerformancebutton=true;
			$scope.viewReviewtable=true;
			$scope.hideTaskTable=true;
			$scope.performanceReport=true;
			$scope.performance={};
		};
		
		//Assign Task button
		
		$scope.AssignTask=function(){
			
			$scope.hideperformanceReview=true;
			$scope.hideAssignTaskForm=false;
			$scope.hidePerformancebutton=true;
			$scope.viewReviewtable=true;
			$scope.hideTaskTable=true;
			$scope.performanceReport=true;
		};
		
		// Cancel Assign
		$scope.cancelAssign=function(assignTask){
			$scope.hideperformanceReview=true;
			$scope.hideAssignTaskForm=true;	
			$scope.hidePerformancebutton=false;
			$scope.viewReviewtable=true;
			$scope.hideTaskTable=true;
			$scope.performanceReport=false;
		};
		
		// Cancel Performance
		
		$scope.cancelPerformance=function(performance){
			$scope.hideperformanceReview=true;
			$scope.hideAssignTaskForm=true;
			$scope.hidePerformancebutton=false;
			$scope.viewReviewtable=true;
			$scope.hideTaskTable=true;
			$scope.performanceReport=false;
		};
		
		// Assign Task submit
		$scope.assign=function(data){
			
			data= {
			
			taskId:$scope.assignTask.taskId,
			employeeId:$scope.assignTask.empId,
			taskAssigned:$scope.assignTask.taskAssigned,
			taskWeight:$scope.assignTask.taskWeight
			
			};
			
			PerformanceReviewService.submitTask(data,$scope);
		};
		
		// submit Performance Admin
		$scope.submitPerformance=function(data){
			
			data= {
					
					performanceId:$scope.performance.performanceId,
					employeeId:$scope.performance.empId,
					reviewedBy:$scope.performance.reviewedBy,
					reviewedDate:$scope.performance.reviewedDate,
					reviewPeriodTo:$scope.performance.reviewPeriodTo,
					reviewPeriod:$scope.performance.reviewPeriod,
					quarter:$scope.performance.quarter,
					year:$scope.performance.year,
					comments:$scope.performance.comments,
					jobskill:$scope.performance.jobskill,
					newskill:$scope.performance.newskill,
					resourseUses:$scope.performance.resourseUses,
					assignResponsibilities:$scope.performance.assignResponsibilities,
					meetAttendance:$scope.performance.meetAttendance,
					listenToDirection:$scope.performance.listenToDirection,
					responsibility:$scope.performance.responsibility,
					commitments:$scope.performance.commitments,
					problemSolving:$scope.performance.problemSolving,
					suggestionImprovement:$scope.performance.suggestionImprovement,
					ideaAndSolution:$scope.performance.ideaAndSolution,
					meetChallenges:$scope.performance.meetChallenges,
					innovativeThinking:$scope.performance.innovativeThinking

				};
			
			PerformanceReviewService.submitPerformance(data,$scope);
		};
		
		// collect task list based on Employee
		$scope.taskList = [];
		$scope.taskList = function() {
			var empId = sessionStorage.getItem("idemp");
			PerformanceReviewService.taskList(empId,$scope);
		};
		$scope.taskList();
		
		
		// Add Activity
		$scope.hideActivityform=true;
		$scope.disableTaskAssigned=true;
		$scope.AddActivity =function(taskId,employeeId,taskAssigned){
			$scope.hideActivityform=false;
			$scope.disableTaskAssigned=true;
			$scope.hideViewAcivity=true;
			$scope.performance1={
					taskId:taskId,
					employeeId:employeeId,
					taskAssigned:taskAssigned
			};
		};
		
		// submit Activity
		$scope.submitActivity=function(performance1){
			
			performance1={
					
					activityId:$scope.performance1.activityId,
					taskId:$scope.performance1.taskId,
					employeeId:$scope.performance1.employeeId,
					taskAssigned:$scope.performance1.taskAssigned,
					projectName:$scope.performance1.projectName,
					projectDescription:$scope.performance1.projectDescription,
					taskComplete:$scope.performance1.taskComplete,
					issuesCleared:$scope.performance1.issuesCleared
			};
			PerformanceReviewService.performanceActivity(performance1,$scope);
			
		};
		
		// cancel Activity
		$scope.Activitycancel=function(performance1){
			$scope.performance1={};
			$scope.hideActivityform=true;
			
		};
		
		// Activity List
		$scope.taskActivityList = [];
		$scope.taskActivityList = function() {
			var empId = sessionStorage.getItem("idemp");
			PerformanceReviewService.taskActivityList(empId,$scope);
		};
		$scope.taskActivityList();
		
		
		
		// View Activity
		$scope.hideViewAcivity=true;
		$scope.viewActivity=function(activityId){
			$scope.hideViewAcivity=false;
			$scope.hideActivityform=true;
			PerformanceReviewService.viewActivity(activityId,$scope);
			
		};
		
		//View Close
		$scope.Viewclose=function(){
			$scope.hideViewAcivity=true;
			$scope.hideActivityform=true;
		};
		
		// TASK LIST ALL
		$scope.taskListAllEmp = [];
		$scope.taskListAllEmp = function() {
			
			
			var response;
			 response = $http.get('taskListAllEmp');
			
			 response.success(function(data, status, headers, config) {

				 $scope.contactsTaskListAllEmp = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://localhost:8086/HRSYSTEM/taskListAllEmp',
//				data : {}
//			}).success(function(result) {
//				
//				
//				$scope.contactsTaskListAllEmp = result;
//				
//			});
		};
		$scope.taskListAllEmp();
		
		// ACTIVITY LIST ALL
		$scope.activityListAllEmp = [];
		$scope.activityListAllEmp = function() {
			
			
			var response;
			 response = $http.get('activityListAllEmp');
			
			 response.success(function(data, status, headers, config) {

				 $scope.contactsActivityListAllEmp = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://localhost:8086/HRSYSTEM/activityListAllEmp',
//				data : {}
//			}).success(function(result) {
//				
//				
//				$scope.contactsActivityListAllEmp = result;
//				
//			});
		};
		$scope.activityListAllEmp();
		
		$scope.viewReviewtable=true;
		$scope.ReviewTask=function(taskId){
			$scope.viewReviewtable=false;
			PerformanceReviewService.ReviewTask(taskId,$scope);
			PerformanceReviewService.ReviewTask1(taskId,$scope);
		};
		
		$scope.SubmitWeight=function(taskId,WeightAchieved,taskWeight){
			
			if((parseInt(WeightAchieved)) > (parseInt(taskWeight))){
				alert("Please Check Task Weight");
			}else{
				
				var submitweightvalue = [];
				submitweightvalue = [taskId,WeightAchieved];
				PerformanceReviewService.submitweight(submitweightvalue,$scope);
			}
	
		};
		
		
		$scope.hideTaskTable=true;
		
		
		
		
		$scope.ViewReviewclose=function(){
			
			$scope.viewReviewtable=true;
		};
		$scope.performanceReportTable=true;
		//performance Report
		
		$scope.performanceReport=false;
		
		
		
		
		$scope.PerformanceReport=function(){
			$scope.hidePerformancebutton=false;
			$scope.hideAssignTaskForm=true;
			$scope.hideperformanceReview=true;
			$scope.viewReviewtable=true;
			$scope.hideTaskTable=false;
			$scope.performanceReport=true;
			
						
		};
		
		$scope.CancelPerformanceReport=function(){
			$scope.hidePerformancebutton=false;
			$scope.hideAssignTaskForm=true;
			$scope.hideperformanceReview=true;
			$scope.viewReviewtable=true;
			$scope.hideTaskTable=false;
			$scope.performanceReport=true;
			$scope.performanceReportTable=true;
			
		};
		$scope.performanceReportempId=function(empId){
			
			$scope.ReportTaskByEmpId={};
			$scope.ReviewByEmpId={};
			$scope.totalweight=0;
			 $scope.twAchieved=0;
			 $scope.twAssigned=0;
//			$scope.performanceReportTable=true;
			PerformanceReviewService.getTaskByEmpId(empId,$scope);
			PerformanceReviewService.getReviewByEmpId(empId,$scope);
			$scope.taskListAllEmp();
			
		};
		$scope.hidePerformanceReportN2=true;
		$scope.hidePerformanceReportPatton=true;
		
		 $scope.clearC1= function(){
	  		 $scope.acceptR1.clearC1(); 
	  		  
	  	 };
	  	 $scope.clearC2= function(){
	  		 $scope.acceptR2.clearC2();
	  		
	  	 };
	  	 
	  	$scope.clearC11= function(){
	  		 $scope.acceptR11.clearC11(); 
	  		  
	  	 };
	  	 $scope.clearC22= function(){
	  		 $scope.acceptR22.clearC22();
	  		
	  	 };
	  	
		$scope.performanceReportGeneration=function(performanceId,empId){
			$scope.clearC1();
			
			$scope.clearC2();
			$scope.clearC11();
			
			$scope.clearC22();
//			$scope.hidePerformanceReportN2=false;
			PerformanceReviewService.getReviewByPerformanceId(performanceId,$scope);
			PerformanceReviewService.getEmpPayroll(empId,$scope);
		};
		
		$scope.export = function(){
	          html2canvas(document.getElementById('exportthisPerformance'), {
	              onrendered: function (canvas) {
	                  var data = canvas.toDataURL();
	                  var docDefinition = {
	                      content: [{
	                          image: data,
	                          width: 500,
	                      }]
	                  };
	                  pdfMake.createPdf(docDefinition).download("Performance Report.pdf");
	              }
	          });
	         
	       };
	       
	       $scope.export1 = function(){
		          html2canvas(document.getElementById('exportthisPerformancePatton'), {
		              onrendered: function (canvas) {
		                  var data = canvas.toDataURL();
		                  var docDefinition = {
		                      content: [{
		                          image: data,
		                          width: 500,
		                      }]
		                  };
		                  pdfMake.createPdf(docDefinition).download("Performance Report.pdf");
		              }
		          });
		         
		       };
	       
	       $scope.cancelReport= function(){
	    	   $scope.hidePerformanceReportN2=true;
	    	   $scope.hidePerformanceReportPatton=true;
	    	   $scope.ReviewAdmin= "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==";
			   $scope.Reviewemp="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==";
	       };
	       
	       
	       
	       
	       $scope.performanceReportDelete= function(performanceId,empId){
	    	   var result= confirm("Are you sure to Delete ");
	       		 var performanceDelete=[];
	       		performanceDelete=[performanceId,empId];
	       		 
       		if(result == true){
	    	   PerformanceReviewService.DeleteReviewByPerformanceId(performanceDelete,$scope);
       		}
	       };
	       
	       $scope.performanceReportEdit= function(performanceId,employeeId){
	    	   

				//search contact with given id and update it
	    	   $scope.hideperformanceReview=false;
				$scope.hideAssignTaskForm=true;
				$scope.hidePerformancebutton=true;
				$scope.viewReviewtable=true;
				$scope.hideTaskTable=true;
				$scope.performanceReport=true;
				var i;
			        for(i in $scope.ReviewByEmpId) {
			            if($scope.ReviewByEmpId[i].performanceId == performanceId) {
			            	//we use angular.copy() method to create 
			            	//copy of original object

		  	            	var result= confirm("Are you sure to Edit Performance?");
			       		 
			        		if(result == true){
			      $scope.performance = {
			        			performanceId:$scope.ReviewByEmpId[i].performanceId,
								empId:$scope.ReviewByEmpId[i].employeeId,
								reviewedBy:$scope.ReviewByEmpId[i].reviewedBy,
								reviewedDate:new Date($scope.ReviewByEmpId[i].reviewedDate),
								reviewPeriodTo:new Date($scope.ReviewByEmpId[i].reviewPeriodTo),
								reviewPeriod:new Date($scope.ReviewByEmpId[i].reviewPeriod),
								comments:$scope.ReviewByEmpId[i].comments,
								jobskill:$scope.ReviewByEmpId[i].jobskill,
								newskill:$scope.ReviewByEmpId[i].newskill,
								resourseUses:$scope.ReviewByEmpId[i].resourseUses,
								assignResponsibilities:$scope.ReviewByEmpId[i].assignResponsibilities,
								meetAttendance:$scope.ReviewByEmpId[i].meetAttendance,
								listenToDirection:$scope.ReviewByEmpId[i].listenToDirection,
								responsibility:$scope.ReviewByEmpId[i].responsibility,
								commitments:$scope.ReviewByEmpId[i].commitments,
								problemSolving:$scope.ReviewByEmpId[i].problemSolving,
								suggestionImprovement:$scope.ReviewByEmpId[i].suggestionImprovement,
								ideaAndSolution:$scope.ReviewByEmpId[i].ideaAndSolution,
								meetChallenges:$scope.ReviewByEmpId[i].meetChallenges,
								innovativeThinking:$scope.ReviewByEmpId[i].innovativeThinking,
								year:$scope.ReviewByEmpId[i].year,
								quarter:$scope.ReviewByEmpId[i].quarter
			        			}
			            	
			            }
			  }
				}
			    
	       }
	       
	       
	    // Activity List
			$scope.performanceReviewViewEmployee = [];
			$scope.performanceReviewViewEmployee = function() {
				var empId = sessionStorage.getItem("idemp");
				PerformanceReviewService.performanceReviewViewEmp(empId,$scope);
			};
			$scope.performanceReviewViewEmployee();
			
			$scope.hidePerformanceReportN2Emp=true;
			$scope.hidePerformanceReportPattonEmp=true;
			
			 $scope.clearEmp= function(){
		  		 $scope.acceptEmp.clearEmp();
		  		
		  	 };
		  	$scope.clearEmp1= function(){
		  		 $scope.acceptEmp1.clearEmp1();
		  		
		  	 };
			
			$scope.performanceReportGenerationEmp=function(performanceId){
				$scope.clearEmp();
				$scope.clearEmp1();
			
				var empId = sessionStorage.getItem("idemp");
				
				PerformanceReviewService.getReviewByPerformanceId(performanceId,$scope);
				PerformanceReviewService.getEmpPayroll(empId,$scope);
			};
			
			$scope.CancelEmp=function(){
				$scope.clearEmp();
				$scope.clearEmp1();
				$scope.hidePerformanceReportN2Emp=true;
				$scope.hidePerformanceReportPattonEmp=true;
				$scope.PerformanceEmpSignature1= "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==";
				$scope.PerformanceEmpSignature="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==";
			};
			// signature
			
			  $scope.restore2 = function() {
			        var dataurl = $scope.acceptEmp().dataUrl;
			        	        	        
			        $scope.clearEmp();
			        $scope.PerformanceEmpSignature = dataurl;
			        
			      };
			      $scope.restore3 = function() {
				        var dataurl = $scope.acceptR1().dataUrl;
				        $scope.clearC1();        	        
				        
				        $scope.Reviewemp = dataurl;
				        
				      };
				      $scope.restore4 = function() {
					        var dataurl = $scope.acceptR11().dataUrl;
					        $scope.clearC11();        	        
					        
					        $scope.Reviewemp = dataurl;
					        
					      };
					      
					      $scope.restore5 = function() {
						        var dataurl = $scope.acceptEmp1().dataUrl;
						        	        	        
						        $scope.clearEmp1();
						        $scope.PerformanceEmpSignature1 = dataurl;
						        
						      };
						      
						      
			      $scope.clearEmp = function() {
			    	  
			    	  $scope.acceptEmp().clearEmp(); 
			      };
			      
			      
			
			$scope.SubmitReviewSign=function(performanceIdReport){
				$scope.restore2();
				var signReportEmp=[];
				var empId = sessionStorage.getItem("idemp");
				if($scope.PerformanceEmpSignature == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" || $scope.PerformanceEmpSignature == "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==" || $scope.PerformanceEmpSignature == null){
					
					alert("Signature is needed");
				}else{
					
		
					signReportEmp=[performanceIdReport,$scope.PerformanceEmpSignature,empId];
					PerformanceReviewService.saveSignEmp(signReportEmp,$scope);

				}
				
			};	
			
			$scope.SubmitReviewSign1=function(performanceIdReport){
				$scope.restore5();
				var signReportEmp1=[];
				var empId = sessionStorage.getItem("idemp");
				if($scope.PerformanceEmpSignature1 == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" || $scope.PerformanceEmpSignature1 == "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==" || $scope.PerformanceEmpSignature1 == null){
					
					alert("Signature is needed");
				}else{
					
		
					signReportEmp1=[performanceIdReport,$scope.PerformanceEmpSignature1,empId];
					PerformanceReviewService.saveSignEmp(signReportEmp1,$scope);

				}
				
			};
			
			$scope.AdminSignUpdate=function(performanceIdReport,employeeIdReport){
				$scope.restore3();
				var signReportAdmin=[];
			
				if($scope.Reviewemp == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" || $scope.Reviewemp == "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==" || $scope.Reviewemp == null){
					
					alert("Signature is needed");
				}else{
					

					signReportAdmin=[performanceIdReport,$scope.Reviewemp,employeeIdReport];
					PerformanceReviewService.saveSignAdmin(signReportAdmin,$scope);

				}
			};
	       
			$scope.AdminSignUpdatePL=function(performanceIdReport,employeeIdReport){
				$scope.restore4();
				var signReportAdmin=[];
			
				if($scope.Reviewemp == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=" || $scope.Reviewemp == "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==" || $scope.Reviewemp == null){
					
					alert("Signature is needed");
				}else{
					

					signReportAdmin=[performanceIdReport,$scope.Reviewemp,employeeIdReport];
					PerformanceReviewService.saveSignAdmin(signReportAdmin,$scope);

				}
			};
			
	}
	else {
		$window.location.href = 'login.jsp';

	}

	 });


