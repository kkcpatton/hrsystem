'use strict';

app.controller('AddemployeeCtrl', function($scope, $window, $http,addemployeeService, $rootScope) {
 

	var sess = sessionStorage.getItem("key1");
	if (sess) {
	
		
//		alert("inside AddemployeeCtrl");
		$scope.val='AddemployeeCtrl';
		
		$scope.val1='AddemployeeCtrl';
		
		
		$scope.employeeRole = ["USER","MANAGER","ADMIN"];
		$scope.employeeStatus = ["Active","In-Active"];	
		$scope.employeePayroll= ["N2 Service","Patton Labs"];
		
		$scope.addEmployee=true;
		$scope.ShowAddEmployee=function(){
			$scope.EditFn=false;
	    	$scope.EditPhoneFnValid="";
	    	$scope.EditEmailFnValid="";
			$scope.addEmployee = !$scope.addEmployee;
			$scope.viewemployee=true;	
		};
		$scope.submit = function(data) {
			
				data= {
						
						employeeId:$scope.addemployee.employeeId,
						firstName:$scope.addemployee.firstname,
						lastName:$scope.addemployee.lastname,
						dateOfBirth:$scope.addemployee.dob,
						phoneNumberPrimary:$scope.addemployee.primaryPhoneNumber,
						phoneNumberSecondary:$scope.addemployee.secondaryPhoneNumber,
						emailPrimary:$scope.addemployee.primaryEmail,
						emailSecondary:$scope.addemployee.secondaryEmail,
						location:$scope.addemployee.location,
						dateOfJoined:$scope.addemployee.doj,
						clientName:$scope.addemployee.clientName,
						designation:$scope.addemployee.designation,
						role:$scope.addemployee.emprole,
						employeePassword:$scope.addemployee.password,
						employeeStatus:$scope.addemployee.empstatus,
						payroll:$scope.addemployee.payroll,
						reportTo:$scope.addemployee.reportTo
						
		 				  
				};
				
if(($scope.addemployee.primaryEmail == "") ||($scope.addemployee.primaryPhoneNumber == null)|| ($scope.addemployee.primaryEmail == null)||($scope.addemployee.emprole == null)||($scope.addemployee.empstatus == null)||($scope.addemployee.password == null)){
	
	alert("Please fill all the fields");
}else{
	
	addemployeeService.submit(data,$scope);
	
	
}
				
				
					
					
		};
		
		$scope.employeeList = [];
		$scope.employeeList = function() {
			
			var response;
			 response = $http.get('employeelist');
			
			 response.success(function(data, status, headers, config) {
				 $scope.contacts = data;
					$scope.totalItems = data.length;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
//			$http({
//				method : 'GET',
//				url : 'http://localhost:8086/HRSYSTEM/employeelist',
//				data : {}
//			}).success(function(result) {
//				
//				
//				$scope.contacts = result;
//				$scope.totalItems = result.length; 
				
//				
//			});
				
		};
		$scope.employeeList();
		
		
		// validation
		$scope.alertPhoneNo=false;
		 $scope.$watch('addemployee.primaryPhoneNumber',function(val,old){

		       $scope.alertPhoneNo=false;
		       $scope.total = function(data) {
					 $scope.alertPhoneNo=false;
					
				
					 
					 var i;
				        for(i in $scope.contacts) {
				        	 var S_month="";
							
				        	 S_month=$scope.contacts[i].phoneNumberPrimary;
				        	 if($scope.EditPhoneFnValid == val){
				        		 $scope.alertPhoneNo=false;
				        		 break;
				        	 }
				        	 if(S_month== val){
				        		 $scope.alertPhoneNo=true;	 
				        		  break;
				        	 }
				        }

					 
					 
						};
						
				 
						 if($scope.EditFn!=true){
							 $scope.total($scope.contacts);
						 }else{
							 $scope.total($scope.contacts);
							
						 }
		    });
		
		 $scope.alertEmailId=false;
		 $scope.$watch('addemployee.primaryEmail',function(val,old){

		       $scope.alertEmailId=false;
		       $scope.total = function(data) {
					 $scope.alertEmailId=false;
					
					 var i;
				        for(i in $scope.contacts) {
				        	 var S_Email="";
				        	 if($scope.EditEmailFnValid == val){
				        		 $scope.alertEmailId=false;	
				        		 break;
				        	 }
				        	 S_Email=$scope.contacts[i].emailPrimary;
				        	 if(S_Email== val){
				        		 $scope.alertEmailId=true;	 
				        		  break;
				        	 }
				        }

					 
					 
						};
						
						
						
				 
				 if($scope.EditFn!=true){
				 $scope.total($scope.contacts);
				 }else{
					 $scope.total($scope.contacts);
				 }
		    });
		
		
		
		
		
		
		
		$scope.managerList = [];
		$scope.managerList = function() {
			
			
			var response;
			 response = $http.get('managerlist');
			
			 response.success(function(data, status, headers, config) {
				 $scope.contactsManager = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://localhost:8086/HRSYSTEM/managerlist',
//				data : {}
//			}).success(function(result) {
//				
//				console.log(result);
//				$scope.contactsManager = result;
//				
//				
//				
//			});
		};
		$scope.managerList();
		
		$scope.cancel = function (addemployee){
			$scope.addemployee = {};     			
			$scope.addEmployee=true;	            			
			$scope.EditFn=false;
	    	$scope.EditPhoneFnValid="";
	    	$scope.EditEmailFnValid="";
			};
			
			
			$scope.edit = function(EmployeeId) {
				//search contact with given id and update it
				$scope.addEmployee=false;
				$scope.EditFn=true;
				
				$scope.EditPhoneFnValid="";
				$scope.EditEmailFnValid="";
				var i;
			        for(i in $scope.contacts) {
			            if($scope.contacts[i].employeeId == EmployeeId) {
			            	//we use angular.copy() method to create 
			            	//copy of original object
			            	$scope.EditPhoneFnValid=$scope.contacts[i].phoneNumberPrimary;
			            	$scope.EditEmailFnValid=$scope.contacts[i].emailPrimary;
		  	            	var result= confirm("Are you sure to Edit "+ $scope.contacts[i].firstName +" ?");
			       		 
			        		if(result == true){
			         		
			        		
			            	$scope.addemployee = {
			            			
			            			employeeId:$scope.contacts[i].employeeId,
			            			firstname:$scope.contacts[i].firstName,
									lastname:$scope.contacts[i].lastName,
									dob:$scope.contacts[i].dateOfBirth= new Date($scope.contacts[i].dateOfBirth),
									primaryPhoneNumber:$scope.contacts[i].phoneNumberPrimary,
									secondaryPhoneNumber:$scope.contacts[i].phoneNumberSecondary,
									primaryEmail:$scope.contacts[i].emailPrimary,
									secondaryEmail:$scope.contacts[i].emailSecondary,
									location:$scope.contacts[i].location,
									doj:$scope.contacts[i].dateOfJoined= new Date($scope.contacts[i].dateOfJoined),
									clientName:$scope.contacts[i].clientName,
									designation:$scope.contacts[i].designation,
									emprole:$scope.contacts[i].role,
									password:$scope.contacts[i].employeePassword,
									payroll:$scope.contacts[i].payroll,
									empstatus:$scope.contacts[i].employeeStatus,		            			
									reportTo:$scope.contacts[i].reportTo
			            	};
			            }
			  }
				}
			    };
			
			
			    $scope.viewemployee=true;
			    $scope.viewEmployee= function(employeeId){
			    	
			    	addemployeeService.viewEmployee(employeeId,$scope);
			    	$scope.addEmployee=true;
			    	$scope.viewemployee=false;
			    };
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			 $scope.delet = function(employeeId){
			    	
					var i;
			        for(i in $scope.contacts) {
			            if($scope.contacts[i].employeeId == employeeId) {
					var result= confirm("Are you sure to delete "+$scope.contacts[i].employeeId+" ?");
					 
					if(result == true){
						addemployeeService.delet(employeeId, $scope);
							}
			              }
			            }
			 	    };
			 	    
			 	 	    
			 	    
			
		$scope.predicate = 'name';  
	 	   $scope.reverse = true;  
	 	   $scope.currentPage = 1;  
	 	   
		$scope.numPerPage = 15;  
	    
	 	   $scope.paginate = function (value) {  
	 	     var begin, end, index;  
	 	     begin = ($scope.currentPage - 1) * $scope.numPerPage;  
	 	     end = begin + $scope.numPerPage;  
	 	     index = $scope.contacts.indexOf(value);  
	 	     return (begin <= index && index < end);  
	 	   }; 
	 	   
	 	   
	 	   $scope.getListOfPage = function(chooseTeam){
	 	 	  
	 	 	  if($scope.choose1.page == "100"){
	 	 		  	  $scope.numPerPage = 100;  	 
	 	 	  }
	 	 	  if($scope.choose1.page == "20"){
	 	 		   $scope.numPerPage = 20;  	   
	 	 	  }
	 	 	  if($scope.choose1.page == "30"){
	 	 		  
	 	 		  $scope.numPerPage = 30;  
	 	 	  }
	 	 	  if($scope.choose1.page == "40"){
	 	 		  $scope.numPerPage = 40;  
	 	 	  }
	 	 	  if($scope.choose1.page == ""){
	 	 		  $scope.numPerPage = 15;  
	 	 	  }
	 	 	  if($scope.choose1.page == "200"){
	 	 		  $scope.numPerPage = 200;  
	 	 	  }
	 	 	  
	 	   }
		
	}
	else {
		$window.location.href = 'login.jsp';

	}

	 });