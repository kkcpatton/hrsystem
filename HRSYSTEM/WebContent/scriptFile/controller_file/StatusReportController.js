'use strict';

app
		.controller(
				'StatusReportCtrl',
				function($scope, statusreportService, $timeout, $window, $http,
						$rootScope, $filter) {

					var sess = sessionStorage.getItem("key1");
					if (sess) {

						//		alert("inside StatusReportCtrl");
						$scope.val = 'StatusReportCtrl';

						$scope.val1 = 'StatusReportCtrl1';
						$scope.hidestatus = false;
						$scope.hideStatusTable = false;
						$scope.hideStatusTable1 = true;
						$scope.StatusReportOn = true;
						$scope.statuscode = [ "On-track", "Date at Risk",
								"Impacts End Date" ];

						$scope.selectMonth1 = [ "January", "February", "March",
								"April", "May", "June", "July", "August",
								"September", "October", "November", "December" ];
						$scope.selectYear1 = [ "2017", "2018", "2019", "2020",
								"2021", "2022", "2023", "2024" ];

						$scope.hideMonthlyTable = true;
						$scope.ViewMonthlyStatusReport = function() {
							$scope.hideMonthlyTable = false;
						};

						$scope.submit = function(data) {

							$scope.restore();

							var idemp = sessionStorage.getItem("idemp");
							data = {

								statusId : $scope.status.statusId,
								employeeId : idemp,
								activity : $scope.status.activity,
								statusMonth : $scope.status.statusMonth,
								startPlannedDate : $scope.status.startPlanDate,
								startActualDate : $scope.status.startActualDate,
								finishPlannedDate : $scope.status.finishPlannedDate,
								finishEstimatedDate : $scope.status.finishEstimatedDate,
								status : $scope.status.statuscode,
								clientDetails : $scope.status.clientDetails,
								currentProject : $scope.status.currentProject,
								managerName : $scope.status.managerName,
								currentModule : $scope.status.currentModule,
								managerEmail : $scope.status.managerEmail,
								managerPhonenumber : $scope.status.managerPhoneNumber,
								employeeSignature : $scope.storedSignature,
								action_status : "Pending"

							};

							if ($scope.storedSignature == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
									|| $scope.storedSignature == null
									|| $scope.storedSignature == "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==") {

								alert("Signature is needed");
							} else {

								statusreportService.submit(data, $scope);

							}
							//			}

						};

						$scope.DeleteStatusReport1 = function(statusId, sMonth) {
							var deleteUser = $window
									.confirm('Do you want to delete this record?');
							if (deleteUser) {
								var empId = sessionStorage.getItem("idemp");
								var statusReportDelete = [];
								statusReportDelete = [ empId, statusId, sMonth ];
								statusreportService.DeleteStatusReport(
										statusReportDelete, $scope);
							}
						};

						//	$scope.statusList = [];
						//	$scope.statusList = function() {
						//		var empId = sessionStorage.getItem("idemp");
						//		statusreportService.statusGet(empId, $scope);
						//
						//	};
						//	$scope.statusList();

						$scope.statusListEmp = [];
						$scope.statusListEmp = function() {
							var thismonth = new Date().getMonth() + 1;
							if (thismonth == 1) {
								thismonth = "01";
							} else if (thismonth == 2) {
								thismonth = "02";
							} else if (thismonth == 3) {
								thismonth = "03";
							} else if (thismonth == 4) {
								thismonth = "04";
							} else if (thismonth == 5) {
								thismonth = "05";
							} else if (thismonth == 6) {
								thismonth = "06";
							} else if (thismonth == 7) {
								thismonth = "07";
							} else if (thismonth == 8) {
								thismonth = "08";
							} else if (thismonth == 9) {
								thismonth = "09";
							} else if (thismonth == 10) {
								thismonth = "10";
							} else if (thismonth == 11) {
								thismonth = "11";

							} else if (thismonth == 12) {
								thismonth = "12";
							}
							var curentyear = new Date().getFullYear();
							var empId = sessionStorage.getItem("idemp");
							var statusRecentUpdates = [];
							statusRecentUpdates = [ empId, thismonth,
									curentyear ];

							statusreportService.statusRecentUpdatesGet(
									statusRecentUpdates, $scope);

						};
						$scope.statusListEmp();

						// By Default load in Admin Screen
						//	
						//	$scope.statusListAdmin = [];
						//	$scope.statusListAdmin = function() {
						//		$http({
						//			method : 'GET',
						//			url : 'http://localhost:8080/HRSYSTEM/statuslist',
						//			data : {}
						//		}).success(function(result) {
						//			
						//			console.log(result);
						//			$scope.contactsAdmin = result;
						//		
						//			
						//		});
						//	};
						//	$scope.statusListAdmin();

						$scope.cancel = function(status) {
							$scope.status = {};
							$scope.status = {

								statusMonth : new Date()

							};

							$scope.hideexpenseform = true;
							$scope.hideviewtable = true;
							$scope.hideStatusTable = false;
							$scope.hideStatusTable1 = false;
							$scope.hidestatus = false;
						};

						$scope.hideexpenseform = true;
						$scope.ShowActivity = function() {

							$scope.hideviewtableEmp = true;
							$scope.hideexpenseform = false;
							$scope.hideviewtable = true;
							$scope.hidestatus=true;
							$scope.hideStatusTable = true;
							$scope.hideStatusTable1 = true;
							$scope.status = {

								statusMonth : new Date()

							};
							$scope.selectYear2 = "";
							$scope.selectMonth2 = "";

						};
						$scope.Close = function() {
							$scope.hideviewtableEmp = true;
							$scope.hideexpenseform = true;
						};

						$scope.hideselect = false;
						$scope.hideviewtable = true;
						$scope.viewStatus = function(statusId) {
							$scope.clear1();

							statusreportService.viewStatus(statusId, $scope);
							$scope.hideviewtable = false;
							$scope.hideselect = true;
						};

						$scope.hideviewtable = true;

						var fullName = sessionStorage.getItem("name");
						$scope.nameStatus = fullName;

						$scope.AcceptStatus = function(statusId) {
							var result = confirm("Are you sure to Accept "
									+ statusId);

							var empId = sessionStorage.getItem("idemp");
							$scope.restore1();
							if (result == true) {

								var sign = null;
								var sign = $scope.storedSignature1;
								var acceptStatus = [];
								if ((sign == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=")
										|| (sign == null)) {
									alert("Signature is Needed");
								} else {
									acceptStatus = [ statusId, sign, empId ];
									statusreportService.acceptstatus(
											acceptStatus, $scope);
									$scope.storedSignature1 = "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=";
									$scope.hideviewtable = true;
									$scope.hideselect = false;
								}
							}
						};

						$scope.CancelAdmin = function() {

							$scope.hideviewtable = true;
							$scope.hideselect = false;
						};
						$scope.ViewAllActivity = function() {

							var response;
							response = $http.get('viewAllList');

							response.success(function(data, status, headers,
									config) {
								console.log(data);
								$scope.contactsAdmin = data;
								$scope.statusempAd = {};
								$scope.filterstatuscode = {};
								$scope.hideMonthlyTable = true;
								$scope.hideReportN2 = true;
								$scope.hideReportPatton = true;
							});
							response.error(function(data, status, headers,
									config) {
								alert("Error in connection");
							});

							//		$http({
							//			method : 'GET',
							//			url : 'http://localhost:8080/HRSYSTEM/viewAllList',
							//			data : {}
							//		}).success(function(result) {
							//			
							//			console.log(result);
							//			$scope.contactsAdmin = result;
							//			$scope.statusempAd={};
							//			$scope.filterstatuscode={};
							//			$scope.hideMonthlyTable=true;
							//			$scope.hideReportN2=true;
							//			$scope.hideReportPatton=true;
							//		});

						};
						$scope.ViewPendingActivity = function() {
							$scope.statusListAdmin();
							$scope.statusempAd = {};
							$scope.filterstatuscode = {};
						};

						$scope.hideviewtableEmp = true;
						$scope.viewStatusEmp = function(statusId) {
							$scope.hideexpenseform = true;
							statusreportService.viewStatusEmp(statusId, $scope);
							$scope.hideviewtableEmp = false;
						}

						$scope.getMonthReportView = function() {
							$scope.hideexpenseform = true;

							$scope.hideviewtableEmp = false;
						}

						$scope.edit = function(statusId) {

							$scope.hideStatusTable = true;
							$scope.hideStatusTable1 = true;

							$scope.hideviewtableEmp = true;
							$scope.hideexpenseform = false;
							var i;
							for (i in $scope.contacts) {
								if ($scope.contacts[i].statusId == statusId) {

									var result = confirm("Are you sure to Edit "
											+ $scope.contacts[i].statusId
											+ " ?");

									if (result == true) {

										//	            	$scope.status = {
										//	            			
										//	            			statusId:$scope.contacts[i].statusId,
										//	            			employeeId:$scope.contacts[i].employeeId,
										//	    					activity:$scope.contacts[i].activity,
										//	    					statusMonth:new Date($scope.contacts[i].statusMonth),
										//	    					startPlanDate:new Date($scope.contacts[i].startPlannedDate),
										//	    					startActualDate:new Date($scope.contacts[i].startActualDate),
										//	    					finishPlannedDate:new Date($scope.contacts[i].finishPlannedDate),
										//	    					finishEstimatedDate:new Date($scope.contacts[i].finishEstimatedDate),
										//	    					statuscode:$scope.contacts[i].status,
										//	    					clientDetails:$scope.contacts[i].clientDetails,
										//	    					currentProject:$scope.contacts[i].currentProject,
										//	    					managerName:$scope.contacts[i].managerName,
										//	    					currentModule:$scope.contacts[i].currentModule,
										//	    					managerEmail:$scope.contacts[i].managerEmail,
										//	    					managerPhoneNumber:$scope.contacts[i].managerPhonenumber,
										//	    					action_status:$scope.contacts[i].action_status		            			
										//	            			
										//	            	};

										$scope.ActivityRepeat1 = [ {
											statusId : $scope.contacts[i].statusId,
											activity : $scope.contacts[i].activity,
											startPlanDate : new Date(
													$scope.contacts[i].startPlannedDate),
											startActualDate : new Date(
													$scope.contacts[i].startActualDate),
											finishPlannedDate : new Date(
													$scope.contacts[i].finishPlannedDate),
											finishEstimatedDate : new Date(
													$scope.contacts[i].finishEstimatedDate),
											statuscode : $scope.contacts[i].status

										} ];

										var AssignMonth1 = "";
										var AssignYear = "";
										sheetMonth = $scope.selectMonth;
										AssignYear = $scope.selectYear;
										if (sheetMonth == "January") {

											AssignMonth1 = "01";
										} else if (sheetMonth == "February") {

											AssignMonth1 = "02";
										} else if (sheetMonth == "March") {

											AssignMonth1 = "03";
										} else if (sheetMonth == "April") {

											AssignMonth1 = "04";
										} else if (sheetMonth == "May") {

											AssignMonth1 = "05";
										} else if (sheetMonth == "June") {

											AssignMonth1 = "06";
										} else if (sheetMonth == "July") {

											AssignMonth1 = "07";
										} else if (sheetMonth == "August") {

											AssignMonth1 = "08";
										} else if (sheetMonth == "September") {

											AssignMonth1 = "09";
										} else if (sheetMonth == "October") {

											AssignMonth1 = "10";
										} else if (sheetMonth == "November") {

											AssignMonth1 = "11";
										} else if (sheetMonth == "December") {

											AssignMonth1 = "12";
										}

										var dateEdit = AssignYear + "-"
												+ AssignMonth1 + "-05";

										//	        			alert(" $scope.contacts[i].managerPhonenumber "+$scope.contacts[i].managerPhonenumber);	 

										$scope.status = {

											statusId : $scope.contacts[i].statusId,
											employeeId : $scope.contacts[i].employeeId,
											statusMonth : new Date(dateEdit),
											clientDetails : $scope.contacts[i].clientDetails,
											currentProject : $scope.contacts[i].currentProject,
											managerName : $scope.contacts[i].managerName,
											currentModule : $scope.contacts[i].currentModule,
											managerEmail : $scope.contacts[i].managerEmail,
											managerPhoneNumber : $scope.contacts[i].managerPhonenumber,
											action_status : $scope.contacts[i].action_status

										};

									}
								}
							}

						};

						$scope.employeeList3 = [];
						$scope.employeeList3 = function() {

							var response;
							response = $http.get('employeelist1');

							response.success(function(data, status, headers,
									config) {
								$scope.statusEmplist = data;
							});
							response.error(function(data, status, headers,
									config) {
								alert("Error in connection");
							});

							//		$http({
							//			method : 'GET',
							//			url : 'http://localhost:8080/HRSYSTEM/employeelist1',
							//			data : {}
							//		}).success(function(result) {
							//			
							//			
							//			$scope.statusEmplist = result;
							//			
							//		});
						};
						$scope.employeeList3();

						$scope.StatusReportCode = [ "Pending", "Accepted" ];

						$scope.filterStatusempAd = function(statusEmpId) {
							if (statusEmpId == null) {

								$scope.statusListAdmin();

							} else {
								statusreportService.filterStatusempId(
										statusEmpId, $scope);
								$scope.hideActivityTable = false;
							}
						};
						$scope.filterStatusempAdStatus = function(statusEmpId,
								status) {

							var filterstatus3 = [];
							filterstatus3 = [ statusEmpId, status ];
							statusreportService.filterStatus(filterstatus3,
									$scope);
							$scope.hideActivityTable = false;

						};

						//signature
						$scope.restore = function() {
							$scope.storedSignature = null;
							var dataurl = $scope.accept().dataUrl;
							$scope.storedSignature = dataurl;
							$scope.clear();

						};

						$scope.restore1 = function() {
							var dataurl1 = $scope.accept1().dataUrl;

							$scope.storedSignature1 = dataurl1;

						};

						$scope.restoreAdmin = function() {
							var dataurl1 = $scope.acceptAdmin().dataUrl;

							$scope.AdminSignature1 = dataurl1;

						};

						$scope.clear1 = function() {

							$scope.accept1().clear1();
						};

						// Report generation
						$scope.hideReportN2 = true;
						$scope.hideReportPatton = true;
						$scope.export = function() {
							html2canvas(
									document.getElementById('exportthis'),
									{
										onrendered : function(canvas) {
											var data = canvas.toDataURL();
											var docDefinition = {
												content : [ {
													image : data,
													width : 500,
												} ]
											};
											pdfMake
													.createPdf(docDefinition)
													.download(
															"Status Report.pdf");
										}
									});
							$scope.counter = 1;
							$scope.countdown();
						};

						$scope.export1 = function() {
							html2canvas(document
									.getElementById('exportthisPatton'), {
								onrendered : function(canvas) {
									var data = canvas.toDataURL();
									var docDefinition = {
										content : [ {
											image : data,
											width : 500,
										} ]
									};
									pdfMake.createPdf(docDefinition).download(
											"Status Report.pdf");
								}
							});
							$scope.counter = 1;
							$scope.countdown();
						};

						$scope.clear3 = function() {
							$scope.accept3.clear3();

						};
						$scope.clear4 = function() {
							$scope.accept4.clear4();

						};
						$scope.clear6 = function() {

							$scope.accept6.clear6();
						};
						$scope.clear7 = function() {

							$scope.accept7.clear7();
						};
						$scope.clearAdmin = function() {
							$scope.acceptAdmin.clearAdmin();

						};

						$scope.GenerateReport = function(statusId, empId) {

							//		  		$scope.accept4().clear(); 
							//		  		$scope.accept3().clear();

							$scope.clear3();
							$scope.clear4();
							$scope.clear6();
							$scope.clear7();
							statusreportService.viewStatusReport(statusId,
									$scope);
							statusreportService.getEmpPayrollStatus(empId,
									$scope);
							$scope.hideviewtable = true;
							$scope.hideselect = true;

							$scope.hideselect = false;

						};

						$scope.cancelStatusReport = function() {
							$scope.hideReportN2 = true;
							$scope.hideReportPatton = true;
							$scope.hideActivityTable = false;
							$scope.hideMonthlyTable = true;

						};
						$scope.month = [ "January", "February", "March",
								"April", "May", "June", "July", "August",
								"September", "October", "November", "December" ];
						$scope.year = [ "2017", "2018", "2019", "2020", "2021",
								"2022", "2023", "2024", "2025" ];
						$scope.hideActivityTable = false;

						$scope.GetMonthlyStatusReport = function(empId, month,
								year) {
							var monthStatusReport = [];
							var AssignMonth;

							if (month == "January") {
								AssignMonth = "01";

							} else if (month == "February") {
								AssignMonth = "02";
							} else if (month == "March") {
								AssignMonth = "03";
							} else if (month == "April") {
								AssignMonth = "04";
							} else if (month == "May") {
								AssignMonth = "05";
							} else if (month == "June") {
								AssignMonth = "06";
							} else if (month == "July") {
								AssignMonth = "07";
							} else if (month == "August") {
								AssignMonth = "08";
							} else if (month == "September") {
								AssignMonth = "09";
							} else if (month == "October") {
								AssignMonth = "10";
							} else if (month == "November") {
								AssignMonth = "11";
							} else if (month == "December") {
								AssignMonth = "12";
							}

							$scope.clear3();
							$scope.clear4();
							$scope.clear6();
							$scope.clear7();
							monthStatusReport = [ empId, AssignMonth, year ];
							if ((empId == null) || (month == null)
									|| (year == null)) {
								alert("Choose Employee, month and Year should not Empty");
							} else {
								statusreportService.GetMonthlyStatusReport(
										monthStatusReport, $scope);
								statusreportService.getEmpPayrollStatus(empId,
										$scope);
							}
						};

						// timer

						$scope.counter = 1;
						var stopped;

						$scope.countdown = function() {
							stopped = $timeout(function() {
								//   console.log($scope.counter);
								$scope.counter--;
								if ($scope.counter == 0) {

									$scope.cancelStatusReport();

								} else {
									$scope.countdown();
								}
							}, 1000);
						};

						$scope.stop = function() {
							$timeout.cancel(stopped);

						}

						var idemp = sessionStorage.getItem("idemp");

						$scope.ActivityRepeat1 = [ {}, {}, {}, {} ];

						$scope.submit1 = function(data) {

							var activty = "Value";
							angular
									.forEach(
											$scope.ActivityRepeat1,
											function(value, key) {
												var pActivity = "";
												pActivity = $scope.ActivityRepeat1[key].activity;
												if (pActivity == null
														|| pActivity == "undefined"
														|| pActivity == "") {
													activty = "";
												}
											});
							if (activty == "Value") {
								$scope.ActivityRepeat1.push({});
							} else {
								alert("Please Enter the Activity");
							}

						};

						$scope.RemoveSheet = function(index) {

							if ($scope.ActivityRepeat1.length > 1) {
								alert("index" + index);
								$scope.ActivityRepeat1.splice(index, 1);
								var data = [ $scope.ActivityRepeat1 ];
								angular.forEach(data, function(value, index) {
									var pstatusId = null;
									pstatusId = data[index].statusId;

								});
							} else {
								alert("Atleast one Task is Must");
							}
						};

						$scope.submitActivity = function(status) {

							if (($scope.selectYear2 == null)
									|| ($scope.selectYear2 == "")
									|| ($scope.selectYear2 == "undefined")
									|| ($scope.selectMonth2 == null)
									|| ($scope.selectMonth2 == "")
									|| ($scope.selectMonth2 == "undefined")) {
								alert("Please check the value");
							} else {

								$scope.restore();

								var sheetMonth = "";
								var AssignMonth1 = "";
								var AssignYear = "";

								sheetMonth = $scope.selectMonth2;
								AssignYear = $scope.selectYear2;
								if (sheetMonth == "January") {

									AssignMonth1 = "01";
								} else if (sheetMonth == "February") {

									AssignMonth1 = "02";
								} else if (sheetMonth == "March") {

									AssignMonth1 = "03";
								} else if (sheetMonth == "April") {

									AssignMonth1 = "04";
								} else if (sheetMonth == "May") {

									AssignMonth1 = "05";
								} else if (sheetMonth == "June") {

									AssignMonth1 = "06";
								} else if (sheetMonth == "July") {

									AssignMonth1 = "07";
								} else if (sheetMonth == "August") {

									AssignMonth1 = "08";
								} else if (sheetMonth == "September") {

									AssignMonth1 = "09";
								} else if (sheetMonth == "October") {

									AssignMonth1 = "10";
								} else if (sheetMonth == "November") {

									AssignMonth1 = "11";
								} else if (sheetMonth == "December") {

									AssignMonth1 = "12";
								}

								var dateEdit = AssignYear + "-" + AssignMonth1
										+ "-05";

								var date = new Date(dateEdit);

								var idemp = sessionStorage.getItem("idemp");

								if ($scope.storedSignature == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
										|| $scope.storedSignature == null
										|| $scope.storedSignature == "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==") {

									alert("Signature is needed");
									$scope.storedSignature == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=";
								} else {

									var ActivityRepeatFinal = [];
									var valueMissing = "";
									var data = [];
									$scope.data1 = [];
									data = $scope.ActivityRepeat1;
									angular
											.forEach(
													data,
													function(value, key) {

														//valueMissing="";
														var pActivity = "";
														var pstartPlanDate = "";
														var pstartActualDate = "";
														var pfinishPlannedDate = "";
														var pfinishEstimatedDate = "";
														var pstatus = "";
														var pstatusId = null;
														pstatusId = data[key].statusId;
														pActivity = data[key].activity;

														//pstartPlanDate = data[key].startPlanDate;
														pstartPlanDate = $filter(
																'date')
																(
																		new Date(
																				data[key].startPlanDate),
																		"yyyy-MM-dd");
														//alert("date  == "+$filter('date')(new Date(data[key].startPlanDate), "yyyy-MM-dd"));
														//alert("Year  == "+$filter('date')(new Date($scope.statusEmp.monthYear), "yyyy"));					

														//pstartActualDate= data[key].startActualDate;
														pstartActualDate = $filter(
																'date')
																(
																		new Date(
																				data[key].startActualDate),
																		"yyyy-MM-dd");
														//pfinishPlannedDate= data[key].finishPlannedDate;
														pfinishPlannedDate = $filter(
																'date')
																(
																		new Date(
																				data[key].finishPlannedDate),
																		"yyyy-MM-dd");
														//pfinishEstimatedDate= data[key].finishEstimatedDate;
														pfinishEstimatedDate = $filter(
																'date')
																(
																		new Date(
																				data[key].finishEstimatedDate),
																		"yyyy-MM-dd");

														pstatus = data[key].statuscode;
														//	alert("pstatus    "+pstatus);
														//							alert("pActivityp    "+pActivity);
														var data12;

														if (pActivity == null
																|| pActivity == "undefined"
																|| $scope.selectYear2 == null
																|| $scope.selectYear2 == "undefined"
																|| $scope.selectMonth2 == null
																|| $scope.selectMonth2 == "undefined") {

														} else {
															valueMissing = "Value";
															if (pstatus == null
																	|| pstatus == "undefined") {
																pstatus = "On-track";
															}

															data12 = {

																statusId : pstatusId,
																employeeId : idemp,
																activity : pActivity,
																statusMonth : date,
																startPlannedDate : pstartPlanDate,
																startActualDate : pstartActualDate,
																finishPlannedDate : pfinishPlannedDate,
																finishEstimatedDate : pfinishEstimatedDate,
																status : pstatus,
																clientDetails : $scope.status.clientDetails,
																currentProject : $scope.status.currentProject,
																managerName : $scope.status.managerName,
																currentModule : $scope.status.currentModule,
																managerEmail : $scope.status.managerEmail,
																managerPhonenumber : $scope.status.managerPhoneNumber,
																employeeSignature : $scope.storedSignature,
																action_status : "Pending"

															};

															$scope.data1
																	.push(data12);

														}
													});
									if (valueMissing == "Value") {
										statusreportService.submitNewStatus1(
												$scope.data1, $scope);
									} else {
										alert("Please Enter atleast a Activity Name");
									}

								}

							}

						};

						$scope.cancelActivity = function() {
							$scope.status = {

								statusMonth : new Date()

							};
							$scope.hideexpenseform = true;
							$scope.hideviewtable = true;
							$scope.hideStatusTable = false;
							var data = [];
							data = $scope.contacts;
							if (data.length > 0) {
								$scope.hideStatusTable1 = false;
							} else {
								$scope.hideStatusTable1 = true;
							}

							$scope.hidestatus = false;
							$scope.ActivityRepeat1 = [ {}, {}, {}, {}, {} ];
							$scope.clear();
						};

						$scope.getStatusReportByMonth = function() {
							var idemp = sessionStorage.getItem("idemp");
							//kkc Start ------working for data format filter add first line controller in $filter like $scope--------
							//alert("value11 == "+$scope.statusEmp.monthYear);
							//alert("Month  == "+$filter('date')(new Date($scope.statusEmp.monthYear), "M"));
							//alert("Year  == "+$filter('date')(new Date($scope.statusEmp.monthYear), "yyyy"));
							//KKC End --------------------------------------------------------------------------------------------
							if (($scope.statusEmp.month == null)
									|| ($scope.statusEmp.month == "")
									|| ($scope.statusEmp.month == "undefined")
									|| ($scope.statusEmp.year == null)
									|| ($scope.statusEmp.year == "")
									|| ($scope.statusEmp.year == "undefined")) {
								alert("Please choose Month and Year");
							} else {

								var StatusMonth = [ idemp,
										$scope.statusEmp.month,
										$scope.statusEmp.year ];
								statusreportService.getStatusReportByMonthEmp(
										StatusMonth, $scope);

							}

						};

						$scope.singleEdit = function(s_month, statusId) {
							
							var result = confirm("Are you sure to Edit this Activity ?");

							if (result == true) {

							$scope.hidestatus = true;

							var dateM = new Date(s_month);
							var idemp = sessionStorage.getItem("idemp");
							var thismonth = new Date(s_month).getMonth() + 1;
							var curentyear = new Date(s_month).getFullYear();

							$scope.selectYear2 = curentyear.toString();

							if (thismonth == 1) {
								thismonth = "01";
								$scope.selectMonth2 = "January";

							} else if (thismonth == 2) {
								thismonth = "02";
								$scope.selectMonth2 = "February";

							} else if (thismonth == 3) {
								thismonth = "03";

								$scope.selectMonth2 = "March";

							} else if (thismonth == 4) {
								thismonth = "04";
								$scope.selectMonth2 = "April";

							} else if (thismonth == 5) {
								thismonth = "05";
								$scope.selectMonth2 = "May";

							} else if (thismonth == 6) {
								thismonth = "06";
								$scope.selectMonth2 = "June";

							} else if (thismonth == 7) {
								thismonth = "07";
								$scope.selectMonth2 = "July";

							} else if (thismonth == 8) {
								thismonth = "08";
								$scope.selectMonth2 = "August";

							} else if (thismonth == 9) {
								thismonth = "09";
								$scope.selectMonth2 = "September";

							} else if (thismonth == 10) {
								thismonth = "10";
								$scope.selectMonth2 = "October";

							} else if (thismonth == 11) {
								thismonth = "11";
								$scope.selectMonth2 = "November";

							} else if (thismonth == 12) {
								thismonth = "12";
								$scope.selectMonth2 = "December";

							}

							$scope.ActivityRepeat1 = [];
							$scope.ActivityEdit = {};
							$scope.hideStatusTable = true;
							$scope.hideStatusTable1 = true;
							$scope.hideviewtableEmp = true;
							$scope.hideexpenseform = false;
							var i;
							for (i in $scope.contacts) {
								if ($scope.contacts[i].statusId == statusId) {
									

										$scope.ActivityEdit = {
											statusId : $scope.contacts[i].statusId,
											activity : $scope.contacts[i].activity,
											startPlanDate : new Date(
													$scope.contacts[i].startPlannedDate),
											startActualDate : new Date(
													$scope.contacts[i].startActualDate),
											finishPlannedDate : new Date(
													$scope.contacts[i].finishPlannedDate),
											finishEstimatedDate : new Date(
													$scope.contacts[i].finishEstimatedDate),
											statuscode : $scope.contacts[i].status

										};

										$scope.ActivityRepeat1
												.push($scope.ActivityEdit);

										$scope.status = {

											//		            			statusId:$scope.contacts[i].statusId,
											employeeId : $scope.contacts[i].employeeId,
											statusMonth : new Date(
													$scope.contacts[i].statusMonth),
											clientDetails : $scope.contacts[i].clientDetails,
											currentProject : $scope.contacts[i].currentProject,
											managerName : $scope.contacts[i].managerName,
											currentModule : $scope.contacts[i].currentModule,
											managerEmail : $scope.contacts[i].managerEmail,
											managerPhoneNumber : parseInt($scope.contacts[i].managerPhonenumber),
											action_status : $scope.contacts[i].action_status

										};
									}
								}
							}
							
						};

						$scope.getMonthReport = function(s_month, statusId) {

							$scope.hidestatus = true;

							var dateM = new Date(s_month);
							var idemp = sessionStorage.getItem("idemp");
							var thismonth = new Date(s_month).getMonth() + 1;
							var curentyear = new Date(s_month).getFullYear();

							$scope.selectYear2 = curentyear.toString();

							if (thismonth == 1) {
								thismonth = "01";
								$scope.selectMonth2 = "January";

							} else if (thismonth == 2) {
								thismonth = "02";
								$scope.selectMonth2 = "February";

							} else if (thismonth == 3) {
								thismonth = "03";

								$scope.selectMonth2 = "March";

							} else if (thismonth == 4) {
								thismonth = "04";
								$scope.selectMonth2 = "April";

							} else if (thismonth == 5) {
								thismonth = "05";
								$scope.selectMonth2 = "May";

							} else if (thismonth == 6) {
								thismonth = "06";
								$scope.selectMonth2 = "June";

							} else if (thismonth == 7) {
								thismonth = "07";
								$scope.selectMonth2 = "July";

							} else if (thismonth == 8) {
								thismonth = "08";
								$scope.selectMonth2 = "August";

							} else if (thismonth == 9) {
								thismonth = "09";
								$scope.selectMonth2 = "September";

							} else if (thismonth == 10) {
								thismonth = "10";
								$scope.selectMonth2 = "October";

							} else if (thismonth == 11) {
								thismonth = "11";
								$scope.selectMonth2 = "November";

							} else if (thismonth == 12) {
								thismonth = "12";
								$scope.selectMonth2 = "December";

							}

							var StatusMonth = [];
							var StatusMonth = [ idemp, thismonth, curentyear ];

							statusreportService.getStatusReportByMonthEmpEdit(
									StatusMonth, $scope);

						};

						$scope.GetStatusReportReview = function(empId, status,
								month, year) {
							var StatusMonthReview = [];
							var StatusMonthReview = [ empId, status, month,
									year ];
							statusreportService
									.getStatusReportByMonthReviewAdmin(
											StatusMonthReview, $scope);
						};

						$scope.AcceptStatusAdmin = function() {

							$scope.data2 = [];
							$scope.restoreAdmin();
							var empId = sessionStorage.getItem("idemp");

							if ($scope.AdminSignature1 == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
									|| $scope.AdminSignature1 == null
									|| $scope.AdminSignature1 == "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQwAAAB4CAYAAAAHSMzwAAADPUlEQVR4Xu3UsQ0AAAjDMPr/07yQA8zcyULZOQIECESBxZ0ZAQIETjA8AQECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAgGD4AQIEsoBgZCpDAgQEww8QIJAFBCNTGRIgIBh+gACBLCAYmcqQAAHB8AMECGQBwchUhgQICIYfIEAgCwhGpjIkQEAw/AABAllAMDKVIQECguEHCBDIAoKRqQwJEBAMP0CAQBYQjExlSICAYPgBAgSygGBkKkMCBATDDxAgkAUEI1MZEiAgGH6AAIEsIBiZypAAAcHwAwQIZAHByFSGBAgIhh8gQCALCEamMiRAQDD8AAECWUAwMpUhAQKC4QcIEMgCgpGpDAkQEAw/QIBAFhCMTGVIgIBg+AECBLKAYGQqQwIEBMMPECCQBQQjUxkSICAYfoAAgSwgGJnKkAABwfADBAhkAcHIVIYECAiGHyBAIAsIRqYyJEBAMPwAAQJZQDAylSEBAoLhBwgQyAKCkakMCRAQDD9AgEAWEIxMZUiAwAPbnAB5QfXctwAAAABJRU5ErkJggg==") {

								alert("Signature is needed");
								$scope.AdminSignature1 == "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=";
							} else {

								var i;
								for (i in $scope.contactsAdmin) {

									var s_Id = "";
									var data;
									s_Id = $scope.contactsAdmin[i].statusId;

									data = {
										statusId : s_Id,
										adminSign : $scope.AdminSignature1,
										employeeId : empId
									};

									$scope.data2.push(data);
									console.log($scope.data2);

								}

								alert("$scope.AcceptStatus  " + $scope.data2);

								statusreportService.AcceptStatusAdmin1(
										$scope.data2, $scope);
							}
						};

					} else {
						$window.location.href = 'login.jsp';

					}

				});

app
		.directive(
				'signaturePad',
				[
						'$window',
						function($window) {
							'use strict';

							var signaturePad, canvas, element, EMPTY_IMAGE = 'data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA=';
							return {
								restrict : 'EA',
								replace : true,
								template : '<div class="signature"><canvas></canvas></div>',
								controller : 'StatusReportCtrl',
								scope : {
									accept : '=',
									clear : '=',
									dataurl : '=',
									height : '@',
									width : '@'
								},
								controller : [
										'$scope',
										function($scope) {

											$scope.accept = function() {
												var signature = {};

												if (!$scope.signaturePad
														.isEmpty()) {
													signature.dataUrl = $scope.signaturePad
															.toDataURL();
													signature.isEmpty = false;
												} else {
													signature.dataUrl = EMPTY_IMAGE;
													signature.isEmpty = true;
												}

												return signature;
											};

											$scope.clear = function() {
												$scope.signaturePad.clear();

											};

											$scope
													.$watch(
															"dataurl",
															function(dataUrl) {
																if (dataUrl) {
																	$scope.signaturePad
																			.fromDataURL(dataUrl);
																}
															});

										} ],
								link : function(scope, element) {
									canvas = element.find('canvas')[0];

									scope.onResize = function() {
										var canvas = element.find('canvas')[0];
										var ratio = Math.max(
												$window.devicePixelRatio || 1,
												1);
										canvas.width = canvas.offsetWidth
												* ratio;
										canvas.height = canvas.offsetHeight
												* ratio;
										canvas.getContext("2d").scale(ratio,
												ratio);
									}

									scope.onResize();

									scope.signaturePad = new SignaturePad(
											canvas);

									angular.element($window).bind('resize',
											function() {
												scope.onResize();
											});
								}
							};
						} ]);
app.controller('RepeatCtrl', function($scope, statusreportService, $timeout,
		$window, $http, $rootScope) {

	var sess = sessionStorage.getItem("key1");
	if (sess) {

		$scope.status = $scope.act;

	} else {
		$window.location.href = 'login.jsp';

	}

});
