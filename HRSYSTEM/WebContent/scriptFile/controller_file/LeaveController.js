'use strict';

app.controller('leaveCtrl', function($scope, $window, $http, $rootScope,leaveService,$filter) {
 

	var sess = sessionStorage.getItem("key1");
	if (sess) {
	
    	
		
		$rootScope.leave = $scope.leave;
    
		var empId = sessionStorage.getItem("idemp");
 		var firstname = sessionStorage.getItem("firstname");	
 		var designation = sessionStorage.getItem("designation");
 
 		$scope.mintday = new Date();	
 		
  		$scope.getFromDate = function(){
 			
  			$rootScope.leave={
  					leaveType : $rootScope.leave.leaveType,
  					leaveFrom: new Date($rootScope.leave.leaveFrom),
  					leaveTo: new Date ($rootScope.leave.leaveFrom)
  			};
  			
 //  	 		$scope.leaveTo = new Date("12/12/2017");
//  	 		angular.element(document.getElementsByClassName("To")).addClass("bg");

//  			angular.element(document.getElementById("To")).addClass("bg");

 		};
 		
 		$scope.val='leaveCtrl';
		
		$scope.val1='leaveCtrl';
		$scope.Type = ["Annual Leave" ,"Sick Leave"];
		$scope.leaveStatus = ["Pending","Approved","Rejected"];
		$scope.Newleave = true;  // tabular frmt form
		$scope.applyBtn = false;
		$rootScope.tableAd = false;
		$rootScope.tableUser = false;
		$rootScope.form2 = false;
		
		$rootScope.hideselect = false;
		
		$rootScope.viewleavebox = true;
		
		
		// employee used to get list of leaves applied by id
		$scope.getListById = [];
		$scope.getListById = function(){
			
				leaveService.getListById(empId,$scope);
		};
		$scope.getListById();
		
//		$scope.changeStatus = [];
//		$scope.changeStatus = function(){
//			
//			alert("hange statsu");
//			$scope.tday = new Date();
//			
//			$scope.formattedDate =   $filter('date')($scope.tday, "yyyy-MM-dd");
//
//			var now = $scope.formattedDate;
//			
//			 leaveService.changeStatus(now);
//		};
//		$scope.changeStatus();
		
		
		// this is to display employee id and name default
		$scope.employeeList2 = [];
		$scope.employeeList2 = function() {
			
			
			var response;
			 response = $http.get('employeelist1');
			
			 response.success(function(data, status, headers, config) {
				 console.log(data);
					
				 $scope.contactsEmp = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://n2apps.cloudapp.net:8080/HRSYSTEM/employeelist1',
//				data : {}
//			}).success(function(result) {
//					
//				$scope.contactsEmp = result;
//				
//			});
		};
		
		// this is to display employee details
		$scope.employeeList2();

		
		
		// Admin side this is to display employee id and name dropdown
		$scope.leaveList = [];
		$scope.leaveList = function() {
			
			
			var response;
			 response = $http.get('leaveList');
			
			 response.success(function(data, status, headers, config) {
				 console.log(data);
					
				 $scope.contactsAdmin = data;
			 });
				response.error(function(data, status, headers, config) {
					alert("Error in connection");
				});
			
			
			
//			$http({
//				method : 'GET',
//				url : 'http://n2apps.cloudapp.net:8080/HRSYSTEM/leaveList',
//				data : {}
//			}).success(function(result) {
//				
//				
//				$scope.contactsAdmin = result;
//				
// 		
//			});
		};

		$scope.leaveList();

		

		// this is to show new leave form
		$scope.ShowNewLeave = function(){
			
			$scope.Newleave = true; // tabularformat
			$rootScope.form2 = false;
			
			$rootScope.tableUser = true;  // hide table

			
			$scope.applyBtn = true;
			$rootScope.tableAd = true;
		};
		
		
		$scope.submit =function(data){
			
//			alert("employee id" + empId);
			 
	          
	          var type = $rootScope.leave.leaveType;
	          var from = $rootScope.leave.leaveFrom;
	          var to = $rootScope.leave.leaveTo;
	          var reason = $rootScope.leave.description;
	          
 	          
	          if( type == undefined ||from == undefined || to == undefined || reason == undefined )
	        	  {
	        	  	alert("Please enter all fields");
	        	  	//$rootScope.leave = {};
	        	  }else {
	
	        			var timeDiff = Math.abs($rootScope.leave.leaveTo.getDate() - $rootScope.leave.leaveFrom.getDate()+1);
	       	          
	      	          $scope.d = timeDiff ; 
//	      	          alert($scope.d);
	      	
			data ={
					employeeId:empId,
					name:firstname,
					designation:designation,
					leaveType:$rootScope.leave.leaveType,
					leaveFrom:$rootScope.leave.leaveFrom,
					leaveTo:$rootScope.leave.leaveTo,
					duration:$scope.d,
					status:$rootScope.leave.status,
					description:$rootScope.leave.description
					}; 
		 
			leaveService.submit(data,$scope);
			
	        	  }
		};
		
		

		$scope.CancelLeave = function(leaveId){
			
			var result= confirm("Are you sure to Cancel ?");
        	if(result == true){
 
        		$scope.cancelEmpid = [];
    			$scope.cancelEmpid = [leaveId,empId];
    			leaveService.CancelLeave($scope.cancelEmpid,$scope);
 
        	}
    
		};
		
	
		$scope.cancel = function(leave){
			
			$rootScope.leave = {};
//			$scope.Newleave = true;
			$rootScope.form2 = false;
			$rootScope.tableUser = false;  // hide table
			
			$scope.applyBtn = false;
			$rootScope.tableAd = false;
 		};
		
 		
 		$scope.cancel1 = function(leave){
 			
 			$rootScope.leave = {};
 			$rootScope.RescheduleForm = true;
 			$rootScope.form2 = false;
 		};
 		
 		$scope.cancelAd = function(){
 			
  			$rootScope.viewleavebox = true;
 			$rootScope.hideselect = false;
 			
			$scope.applyBtn = false;
			
			$rootScope.form2 = false;
 			$rootScope.tableAd = false;
 		};
		
 		$scope.CancelGo = function(){
 			
 			$scope.hideTotalCheck = true;
 		};
 		
 		
 		$scope.hideTotalCheck = true;
 		$scope.totalLeave = function(){
 			
 			$scope.hideTotalCheck = false;
 			
 			var empId = sessionStorage.getItem("idemp");
 			var filtertable1 = [];
 			filtertable1 = [empId,$scope.filtertable.leaveFrom,$scope.filtertable.leaveTo,$scope.filtertable.status];
// 			ExpensesService.go(filtertable1,$scope);
 		
 			
 		};
 		
 		$scope.GetTotal = function(filter){
 			
// 			alert("inside filter"+ $scope.filter.leaveFrom);
 			var empId = sessionStorage.getItem("idemp");
 			var filtertable1 = [];
 			filtertable1 = [$scope.filter.leaveFrom,$scope.filter.leaveTo,$scope.filter.status,empId];
 			leaveService.GetTotal(filtertable1,$scope);
 			
 		};
 		
		$scope.viewLeave = function(leaveId){
//			alert(leaveId);
			leaveService.viewLeave(leaveId,$scope);
			
			$rootScope.viewleavebox = false;
			$rootScope.hideselect = true; // hide drop down
			$rootScope.tableUser = false;  // hide table
			$rootScope.form2 = true;      // apply form
			$scope.applyBtn = true; // apply button
		};
	
		
		
		$scope.approve = function(leaveId){
		     			
			var result= confirm("Are you sure to Approve ?");
        	if(result == true){

			
			$rootScope.viewleavebox = false;
			$rootScope.hideselect = true; // hide drop down
			$rootScope.tableUser = false;  // hide table
			$rootScope.form2 = true;      // apply form
			$scope.applyBtn = true; // apply button

			$scope.arrApprove = [];
			$scope.arrApprove = [leaveId,empId];
			
			leaveService.approve($scope.arrApprove,$scope);
        	}
		};
		
		$scope.reject = function(leaveId){
			var result= confirm("Are you sure to Reject ?");
        	if(result == true){

			
			$scope.arrReject = [];
			$scope.arrReject = [leaveId,empId];
			leaveService.reject($scope.arrReject,$scope);
        	}
		};
		
		
		$scope.filterEmployeeId = function(employeeId,status){
			 
 			if( employeeId == null || employeeId == undefined  ){
				
				$scope.leaveList();
			}	
			else {
				
 				leaveService.filterEmployeeStatus(employeeId,$scope);
			}

		};

 		
		$rootScope.RescheduleForm = true;
		$scope.RescheduleEmp = function(leaveId){
			
			
			var result= confirm("Are you sure to Reschedule ?");
        	if(result == true){
        		
        	$rootScope.viewleavebox = true;
			leaveService.viewReschedule(leaveId,$scope);
			$rootScope.RescheduleForm = false;
			$rootScope.form2 = true;
			

        	}
        };
		
		
		$scope.submitReschedule = function(data){
			
			
//			alert("Reschedule employee id");
//			alert($rootScope.g);
		
	
	          var type = $rootScope.leave.leaveType;
	          var from = $rootScope.leave.leaveFrom;
	          var to = $rootScope.leave.leaveTo;
	          var reason = $rootScope.leave.description;
	          
	          if( type == undefined ||from == undefined || to == undefined || reason == undefined )
	        	  {
	        	  	alert("Please enter all fields");
	        	  	$rootScope.leave = {};
	        	  }else {
	
	        	      var timeDiff = Math.abs($rootScope.leave.leaveTo.getDate() - $rootScope.leave.leaveFrom.getDate()+1);
	     	          
	    	          $scope.d = timeDiff ; 
//	    	          alert($scope.d);
	          
	    	   data ={
					employeeId:empId,
					leaveId:$rootScope.leave.leaveId,
					leaveType:$rootScope.leave.leaveType,
					leaveFrom:$rootScope.leave.leaveFrom,
					leaveTo:$rootScope.leave.leaveTo,
					duration:$scope.d,
					status:$rootScope.g,
					description:$rootScope.leave.description,
					description1:$rootScope.leave.description1
				}; 
	
			leaveService.submitReschedule(data,$scope);
			
	        }
		};
		
	 	
	}
	else {
		$window.location.href = 'login.jsp';

	}

	 });