'use strict';

app.controller('helpCtrl', function($scope, $window, $http, $rootScope) {
 

	var sess = sessionStorage.getItem("key1");
	if (sess) {
	
		var Emprole = sessionStorage.getItem("EmpRole");
		
		// EXPENSE
		$scope.hideExpenseEmp=true;
		$scope.hideExpenseAdmin=true;
		
		if(Emprole != "ADMIN"){
			$scope.hideExpenseEmp=false;
			$scope.hideExpenseAdmin=true;
		}
		else{
			$scope.hideExpenseEmp=true;
			$scope.hideExpenseAdmin=false;
		}
		
		//STATUS REPORT
		

		
		$scope.hideStatusEmp=true;
		$scope.hideStatusAdmin=true;
		
		if(Emprole != "ADMIN"){
			$scope.hideStatusEmp=false;
			$scope.hideStatusAdmin=true;
		}
		else{
			$scope.hideStatusEmp=true;
			$scope.hideStatusAdmin=false;
		}
		
		
		
		//PERFORMANCE REVIEW
		$scope.hideReviewEmp=true;
		$scope.hideReviewAdmin=true;
		if(Emprole != "ADMIN"){
			$scope.hideReviewEmp=false;
			$scope.hideReviewAdmin=true;
		}
		else{
			$scope.hideReviewEmp=true;
			$scope.hideReviewAdmin=false;
		}
		
		
		
		
		//PAYROLL  // myprofile changed
		$scope.hidePayrollEmp=true;
		$scope.hidePayrollAdmin=true;
		
		if(Emprole != "ADMIN"){
			$scope.hidePayrollEmp=false;
			$scope.hidePayrollAdmin=true;
		}
		else{
			$scope.hidePayrollEmp=true;
			$scope.hidePayrollAdmin=false;
		}
		
		// blog
		
		$scope.hideBlogEmp=true;
		$scope.hideBlogAdmin=true;
		
		if(Emprole != "ADMIN"){
			$scope.hideBlogEmp=false;
			$scope.hideBlogAdmin=true;
		}
		else{
			$scope.hideBlogEmp=true;
			$scope.hideBlogAdmin=false;
		}
		
		
		// Leave
		
		$scope.hideLeaveEmp=true;
		$scope.hideLeaveAdmin=true;
		
		if(Emprole != "ADMIN"){
			$scope.hideLeaveEmp=false;
			$scope.hideLeaveAdmin=true;
		}
		else{
			$scope.hideLeaveEmp=true;
			$scope.hideLeaveAdmin=false;
		}
		
		
		// time sheet
		
		$scope.hideTimeSheetEmp=true;
		$scope.hideTimeSheetAdmin=true;
		
		if(Emprole != "ADMIN"){
			$scope.hideTimeSheetEmp=false;
			$scope.hideTimeSheetAdmin=true;
		}
		else{
			$scope.hideTimeSheetEmp=true;
			$scope.hideTimeSheetAdmin=false;
		}
		
		
		
		
		// coe
		
		$scope.hideCoeEmp=true;
		$scope.hideCoeAdmin=true;
		
		if(Emprole != "ADMIN"){
			$scope.hideCoeEmp=false;
			$scope.hideCoeAdmin=true;
		}
		else{
			$scope.hideCoeEmp=true;
			$scope.hideCoeAdmin=false;
		}
		
		
		
		
		
		
		$scope.expenseHelp = true;
		$scope.statusreportHelp = true;
		$scope.performanceHelp = true;
		$scope.payrollHelp = true;
		$scope.homeHelp=false;
		
		$scope.blogHelp= true;
		$scope.timeSheetHelp= true;
		$scope.coeHelp= true;
		$scope.leaveHelp= true;
		
		$scope.expenseHelpFn = function(){
	 		$scope.expenseHelp =  false;
			
			$scope.statusreportHelp = true;
			$scope.performanceHelp = true;
			$scope.payrollHelp = true;
			$scope.localHelp = true;
			$scope.homeHelp=true;
			$scope.blogHelp= true;
			$scope.timeSheetHelp= true;
			$scope.coeHelp= true;
			$scope.leaveHelp= true;
		};
		
		$scope.statusreportHelpFn = function(){
	 		$scope.statusreportHelp =  false ;
			
	 		$scope.expenseHelp = true;
	 	 
	 		$scope.performanceHelp = true;
	 		$scope.payrollHelp = true;
	 		$scope.homeHelp=true;
	 		$scope.blogHelp= true;
			$scope.timeSheetHelp= true;
			$scope.coeHelp= true;
			$scope.leaveHelp= true;
		 
		};
		
		$scope.performanceFn = function(){
			 
			$scope.performanceHelp =  false;
			
			$scope.expenseHelp = true;
			$scope.statusreportHelp = true;
	 
			$scope.payrollHelp = true;
			$scope.homeHelp=true;
			$scope.blogHelp= true;
			$scope.timeSheetHelp= true;
			$scope.coeHelp= true;
			$scope.leaveHelp= true;
			 
		};
		
		$scope.payrollFn = function(){
			 
			$scope.payrollHelp = false ;
		
			$scope.expenseHelp = true;
			$scope.statusreportHelp = true;
			$scope.performanceHelp = true;
			$scope.homeHelp=true;
			$scope.blogHelp= true;
			$scope.timeSheetHelp= true;
			$scope.coeHelp= true;
			$scope.leaveHelp= true;
			
		};
		
		$scope.blogFn=function(){
			
			$scope.payrollHelp = true ;
			
			$scope.expenseHelp = true;
			$scope.statusreportHelp = true;
			$scope.performanceHelp = true;
			$scope.homeHelp=true;
			$scope.blogHelp= false;
			$scope.timeSheetHelp= true;
			$scope.coeHelp= true;
			$scope.leaveHelp= true;
		};
		
		$scope.timeSheetFn=function(){
			$scope.payrollHelp = true ;
			
			$scope.expenseHelp = true;
			$scope.statusreportHelp = true;
			$scope.performanceHelp = true;
			$scope.homeHelp=true;
			$scope.blogHelp= true;
			$scope.timeSheetHelp= false;
			$scope.coeHelp= true;
			$scope.leaveHelp= true;
		};
		
		$scope.coeFn=function(){
			$scope.payrollHelp = true ;
			
			$scope.expenseHelp = true;
			$scope.statusreportHelp = true;
			$scope.performanceHelp = true;
			$scope.homeHelp=true;
			$scope.blogHelp= true;
			$scope.timeSheetHelp= true;
			$scope.coeHelp= false;
			$scope.leaveHelp= true;
		};
		
		$scope.leaveFn=function(){
			$scope.payrollHelp = true ;
			$scope.expenseHelp = true;
			$scope.statusreportHelp = true;
			$scope.performanceHelp = true;
			$scope.homeHelp=true;
			$scope.blogHelp= true;
			$scope.timeSheetHelp= true;
			$scope.coeHelp= true;
			$scope.leaveHelp= false;
		};
		
		
		
		
	}
	else {
		$window.location.href = 'login.jsp';

	}

	 });