'use strict';

app.factory('timesheetService', function($http,$location,$window,$rootScope,$filter) {
	var session = {};
	
session.submitNewSheet = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside timesheet service")
		var response;
		
		if($scope.sheet.sheetId == null){
 		
		 response = $http.post('newTimeSheet', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 			
			console.log(log);
			 response = $http.post('newTimeSheet', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			
			$scope.dataFinal=[];
			 $scope.SheetTaskRepeat=[];
//			 $scope.sheet = {
//	        			
//					 TimeSheetMonth:new Date()
//						
//	        	};
			 	$scope.DisableEdit=false;
				$scope.DisableDay01=true;
				$scope.DisableDay02=true;					
				$scope.DisableDay03=true;
				$scope.DisableDay04=true;
				$scope.DisableDay05=true;
				$scope.DisableDay06=true;
				$scope.DisableDay07=true;					
				$scope.DisableDay08=true;
				$scope.DisableDay09=true;
				$scope.DisableDay10=true;
				$scope.DisableDay11=true;
				$scope.DisableDay12=true;					
				$scope.DisableDay13=true;
				$scope.DisableDay14=true;
				$scope.DisableDay15=true;
				$scope.DisableDay16=true;
				$scope.DisableDay17=true;					
				$scope.DisableDay18=true;
				$scope.DisableDay19=true;
				$scope.DisableDay20=true;
				$scope.DisableDay21=true;
				$scope.DisableDay22=true;					
				$scope.DisableDay23=true;
				$scope.DisableDay24=true;
				$scope.DisableDay25=true;
				$scope.DisableDay26=true;
				$scope.DisableDay27=true;					
				$scope.DisableDay28=true;
				$scope.DisableDay29=true;
				$scope.DisableDay30=true;
				$scope.DisableDay31=true;
				
				
				$scope.DisableManager=true;
				$scope.DisableClient=true;
				$scope.DisableProject=true;
				$scope.DisableResourceComments=true;
				
				$scope.DisableSubmit=true;
				$scope.DisableTaskName=true;
				$scope.RemoveSheetImage=true;
				$scope.AddSheetButton=true;
				$scope.DisableClear=false;
				for(var i in $scope.contacts){
					
										 
					 $scope.sheet = {
						 sheetId:$scope.contacts[i].sheetId,	            			
						 taskName:$scope.contacts[i].taskName,
						day1: $scope.contacts[i].day1,
						day2:$scope.contacts[i].day2,
						day3:$scope.contacts[i].day3,
						day4:$scope.contacts[i].day4,
						day5:$scope.contacts[i].day5,
						day6:$scope.contacts[i].day6,
						day7:$scope.contacts[i].day7,
						day8:$scope.contacts[i].day8,
						day9:$scope.contacts[i].day9,
						day10: $scope.contacts[i].day10,
						day11: $scope.contacts[i].day11,
						day12: $scope.contacts[i].day12,
						day13: $scope.contacts[i].day13,
						day14: $scope.contacts[i].day14,
						day15: $scope.contacts[i].day15,
						day16: $scope.contacts[i].day16,
						day17: $scope.contacts[i].day17,
						day18: $scope.contacts[i].day18,
						day19: $scope.contacts[i].day19,
						day20: $scope.contacts[i].day20,
						day21: $scope.contacts[i].day21,
						day22: $scope.contacts[i].day22,
						day23: $scope.contacts[i].day23,
						day24: $scope.contacts[i].day24,
						day25: $scope.contacts[i].day25,
						day26: $scope.contacts[i].day26,
						day27: $scope.contacts[i].day27,
						day28: $scope.contacts[i].day28,
						day29: $scope.contacts[i].day29,
						day30: $scope.contacts[i].day30,
						day31: $scope.contacts[i].day31
        				 
        			 };
					 $scope.SheetTaskRepeat.push($scope.sheet);
					 
					 $scope.sheet={
							 TimeSheetMonth:new Date($scope.contacts[i].TimeSheetMonth),
							 selectMonth:$scope.contacts[i].selectMonth,
							 selectYear:$scope.contacts[i].selectYear,
								resourceComments:$scope.contacts[i].resourceComments,
								project:$scope.contacts[i].project,
								client:$scope.contacts[i].client,
								clientManager:$scope.contacts[i].clientManager,
								status:$scope.contacts[i].status
					 };
					 
		 
						
				 }
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
 	 	 
	
	session.getEmpTimeSheetByMonth = function(data, $scope) {	
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside timesheet service")
		var response;
		
		if($scope.sheet.sheetId == null){
 		
		 response = $http.post('getTimeSheet', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 			
			console.log(log);
			 response = $http.post('getTimeSheet', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
		
		
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			if(data == "NOT-AVAILABLE"){
				$scope.SheetTaskRepeat=[{}];
				$scope.DisableEdit=true;
//				$scope.sheet = {
//						 TimeSheetMonth:new Date($scope.sheet.TimeSheetMonth),
//						
//				};
			}else{
				
				
				
				
				
				
			$scope.dataFinal=[];
			 $scope.SheetTaskRepeat=[];
			 $scope.sheet = {
	        			
					 TimeSheetMonth:new Date()
						
	        	};
			
				$scope.DisableDay01=true;
				$scope.DisableDay02=true;					
				$scope.DisableDay03=true;
				$scope.DisableDay04=true;
				$scope.DisableDay05=true;
				$scope.DisableDay06=true;
				$scope.DisableDay07=true;					
				$scope.DisableDay08=true;
				$scope.DisableDay09=true;
				$scope.DisableDay10=true;
				$scope.DisableDay11=true;
				$scope.DisableDay12=true;					
				$scope.DisableDay13=true;
				$scope.DisableDay14=true;
				$scope.DisableDay15=true;
				$scope.DisableDay16=true;
				$scope.DisableDay17=true;					
				$scope.DisableDay18=true;
				$scope.DisableDay19=true;
				$scope.DisableDay20=true;
				$scope.DisableDay21=true;
				$scope.DisableDay22=true;					
				$scope.DisableDay23=true;
				$scope.DisableDay24=true;
				$scope.DisableDay25=true;
				$scope.DisableDay26=true;
				$scope.DisableDay27=true;					
				$scope.DisableDay28=true;
				$scope.DisableDay29=true;
				$scope.DisableDay30=true;
				$scope.DisableDay31=true;
				
				$scope.DisableManager=true;
				$scope.DisableClient=true;
				$scope.DisableProject=true;
				$scope.DisableResourceComments=true;
				
				$scope.DisableSubmit=true;
				$scope.DisableTaskName=true;
				$scope.RemoveSheetImage=true;
				$scope.AddSheetButton=true;
				$scope.DisableClear=false;
				$scope.DisableEdit=false;
				for(var i in $scope.contacts){
					
										 
					 $scope.sheet = {
						 sheetId:$scope.contacts[i].sheetId,	            			
						 taskName:$scope.contacts[i].taskName,
						day1: $scope.contacts[i].day1,
						day2:$scope.contacts[i].day2,
						day3:$scope.contacts[i].day3,
						day4:$scope.contacts[i].day4,
						day5:$scope.contacts[i].day5,
						day6:$scope.contacts[i].day6,
						day7:$scope.contacts[i].day7,
						day8:$scope.contacts[i].day8,
						day9:$scope.contacts[i].day9,
						day10: $scope.contacts[i].day10,
						day11: $scope.contacts[i].day11,
						day12: $scope.contacts[i].day12,
						day13: $scope.contacts[i].day13,
						day14: $scope.contacts[i].day14,
						day15: $scope.contacts[i].day15,
						day16: $scope.contacts[i].day16,
						day17: $scope.contacts[i].day17,
						day18: $scope.contacts[i].day18,
						day19: $scope.contacts[i].day19,
						day20: $scope.contacts[i].day20,
						day21: $scope.contacts[i].day21,
						day22: $scope.contacts[i].day22,
						day23: $scope.contacts[i].day23,
						day24: $scope.contacts[i].day24,
						day25: $scope.contacts[i].day25,
						day26: $scope.contacts[i].day26,
						day27: $scope.contacts[i].day27,
						day28: $scope.contacts[i].day28,
						day29: $scope.contacts[i].day29,
						day30: $scope.contacts[i].day30,
						day31: $scope.contacts[i].day31
        				 
        			 };
					 $scope.SheetTaskRepeat.push($scope.sheet);
					 
					 $scope.sheet={
							 TimeSheetMonth:new Date($scope.contacts[i].TimeSheetMonth),
							 selectMonth:$scope.contacts[i].selectMonth,
							 employeeId:$scope.contacts[i].employeeId,
							 selectYear:$scope.contacts[i].selectYear,
								resourceComments:$scope.contacts[i].resourceComments,
								project:$scope.contacts[i].project,
								client:$scope.contacts[i].client,
								clientManager:$scope.contacts[i].clientManager,
								status:$scope.contacts[i].status
					 };
					 
		 
						
				 }
				
				$scope.statusAdminApprove="";
				for(var i in data){
					$scope.statusAdminApprove=data[i].status;
				}
				
				if($scope.statusAdminApprove == "Accepted"){
					$scope.DisableEdit=true;
				}else if($scope.statusAdminApprove == "Rejected"){
					$scope.DisableEdit=false;
				}else{
					$scope.DisableEdit=false;
				}
				
				
				
			}
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
session.getTimeSheetAdmin = function(data, $scope) {
		
		
		var log = data;
		console.log(log);
		$scope.SheetTaskRepeatAdimn=[];
		var response;
		
		
 		
		 response = $http.post('getTimeSheetAdmin', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		
				
		response.success(function(data, status, headers, config) {
			
			console.log("data "+data);		
			$scope.contacts = data;
			$scope.totaltask=0.00;
			$scope.singleRowTotal=0.00;
			
			$scope.totalDay1=0.00;
			$scope.totalDay2=0.00;
			$scope.totalDay3=0.00;
			$scope.totalDay4=0.00;
			$scope.totalDay5=0.00;
			$scope.totalDay6=0.00;
			$scope.totalDay7=0.00;
			$scope.totalDay8=0.00;
			$scope.totalDay9=0.00;
			$scope.totalDay10=0.00;
			$scope.totalDay11=0.00;
			$scope.totalDay12=0.00;
			$scope.totalDay13=0.00;
			$scope.totalDay14=0.00;
			$scope.totalDay15=0.00;
			$scope.totalDay16=0.00;
			$scope.totalDay17=0.00;
			$scope.totalDay18=0.00;
			$scope.totalDay19=0.00;
			$scope.totalDay20=0.00;
			$scope.totalDay21=0.00;
			$scope.totalDay22=0.00;
			$scope.totalDay23=0.00;
			$scope.totalDay24=0.00;
			$scope.totalDay25=0.00;
			$scope.totalDay26=0.00;
			$scope.totalDay27=0.00;
			$scope.totalDay28=0.00;
			$scope.totalDay29=0.00;
			$scope.totalDay30=0.00;
			$scope.totalDay31=0.00;
			
			if(data == "NOT-AVAILABLE"){
				 $scope.SheetTaskRepeatAdimn=[];
				 $scope.hideTableAdmin=true;
				 alert("Time Sheet Not Available");

			}else{
			
				$scope.hideTableAdmin=false;
			
			for(var i in data){
				$scope.totalDay1=$scope.totalDay1+parseFloat(data[i].day1);	
				$scope.totalDay2=$scope.totalDay2+parseFloat(data[i].day2);
				$scope.totalDay3=$scope.totalDay3+parseFloat(data[i].day3);
				$scope.totalDay4=$scope.totalDay4+parseFloat(data[i].day4);
				$scope.totalDay5=$scope.totalDay5+parseFloat(data[i].day5);
				$scope.totalDay6=$scope.totalDay6+parseFloat(data[i].day6);
				$scope.totalDay7=$scope.totalDay7+parseFloat(data[i].day7);
				$scope.totalDay8=$scope.totalDay8+parseFloat(data[i].day8);
				$scope.totalDay9=$scope.totalDay9+parseFloat(data[i].day9);
				$scope.totalDay10=$scope.totalDay10+parseFloat(data[i].day10);
				$scope.totalDay11=$scope.totalDay11+parseFloat(data[i].day11);
				$scope.totalDay12=$scope.totalDay12+parseFloat(data[i].day12);
				$scope.totalDay13=$scope.totalDay13+parseFloat(data[i].day13);
				$scope.totalDay14=$scope.totalDay14+parseFloat(data[i].day14);
				$scope.totalDay15=$scope.totalDay15+parseFloat(data[i].day15);
				$scope.totalDay16=$scope.totalDay16+parseFloat(data[i].day16);
				$scope.totalDay17=$scope.totalDay17+parseFloat(data[i].day17);
				$scope.totalDay18=$scope.totalDay18+parseFloat(data[i].day18);
				$scope.totalDay19=$scope.totalDay19+parseFloat(data[i].day19);
				$scope.totalDay20=$scope.totalDay20+parseFloat(data[i].day20);
				$scope.totalDay21=$scope.totalDay21+parseFloat(data[i].day21);
				$scope.totalDay22=$scope.totalDay22+parseFloat(data[i].day22);
				$scope.totalDay23=$scope.totalDay23+parseFloat(data[i].day23);
				$scope.totalDay24=$scope.totalDay24+parseFloat(data[i].day24);
				$scope.totalDay25=$scope.totalDay25+parseFloat(data[i].day25);
				$scope.totalDay26=$scope.totalDay26+parseFloat(data[i].day26);
				$scope.totalDay27=$scope.totalDay27+parseFloat(data[i].day27);
				$scope.totalDay28=$scope.totalDay28+parseFloat(data[i].day28);
				$scope.totalDay29=$scope.totalDay29+parseFloat(data[i].day29);
				$scope.totalDay30=$scope.totalDay30+parseFloat(data[i].day30);
				$scope.totalDay31=$scope.totalDay31+parseFloat(data[i].day31);
				
			}
			
			for(var i in data){
				
				$scope.singleRowTotal=0.0;
				$scope.singleRowTotal=parseFloat(data[i].day1)+parseFloat(data[i].day2)+
				parseFloat(data[i].day3)+parseFloat(data[i].day4)+parseFloat(data[i].day5)+parseFloat(data[i].day6)
				+parseFloat(data[i].day7)+parseFloat(data[i].day8)+parseFloat(data[i].day9)+parseFloat(data[i].day10)
				 +parseFloat(data[i].day11)+parseFloat(data[i].day12)+parseFloat(data[i].day13)+parseFloat(data[i].day14)+parseFloat(data[i].day15)
				+parseFloat(data[i].day16)+parseFloat(data[i].day17)+parseFloat(data[i].day18)+parseFloat(data[i].day19)+parseFloat(data[i].day20)
				+parseFloat(data[i].day21)+parseFloat(data[i].day22)+parseFloat(data[i].day23)+parseFloat(data[i].day24)+parseFloat(data[i].day25)	
				+parseFloat(data[i].day26)+parseFloat(data[i].day27)+parseFloat(data[i].day28)+parseFloat(data[i].day29)+parseFloat(data[i].day30)
				+parseFloat(data[i].day31);
				
				$scope.totaltask=$scope.totaltask+parseFloat(data[i].day1)+parseFloat(data[i].day2)+
				parseFloat(data[i].day3)+parseFloat(data[i].day4)+parseFloat(data[i].day5)+parseFloat(data[i].day6)
				+parseFloat(data[i].day7)+parseFloat(data[i].day8)+parseFloat(data[i].day9)+parseFloat(data[i].day10)
				 +parseFloat(data[i].day11)+parseFloat(data[i].day12)+parseFloat(data[i].day13)+parseFloat(data[i].day14)+parseFloat(data[i].day15)
				+parseFloat(data[i].day16)+parseFloat(data[i].day17)+parseFloat(data[i].day18)+parseFloat(data[i].day19)+parseFloat(data[i].day20)
				+parseFloat(data[i].day21)+parseFloat(data[i].day22)+parseFloat(data[i].day23)+parseFloat(data[i].day24)+parseFloat(data[i].day25)	
				+parseFloat(data[i].day26)+parseFloat(data[i].day27)+parseFloat(data[i].day28)+parseFloat(data[i].day29)+parseFloat(data[i].day30)
				+parseFloat(data[i].day31);
			
				$scope.sheet = {
					 sheetId:data[i].sheetId,	            			
					 taskName:data[i].taskName,
					day1: data[i].day1,
					day2:data[i].day2,
					day3:data[i].day3,
					day4:data[i].day4,
					day5:data[i].day5,
					day6:data[i].day6,
					day7:data[i].day7,
					day8:data[i].day8,
					day9:data[i].day9,
					day10: data[i].day10,
					day11: data[i].day11,
					day12: data[i].day12,
					day13: data[i].day13,
					day14: data[i].day14,
					day15: data[i].day15,
					day16: data[i].day16,
					day17: data[i].day17,
					day18: data[i].day18,
					day19: data[i].day19,
					day20: data[i].day20,
					day21: data[i].day21,
					day22: data[i].day22,
					day23: data[i].day23,
					day24: data[i].day24,
					day25: data[i].day25,
					day26: data[i].day26,
					day27: data[i].day27,
					day28: data[i].day28,
					day29: data[i].day29,
					day30: data[i].day30,
					day31: data[i].day31,
					TimeSheetMonth:new Date(data[i].TimeSheetMonth),
					 selectMonth:data[i].selectMonth,
					 selectYear:data[i].selectYear,
						resourceComments:data[i].resourceComments,
						project:data[i].project,
						client:data[i].client,
						clientManager:data[i].clientManager,
						status:data[i].status,
						total:$scope.singleRowTotal,
						totaltask:$scope.totaltask
						
   			 };
				console.log(" insert sheet "+$scope.sheet);
			
				 $scope.SheetTaskRepeatAdimn.push($scope.sheet);
				 

				 
	 
					
			 }
			
			$scope.statusAdminApprove="";
			for(var i in data){
				$scope.statusAdminApprove=data[i].status;
			}
			
			if($scope.statusAdminApprove == "Accepted"){
				$scope.DisableAccept=true;
				$scope.DisableReject=true;
			}else if($scope.statusAdminApprove == "Rejected"){
				$scope.DisableAccept=true;
				$scope.DisableReject=true;
			}else{
				$scope.DisableAccept=false;
				$scope.DisableReject=false;
			}
			
			}	
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
session.getTimeSheetEmpName = function(data, $scope) {
		
		
		var log = data;
		console.log(log);
		
		var response;
		
		 response = $http.post('getTimeSheetEmpName', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		
				
		response.success(function(data, status, headers, config) {
			console.log(data);
			$scope.consultantName=data;
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	session.PostTimeSheetStatus= function(data, $scope) {
		
		
		var log = data;
		console.log(log);
		
		var response;
		
		 response = $http.post('postTimeSheetStatus', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		
				
		response.success(function(data, status, headers, config) {
			console.log(data);
			$scope.statusAdminApprove="";
			for(var i in data){
				$scope.statusAdminApprove=data[i].status;
			}
			
			if($scope.statusAdminApprove == "Accepted"){
				$scope.DisableAccept=true;
				$scope.DisableReject=true;
			}else if($scope.statusAdminApprove == "Rejected"){
				$scope.DisableAccept=true;
				$scope.DisableReject=true;
			}else{
				$scope.DisableAccept=false;
				$scope.DisableReject=false;
			}
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	return session;
});
