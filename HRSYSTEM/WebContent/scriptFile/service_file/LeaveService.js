'use strict';

app.factory('leaveService', function($http,$location,$window,$rootScope) {
	var session = {};
	
		session.getListById = function(empId, $scope) {
		
		
		var log = angular.fromJson(empId);
		console.log(log);
//		alert("inside leave service");
		var response;
		
  		
		 response = $http.post('getListById', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
 				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$rootScope.leave = {};
			
 			$scope.total = function(data) {
				 
				
				 $scope.amt1=0;
				 $scope.Rjt1 = 0;
				 angular.forEach(data, function (value, key) {
				    
					 if(data[key].status =='Approved'){
						 
					 var amt2 = data[key].duration;
					 $scope.amt1=parseInt($scope.amt1)+parseInt(amt2);	
					 
					 }else if(data[key].status != 'Cancelled'){
				
						 
						 var Rjt2 = data[key].duration;
						 $scope.Rjt1 = parseInt($scope.Rjt1)+parseInt(Rjt2);	
						 
					 } 	
		              
		              
		          });
					}
			 $scope.total(data);
			
 			
 			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
			
 
 	
	session.submit = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside leave service");
		var response;
		
  		
		 response = $http.post('applyLeave', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
 				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$rootScope.leave = {};
			
			// this to hide and show tables
			
			$scope.table1 = false;	 
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
			
	session.submitReschedule = function(data, $scope) {
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside reschedule leave service");
		var response;
		
  		
		response = $http.post('rescheduleLeave', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
 				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$rootScope.leave = {};
			
			// this to hide and show tables
			
			$scope.table1 = false;
			$rootScope.form2 = false;
			$rootScope.RescheduleForm = true;
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
			
	
	
	
	session.GetTotal = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside leave service");
		var response;
		
  		
		 response = $http.post('getTotal', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
 				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$rootScope.leave = {};
			
			// this to hide and show tables
			
			$scope.table1 = false;	 
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
			
	

	
	
	session.viewLeave = function(leaveId,$scope){

		var log = angular.fromJson(leaveId);
		console.log(log);
		
		var response;
		response=$http.post('viewLeave',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactViewUser = data;

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
		
	};

	session.viewReschedule = function(leaveId,$scope){

		var log = angular.fromJson(leaveId);
		console.log(log);
		
		var response;
		response=$http.post('viewLeave',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactViewUser = data;
 			
 			var i;
 			for(i in $scope.contactViewUser){
 				if($scope.contactViewUser[i].leaveId == leaveId){
 					
 					$rootScope.a = $scope.contactViewUser[i].leaveId;
  					$rootScope.b = $scope.contactViewUser[i].leaveType;
  					$rootScope.c = $scope.contactViewUser[i].leaveFrom;
  					$rootScope.d = $scope.contactViewUser[i].leaveTo;
  					$rootScope.e = $scope.contactViewUser[i].duration;
  					$rootScope.f = $scope.contactViewUser[i].description;
  					$rootScope.g = $scope.contactViewUser[i].status;
  					
  					$rootScope.c = new Date($rootScope.c);
  					$rootScope.d = new Date($rootScope.d);
  					$rootScope.leave={
  							leaveId:$rootScope.a,
 							leaveType:$rootScope.b,
 							leaveFrom:$rootScope.c,
 							leaveTo:$rootScope.d,
 							duration:parseInt($rootScope.e),
 							description:$rootScope.f
 							 
 							
 					};
 				}
 				
 			}
			

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
		
	};

	
	session.approve = function(data,$scope){

		var log = angular.fromJson(data);
		console.log(log);
		
		var response;
		response=$http.post('approveLeave',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactsAdmin = data;
 		
 			$rootScope.viewleavebox = true;
 			$rootScope.hideselect = false;
 			
			$scope.applyBtn = false;
			
			$rootScope.form2 = false;
 			$rootScope.tableAd = false;

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};

	session.reject = function(data,$scope){

		var log = angular.fromJson(data);
		console.log(log);
		
		var response;
		response=$http.post('rejectLeave',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactsAdmin = data;
			
 			$rootScope.viewleavebox = true;
 			$rootScope.hideselect = false;
 			
			$scope.applyBtn = false;
			
			$rootScope.form2 = false;
 			$rootScope.tableAd = false;

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});		
	};
	
	 
	session.CancelLeave = function(data,$scope){
		 
		var log = angular.fromJson(data);
		console.log(log);
		
		var response;
		response=$http.post('cancel',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contacts = data;
			

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});		
	}
	 	 

	
	session.filterEmployeeId = function(employeeId,$scope){
//		alert("filterEmpId");
		var log = angular.fromJson(employeeId);
		console.log(log);
		
		var response;
		response=$http.post('filterEmpId',log, {
			headers : {
				"Content-Type" : "application/json" 
					}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);
//			alert("filterEmpId success");
 			$scope.contactsAdmin = data;
			

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

	};
	

	
	session.filterAllEmployeeStatus = function(status,$scope){

		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('filterAllEmployeeStatus',log, {
			headers : {
				"Content-Type" : "application/json" 
					}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactsAdmin = data;
			

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

	};
	

	
		session.filterEmployeeStatus = function(filterstatus,$scope){

			var log = angular.fromJson(filterstatus);
			console.log(log);
			
			var response;
			response=$http.post('filterStatus',log, {
				headers : {
					"Content-Type" : "application/json"
	 
				}
			});
			
			response.success(function(data, status, headers, config) {
				console.log(data);

	 			$scope.contactsAdmin = data;
	 			

	 			$scope.total = function(data) {
					 
					
					 $scope.amt1=0; // approved count
					 $scope.Rjt1 = 0; // total count
					 angular.forEach(data, function (value, key) {
					    
						 if(data[key].status =='Approved' || data[key].status =='Consumed') {
							 
						 var amt2 = data[key].duration;
						 $scope.amt1=parseInt($scope.amt1)+parseInt(amt2);	
						 
						 }
						 if(data[key].status != 'Cancelled'){
					
							 
							 var Rjt2 = data[key].duration;
							 $scope.Rjt1 = parseInt($scope.Rjt1)+parseInt(Rjt2);	
							 
						 } 	
			              
			              
			          });
				};
				 $scope.total(data);
				
	 		
				

			});
			response.error(function(data, status, headers, config) {
				alert("Error in connection");
			});

		};
	
	
	
	
	
	return session;
});
