'use strict';

app.factory('PerformanceReviewService', function($http, $location, $window) {
	var session = {};
	
	
session.submitTask = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
		//alert("inside serv service"+data);
		var response;
		
		if($scope.assignTask.taskId == null){
 		
		 response = $http.post('addTask', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 		
			 response = $http.post('addTask', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsTask = data;
			$scope.contactsTaskListAllEmp = data;
	
			$scope.assignTask={};
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};	
	
	
session.submitPerformance = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
		//alert("inside serv service"+data);
		var response;
		
		if($scope.performance.performanceId == null){
 		
		 response = $http.post('addperformanceEmp', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 		
			 response = $http.post('addperformanceEmp', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsPerformance = data;
			$scope.ReviewByEmpId= data;
			$scope.performance={};
			$scope.performanceReport=false;
			$scope.hidePerformancebutton=false;
			$scope.hideperformanceReview=true;
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};	
	
	
session.taskList = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		
				 response = $http.post('taskList', log, {
					headers : {
				"Content-Type" : "application/json"

				}
				});
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactstaskList = data;
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
session.performanceActivity = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		if($scope.performance1.activityId == null){
	 		
			 response = $http.post('taskActivity', log, {
				headers : {
					"Content-Type" : "application/json"
//					"userid" : "sarath"
				}
			});
			
			}else{
				
	 		
				 response = $http.post('taskActivity', log, {
						headers : {
					"Content-Type" : "application/json"
//							"userid" : "sarath"
					}
					});
				
			}
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsActivityList = data;
			$scope.contactsTaskActivityList = data;
			$scope.hideActivityform=true;
			$scope.performance1={};
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};

	
session.taskActivityList = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('taskActivityList', log, {
				headers : {
					"Content-Type" : "application/json"
//					"userid" : "sarath"
				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsTaskActivityList = data;
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};

	
session.viewActivity = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('viewActivity', log, {
				headers : {
					"Content-Type" : "application/json"
//					"userid" : "sarath"
				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsviewActivity = data;
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
session.ReviewTask = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('ReviewTask', log, {
				headers : {
					"Content-Type" : "application/json"
//					"userid" : "sarath"
				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsReviewTask = data;
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
session.ReviewTask1 = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('ReviewTask1', log, {
				headers : {
					"Content-Type" : "application/json"
//					"userid" : "sarath"
				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsReviewTask1 = data;
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
session.submitweight = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('submitweight', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsTaskListAllEmp = data;
			$scope.viewReviewtable=true;
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
session.getTaskByEmpId = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('getTaskByEmpId', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.ReportTaskByEmpId = data;
//			$scope.performanceReport=false;
			$scope.performanceReportTable=false;
			
				
			 $scope.total = function(data) {
				 $scope.twAssigned=0;
				 $scope.twAchieved=0;
				 angular.forEach(data, function (value, key) {
					 var weight2=0;
					 var weight3=0;
					 
					  weight2 = data[key].taskWeight;
					  if((weight2 == "") || (weight2 == null)){
					  		
						  weight2=0;
					  	}
					  
					 $scope.twAssigned=parseFloat($scope.twAssigned)+parseFloat(weight2);	
					 weight3 = data[key].weightAchieved;
					  	if((weight3 == "") || (weight3 == null)){
					  		
					  		weight3=0;
					  	}				 
					 $scope.twAchieved=parseFloat($scope.twAchieved)+parseFloat(weight3);	
					 $scope.totalweight=(parseFloat($scope.twAchieved)* 50)/(parseFloat($scope.twAssigned));
					 
		          });
				 
				 
					}
			 
			 
			 $scope.total(data);
			 
			 
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
session.getReviewByEmpId = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('getReviewByEmpId', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.ReviewByEmpId = data;
			$scope.performanceReport=false;
			$scope.performanceReportTable=false;
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	

	
session.DeleteReviewByPerformanceId = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('DeleteReviewById', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.ReviewByEmpId = data;
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
session.getEmpPayroll = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('getEmpPayroll', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.empPayroll = data;
			if(data == "N2 Service"){
				$scope.hidePerformanceReportN2=false;
				$scope.hidePerformanceReportPatton=true;
				$scope.hidePerformanceReportN2Emp=false;
				$scope.hidePerformanceReportPattonEmp=true;
			}else{
				$scope.hidePerformanceReportPatton=false;
				$scope.hidePerformanceReportN2=true;
				$scope.hidePerformanceReportN2Emp=true;
				$scope.hidePerformanceReportPattonEmp=false;
			}
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
session.performanceReviewViewEmp = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('GetReviewViewEmp', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.PerformanceReviewViewEmp = data;
				
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
		
session.saveSignEmp = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('saveSignEmp', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.PerformanceReviewViewEmp = data;
			$scope.hidePerformanceReportN2Emp=true;
			$scope.hidePerformanceReportPattonEmp=true;
			 $scope.clearEmp();
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
session.saveSignAdmin = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('saveSignAdmin', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
			$scope.ReviewByPerformanceId = data;
			
			
			
			 $scope.total = function(data) {
				 $scope.twAssigned="";
				 $scope.jobskillReport ="";
				 $scope.newskillReport ="";
				 $scope.resourseUsesReport ="";
				 $scope.assignResponsibilitiesReport ="";
				 $scope.meetAttendanceReport ="";
				 $scope.listenToDirectionReport ="";
				 $scope.responsibilityReport ="";
				 $scope.commitmentsReport ="";
				 $scope.problemSolvingReport ="";
				 $scope.suggestionImprovementReport ="";
				 $scope.ideaAndSolutionReport ="";
				 $scope.meetChallengesReport ="";
				 $scope.innovativeThinkingReport ="";
				 $scope.employeeIdReport ="";
				 $scope.reviewedByReport ="";
				 $scope.reviewedDateReport ="";
				 $scope.reviewPeriodReport ="";
				 $scope.commentsReport ="";
				 $scope.totalWeightReport ="";
				 $scope.employeeNameReport ="";
				 $scope.performanceIdReport ="";
				 	$scope.ReviewAdmin=null;
					$scope.Reviewemp=null;
					$scope.PerformanceEmpSignature=null;
					$scope.PerformanceEmpSignature1=null;
					
					$scope.reviewQuarter="";
					 $scope.reviewYear="";
					
				 angular.forEach(data, function (value, key) {
					 
					 var jobskill="";
					 var newskill="";
					 var resourseUses="";
					 var assignResponsibilities="";
					 var meetAttendance="";
					 var listenToDirection="";
					 var responsibility="";
					 var commitments="";
					 var problemSolving="";
					 var suggestionImprovement="";
					 var ideaAndSolution="";
					 var meetChallenges;
					 var innovativeThinking="";
					 var employeeId="";
					 var reviewedBy="";
					 var reviewedDate="";
					 var reviewPeriod="";
					 var reviewPeriodTo="";
					 var comments="";
					 var totalWeight="";
					 var employeeName="";
					 var performanceIdRp="";
					 
					 var empsign1=null;
					 var adminSign1=null;
					 var year1="";
					 var reviewQuarter1="";
					 
					 performanceIdRp = data[key].performanceId;
					 jobskill = data[key].jobskill;
					 newskill = data[key].newskill;
					 resourseUses = data[key].resourseUses;
					 assignResponsibilities = data[key].assignResponsibilities;
					 meetAttendance = data[key].meetAttendance;
					 listenToDirection = data[key].listenToDirection;
					 responsibility = data[key].responsibility; 
					 commitments = data[key].commitments;
					 problemSolving = data[key].problemSolving;
					 suggestionImprovement = data[key].suggestionImprovement;
					 ideaAndSolution = data[key].ideaAndSolution;
					 meetChallenges = data[key].meetChallenges;
					 innovativeThinking = data[key].innovativeThinking;
					 employeeId = data[key].employeeId;
					 reviewedBy = data[key].reviewedBy; 
					 reviewedDate = data[key].reviewedDate;
					 reviewPeriod = data[key].reviewPeriod;
					 reviewPeriodTo = data[key].reviewPeriodTo;
					 comments = data[key].comments;
					 totalWeight = data[key].totalWeight; 
					 employeeName = data[key].employeeName;
					 empsign1 = data[key].empSign;
					 adminSign1 = data[key].adminSign;
					 
					 reviewQuarter1 = data[key].quarter;
					 year1 = data[key].year;
					 
					 $scope.ReviewAdmin=empsign1;
					 $scope.Reviewemp=adminSign1;
					 $scope.PerformanceEmpSignature=empsign1;
					 $scope.PerformanceEmpSignature1=empsign1;
					 
					 $scope.performanceIdReport =performanceIdRp;
					 $scope.jobskillReport =jobskill;
					 $scope.newskillReport =newskill;
					 $scope.resourseUsesReport =resourseUses;
					 $scope.assignResponsibilitiesReport =assignResponsibilities;
					 $scope.meetAttendanceReport =meetAttendance;
					 $scope.listenToDirectionReport =listenToDirection;
					 $scope.responsibilityReport =responsibility;
					 $scope.commitmentsReport =commitments;
					 $scope.problemSolvingReport =problemSolving;
					 $scope.suggestionImprovementReport =suggestionImprovement;
					 $scope.ideaAndSolutionReport =ideaAndSolution;
					 $scope.meetChallengesReport =meetChallenges;
					 $scope.innovativeThinkingReport =innovativeThinking;
					 $scope.employeeIdReport =employeeId;
					 $scope.reviewedByReport =reviewedBy;
					 $scope.reviewedDateReport =reviewedDate;
					 $scope.reviewPeriodReport =reviewPeriod;
					 $scope.reviewPeriodToReport =reviewPeriodTo;
					 $scope.commentsReport =comments;
					 $scope.totalWeightReport =totalWeight;
					 $scope.employeeNameReport =employeeName;
					 $scope.reviewQuarter=reviewQuarter1;
					 $scope.reviewYear=year1;
					 
		          });
				 
				 
					};
			 
			 
			 $scope.total(data);
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
session.getReviewByPerformanceId = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
	
		var response;
		

			 response = $http.post('getReviewByPerformanceId', log, {
				headers : {
					"Content-Type" : "application/json"

				}
			});
			
			
			
		response.success(function(data, status, headers, config) {
			console.log(data);
			
			
				$scope.ReviewByPerformanceId = data;
			 
			
			
			
			
			 $scope.total = function(data) {

				 
				 $scope.twAssigned="";
				 $scope.jobskillReport ="";
				 $scope.newskillReport ="";
				 $scope.resourseUsesReport ="";
				 $scope.assignResponsibilitiesReport ="";
				 $scope.meetAttendanceReport ="";
				 $scope.listenToDirectionReport ="";
				 $scope.responsibilityReport ="";
				 $scope.commitmentsReport ="";
				 $scope.problemSolvingReport ="";
				 $scope.suggestionImprovementReport ="";
				 $scope.ideaAndSolutionReport ="";
				 $scope.meetChallengesReport ="";
				 $scope.innovativeThinkingReport ="";
				 $scope.employeeIdReport ="";
				 $scope.reviewedByReport ="";
				 $scope.reviewedDateReport ="";
				 $scope.reviewPeriodReport ="";
				 $scope.commentsReport ="";
				 $scope.totalWeightReport ="";
				 $scope.employeeNameReport ="";
				 $scope.performanceIdReport ="";
				 
				 $scope.reviewQuarter="";
				 $scope.reviewYear="";
				 
					$scope.ReviewAdmin=null;
					$scope.Reviewemp=null;
//					 $scope.PerformanceEmpSignature=null;
//					 $scope.PerformanceEmpSignature1=null;
				 angular.forEach(data, function (value, key) {
					 
					 var jobskill="";
					 var newskill="";
					 var resourseUses="";
					 var assignResponsibilities="";
					 var meetAttendance="";
					 var listenToDirection="";
					 var responsibility="";
					 var commitments="";
					 var problemSolving="";
					 var suggestionImprovement="";
					 var ideaAndSolution="";
					 var meetChallenges;
					 var innovativeThinking="";
					 var employeeId="";
					 var reviewedBy="";
					 var reviewedDate="";
					 var reviewPeriod="";
					 var reviewPeriodTo="";
					 var comments="";
					 var totalWeight="";
					 var employeeName="";
					 var performanceIdRp="";
					 var reviewQuarter1="";
					 var empsign1;
					 var adminSign1;
					 var year1="";
					 performanceIdRp = data[key].performanceId;
					 jobskill = data[key].jobskill;
					 newskill = data[key].newskill;
					 resourseUses = data[key].resourseUses;
					 assignResponsibilities = data[key].assignResponsibilities;
					 meetAttendance = data[key].meetAttendance;
					 listenToDirection = data[key].listenToDirection;
					 responsibility = data[key].responsibility; 
					 commitments = data[key].commitments;
					 problemSolving = data[key].problemSolving;
					 suggestionImprovement = data[key].suggestionImprovement;
					 ideaAndSolution = data[key].ideaAndSolution;
					 meetChallenges = data[key].meetChallenges;
					 innovativeThinking = data[key].innovativeThinking;
					 employeeId = data[key].employeeId;
					 reviewedBy = data[key].reviewedBy; 
					 reviewedDate = data[key].reviewedDate;
					 reviewPeriod = data[key].reviewPeriod;
					 reviewPeriodTo = data[key].reviewPeriodTo;
					 comments = data[key].comments;
					 totalWeight = data[key].totalWeight; 
					 
					 employeeName = data[key].employeeName;
					 empsign1 = data[key].empSign;
					 adminSign1 = data[key].adminSign;
					 reviewQuarter1 = data[key].quarter;
					 year1 = data[key].year;
					 
					 $scope.performanceIdReport =performanceIdRp;
					 $scope.jobskillReport =jobskill;
					 $scope.newskillReport =newskill;
					 $scope.resourseUsesReport =resourseUses;
					 $scope.assignResponsibilitiesReport =assignResponsibilities;
					 $scope.meetAttendanceReport =meetAttendance;
					 $scope.listenToDirectionReport =listenToDirection;
					 $scope.responsibilityReport =responsibility;
					 $scope.commitmentsReport =commitments;
					 $scope.problemSolvingReport =problemSolving;
					 $scope.suggestionImprovementReport =suggestionImprovement;
					 $scope.ideaAndSolutionReport =ideaAndSolution;
					 $scope.meetChallengesReport =meetChallenges;
					 $scope.innovativeThinkingReport =innovativeThinking;
					 $scope.employeeIdReport =employeeId;
					 $scope.reviewedByReport =reviewedBy;
					 $scope.reviewedDateReport =reviewedDate;
					 $scope.reviewPeriodReport =reviewPeriod;
					 $scope.reviewPeriodToReport =reviewPeriodTo;
					 $scope.commentsReport =comments;
					 $scope.totalWeightReport =totalWeight;
					 $scope.employeeNameReport =employeeName;
					 $scope.ReviewAdmin=empsign1;
					 $scope.Reviewemp=adminSign1;
					 $scope.PerformanceEmpSignature=empsign1;
					 $scope.PerformanceEmpSignature1=empsign1;
					 $scope.reviewQuarter=reviewQuarter1;
					 $scope.reviewYear=year1;
//					 alert("sign"+$scope.PerformanceEmpSignature);
//					 alert("sign1"+$scope.PerformanceEmpSignature1);
		          });
				 
				 
					}
			 
			 
			 $scope.total(data);
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
return session;
});