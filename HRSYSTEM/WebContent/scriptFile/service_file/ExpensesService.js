'use strict';

app.factory('ExpensesService', function($http, $location, $window) {
	var session = {};
	session.submit = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside exp service");
		var response;
		
		if($scope.expense.expenseId == null){
 		
		 response = $http.post('addexpense', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 			
		//	console.log(log);
			 response = $http.post('addexpense', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$scope.expense = {};
			
			var doj = sessionStorage.getItem("jod");
			$scope.expense = {
								reportingdate:doj=new Date(doj),
								submissiondate:new Date()
					};
			
			 $scope.total = function(data) {
				 $scope.amtemp=0;
				 angular.forEach(data, function (value, key) {
					 var amt2=0;	
					  amt2 = data[key].expenseAmt;
					  var activity="";
					  activity=data[key].expenseStatus;
					  if(activity == "Accepted"){
					 $scope.amtemp=parseFloat($scope.amtemp)+parseFloat(amt2);	
		              	
					  }
					  else{
						  activity=""; 
					  }
		              
		          });
					}
			 $scope.total(data);
			 	$scope.viewEmp=true;
				$scope.hideexpenseform=true;
				$scope.hideexpensetable=false;
				$scope.hideback1=true;
				$scope.hidestatus=false;
				$scope.hidestatus=false;
				alert("Expense gets submitted Successfully");
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};	
	
	
	
	session.submitview = function(data, $scope) {
			
			
			var log = angular.fromJson(data);
			console.log(log);
//			alert("inside exp service"+data);
			var response;
			
			
				 response = $http.post('submitview', log, {
						headers : {
					"Content-Type" : "application/json"
//							"userid" : "sarath"
					}
					});
				
			
					
			response.success(function(data, status, headers, config) {
				console.log(data);
			
				$scope.contactsAdEmp = data;
				$scope.hideviewexpense=true;
				 $scope.total = function(data) {
					 $scope.amt1=0;
					 angular.forEach(data, function (value, key) {
						 var amt2=0;
						 amt2 = data[key].expenseAmt;
						 $scope.amt1=parseFloat($scope.amt1)+parseFloat(amt2);	
			              	
			              
			              
			          });
						}
				 $scope.total(data);
				 $scope.hidechooseEmployee=false;
				
			});
			response.error(function(data, status, headers, config) {
				alert("Error in connection");
			});
		};
		
		

session.go = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside exp service"+data);
		var response;
		
		if($scope.expense.expenseId == null){
 		
		 response = $http.post('filterstatus', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 			
		//	console.log(log);
			 response = $http.post('filterstatus', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$scope.expense = {};
			
			var doj = sessionStorage.getItem("jod");
			$scope.expense = {
								reportingdate:doj=new Date(doj),
					};
			 $scope.total = function(data) {
				 $scope.amtemp=0;
				 angular.forEach(data, function (value, key) {
					 var amt2=0;	
					  amt2 = data[key].expenseAmt;
					 $scope.amtemp=parseFloat($scope.amtemp)+parseFloat(amt2);	
		              	
		              
		              
		          });
					}
			 $scope.total(data);
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};	
	
session.go1 = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside exp service"+data);
		var response;
		
		
			 response = $http.post('filterstatus', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsAdEmp = data;
			 $scope.total = function(data) {
				 $scope.amt1=0;
				 angular.forEach(data, function (value, key) {
					 var amt2=0;
					  amt2 = data[key].expenseAmt;
					 $scope.amt1=parseFloat($scope.amt1)+parseFloat(amt2);	
		              	
		              
		              
		          });
					}
			 $scope.total(data);
			 $scope.SingleIdhide=false;
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};	
	
	
session.go2 = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside exp service"+data);
		var response;
		
		
			 response = $http.post('totalExpensefilter', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsAdEmp = data;
			 $scope.total = function(data) {
				 $scope.amt1=0;
				 angular.forEach(data, function (value, key) {
					 var amt2=0;
					  amt2 = data[key].expenseAmt;
					 $scope.amt1=parseFloat($scope.amt1)+parseFloat(amt2);	
		              	
		              
		              
		          });
					}
			 $scope.total(data);
			 $scope.SingleIdhide=false;
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	session.View= function(expenseId,$scope){
		var log = angular.fromJson(expenseId);
		console.log(log);
//		alert("emp id"+expenseId);
		var response;
		response=$http.post('expenseIdView',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactsAdView = data;
 			$scope.contactsEmpView = data;			 
			 
			 
			 
			 
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	session.getNamebyId= function(expenseId,$scope){
		var log = angular.fromJson(expenseId);
		console.log(log);

		var response;
		response=$http.post('getName',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.name = data;
 						 
			 
			 
			 
			 
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	session.filterexpenseempAd= function(employeeId,$scope){
		var log = angular.fromJson(employeeId);
		console.log(log);
//		alert("emp id"+employeeId);
		var response;
		response=$http.post('expenselistId',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactsAdEmp = data;
 			$scope.hideviewexpense=true;
 			$scope.hidefilterAdmin=true;
 			 $scope.total = function(data) {
				 $scope.amt1=0;
				 angular.forEach(data, function (value, key) {
				      	
					 var amt2 = data[key].expenseAmt;
					 $scope.amt1=parseFloat($scope.amt1)+parseFloat(amt2);	
		              	
		              
		              
		          });
					}
			 $scope.total(data); 
			 
			 
			 
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	session.filterexpenseempAdStatus= function(filterstatus,$scope){
		var log = angular.fromJson(filterstatus);
		console.log(log);
//		alert("emp id"+filterstatus);
		var response;
		response=$http.post('filterstatusAdmin',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactsAdEmp = data;
 			$scope.hideviewexpense=true;
 			$scope.hidefilterAdmin=true;
 			 $scope.total = function(data) {
				 $scope.amt1=0;
				 angular.forEach(data, function (value, key) {
				      	
					 var amt2 = data[key].expenseAmt;
					 $scope.amt1=parseFloat($scope.amt1)+parseFloat(amt2);	
		              	
		              
		              
		          });
					}
			 $scope.total(data); 
			 
			 
			 
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
	session.DeleteExpense= function(expenseId,$scope){
		var log = angular.fromJson(expenseId);
		console.log(log);

		var response;
		response=$http.post('deleteExpense',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {

			console.log(data);
		
			$scope.contacts = data;
			$scope.expense = {};
			
			var doj = sessionStorage.getItem("jod");
			$scope.expense = {
					reportingdate:doj=new Date(doj),
					submissiondate:new Date()
					};
			
			 $scope.total = function(data) {
				 $scope.amtemp=0;
				 angular.forEach(data, function (value, key) {
					 var amt2=0;	
					  amt2 = data[key].expenseAmt;
					  var activity="";
					  activity=data[key].expenseStatus;
					  if(activity == "Accepted"){
					 $scope.amtemp=parseFloat($scope.amtemp)+parseFloat(amt2);	
		              	
					  }
					  else{
						  activity=""; 
					  }
		              
		          });
					}
			 $scope.total(data);
			 	$scope.viewEmp=true;
				$scope.hideexpenseform=true;
				$scope.hideexpensetable=false;
				$scope.hideback1=true;
				$scope.hidestatus=false;
		
 			
 						 
			 
			 
			 
			 
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
	
	
	
	
	
	
	
	session.expenseget = function(employeeId,$scope){



		var log = angular.fromJson(employeeId);
		//console.log(log);
		
		var response;
		response=$http.post('expenselist',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contacts = data;
 			$scope.expense = {};     			
 			var doj = sessionStorage.getItem("jod");
 			$scope.expense = {
 								reportingdate:doj=new Date(doj),
 								submissiondate:new Date()
 								
 					};
 			
			 $scope.totalItems = data.length; 
			 
			 $scope.total = function(data) {
				 $scope.amtemp=0;
				 angular.forEach(data, function (value, key) {
					 var amt2=0;	
					  amt2 = data[key].expenseAmt;
					  var activity="";
					  activity=data[key].expenseStatus;
					  if(activity == "Accepted"){
					 $scope.amtemp=parseFloat($scope.amtemp)+parseFloat(amt2);	
		              	
					  }
					  else{
						  activity=""; 
					  }
		              
		          });
					}
			 $scope.total(data);
			 
			 
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};

	return session;
});