app.factory('ProfileService', function($http, $location, $window) {
	var session = {};
	

	
	
	session.viewEmployee = function(employeeId,$scope){



		var log = angular.fromJson(employeeId);
		console.log(log);
		
		var response;
		response=$http.post('employeeView',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.profileView = data;
 			
 			angular.forEach(data, function (value, key) {
				 
				
				 $scope.pempId= data[key].employeeId;
				 $scope.pfirstname= data[key].firstName;
				 $scope.plastName= data[key].lastName;
				 $scope.pdateOfBirth= data[key].dateOfBirth;
				 $scope.pphoneNumberPrimary= data[key].phoneNumberPrimary;
				 $scope.pphoneNumberSecondary= data[key].phoneNumberSecondary;
				 $scope.pemailPrimary= data[key].emailPrimary;
				 $scope.pemailSecondary= data[key].emailSecondary;
				 $scope.plocation= data[key].location;
				 $scope.pdateOfJoined= data[key].dateOfJoined;
				 $scope.pclientName= data[key].clientName;
				 $scope.pdesignation= data[key].designation;
				 $scope.prole= data[key].role;
				 $scope.pemployeePassword = data[key].employeePassword;
				 $scope.pname= data[key].name;	 
									 
	          });

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};
	
	session.ImageEmployee = function(employeeId,$scope){



		var log = angular.fromJson(employeeId);
		console.log(log);
		
		var response;
		response=$http.post('imageUpload',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.profileImage = data;
 			
 			
 			

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};
	session.viewImage = function(employeeId,$scope){



		var log = angular.fromJson(employeeId);
		console.log(log);
		
		var response;
		response=$http.post('viewProfileImage',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.profileImage = data;
 			
 			
 			

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};
	
	
	
session.submit = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
		
		var response;
		
			
		 response = $http.post('changepwd', log, {
			headers : {
				"Content-Type" : "application/json"

			}
		});
		
		
		
		
		response.success(function(data, status, headers, config) {
			console.log(data);
			alert(data);
			$scope.profilee = {};
			$scope.hideChangePassword=true;
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
	return session;
	});