'use strict';

app.factory('blogService', function($http,$location,$window,$rootScope) {
	var session = {};
	session.submit = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);

		var response;
		
		if($scope.blog.blogId == null){
 		
		 response = $http.post('addBlog', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 			
			console.log(log);
			 response = $http.post('addBlog', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$scope.blog = {};
			$scope.hidePost=true;
			$scope.viewBlog=false;
			
			 
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
session.submitComment = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);

		var response;
		
		
 		
		 response = $http.post('addPostComment', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsPost = data;
			
			
			 
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
	session.viewImage = function(employeeId,$scope){



		var log = angular.fromJson(employeeId);
		console.log(log);
		
		var response;
		response=$http.post('viewProfileImage',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.profileImage = data;
 			
 			
 			

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};
	
	
	session.DeletePost = function(postId,$scope){



		var log = angular.fromJson(postId);
		console.log(log);
		
		var response;
		response=$http.post('postDelete',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contacts = data;
 			$scope.hideDeletePost=true;
			$scope.hideSelectPost=true;
			$scope.hideAddPost=false;

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};
	return session;
});

