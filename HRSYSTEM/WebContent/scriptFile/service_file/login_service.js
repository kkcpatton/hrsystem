'use strict';

app1.factory('loginService', function($http, $location, $window, $rootScope) {
	var session = {};
	
	

	//session.login = function(data, $scope) {
		session.login = function(data, $scope) {
		var log = angular.fromJson(data);
		console.log(log);
		if($rootScope.valid){
		var response = $http.post('login', log, {
			headers : {
				"Content-Type" : "application/json"
	
			}
		});
		}
		else{
			 alert("Invalid Answer");
			 $window.location.href = 'login.jsp';
		}
		response.success(function(data, status, headers, config) {
			
			console.log(data);		
 
			if (data) {				
				try {
					  
		            var a= true;
		            sessionStorage.setItem("key1", JSON.stringify(a));
		           
		           
				}catch (e) {
		            alert("Error in Connection");
		            $window.location.href = 'login.jsp';
				}
				
				var sess=sessionStorage.getItem("key1");
				
				
				if(sess=1){		
					$scope.details= data; 
					var i;
			        for(i in $scope.details) {
			        	$rootScope.currentUser = $scope.details[i].role;
			           	var firstname=$scope.details[i].firstName;
			           	sessionStorage.setItem("firstname", firstname);
			           	sessionStorage.setItem("name", firstname);
			           	var name=$scope.details[i].name;
			           var empId =$scope.details[i].employeeId;
			           var empRole =$scope.details[i].role;
			           var doj=$scope.details[i].dateOfBirth;
			           var payroll=$scope.details[i].payroll;
			           var mymail=$scope.details[i].emailPrimary;
			           sessionStorage.setItem("payroll", payroll);
			           sessionStorage.setItem("mymail", mymail);
			           sessionStorage.setItem("idemp", empId);
			           sessionStorage.setItem("jod", $scope.details[i].dateOfJoined);
			           sessionStorage.setItem("EmpRole", empRole);
			        };
			        
			        if($rootScope.currentUser  == "ADMIN") {
			       		
		        		$window.location.href = 'main.jsp';
 					
		        	}else{
		      			$window.location.href = 'main1.jsp';
		        	}
		        	
				
				}
				else
				{  
					$window.location.href = 'login.jsp';
					}
				
					}
				else  {				
							$window.location.href = 'login.jsp';
							alert("Login Faild, Plase check User Name or Password");
						}


		});

		response.error(function(data, status, headers, config) {
			alert("Error in connection");
			$window.location.href = 'login.jsp';
		});
	};

session.forgotPassword = function(email) {
		
		var response = $http.post('forgot', email, {
			headers : {
				"Content-Type" : "application/json"
			}
		
		});
		response.success(function(data, status, headers, config) {

			if (data) {
				alert(data);
			} 

		});

		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
	
	
	
	session.logout = function() {
		localStorage.removeItem("session");
	};
	session.isloggedIn = function() {
		return localStorage.getItem("session") != null;
	};
	return session;

});

