'use strict';

app.factory('addemployeeService', function($http,$location,$window,$rootScope) {
	var session = {};
	session.submit = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside addemp service")
		var response;
		
		if($scope.addemployee.employeeId == null){
 		
		 response = $http.post('addemployee', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 			
			console.log(log);
			 response = $http.post('addemployee', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$scope.addemployee = {};
			
			 
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
	session.viewEmployee = function(employeeId,$scope){



		var log = angular.fromJson(employeeId);
		console.log(log);
		
		var response;
		response=$http.post('employeeView',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactView = data;
			

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};
	session.delet = function(employeeId,$scope){



		var log = angular.fromJson(employeeId);
		console.log(log);
		
		var response;
		response=$http.post('employeeDelete',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contacts = data;
			 $scope.addemployee = {};

		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});

		
	};
	
	
	return session;
});
