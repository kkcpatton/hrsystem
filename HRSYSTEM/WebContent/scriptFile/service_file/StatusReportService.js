'use strict';

app.factory('statusreportService', function($http,$location,$window,$rootScope) {
	var session = {};
session.submit = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside addemp service")
		var response;
		
		if($scope.status.statusId == null){
 		
		 response = $http.post('statusreport', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 			
			console.log(log);
			 response = $http.post('statusreport', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$scope.status = {};
			$scope.hideexpenseform=true;
			$scope.hideviewtable=true;
			$scope.hideStatusTable=false;
			$scope.hideStatusTable1=false;
			
			$scope.clear();
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
session.DeleteStatusReport = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside addemp service")
		var response;
			
 		
		 response = $http.post('DeleteStatusReport', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
	
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$scope.status = {};
			$scope.hideexpenseform=true;
			$scope.hideviewtable=true;
			$scope.hideStatusTable=false;
			if(data){
				$scope.hideStatusTable1=false;
			}else{
				$scope.hideStatusTable1=true;
			}		
			
			$scope.ActivityRepeat1=[{},{},{},{}];
			$scope.clear();
			
			
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
session.submitNewStatus1 = function(data, $scope) {
		
		
		var log = angular.fromJson(data);
		console.log(log);
//		alert("inside addemp service")
		var response;
		
		if($scope.status.statusId == null){
 		
		 response = $http.post('StatusReportNew', log, {
			headers : {
				"Content-Type" : "application/json"
//				"userid" : "sarath"
			}
		});
		
		}else{
			
 			
			console.log(log);
			 response = $http.post('StatusReportNew', log, {
					headers : {
				"Content-Type" : "application/json"
//						"userid" : "sarath"
				}
				});
			
		}
				
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			
			$scope.total = function(data) {
				 $scope.StatusMonthRow="";
				 $scope.action_statusRow="";
			
				 angular.forEach(data, function (value, key) {
					 var S_month="";
					 var S_Status="";
					 
					 S_month = data[key].statusMonth;
					 S_Status = data[key].action_status;
					$scope.StatusMonthRow=new Date(S_month);
					$scope.action_statusRow=S_Status; 
					
		          });
				 
				 
					}
			 
			 
			 $scope.total(data);
			 
			$scope.status = {};
			$scope.hideexpenseform=true;
			$scope.hideviewtable=true;
			$scope.hideStatusTable=false;
			$scope.hidestatus=false;
			if(data){
				$scope.StatusReportOn=false;
				$scope.hideStatusTable1=false;
			}else{
				$scope.StatusReportOn=true;
				$scope.hideStatusTable1=true;
			}
			$scope.ActivityRepeat1=[{},{},{},{}];
			$scope.clear();
			
			$scope.selectMonth2="";
			$scope.selectYear2="";
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};	
	
	
	session.statusGet = function(data, $scope) {
		var log = angular.fromJson(data);
		var response;
		response = $http.post('statuslistEmp', log, {
			headers : {
		"Content-Type" : "application/json"
//				"userid" : "sarath"
		}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contacts = data;
			$scope.status = {
        			
					statusMonth:new Date()
					
        	};
		
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	session.viewStatus = function(data, $scope) {
		var log = angular.fromJson(data);
		var response;
		response = $http.post('statusviewAdmin', log, {
			headers : {
		"Content-Type" : "application/json"
//				"userid" : "sarath"
		}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsview = data;
			$scope.total = function(data) {
				 
				 angular.forEach(data, function (value, key) {
					 $scope.empsign="";
					 $scope.empsign = data[key].employeeSignature;
					 
										 
		          });
				 
				 
					}
			 
			 
			 $scope.total(data);
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	
	session.viewStatusReport = function(data, $scope) {
		var log = angular.fromJson(data);
		var response;
		response = $http.post('statusviewAdmin', log, {
			headers : {
		"Content-Type" : "application/json"
//				"userid" : "sarath"
		}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.empsign1=null;
			$scope.empsign2=null;
			$scope.statusMonthReport=null;
			$scope.contactsviewReport = data;
			$scope.total = function(data) {
				 
				 angular.forEach(data, function (value, key) {
					var sig1=null;
					var sig2=null;
					var data1=null;
					var statusMonthReport1=null;
					sig1  = data[key].employeeSignature;
					  $scope.empsign1=sig1;
					 sig2= data[key].adminSignature;
					 $scope.statusMonthReport= new Date(data[key].statusMonth);

					 $scope.empsign2=sig2;				 
		          });
				 
				 
					}
			 
			 
			 $scope.total(data);
			
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	session.viewStatusEmp = function(data, $scope) {
		var log = angular.fromJson(data);
		var response;
		response = $http.post('statusviewAdmin', log, {
			headers : {
		"Content-Type" : "application/json"
//				"userid" : "sarath"
		}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsview = data;
	
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	session.acceptstatus = function(data, $scope) {
		
		var log = angular.fromJson(data);
		var response;
		response = $http.post('statusAdminAccept', log, {
			headers : {
		"Content-Type" : "application/json"
//				"userid" : "sarath"
		}
		});
		
		response.success(function(data, status, headers, config) {
			console.log(data);
		
			$scope.contactsAdmin = data;
			$scope.statusempAd={};
			$scope.filterstatuscode={};
			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	session.filterStatusempId= function(statusEmpId,$scope){
		var log = angular.fromJson(statusEmpId);
		console.log(log);
		
		var response;
		response=$http.post('statusListEmpId',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactsAdmin = data;
 			 
			 
			 
			 
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	session.filterStatus= function(status,$scope){
		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('statusfilter',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.contactsAdmin = data;
 			
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	session.getEmpPayrollStatus= function(status,$scope){
		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('getEmpPayrollStatus',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);

 			$scope.empPayrollStatus = data;
 			if(data == "N2 Service"){
 				$scope.hideReportN2=false;
 				$scope.hideReportPatton=true;
 			}else{
 				$scope.hideReportN2=true;
 				$scope.hideReportPatton=false;
 			}
 			
	  		
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	session.GetMonthlyStatusReport= function(status,$scope){
		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('monthlyStatusReport',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {

//			console.log(data);
//			$scope.hideActivityTable=true; 
//			$scope.hideReportN2=false;
//			$scope.hideReportPatton=false;
//			$scope.empsign1=null;
//			$scope.empsign2=null;
//			$scope.statusMonthReport=null
//			$scope.contactsviewReport = data;
//			$scope.total = function(data) {
//				 
//				 angular.forEach(data, function (value, key) {
//					var sig1=null;
//					var sig2=null;
//					var data1=null;
//					var statusMonthReport1=null;
//					sig1  = data[key].employeeSignature;
//					  $scope.empsign1=sig1;
//					 sig2= data[key].adminSignature;
//					 statusMonthReport1= data[key].statusMonth;
//					  var date = new Date(statusMonthReport1);
//					  var data1 = date.getMonth()+1;
//					  if(data1 == '12'){
//						  $scope.statusMonthReport="December";  
//					  }
//					  if(data1 == '11'){
//						  $scope.statusMonthReport="November";  
//					  }
//					  if(data1 == '10'){
//						  $scope.statusMonthReport="October";  
//					  }
//					  if(data1 == '09'){
//						  $scope.statusMonthReport="September";  
//					  }
//					  if(data1 == '08'){
//						  $scope.statusMonthReport="August";  
//					  }
//					  if(data1 == '07'){
//						  $scope.statusMonthReport="July";  
//					  }
//					  if(data1 == '06'){
//						  $scope.statusMonthReport="June";  
//					  }
//					  if(data1 == '05'){
//						  $scope.statusMonthReport="May";  
//					  }
//					  if(data1 == '04'){
//						  $scope.statusMonthReport="April";  
//					  }
//					  if(data1 == '03'){
//						  $scope.statusMonthReport="March";  
//					  }
//					  if(data1 == '02'){
//						  $scope.statusMonthReport="February";  
//					  }
//					  if(data1 == '01'){
//						  $scope.statusMonthReport="January";  
//					  }
//					 $scope.empsign2=sig2;				 
//		          });
//				 
//				 
//					}
//			 
//			 
//			 $scope.total(data);
//			 $scope.statusMonth={};
		
			
			console.log(data);

 			$scope.contactsviewReport = data;
 			
 			$scope.hideActivityTable=true; 
 			
 			$scope.statusMonthReport=null;
 			
 			
 			$scope.total = function(data) {
				 
				 angular.forEach(data, function (value, key) {
					var sig1=null;
					var sig2=null;
					var statusMonthReport1=null;
					sig1  = data[key].employeeSignature;
					  $scope.empsign1=sig1;
					 sig2= data[key].adminSignature;
					 
					 $scope.statusMonthReport= new Date(data[key].statusMonth);

					 $scope.empsign2=sig2;				 
		          });
				 
				 
					}
			 
			 
			 $scope.total(data);
 			$scope.statusMonth={};
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	session.getStatusReportByMonthEmp= function(status,$scope){
		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('getStatusReportByMonthEmp',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);
			if(data){
				$scope.StatusReportOn=false;
				$scope.hideStatusTable1=false;
			}else{
				$scope.StatusReportOn=true;
				$scope.hideStatusTable1=true;
				$scope.hideviewtableEmp=true;
				alert("There isn't any Record.");
			}
		
			$scope.contacts = data;
			
			
			$scope.total = function(data) {
				 $scope.StatusMonthRow="";
				 $scope.action_statusRow="";
			
				 angular.forEach(data, function (value, key) {
					 var S_month="";
					 var S_Status="";
					 
					 S_month = data[key].statusMonth;
					 S_Status = data[key].action_status;
					$scope.StatusMonthRow=new Date(S_month);
					$scope.action_statusRow=S_Status; 
					
		          });
				 
				 
					};
			 
			 
			 $scope.total(data);
			 
			$scope.status = {
        			
					statusMonth:new Date()
					
        	};
 			
	  		
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	session.statusRecentUpdatesGet= function(status,$scope){
		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('statusRecentUpdatesGet',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);
			$scope.contacts = data;
			
			$scope.total = function(data) {
				 $scope.StatusMonthRow="";
				 $scope.action_statusRow="";
			
				 angular.forEach(data, function (value, key) {
					 var S_month="";
					 var S_Status="";
					 
					 S_month = data[key].statusMonth;
					 S_Status = data[key].action_status;
					$scope.StatusMonthRow=new Date(S_month);
					$scope.action_statusRow=S_Status; 
					
		          });
				 
				 
					}
			 
			 
			 $scope.total(data);
			$scope.status = {
        			
					statusMonth:new Date()
					
        	};
 			
	  		
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	session.getStatusReportByMonthEmpEdit= function(status,$scope){
		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('getStatusReportByMonthEmp',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);
			$scope.contactsEdit = data;
			$scope.ActivityRepeat1 = [];
			
			
			$scope.ActivityEdit={};
			$scope.hideStatusTable=true;
			$scope.hideStatusTable1=true;
			$scope.hideviewtableEmp=true;
			$scope.hideexpenseform=false;
			
			
			
			var i;
	        for(i in $scope.contactsEdit) {
	            
	            	
	        			 $scope.ActivityEdit = {
	        				 statusId:$scope.contactsEdit[i].statusId,		            			
		    					activity:$scope.contactsEdit[i].activity,		    					
		    					startPlanDate:new Date($scope.contactsEdit[i].startPlannedDate),
		    					startActualDate:new Date($scope.contactsEdit[i].startActualDate),
		    					finishPlannedDate:new Date($scope.contactsEdit[i].finishPlannedDate),
		    					finishEstimatedDate:new Date($scope.contactsEdit[i].finishEstimatedDate),
		    					statuscode:$scope.contactsEdit[i].status
	        				 
	        			 };
	        			 
	        			 $scope.ActivityRepeat1.push($scope.ActivityEdit);
	        			 
		            	$scope.status = {
            			
//            			statusId:$scope.contactsEdit[i].statusId,
            			employeeId:$scope.contactsEdit[i].employeeId,
            			statusMonth:new Date($scope.contactsEdit[i].statusMonth),
    					clientDetails:$scope.contactsEdit[i].clientDetails,
    					currentProject:$scope.contactsEdit[i].currentProject,
    					managerName:$scope.contactsEdit[i].managerName,
    					currentModule:$scope.contactsEdit[i].currentModule,
    					managerEmail:$scope.contactsEdit[i].managerEmail,
    					managerPhoneNumber:parseInt($scope.contactsEdit[i].managerPhonenumber),
    					action_status:$scope.contactsEdit[i].action_status		            			
            			
            	};		
	        			
	        
		}
			
			
	        
			
			
			
	  		
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	session.getStatusReportByMonthReviewAdmin= function(status,$scope){
		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('getStatusReportReview',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);
			
			$scope.contactsAdmin = data;
			
			$scope.statusAdmin={};

			
			
	  		
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	session.AcceptStatusAdmin1= function(status,$scope){
		var log = angular.fromJson(status);
		console.log(log);
		
		var response;
		response=$http.post('AcceptStatusAdmin',log, {
			headers : {
				"Content-Type" : "application/json"
 
			}
		});

		
		response.success(function(data, status, headers, config) {
			console.log(data);
			$scope.clearAdmin();
			$scope.contactsAdmin = {};
			$scope.statusAdmin={};
			
	        

			
			
	  		
		});
		response.error(function(data, status, headers, config) {
			alert("Error in connection");
		});
	};
	
	
	return session;
});