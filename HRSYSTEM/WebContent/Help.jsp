<div ng-controller="helpCtrl" style="width: 1100px;margin-left: 100px;">


</br>
<div class="container-fluid">
  <div class="row content">
    <div class="col-md-2 sidenav" style="background-color:#FCF9F9;">
<!--       <h4>Help desk</h4> -->
      <ul class="nav nav-pills nav-stacked" style="position: relative;
    top: 30px;">
        <li ><a href="#help" ng-click="expenseHelpFn()">Expenses</a></li>
        <li><a href="#help"  ng-click="statusreportHelpFn()">Status Report</a></li>
        <li><a href="#help"  ng-click="performanceFn()">Performance Review</a></li>
        <li><a href="#help"  ng-click="payrollFn()">My Profile</a></li>
        <li><a href="#help"  ng-click="blogFn()">Blog</a></li>
        <li><a href="#help"  ng-click="leaveFn()">Leave</a></li>
        <li><a href="#help"  ng-click="timeSheetFn()">Time Sheet</a></li>
        <li><a href="#help"  ng-click="coeFn()">Center OF Excellence</a></li>
        
      
        
      </ul><br>
    
     
    </div>
    <div ng-hide="homeHelp">
   
    	<p>
 		<img alt="image1" src="style/Need-help.jpg" width="800px" height="600px" style="position: relative; left: 2px;  top: 17px;"> 
 		</p>
 		
 		
    </div>
  

   <div class="col-md-9" ng-hide="expenseHelp" style="  text-align: initial;">
	
      <h4 style="    position: relative;
    top: 5px;" >Expenses</h4>
       
      <hr>
       	
       	<div ng-hide="hideExpenseEmp">
 			<p>
 			Employee can able to create a new Expense by clicking  <button> <span class="glyphicon glyphicon-plus"
				style="color: rgba(0, 0, 0, 0.38); margin-right:0px; font-size: 0.86em; "></span> New Expense</button>  button which is located 
 			at right corner of Expenses tab. 	
 			</p>
 		
 			<p>	
 			Employee can submit New Expense detatils by clicking <input class="btn btn-info" type="button"
						style="width: 70px; margin-right: 5px; "
						value="Submit"  /> button.	</p>
 			<p>	Submit Button which is located at below the form.</p>
 			<p>	<input class="btn btn-info" type="button"
						style="width: 70px; margin-right: 5px; "
						value="Cancel"  /> button which is located at below the form.It will clear the form
						sets back to Expense page.
 			</p>
		 	<p>	Employee wants to know the status of Expense submitted by him can check on status column.</p>				 
	 	
	 		<p>	If Employee wants to view the Expense submitted by him can able to view by clicking Account code Hyperlink
	 		for each Expense. </p>
	 		<p>Example : <a> 6120-Bank Service Charges </a> .</p>
	 	
	 	
	 	</div>
	 	<div ng-hide="hideExpenseAdmin">
	 	<p>	By default It will show all  pending Expenses list. If you want to review the particular Employee you can go with choose employee
	 	in that you can choose expense based on Employee name.
	 	 </p>
	 	<p> One more drop down is there to choose the status of expense to find the pending,accepted and rejected Expense list of Particular Employee. 
	 	 </p>	
	 	 <p>If Employee wants to view the Expense submitted by Employee can able to view by clicking Account code Hyperlink
	 		for each Expense. </p>
	 	<p>Example : <a> 6120-Bank Service Charges </a>.</p>
	 	
	 	<p> If you want to Accept or Reject the Expense list ,Each Expense List row Action column will provide <a> Accept </a> or <a> Reject </a> hyperlink  click  respectively. </p>
	 	
	 	<p> If you want to review the particular Expense in detail by click the particular Hyperlink Expense Account code it shows one more table which is shown on top of the Expense list </p>
	 	
	 	<p> If you want to review the Expense Receipt by downloading the Expense Receipt </p>
	 	
	 	<p> If you want to Accept or Reject the same Expense below the Expense Information table Accept and Reject buttons are there. </p>
	 	
	 	<p>
	 	<input class="btn btn-default"  type="button"
						style="width: 70px;background-color: #dbdbdb;"
						value="Accept" /> button which is used Accept the Expense List submitted by Employee.
	 	
	 	</p>
	 	
	 	<p>
	 	<input class="btn btn-default"  type="button"
						style="width: 70px;background-color: #dbdbdb;"
						value="Reject" /> button which is used Reject the Expense List submitted by Employee.
	 	
	 	</p>
	 	
	 	<p> If you want to add any notes to the Particular Expense list by selecting <a> Notes </a> which is present below the Expense Information table at left corner.</p>
	 	
	 	
	 	<p>
	 	<input class="btn btn-default"  type="button"
						style="width: 70px;background-color: #dbdbdb;"
						value="Cancel" /> button which is used close the Expense Information table.
	 	
	 	</p>
	 	
	 	<p> 
	 	<input class="btn btn-default"  type="button"
						style="width: 140px;background-color: #dbdbdb;"
						value="Employee Expense" /> button which will show the Employee Expense Filter By Submission Date form.
						Choose the Employee ,status and select the From date and To date (Based on Submission Date) Then Select  <input class="btn btn-default"  type="button"
						style="width: 60px;background-color: #dbdbdb;"
						value="Go >>" /> button. It will show the Expense list with the total amount will be present at bottom of the table.
	 	
	 	</p>
	 	<p>
	 	<input class="btn btn-default"  type="button"
						style="width: 70px;background-color: #dbdbdb;"
						value="Cancel" /> button which is used close the Employee Expense Filter By Submission Date form.
	 	
	 	</p>
	 	<p> 
	 	<input class="btn btn-default"  type="button"
						style="width: 110px;background-color: #dbdbdb;"
						value="Total Expense" /> button which will show the Employee Expense Filter By Submission Date form.
						 select the From date and To date (Based on Submission Date) and Select status (Pending,Accepted, Rejected) dropdown selst and click <input class="btn btn-default"  type="button"
						style="width: 60px;background-color: #dbdbdb;"
						value="Go >>" /> button. It will show the Expense list of all the Employee based on status we have choosed ,the total amount is shown  at footer  of the table.
	 	
	 	
	 	</p>
	 	
	 	<p>
	 	<input class="btn btn-default"  type="button"
						style="width: 70px;background-color: #dbdbdb;"
						value="Cancel" /> button which is used close the Total Expense Filter By Submission Date form.
	 	
	 	</p>
	 	</div>
	 	<br/>
	 </div>	
    
    
    
    <div class="col-md-9" ng-hide="statusreportHelp" style="    text-align: initial;">

      <h4 style="    position: relative;
    top: 5px;">Status Report</h4>
      <hr>
      <div ng-hide="hideStatusEmp">
    	
    	<p> 
    	
    	Employee can add a new Activity by clicking  <input class="btn btn-default"  type="button"
						style="width: 110px;background-color: #dbdbdb;"
						value="New Activity" /> Button.
			</p>			
		<p> Employee has to fill all the fields present there with signature and click <input class="btn btn-info" type="button"
					style="width: 90px; " value="Submit"
					 /> button.				
    	
    	
    	  </p>
    	<p> Employee has to cancel the activity by clicking  <input class="btn btn-info" type="button"
					style="width: 90px; " value="Cancel"
					 /> button.				
    	
    	
    	  </p>
    	  	<p> 
    	
    	By default it will show all the pending activity which has not approved by supervisor/manager.
    	Employee can edit those activity by clicking <a>Edit</a> Hyperlink which is present at Action Column.
    	
    	
    	  </p>
    	<p> 
    	
    	Employee has to view the Each activity by selecting Activity column <a> activity </a> hyperlink to view particular activity.
    	It will show status Report information table with all informations provided on the top of the status Report table.
    	
    	  </p>
		</div> 
		
      <div ng-hide="hideStatusAdmin">
    	
    	
    	<p> 
    	
    	By Default it will show all employee's pending activity listed below table.</p>
    	<p>If you want to view or Accept the Activity of the Employee by selecting <a>Accept</a> Hyperlink or
    	selecting <a>Activity</a> Hyperlink. 
    	
    	</p>
    	
    	<p>It will show particular activity information on top of the table with Employee Signature. 
    	
    	  </p>
    	
    	<p> If you want to accept the particular activity by Selecting  <input class="btn btn-info" type="button"
					style="width: 90px; " value="Accept"
					 /> button with reviewed person signature is needed. </p>
					 
		<p> If you want to Cancel the particular activity by Selecting  <input class="btn btn-info" type="button"
					style="width: 90px; " value="Cancel"
					 /> button. it will show default status Report page. </p>
					 
		<p>If you want to view the particular Employee status report by selceting Choose Employee drop down listed. </p>
    	
    	<p> One more dropdown near by Choose Employee is  accepted and pending status. </p>
    	<p> If the activity is Accepted it will show <a>Report</a> Hyperlink which is present at Action Column of the Table.  </p>
		<p> By clicking Report Hyperlink it will show status Report with Admin and Employee Signature. </p>
		<p> Admin/Manager cross verify the status Report and download the status Report By selecting 
		
		<input class="btn btn-info" type="button"
					style="width: 90px; " value="Download"
					 /> button.
		
		  </p>
		<p> By secting the <input class="btn btn-info" type="button"
					style="width: 90px; " value="Cancel"
					 /> button. It will move to default Status Report Page. </p>
		</div> 
     
    </div>
    
    
    
    
    
    
    
    <div class="col-md-9" ng-hide="performanceHelp" style="    text-align: initial;">

      <h4 style="    position: relative;
    top: 5px;">Performance Review</h4>
      <hr>
   	   <div ng-hide="hideReviewEmp">
    	
    	
    	
    	<p> 
    	Employee will receive Task  with weightage from Manager.
    	  </p>
    	<p> To achive this task what are activty involving everything Employee has to fill by clicking 
    	<a>Add Activity</a> Hyperlink which is present on Action column corresponding task row.
    	
    	  </p>
    	<p> It will Show form with the following fields Task Assigned,Project Name,Description of the project,Task Completed ,
No of defects / issues cleared.  </p>
	<p> Employee has to fill and click <input class="btn btn-info" type="button"
					style="width: 90px; " value="Submit"
					 /> button.</p>
					 
			<p> 		 If you want to cancel by selecting <input class="btn btn-info" type="button"
					style="width: 90px; " value="Cancel"
					 /> button. </p>

    	<p> Employee can view the particular activity by selecting <a>Activity</a> Hyperlink which is present at Activity column. </p>
    	<p> It will show the following Activity information on the top of the table. </p>
    	<p> If you want to close by selecting Close Button. </p>
		</div> 
		
      <div ng-hide="hideReviewAdmin">
    	
    	
    	    	
    	<p> 
    	
    	 <input class="btn btn-default"  type="button"
						style="width: 100px;background-color: #dbdbdb;"
						value="Assign Task" />  button which is present at top right corner between Performance Report button and performance Review.
				 
    	Admin/Manager can able to assign task by clicking Assign Task button.  </p>
    	  <p>It will Show Assign Task form. In that choose Employee And asign task with weightage.
    	It will assign task to particular Employee with Weightage.  </p>
    	
    	 <p> By default it will show all the task with  activity list table is shown.  </p> 
    	<p> If you want to review the particular task by clicking <a> Review Task </a> on action column. </p>
    	<p>By selecting Review Task it will show Review Task table </p>
    	<p>By reviewing the task enter the Review Weight and  submit the weightage <input class="btn btn-info" type="button"
					style="width: 140px; " value="Submit Weight"
					 /> button. </p>
					 
			<p> If you want to close the Review Task table by clicking <input class="btn btn-default"  type="button"
						style="width: 70px;background-color: #dbdbdb;"
						value="Close" /> button  </p>	
						
			<p> If you want to do Performance Review for the Employee by clicking <input class="btn btn-default"  type="button"
						style="width: 160px;background-color: #dbdbdb;"
						value="Performance Review" /> button . </p>
						
			<p> It will Show performance list you can choose the rate the Employee and fill following fields and submit by using submit button.
			 </p>							
				<p> If you want to cancel by selecting cancel button.</p>	
				
			<p> If you want to download Performance Report for the Employee by clicking <input class="btn btn-default"  type="button"
						style="width: 160px;background-color: #dbdbdb;"
						value="Performance Report" /> button which is present at top right corner. </p>	
						
				<p>It will show  Choose Employee dropdown By selcting Employee it will show Performance Review table and Task list with weightage table is shown.  </p>			
				
				<p> You can able to see total assigned task weigthage and achieved in below table footer.  </p>			 
		<p> If you want to download performance Review by clicking <a> Report </a>  Hyperlink which is present at report column.</p>
		<p> By clicking Report Hyperlink it will show the performance report below of the table.
		If you wnat to download by clicking  <input class="btn btn-info" type="button"
					style="width: 90px; " value="Download"
					 /> button. or If you want to cancel by selecting <input class="btn btn-info" type="button"
					style="width: 90px; " value="Cancel"
					 /> button. </p>
						
						
		</div> 
      
     
    </div>
    
    
    
    
    
    
    
    <div class="col-md-9" ng-hide="payrollHelp" style="    text-align: initial;">

      <h4 style="    position: relative;
    top: 5px;">My Profile</h4>
      <hr>
    <div ng-hide="hidePayrollEmp">
    	
    	
    	
    	<p> 
    	
    	Employee Profile Details and Oraganisation Chart is present there. If you want to change Profile Image By click 
    	Profile image and choose image and click Update button to update the profile Photo. 
    	
    	
    	  </p>
    	  
		</div> 
		
      <div ng-hide="hidePayrollAdmin">
    	
    	
    	<p> 
    	Admin/Manager Profile Details and Oraganisation Chart is present there.If you want to change Profile Image By click 
    	Profile image and choose image and click Update button to update the profile Photo.
    	
    	
    	
    	  </p>
    	
    	
		</div>
		
		
    
    </div>
    
     
     <div class="col-md-9" ng-hide="blogHelp" style="    text-align: initial;">

      <h4 style="    position: relative;
    top: 5px;">Blog</h4>
      <hr>
    <div ng-hide="hideBlogEmp">
    	
    	
    	
    	<p> 
    	
    	Employee can add new Post by clicking <input class="btn btn-default"  type="button"
						style="width: 90px;background-color: #dbdbdb;"
						value="Add Post" /> button.  </p>	
						
						
    	<p>
     It will show Add Post form ,with the following fields  post Heading &  Content.
     By entering the fields and click <input class="btn btn-info" type="button"
					style="width: 90px; " value="Submit"
					 /> button to submit the post.Following post is visible to all the Users in this Application. </p>
					 <p> Admin only has the access to delete the post. If you wants to cancel the post by selecting 
					 <input class="btn btn-info" type="button"
					style="width: 80px; " value="Cancel"
					 /> button.
    	  </p>
    	
		</div> 
		
      <div ng-hide="hideBlogAdmin">
    	<p> 
    	
    	Admin/Manager can add new Post by clicking <input class="btn btn-default"  type="button"
						style="width: 80px;background-color: #dbdbdb;"
						value="Add Post" /> button.  </p>	
						
		<p> 
    	If you wnat to delete the post by clicking <input class="btn btn-default"  type="button"
						style="width: 100px;background-color: #dbdbdb;"
						value="Delete Post" /> button. It will show Post heading's dropdown. Admin can select and Delete by 
						By clicking <input class="btn btn-default"  type="button"
						style="width: 60px;background-color: #dbdbdb;"
						value="Delete " /> button , particular post get deleted.
    	
    	
    	
    	  </p>				
    	<p>
     It will show Add Post form ,with the following fields  post Heading &  Content.
     By entering the fields and click <input class="btn btn-info" type="button"
					style="width: 90px; " value="Submit"
					 /> button to submit the post.Following post is visible to all the Users in this Application. </p>
					 <p> Admin only has the access to delete the post. If you wants to cancel the post by selecting 
					 <input class="btn btn-info" type="button"
					style="width: 80px; " value="Cancel"
					 /> button.
    	  </p>
    	
    	
    	
    	
    	  </p>
		</div>
		
		
    
    </div>
   
   
       <div class="col-md-9" ng-hide="leaveHelp" style="    text-align: initial;">

      <h4 style="    position: relative;
    top: 5px;">Leave</h4>
      <hr>
    <div ng-hide="hideLeaveEmp">
    	
    	  	 <p>Leave module is used to apply leaves.</p>
      
      <p style="  font-weight: 600;">Access for User :</p> 

		<p> Leave module presents with apply for leave form.  <p>
    	<p>	Once leave is applied the leave status will be pending. 
    	If user wants to reschedule leave can go with <a>Reschedule</a> when the leave status is Pending / Approved. </p>
		<p> If user wants to cancel the leave hit <a>cancel</a>. </p>
		<p>User cann't <a>Reschedule</a> or <a>Cancel</a> leave when the leave status is consumed.</p>
		
		
    	  
		</div> 
		
      <div ng-hide="hideLeaveAdmin">
    	<p> Admin can able to Approve and Reject applied leaves by user. </p>
		<p>	Admin can able to check the applied leave with help of drop down. </p>
		<p> If admin wants to approve leave hit <a>Approve</a>. </p>
		<p> If admin wants to reject leave hit <a>Reject</a>. </p>

		<p>	Admin can able to check total applied leaves and total allocated leaves by admin. </p>
		
    	  
		</div>
		
		
    
    </div>
   
   
    
    
    
    <div class="col-md-9" ng-hide="timeSheetHelp" style="    text-align: initial;">

      <h4 style="    position: relative;
    top: 5px;">Time Sheet</h4>
      <hr>
    <div ng-hide="hideTimeSheetEmp">
    	
    	  		 <p>Timesheet is used to added user's daily worked hours. </p>
		
		<p style="  font-weight: 600;">Access for User :</p>
	<p>		User can able to view timesheet with help of drop down month & year.
	        It will show the particular month timesheet and presents with date, day, hours 8.0 as default. </p>
			
			<p> If user wants to submit the timesheet hit <a>submit </a>.  </p>
			<p>	If user wants to edit the worked hours hit <a>edit</a>. 
				Edit link takes you to edit timesheet form. 
				It will show selected date, day and hours. 
				If the hours is 0 should specify the leave type and finally hit <button class="btn btn-info" style="height:30px;"> Submit</button>  </p>

		<p>	If user wants to update timesheet can go with <a> update </a> when the timesheet status is pending.</p>
    	
    	  
		</div> 
		
      <div ng-hide="hideTimeSheetAdmin">
    	<p>	Admin can able to view timesheets with help of choose drop downs.
			Admin should select user name,month and year. </p>

		<p> It presents with date,day,hours of particular user. </p>
        <p> If admin wants to approve timesheet hit <a>approve </a>. </p>
 		<p>	If admin wants to reject timesheet hit <a>reject </a>. </p>
 		<p> When the hour is 0 the leave type is presents in timesheet. </p> 
    	  
		</div>
		
		
    
    </div>
    
     <div class="col-md-9" ng-hide="coeHelp" style="    text-align: initial;">

      <h4 style="    position: relative;
    top: 5px;">Center Of Excellence</h4>
      <hr>
    <div ng-hide="hideCoeEmp">
    	
    	
    	<p> 
    	The Centre of Excellence will serve as a competency delivery centre that will address the current challenges in learning development or learning technology implementation. 
    	</p>
    	<p> The services provided through the COE can be in the area of consulting AI Practice,
Mobility Practice,BI Practice,SOA Practice,J2EE Practice.
    	
    	
    	  </p>
    	  
		</div> 
		
      <div ng-hide="hideCoeAdmin">
    	
    	
    	<p> 
    	
    	The Centre of Excellence will serve as a competency delivery centre that will address the current challenges in learning development or learning technology implementation.
    	</p>
    	<p> The services provided through the COE can be in the area of consulting AI Practice,
Mobility Practice,BI Practice,SOA Practice,J2EE Practice.
    	
    	
    	
    	  </p>
    	  
		</div>
		
		
    
    </div>
    
    
    
    
    
    </div>
    
 
    
     
  
    
     
    

  </div>
</div>

  


