<div ng-controller="CoeCtrl" style="width: 1100px;margin-left: 100px;">
    
    
<!--     <center> <h2>Center of Excellence </h2></center> -->
   	<div class="container-fluid">
  <div class="row content">
   	
   	<div class="col-md-2 sidenav" style="float:left;margin-left:0px;margin-top:0px;background-color:#FCF9F9;">

      <ul class="nav nav-pills nav-stacked" style="position: relative;
    top: 30px;">
        <li ><a href="#coe" ng-click="AIFn()">AI Practice</a></li>
        <li><a href="#coe"  ng-click="MobilityFn()">Mobility Practice</a></li>
        <li><a href="#coe"  ng-click="BIFn()">BI Practice</a></li>
        <li><a href="#coe"  ng-click="SoaFn()">SOA Practice</a></li>
        <li><a href="#coe"  ng-click="J2EEFn()">J2EE Practice</a></li>
     	 
        




      </ul><br>
    
     
    </div> 
    
    <div ng-hide="hideHomeCoe">
    
    	<div style="float:left;margin-left:150px;margin-top:60px;margin-bottom:20px;">
    	<p >
 		<img alt="image1" src="style/coe_opensource.jpg" width="500px" height="400px" style="position: relative; left: 2px;  "> 
 		</p>
 		<br>
 		</div>
 		
 			<div style="float:left;width:700px;margin-left:250px;margin-top:10px;margin-bottom:50px;">
 				<p style="color:black;">
 				
 				<b>Patton Labs Group is proud to be the world leader in Technology Service and Research Management.
 				</b>
 				</p>
 				<p>
 				Our Patton Labs , Research, and development center use Center of Excellence Methodology to train and support our Engineers and Consultants continuously to match global technology trends. 
 				All our Engineers are trained and Certified by our Unique Certification program called PCE (Patton Certified Engineers).
 				</p>
 			</div>
    	
    	
    	
    </div>
    
    <div class="col-md-9" ng-hide="hideAi" style=" margin-top:0px;   text-align: initial;">

      <h4 style="    position: relative;
   top: 5px;">AI</h4> 
      <hr>
     <p> Artificial intelligence  is intelligence displayed by machines, in contrast with the natural intelligence displayed by humans and other animals. In computer science AI research is defined as the study of "intelligent agents": any device that perceives its environment and takes actions that maximize its chance of success at some goal. Colloquially, the term "artificial intelligence" is applied when a machine mimics "cognitive" functions that humans associate with other human minds, such as "learning" and "problem solving". See glossary of artificial intelligence.</p>

<p>The scope of AI is disputed: as machines become increasingly capable, tasks considered as requiring "intelligence" are often removed from the definition, a phenomenon known as the AI effect, leading to the quip "AI is whatever hasn't been done yet." For instance, optical character recognition is frequently excluded from "artificial intelligence", having become a routine technology. Capabilities generally classified as AI as of 2017 include successfully understanding human speech, competing at a high level in strategic game systems (such as chess and Go), autonomous cars, intelligent routing in content delivery networks, military simulations, and interpreting complex data, including images and videos.</p>
     
    </div>
    

   
   	<div ng-hide="hideMobility" class="col-md-9" style=" margin-top:0px;   text-align: initial;">
    	
 		 <h4 style="    position: relative;   top: 5px;">Mobility</h4> 
      <hr>
      
      <ul class="list-inline">
   		 	<li><a ng-click="mobIos()">iOS native app development</a></li>
    		<li><a ng-click="mobAnroid()">Android</a></li>
    		<li><a ng-click="mobKony()">Kony </a></li>
    		<li><a ng-click="mobPhonegap()">Phonegap</a></li>
    		<li><a ng-click="mobCordova()">Cordova</a></li>
    		
 	 </ul>
       
      
  <div ng-hide="hideIos" style=" margin-top:50px;  ">   
  <h4>iOS native app development</h4>  
  <p>First, we get familiar with the actual development aspects of your application including  technologies that are helpful to know when embarking on an iOS app development project. Many companies try to create a great experience for customers. But few are willing to make the changes required to deliver on that promise. And  few  customers even realize just how good their experience can be. We have a practical battle plan for placing the user at the heart of your company.</p>

<p>N2 designs and develops various iPhone and iPad applications for some of the world greatest companies. iPad app development and iPhone app development are two of our significant competencies, making up the major portion of our mobile app projects. Our native iOS developers  enable the best possible experiences and ensure you receive a quality iOS app.  We have everything you need to deliver great mobile apps, deliver native iOS, Android, and Windows apps using existing skills, teams, and code.</p>

<p>Our team of iOS developers are proficient at Objective C, Swift, Cocoa Touch, Xcode and the Apple regulations. The iOS8 platform has introduced new APIs to provide an experience to the Apple users never known before. At N2 we are well conversed with the latest features for extensibility, health & fitness, TouchID, CloudKit, HomeKit, Sandbox Security protocols and more.</p>

<p>To get your idea transformed into an active app, we have experts to design, develop, test and deploy iPhone Apps. Besides the expertise in iOS application development, design and architecture, we have a strong knowledge of iOS platform and various components embedded in it.</p>

<p>Accelerometer, GPS,
Core Animation,
Core Audio,
Core Location Framework,
iPhone (iOS) SDK,
Objective C / C++ /Cocoa Touch,
OpenGL ES, Open AL and Core Graphics,
Programming tools: Xcode IDE, Interface Builder.
  </p>
</div> 

<div ng-hide="hideAnroid" style=" margin-top:50px;  " >
<h4>Android</h4>
<p>We are one of the leading Android Application development organizations delivering best android apps development solutions to enterprises across the globe. We have proven expertise in Android apps development that ensures value-added services to your mobile operations. Utilizing the maximum potential of the intricate Android SDK platform, our developers utilize a plethora of Android development tools to explore unlimited product development possibilities.</p>

<p>We have a technically-sound team of Android Architects with best-in-class skills and proven expertise in mobile application development by using the Android platform. Our programmers are best of breed in developing scalable and highly robust android mobile applications and porting to distinct mobile platforms.</p>
</div>

<div ng-hide="hideKony" style=" margin-top:50px;  ">
<h4>Kony</h4>
<p>Kony is the fastest growing, cloud based enterprise mobility solutions company and an industry leader among mobile application development platform (MADP) providers. Kony empowers today leading organizations to compete in mobile time by rapidly delivering multiedge mobile apps across the broadest array of devices and systems, today and in the future.</p>

<p>Kony offers ready to run business mobile apps to help organizations better engage with customers and partners, as well as increase employee productivity through mobile device access to company systems and information. Powered by the industry leading Kony Mobility Platform, enterprises can design, build, configure, and manage mobile apps across the entire software development lifecycle, and get to market faster with a lower total of ownership.</p>

</div>
<div ng-hide="hidePhonegap" style=" margin-top:50px;  " >
<h4>Phonegap</h4>

<p>PhoneGap is a free and open source framework that allows you to create mobile apps using standardized web APIs for the platforms you care about.</p>

<p>Building applications for each device iPhone, Android, Windows Mobile and more requires different frameworks and languages. PhoneGap solves this by using standards based web technologies to bridge web applications and mobile devices. Since PhoneGap apps are standards compliant, they are future proofed to work with browsers as they evolve.</p>
</div>

<div ng-hide="hideCordova" style=" margin-top:50px;  ">
<h4>Cordova</h4>
<p>Apache Cordova is a set of device APIs that allow a mobile app developer to access native device function such as the camera or accelerometer from JavaScript. Combined with a UI framework such as jQuery Mobile or Dojo Mobile or Sencha Touch, this allows a smartphone app to be developed with just HTML, CSS, and JavaScript.</p>

<p>When using the Cordova APIs, an app can be built without any native code (Java, Objective-C, etc) from the app developer. Instead, web technologies are used, and they are hosted in the app itself locally (generally not on a remote HTTP server).</p>

<p>Our consultants have worked on numerous projects using Cordova. We can help you develop mobile applications using Cordova cost effectively.</p>
      
   </div>   
      
    </div>
    
    
    <div ng-hide="hideBi"class="col-md-9" style=" margin-top:0px;   text-align: initial;">
    	
 	<h4 style="    position: relative;   top: 5px;">BI</h4> 
      <hr>
      
     <p> Business Intelligence (BI) comprises the strategies and technologies used by enterprises for the data analysis of business information.</p>
   <p>  BI technologies provide historical, current and predictive views of business operations. </p>
   <p>  Common functions of business intelligence technologies include reporting, online analytical processing, analytics, data mining, process mining, complex event processing, business performance management, benchmarking, text mining, predictive analytics and prescriptive analytics.</p>
   <p>  BI technologies can handle large amounts of structured and sometimes unstructured data to help identify, develop and otherwise create new strategic business opportunities. </p>
   <p>  They aim to allow for the easy interpretation of these big data. </p>
   <p>  Identifying new opportunities and implementing an effective strategy based on insights can provide businesses with a competitive market advantage and long-term stability.</p>

<p>Business intelligence can be used by enterprises to support a wide range of business decisions - ranging from operational to strategic. </p>
<p>Basic operating decisions include product positioning or pricing. </p>
<p>Strategic business decisions involve priorities, goals and directions at the broadest level. </p>
<p>In all cases, BI is most effective when it combines data derived from the market in which a company operates (external data) with data from company sources internal to the business such as financial and operations data (internal data). </p>
<p>When combined, external and internal data can provide a complete picture which, in effect, creates an "intelligence" that cannot be derived from any singular set of data.</p>
<p>Amongst myriad uses, business intelligence tools empower organizations to gain insight into new markets, to assess demand and suitability of products and services for different market segments and to gauge the impact of marketing efforts.</p>
      
      
      
    </div>
    
    
    <div ng-hide="hideSoa" class="col-md-9" style=" margin-top:0px;   text-align: initial;">
    	
 	<h4 style="    position: relative;   top: 5px;">SOA</h4> 
      <hr>
      
      <p> A service-oriented architecture (SOA) is a style of software design where services are provided to the other components by application components, through a communication protocol over a network. </p>
      <p>The basic principles of service-oriented architecture are independent of vendors, products and technologies.</p>
      <p>A service is a discrete unit of functionality that can be accessed remotely and acted upon and updated independently, such as retrieving a credit card statement online.</p>

 <p>A service has four properties according to one of many definitions of SOA:</p>

 <p>It logically represents a business activity with a specified outcome.</p>
 <p>It is self-contained.</p>
 <p>It is a black box for its consumers.</p>
 <p>It may consist of other underlying services</p>
    </div>
    
    <div ng-hide="hideJ2ee" class="col-md-9" style=" margin-top:0px;   text-align: initial;">
    	
 	<h4 style="    position: relative;   top: 5px;">J2EE</h4> 
      <hr>
     <p> 
      Java Platform, Enterprise Edition (Java EE) is the standard in community-driven enterprise software. </p>
       <p> Java EE is developed using the Java Community Process, with contributions from industry experts, commercial and open source organizations, Java User Groups, and countless individuals.</p> 
     <p>   Each release integrates new features that align with industry needs, improves application portability, and increases developer productivity.</p>
     <p> </p>
    
    </div>
    
     </div>
</div>
</div>