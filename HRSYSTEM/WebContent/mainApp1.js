var app = angular.module("mainApp1", ['ngRoute','ngAnimate','fcsa-number','validNumber','ngPatternRestrict','ui.bootstrap','naif.base64','ngMaterial']);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "Home1.jsp",
        controller: "mainCtrl1"
    }).when("/Expenses1", {
        templateUrl : "Expenses1.jsp",
        controller : "ExpensesCtrl"	
    }).when("/StatusReport1", {
        templateUrl : "StatusReport1.jsp",
        controller : "StatusReportCtrl"
    }).when("/PerformanceReview1", {
        templateUrl : "PerformanceReview1.jsp",
        controller : "PerformanceReviewCtrl"
    }).when("/profile", {
        templateUrl : "Profile.jsp",
        controller : "ProfileCtrl"
    }).when("/leave", {
        templateUrl : "Leave1.jsp",
        controller : "leaveCtrl"
    }).when("/support", {
        templateUrl : "Support.jsp",
        controller : "supportCtrl"
    }).when("/help", {
        templateUrl : "Help.jsp",
        controller : "helpCtrl" 
    }).when("/coe", {
        templateUrl : "Coe.jsp",
        controller : "CoeCtrl"
    }).when("/blog", {
        templateUrl : "Blog.jsp",
        controller : "BlogCtrl"
    }).when("/timesheet", {
        templateUrl : "Timesheet1.jsp",
        controller : "timesheetCtrl"
    
    }).otherwise("/");
});
 

 


app.controller('logoutCtrl', function($scope, $http, $window) {
	var sess=sessionStorage.getItem("key1");
	var firstname=sessionStorage.getItem("firstname");
	var payroll=sessionStorage.getItem("payroll");
	if(sess){
		
		$scope.value = 'Logout';
		if(payroll == "N2 Service" ){
		$scope.PayrollName = "N2 Service Inc/";
		}else{
			$scope.PayrollName = "Pattonlabs Inc/";
		}
		
	$scope.firstname2=firstname;
	$scope.logout = function() {	
		delete sessionStorage.firstname;
		delete sessionStorage.key1;
		$window.location.href = 'login.jsp';	
	
		$window.location.href = 'login.jsp';	
	};
	}
	else{   $window.location.href = 'login.jsp'; }
});


app.controller('mainCtrl1', function($scope, $http, $window, $rootScope){
	$scope.home1 = "home1";
	$scope.expenses1="expenses1";
	$scope.statusreport = "statusreport";
	$scope.statusreport1 = "statusreport1";
	$scope.performancereview1 = "performancereview1";
	$scope.profile = "profile";
	$scope.leave = "leave";
	$scope.support = "support";
	$scope.help = "help";
	$scope.coe = "coe";
	$scope.blog = "blog";
	$scope.timesheet = "timesheet";
	
	
	angular.element(document.getElementById($scope.home1)).addClass("align");
	
	
	 $scope.action=function(){              // Home
	    	
			angular.element(document.getElementById($scope.home1)).addClass("align");
		 
	    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
	    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
	    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
	    	angular.element(document.getElementById($scope.profile)).removeClass("align");
	    	
	    	angular.element(document.getElementById($scope.support)).removeClass("align");
	    	angular.element(document.getElementById($scope.help)).removeClass("align");
	    	
	    	angular.element(document.getElementById($scope.coe)).removeClass("align");
	    	angular.element(document.getElementById($scope.blog)).removeClass("align");
	    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
	    	angular.element(document.getElementById($scope.leave)).removeClass("align");
	    };
	
	
	
	  $scope.action1=function(){          // expense
		  angular.element(document.getElementById($scope.home1)).removeClass("align");
			 
	    	angular.element(document.getElementById($scope.expenses1)).addClass("align");
	    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
	    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
	    	angular.element(document.getElementById($scope.profile)).removeClass("align");
	    
	    	angular.element(document.getElementById($scope.support)).removeClass("align");
	    	angular.element(document.getElementById($scope.help)).removeClass("align");
	    	angular.element(document.getElementById($scope.coe)).removeClass("align");
	    	angular.element(document.getElementById($scope.blog)).removeClass("align");
	    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
	    	angular.element(document.getElementById($scope.leave)).removeClass("align");
	    	    //  disable scroll in table
	  };
	

	
   
    $scope.action2=function(){             // status report
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).addClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	angular.element(document.getElementById($scope.leave)).removeClass("align");
    
    };
    $scope.action3=function(){                     // performancereview
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).addClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	angular.element(document.getElementById($scope.leave)).removeClass("align");
    	
    
    };
    $scope.action4=function(){                        // profile
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).addClass("align");

    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	
    	
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	angular.element(document.getElementById($scope.leave)).removeClass("align");
    };
    
  
        
    $scope.action6=function(){                     // support
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.support)).addClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	angular.element(document.getElementById($scope.leave)).removeClass("align");
    	
    
    };
    
    $scope.action7=function(){                          //help
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).addClass("align");
    	
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	angular.element(document.getElementById($scope.leave)).removeClass("align");
    	
    	
    };
     
    $scope.action10=function(){                          //coe
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).addClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	angular.element(document.getElementById($scope.leave)).removeClass("align");
    		
    	
    	
    };
    $scope.action8=function(){                          //blog
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).addClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	angular.element(document.getElementById($scope.leave)).removeClass("align"); 	
    	
    };
    $scope.action9=function(){                          //timesheet
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).addClass("align");
    	angular.element(document.getElementById($scope.leave)).removeClass("align"); 	
    	
    };
    
    $scope.action11=function(){                     // leave
    	
    	
    	angular.element(document.getElementById($scope.leave)).addClass("align");
    	angular.element(document.getElementById($scope.home1)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses1)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport1)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview1)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	
    	
    	
    
    };
	
});

