<div  ng-controller="leaveCtrl" style="width: 85%;">

 		<div ng-hide="hideselect">
			<div style="width: 20%; float: left; position: relative; top: 4px;font-weight: 800;margin-top:30px;">Choose Employee :</div>
 						
			<select ng-model="leave.employeeId" name="priority"
							ng-options="x.employeeId as x.name  for x in contactsEmp"
							ng-change="filterEmployeeId(leave.employeeId,leave.status)"
							style="width: 200px;  float:left;margin-left: -20px;margin-top:30px;" class="form-control">
							<option  label="All" ></option>
						</select>
			
			
<!-- 			<select ng-model="leave.status" name="priority" -->
<!-- 							ng-options=" x for x in leaveStatus" -->
<!-- 							ng-change="filterEmployeeStatus(leave.employeeId,leave.status)" -->
<!-- 							style="width: 200px;  float:left;margin-left: 15px;margin-top:30px;" class="form-control"> -->
<!-- 							<option label="-Select-"></option> -->
<!-- 						</select> -->
	  		
 			<br/><br/> <br/><br/>
 			</div>		


			<div ng-hide="viewleavebox"  ng-repeat="contact in contactViewUser " style="width:90%;margin-top:140px;resize: both; padding: 38px; overflow: auto; border: 1px solid rgb(221, 221, 221); position: relative; bottom: 73px;">
	
				<div style="text-align: left; margin-left: 0px;">
					<label style="font-weight: 500; color: #777;">Leave Details
						</label>
<!-- 					<hr style= " position: relative; bottom: 18px;">  -->
					 
				</div>
					<table class="table table-bordered" cellspacing="0" width="100%">

						<tbody>
	

				 			<tr>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Leave From :</td>
								<td style="width: 25%;">{{ contact.leaveFrom }}</td>
			
			
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Leave To :</td>
								<td style="width: 25%;">{{ contact.leaveTo }}</td>
				 				
							</tr>
							<tr>
			
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Leave Type :</td>
								<td style="width: 25%;"> {{contact.leaveType}}</td>
			
									<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Number of Days :</td>
								<td style="width: 25%;">{{ contact.duration }}</td>
								
							</tr>
							<tr>
								
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Leave status:</td>
								<td style="width: 25%;"> {{contact.status}}</td>
							 
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									 Reason :</td>
								<td style="width: 25%;">{{ contact.description }}</td>
							
							</tr>	
					  	
				</tbody>
 			</table>
 	
 			<input class="btn btn-info" type="button"
						style="width: 90px; margin-top:5px; "
					ng-show=" contact.status ==='Pending'  || contact.status === 'Cancelled' "	value="Approve" ng-click="approve(contact.leaveId)" />	
  	
  			<input class="btn btn-info" type="button"
						style="width: 90px; margin-top:5px; "
					ng-show="contact.status ==='Pending'"	value="Reject" ng-click="reject(contact.leaveId)" />	
  	
  	
 			<input class="btn btn-info" type="button"
						style="width: 90px; margin-top:5px; "
						value="Close" ng-click="cancelAd()" />	
  					
 			</div>



		<div style="width: 90%;margin-left:0px; margin-top:10px; align: left;"> 

 		<table class="table  table-hover" style="font-size: 14px;" ng-hide="tableAd"
		 cellspacing="0" width="100%">

			<thead style="background-color: rgba(0,0,0,0.12);">

							<tr>
							<th style=" width: 2%;"> </th>
<!--  					<th style=" width: 12%; color: rgba(51, 51, 51, 0.9);">Leave Id</th> -->
					<th style=" width: 13%; color: rgba(51, 51, 51, 0.9);"> Type </th>	
					<th style=" width: 13%; color: rgba(51, 51, 51, 0.9);">From </th>
					<th style=" width: 11%; color: rgba(51, 51, 51, 0.9);">To</th>
					
					<th style=" width: 12%; color: rgba(51, 51, 51, 0.9);">Duration</th>					
					<th style=" width: 13%; color: rgba(51, 51, 51, 0.9);">Leave Status</th>
	 				<th style=" width: 12%; color: rgba(51, 51, 51, 0.9);">Action</th>
 
				</tr>

			</thead>
			
			<tfoot style="background-color: #dbdbdb;">
					<tr>
					
 					 <th></th>
					 
					<th colspan="2" style="color: rgba(51, 51, 51, 0.9);"> Total leaves taken </th>
					<th style="  color: rgba(51, 51, 51, 0.9);"> {{Rjt1}}  </th>
					<th> </th>
					
					<th   style="color: rgba(51, 51, 51, 0.9);"> 
					Total leaves allocated
					</th>
					<th>{{amt1}} </th>
 					 
				</tr>		
			</tfoot>
			
		
		<tbody style="background-color: rgba(0,0,0,0.05);">


<tr>
            <td colspan="9">
        				<div class="scrollit">
            					<table class="table  table-hover " style="font-size: 14px;"cellspacing="0" width="100%">
	

				 <tr ng-repeat="contact in contactsAdmin  | filter:searchJobOrder " style= " height: 37px;" >
					
					<td style=" width: 2%;"> </td>
					 
					<td style=" width: 15%;">{{contact.leaveType}}</td>
					<td style=" width: 15%;">{{contact.leaveFrom}}</td>
					<td style=" width: 15%;">{{contact.leaveTo}}</td>
					<td style=" width: 14%;">{{contact.duration}}</td>
		 			<td style=" width: 14%;">{{contact.status}}</td>
		 			<td style=" width: 14%;">
		 			&nbsp; &nbsp; 
		 			<a  ng-show="contact.status === 'Pending'   " href="#leave" ng-click="viewLeave(contact.leaveId)">Approve</a>
		 			  
		 			<a ng-show="contact.status === 'Pending'" href="#leave" ng-click="viewLeave(contact.leaveId)">Reject</a>
		 			
 		 			
		 			<a ng-show="contact.status != 'Pending'   " href="#leave" ng-click="viewLeave(contact.leaveId)" >View</a>
		 			</td>
 				</tr>

		
		
								</table>
        				</div>
                </td>
        </tr>


		 
     </tbody>
</table>


</div>


 

</div>