<div ng-controller="AddemployeeCtrl" style="width: 1100px;margin-top: 40px;margin-left: 100px;">

<div>
<button class="btn btn-default" id="mybutton"  style="background-color: #dbdbdb; float:right;margin-right: 60px;"
		 ng-click="ShowAddEmployee()" >
			<span 	style="color: rgba(0, 0, 0, 0.38);  font-size: 0.86em;"></span> Add Employee
</button>

<form name="employeeForm">
		<br/> 
		<div ng-hide="addEmployee" class="form-group container"
			style="background-color: white; align: center;">
<!-- <h4 style="width: 100%; float: left;margin-left: -200px; ">Pre Process Check Comments</h4> -->



				
				<div style="text-align: left; margin-left: 15px;">
					<h4 style="font-size: 16px;">Add Employee</h4>
					<hr style= " position: relative; bottom: 18px;"> 
					 
				</div>
			

				<div style="width: 100%; float: left;margin-left:50px;">



					<br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">First Name :</div>
 						<input name="FirstName"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.firstname" >
<!-- 							<p ng-show="employeeForm.FirstName.$invalid" style=" color:red;float:left;margin-top:0px;margin-left:480px;font-size: 0.86em;" >This is a required field</p> -->
						<!-- 						</div> -->
					</div>
					

					
					<br /><br /><br />
					
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Last Name :</div>
 						<input name="Candidate Name"
							style="width: 200px;float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.lastname">
						<!-- 						</div> -->
					</div>
					
				

					<br /><br /><br />
					
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Date of Birth :</div>
 						<input name="DateOfBirth"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="date"
							ng-model="addemployee.dob"  ng-change="dateformate(addemployee.dob)" required>
<!-- 								<p ng-show="employeeForm.DateOfBirth.$invalid" style=" color:red;float:left;margin-top:0px;margin-left:480px;font-size: 0.86em;" >This is a required field</p> -->
						<!-- 						</div> -->
					</div>
					<br /><br /><br />
					
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Primary Phone Number :</div>
 						<input name="primaryPhoneNumber"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.primaryPhoneNumber" ng-pattern-restrict="^\d{0,26}?$">
<!-- 								<p ng-show="employeeForm.primaryPhoneNumber.$invalid" style=" color:red;float:left;margin-top:0px;margin-left:480px;font-size: 0.86em;" >This is a required field</p> -->
								<p style=" color:red;float:right;margin-top:0px;margin-right:570px;font-size: 0.86em;"ng-show="alertPhoneNo"> <i>Phone Number Already Exist! </i></p>			
						<!-- 						</div> -->
					</div>
					<br /><br /><br />
					
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Secondary Phone Number :</div>
 						<input name="Candidate Name"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.secondaryPhoneNumber" ng-pattern-restrict="^\d{0,26}?$" >
						<!-- 						</div> -->
					</div>
					<br /><br /><br />
					
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Primary Email :</div>
 						<input name="primaryEmail"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.primaryEmail">
<!-- 							<p ng-show="employeeForm.primaryEmail.$invalid" style=" color:red;float:left;margin-top:0px;margin-left:480px;font-size: 0.86em;" >This is a required field</p> -->
								<p style=" color:red;float:right;margin-right:570px;font-size: 0.86em;"ng-show="alertEmailId"> <i>Email Id Already Exist! </i></p>
						<!-- 						</div> -->
					</div>
					<br /><br /><br />
					
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Secondary Email :</div>
 						<input name="Candidate Name"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.secondaryEmail">
						<!-- 						</div> -->
					</div>
					<br /><br /><br />
					
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Location :</div>
 						<input name="Candidate Name"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.location">
						
					</div>
					<br /><br /><br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Date of Joining :</div>
 						<input name="Date Of Birth"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="date"
							ng-model="addemployee.doj"  ng-change="dateformate(addemployee.doj)">
						
					</div>
					
					<br /><br /><br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Client Name :</div>
 						<input name="Candidate Name"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.clientName">
						
					</div>
					
					<br /><br /><br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Designation :</div>
 						<input name="Candidate Name"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.designation">
						
					</div>
					
					<br /><br /><br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Employee Role :</div>
 						<select ng-model="addemployee.emprole" name="priority"
							ng-options="x for x in employeeRole"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control" required>
							<option label="-Select-"></option>
						</select>
										</div>
										
					<br /><br /><br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Password :</div>
 						<input name="Candidate Name"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control"
							autofocus="" required="" type="text" 
							ng-model="addemployee.password">
						
					</div>
					
					<br /><br /><br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Report To :</div>
 						<select ng-model="addemployee.reportTo" name="priority1"
							ng-options="x.employeeId as x.name  for x in contactsManager"
							
							style="width: 200px; float: left;margin-left: 20px;" class="form-control" required>
							<option label="-Select-"></option>
							
							
						</select>
						
					</div>
					
					<br /><br /><br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Payroll :</div>
 						<select ng-model="addemployee.payroll" name="priority2"
							ng-options="x for x in employeePayroll"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control" required>
							<option label="-Select-"></option>
						</select>
										</div>
					
						<br /><br /><br />
					<div style="width: 100%;float: left; text-align: right;">
						<div style="width: 30%; float: left; margin-left: 30px; position: relative; top: 4px;">Employee Status :</div>
 						<select ng-model="addemployee.empstatus" name="empStatus"
							ng-options="x for x in employeeStatus"
							style="width: 200px; float: left;margin-left: 20px;" class="form-control" required>
							<option label="-Select-" ></option>
						</select>
<!-- 						 <span ng-show="employeeForm.empStatus.$error.required">Select service</span> -->
						
										</div>
										
					<br /><br /><br />
					
				</div>

			<br/><br /><br />
			
				
				
				
 				 
 				<div style="align: center; float:left;margin-left: 450px; ">
 			 
 				 <input class="btn btn-info" type="button"
					style="width: 90px; margin-right: 4px;margin-top:25px;" value="Submit"
					ng-click="submit(addemployee)" /> 
 					 
				<input class="btn btn-info" type="button"
						style="width: 90px; margin-top:25px; "
						value="Cancel" ng-click="cancel(addemployee)" />	
 					
				</div>
				</div>
			
		
	</form>
	
	
	<div ng-hide="viewemployee" style="width:93%;float:left;margin-top:140px;margin-left:40px; resize: both; padding: 38px; overflow: auto; border: 1px solid rgb(221, 221, 221); position: relative; bottom: 73px;">
	
				<div style="text-align: left; margin-left: 0px;">
					<label style="font-weight: 500; color: #777;">Employee Information
						</label>
<!-- 					<hr style= " position: relative; bottom: 18px;">  -->
					 
				</div>
					<table class="table table-bordered" cellspacing="0" width="100%">

						<tbody ng-repeat="contact in contactView ">
	

							<tr>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Name :</td>
								<td style="width: 25%;">{{ contact.firstName }}</td>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Employee Status :</td>
								<td style="width: 25%;"> {{contact.employeeStatus}}</td>
<!-- 								<td align="right" -->
<!-- 									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)"> -->
<!-- 									Last Name :</td> -->
<!-- 								<td style="width: 25%;">{{ contact.lastName }}</td> -->
								
							</tr>
							<tr>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Date of Birth :</td>
								<td style="width: 25%;">{{ contact.dateOfBirth }}</td>
								
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Date Of Joined :</td>
								<td style="width: 25%;"> {{contact.dateOfJoined}}</td>
							</tr>
							<tr>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Primary Phone Number :</td>
								<td style="width: 25%;">{{ contact.phoneNumberPrimary }}</td>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Secondary Phone Number :</td>
								<td style="width: 25%;">{{ contact.phoneNumberSecondary }}</td>
								
							</tr>
							<tr>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Email Primary :</td>
								<td style="width: 25%;">{{ contact.emailPrimary }}</td>
								
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Email Secondary :</td>
								<td style="width: 25%;"> {{contact.emailSecondary}}</td>
							</tr>
							<tr>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									 Client Name :</td>
								<td style="width: 25%;">{{ contact.clientName }}</td>
								
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Location :</td>
								<td style="width: 25%;"> {{contact.location}}</td>
							</tr>
							<tr>
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									Designation :</td>
								<td style="width: 25%;">{{ contact.designation }}</td>
								
								<td align="right"
									style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
									</td>
								<td style="width: 25%;"> </td>
							</tr>
								
						
	
				</tbody>
 			</table>
 			</div>
	</br></br></br>

<div style="width: 95%;  align: left;float:left;">
 		<table class="table table-box table-hover table-radius" style="font-size: 14px;"
		 cellspacing="0" width="100%">

			<thead style="background-color: rgba(0,0,0,0.12);">

							<tr>
					<th style=" width: 1%; color: rgba(51, 51, 51, 0.9);"> </th>
					<th style=" width: 12%; color: rgba(51, 51, 51, 0.9);">Employee Id</th>
					<th style=" width: 15%; color: rgba(51, 51, 51, 0.9);">Employee Name</th>
			
					<th style=" width: 11%; color: rgba(51, 51, 51, 0.9);">Phone No </th>
					<th style=" width: 16%; color: rgba(51, 51, 51, 0.9);">Email</th>
					<th style=" width: 12%; color: rgba(51, 51, 51, 0.9);">Client Name</th>
					<th style=" width: 12%; color: rgba(51, 51, 51, 0.9);">Designation</th>
					<th style=" width: 8%; color: rgba(51, 51, 51, 0.9);">Role</th>

					<th style=" width: 13%; float:left;margin-left:28px;color: rgba(51, 51, 51, 0.9);">Action</th>

				</tr>

			</thead>
			
			<tfoot style="background-color: #dbdbdb;">
					<tr>
					
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
					<th> </th>
					
					<th colspan="4" > 
					<select ng-model="choose1.page" ng-change="getListOfPage(choose1)"  
			 				 style="width: 185px; height: 31px; line-height: 15px; box-shadow: none;" 
					 class="form-control  no-radius">
							<option label="15 Records per page"></option>
							<option value="20">20 Records per page</option>
							<option value="30">30 Records per page</option>
							<option value="40">40 Records per page</option>
							<option value="100"> 100 Records per page</option>
							<option value="200"> 200 Records per page</option>							
							 
					</select> 
		</th>
 					 
					 
				</tr>		
			</tfoot>

		
		
		<tbody style="background-color: rgba(0,0,0,0.05);">
		
		<tr>
            <td colspan="11">
        				<div class="scrollit">
            					<table class="table  table-hover " style="font-size: 14px;"cellspacing="0" width="100%">
	

				 <tr ng-repeat="contact in contacts | filter:searchJobOrder | filter:paginate  " style= " height: 37px;" >
					
					
					<td style=" width: 12%;">{{contact.employeeId}}</td>
					<td style=" width: 15%;"><a href="#addemployee"	ng-click="viewEmployee(contact.employeeId)">{{contact.firstName}}</a></td>

					<td style=" width: 11%;">{{contact.phoneNumberPrimary}}</td>
					<td style=" width: 16%;">{{contact.emailPrimary}}</td>
					<td style=" width: 12%;">{{contact.clientName}}</td>
					<td style=" width: 12%;"> {{contact.designation}}</td>
					<td style=" width: 8%;"> {{contact.role}}</td>

					<td style=" width: 10%;" ><a 
						ng-click="edit(contact.employeeId)">Edit</a> |<a
						 ng-click="delet(contact.employeeId)">Delete</a>
					</td>
				</tr>

		
		
								</table>
        				</div>
                </td>
        </tr>
    </tbody>
</table>
	
	
	<pagination total-items="totalItems" ng-model="currentPage"  
             max-size="5" boundary-links="true"  
             items-per-page="numPerPage" >  
       </pagination>
	
	
	</div>
	
</div>		
</div>	





