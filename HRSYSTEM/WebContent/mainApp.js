var app = angular.module("mainApp", ['ngRoute','appModule','fcsa-number','validNumber','ngPatternRestrict','ngAnimate','naif.base64','ui.bootstrap','ngMaterial']);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "Home.jsp",
        controller: "mainCtrl"
    }).when("/Expenses", {
        templateUrl : "Expenses.jsp",
        controller : "ExpensesCtrl"
    }).when("/statusreport", {
        templateUrl : "StatusReport.jsp",
        controller : "StatusReportCtrl"
    }).when("/performancereview", {
        templateUrl : "PerformanceReview.jsp",
        controller : "PerformanceReviewCtrl"
    }).when("/profile", {
        templateUrl : "Profile.jsp",
        controller : "ProfileCtrl"
    }).when("/addemployee", {
        templateUrl : "AddEmployee.jsp",
        controller : "AddemployeeCtrl"
    }).when("/leave", {
        templateUrl : "Leave.jsp",
        controller : "leaveCtrl"
    }).when("/support", {
        templateUrl : "Support.jsp",
        controller : "supportCtrl"
    }).when("/help", {
        templateUrl : "Help.jsp",
        controller : "helpCtrl"
    }).when("/coe", {
        templateUrl : "Coe.jsp",
        controller : "CoeCtrl"
    }).when("/blog", {
        templateUrl : "Blog.jsp",
        controller : "BlogCtrl"
    }).when("/timesheet", {
        templateUrl : "Timesheet.jsp",
        controller : "timesheetCtrl"
            	
    }).otherwise("/");
});
 
app.controller("mainCtrl", function ($scope,$rootScope) {
    $scope.msg = "main page";
    
 
	
	
	
	
});
 


app.controller('logoutCtrl', function($scope, $http, $window) {
	var sess=sessionStorage.getItem("key1");
	var firstname=sessionStorage.getItem("firstname");
	
	if(sess){
	var payroll1=sessionStorage.getItem("payroll");
	$scope.value = 'Logout';
	if(payroll1 == "N2 Service" ){
	$scope.PayrollName = "N2 Service Inc/";
	}else{
		$scope.PayrollName = "Pattonlabs Inc/";
	}
	$scope.firstname2=firstname;
	$scope.logout = function() {	
		delete sessionStorage.firstname;
		delete sessionStorage.key1;
		delete sessionStorage.payroll;
		$window.location.href = 'login.jsp';	
	
		$window.location.href = 'login.jsp';	
	};
	}
	else{   $window.location.href = 'login.jsp'; }
});


app.controller('mainCtrl', function($scope, $http, $window,loginService, $rootScope){
	$scope.home = "home";
	$scope.expenses="expenses";
	$scope.statusreport = "statusreport";
	$scope.performancereview = "performancereview";
	$scope.profile = "profile";
	$scope.addemployee = "addemployee";
	$scope.support = "support";
	$scope.help = "help";
	$scope.coe = "coe";
	$scope.blog = "blog";
	$scope.timesheet = "timesheet";
	$scope.leave= "leave";
	
	angular.element(document.getElementById($scope.home)).addClass("align");
	
	
	 $scope.action=function(){              // Home
	    	
			angular.element(document.getElementById($scope.home)).addClass("align");
		 
	    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
	    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
	    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
	    	angular.element(document.getElementById($scope.profile)).removeClass("align");
	    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
	    	angular.element(document.getElementById($scope.support)).removeClass("align");
	    	angular.element(document.getElementById($scope.help)).removeClass("align");
	    	angular.element(document.getElementById($scope.coe)).removeClass("align");
	    	angular.element(document.getElementById($scope.blog)).removeClass("align");
	    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
	    };
	
	
	
	  $scope.action1=function(){          // expense
		  angular.element(document.getElementById($scope.home)).removeClass("align");
			 
	    	angular.element(document.getElementById($scope.expenses)).addClass("align");
	    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
	    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
	    	angular.element(document.getElementById($scope.profile)).removeClass("align");
	    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
	    	angular.element(document.getElementById($scope.support)).removeClass("align");
	    	angular.element(document.getElementById($scope.help)).removeClass("align");
	    	angular.element(document.getElementById($scope.coe)).removeClass("align");
	    	angular.element(document.getElementById($scope.blog)).removeClass("align");
	    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
	    	    //  disable scroll in table
	  };
	

	
   
    $scope.action2=function(){             // status report
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).addClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    
    };
    $scope.action3=function(){                     // performancereview
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).addClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	
    
    };
    $scope.action4=function(){                        // payroll
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).addClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	
    };
    
    $scope.action5=function(){                           // addemployee
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).addClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");	
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	
    
    };
        
    $scope.action6=function(){                     // support
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).addClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    	
    
    };
    
    $scope.action7=function(){                          //help
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).addClass("align");
    	
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    		
    	
    	
    };
     
    
    $scope.action10=function(){                          //coe
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).addClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    		
    	
    	
    };
    
    $scope.action8=function(){                          //blog
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).addClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    		
        	
    };
    $scope.action9=function(){                          //timesheet
    	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).addClass("align");
    		
        	
    };
    
    $scope.action11=function(){                          //Leave
    	
    	
    	
     	angular.element(document.getElementById($scope.leave)).addClass("align");
     	angular.element(document.getElementById($scope.home)).removeClass("align");
		 
    	angular.element(document.getElementById($scope.expenses)).removeClass("align");
    	angular.element(document.getElementById($scope.statusreport)).removeClass("align");
    	angular.element(document.getElementById($scope.performancereview)).removeClass("align");
    	angular.element(document.getElementById($scope.profile)).removeClass("align");
    	angular.element(document.getElementById($scope.addemployee)).removeClass("align");
    	angular.element(document.getElementById($scope.support)).removeClass("align");
    	angular.element(document.getElementById($scope.help)).removeClass("align");
    	angular.element(document.getElementById($scope.coe)).removeClass("align");
    	angular.element(document.getElementById($scope.blog)).removeClass("align");
    	angular.element(document.getElementById($scope.timesheet)).removeClass("align");
    		
    	
    	
    		
    	
    	
    };
	
});

