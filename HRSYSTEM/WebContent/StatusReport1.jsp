<div ng-controller="StatusReportCtrl"
	style="width: 1100px; margin-top: 40px; margin-left: 100px;">

	<button class="btn btn-default" id="mybutton" ng-hide="hidestatus"
		style="background-color: #dbdbdb; float: right; margin-right: 50px; margin-bottom: 10px; height: 29px; line-height: 15px;"
		ng-click="ShowActivity()">
		<span style="color: rgba(0, 0, 0, 0.38); font-size: 0.86em;"></span><span
			class="glyphicon glyphicon-plus"
			style="color: rgba(0, 0, 0, 0.38); margin-right: 0px; font-size: 0.86em;"></span>
		Status Report
	</button>

	<div>

		<div ng-hide="hideviewtableEmp"
			style="width: 95%; float: right; margin-right: 50px; margin-top: 100px; resize: both; padding: 38px; overflow: auto; border: 1px solid rgb(221, 221, 221); position: relative; bottom: 73px;">

			<div style="text-align: left; margin-left: 0px;">
				<label style="font-weight: 500; color: #777;">Status Report
					Information </label>
				<!-- 					<hr style= " position: relative; bottom: 18px;">  -->

			</div>
			<table class="table table-bordered" cellspacing="0" width="100%">

				<!-- 						<tbody ng-repeat="contact in contactsview "> -->
				<tbody ng-repeat="contact in contacts ">

					<tr>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Name :</td>
						<td style="width: 25%;">{{ contact.name }}</td>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Activity :</td>
						<td style="width: 25%;">{{ contact.activity }}</td>

					</tr>
					<tr>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Start Planned Date :</td>
						<td style="width: 25%;">{{ contact.startPlannedDate }}</td>

						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Start Actual Date :</td>
						<td style="width: 25%;">{{contact.startActualDate}}</td>
					</tr>
					<tr>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Finish Planned Date:</td>
						<td style="width: 25%;">{{ contact.finishPlannedDate }}</td>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Finish Estimated Date :</td>
						<td style="width: 25%;">{{ contact.finishEstimatedDate }}</td>

					</tr>
					<tr>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Current Project :</td>
						<td style="width: 25%;">{{ contact.currentProject }}</td>

						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Client Details:</td>
						<td style="width: 25%;">{{contact.clientDetails}}</td>
					</tr>
					<tr>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Status Month :</td>
						<td style="width: 25%;">{{ contact.statusMonth }}</td>

						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Manager Name :</td>
						<td style="width: 25%;">{{contact.managerName}}</td>
					</tr>
					<tr>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Manager Email :</td>
						<td style="width: 25%;">{{ contact.managerEmail }}</td>

						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Manager Phone Number :</td>
						<td style="width: 25%;">{{contact.managerPhonenumber}}</td>
					</tr>
					<tr>
						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Action Status :</td>
						<td style="width: 25%;">{{ contact.action_status }}</td>

						<td align="right"
							style="background-color: rgba(242, 243, 244, 0.45); width: 25%; color: rgba(51, 51, 51, 0.9)">
							Status :</td>
						<td style="width: 25%;"><span
							ng-show="contact.status === 'On-track'"
							style="padding: 2px; margin: 2px; background-color: green; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left; margin-top: 12px;">
						</span> <span ng-show="contact.status === 'Date at Risk'"
							style="padding: 2px; margin: 2px; background-color: yellow; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left; margin-top: 12px;">
						</span> <span ng-show="contact.status === 'Impacts End Date'"
							style="padding: 2px; margin: 2px; background-color: red; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left; margin-top: 12px;">
						</span>
							<P style="margin-top: 10px;">&nbsp &nbsp{{contact.status}}</P></td>
					</tr>
					<tr style="background-color: gray; width: 25%; color: black">
						<td style="width: 25%;"></td>
						<td style="width: 25%;"></td>

						<td style="width: 25%;"></td>
						<td style="width: 25%;"></td>
					</tr>




				</tbody>
			</table>

			<button class="btn btn-default" id="mybutton"
				style="background-color: #dbdbdb; float: right; margin-right: 480px;"
				ng-click="Close()">
				<span style="color: rgba(0, 0, 0, 0.38); font-size: 0.86em;"></span>
				Close
			</button>
		</div>



		<form name="userForm" ng-hide="hideexpenseform"
			style="align: left; float: left; width: 95%;">



			<!-- 					<div style="text-align: left; float: left;width: 900px;"> -->
			<!-- 						<h4 style="font-size: 16px;">New Activity</h4> -->
			<!-- 						<hr style= " position: relative; bottom: 18px;">  -->

			<!-- 					</div> -->

			<!-- 					<div style="width: 50%; float: left;"> -->

			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;"> Activity :</div> -->
			<!--  						<input name="Candidate Name" -->
			<!-- 							style="width: 300px;float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="text"  -->
			<!-- 							ng-model="status.activity"> -->

			<!-- 					</div> -->

			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Current Project :</div> -->

			<!-- 						<textarea name="description" -->
			<!-- 							style="width: 303px; height:73px;float: left; margin-left:  20px; position: relative; bottom: 10px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="text" ng-model="status.currentProject"></textarea> -->
			<!-- 					</div> -->

			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Status Month :</div> -->
			<!--  						<input name="Date Of Birth" -->
			<!-- 							style="width: 300px; float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="date" -->
			<!-- 							ng-model="status.statusMonth"  ng-disabled="false" ng-change="dateformate(status.statusMonth)"> -->

			<!-- 					</div> -->

			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Start Planned Date :</div> -->
			<!--  						<input name="Date Of Birth" -->
			<!-- 							style="width: 300px; float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="date" -->
			<!-- 							ng-model="status.startPlanDate"   ng-change="dateformate(status.startPlanDate)"> -->

			<!-- 					</div> -->





			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Finish Planned Date :</div> -->
			<!--  						<input name="Date Of Birth" -->
			<!-- 							style="width: 300px; float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="date" -->
			<!-- 							ng-model="status.finishPlannedDate"  ng-change="dateformate(status.finishPlannedDate)"> -->

			<!-- 					</div> -->


			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Manager Name :</div> -->
			<!--  						<input name="Candidate Name" -->
			<!-- 							style="width: 300px;float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="text"  -->
			<!-- 							ng-model="status.managerName"> -->

			<!-- 					</div> -->



			<!-- 					</div>	 -->
			<!-- 					<div style="width: 50%; float: right;"> -->

			<!-- 					<div style="width: 100%;float: left;margin-left:100px; text-align: right;margin-top:20px;"> -->

			<!-- 						 	<div style="width: 33%; float: left;  position: relative; top: 4px;margin-left:-100px;">Status :</div> -->

			<!-- 						 	<div style="width: 33%; float: left; margin-left:-65px; position: relative; top: 4px;"> -->

			<!--                     		<input type="radio" name="chickenEgg" value="On-track" ng-model="status.statuscode" > -->
			<!--                     		On-track -->
			<!--                 			</div> -->
			<!--                 	</div>	 -->

			<!--                 	<div style="width: 100%;float: left;margin-left:120px; text-align: right;margin-top:10px;"> 	 -->
			<!--                 		 	<div style="width: 33%; float: left;  position: relative; top: 4px;"> -->

			<!--                     		<input type="radio" name="chickenEgg" value="Date at Risk" ng-model="status.statuscode"> -->
			<!--                    			Date at Risk -->
			<!--                 			</div> -->
			<!--                 	</div>	 -->

			<!--                 	<div style="width: 100%;float: left; margin-left:152px;text-align: right;margin-top:10px;"> 	 -->

			<!--                 			<div style="width: 33%; float: left;  position: relative; top: 4px;"> -->

			<!--                     		<input type="radio" name="chickenEgg" value="Impacts End Date" ng-model="status.statuscode"> -->
			<!--                     		 Impacts End Date -->
			<!--                 			</div> -->


			<!-- 					</div> -->




			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Start Actual Date :</div> -->
			<!--  						<input name="Date Of Birth" -->
			<!-- 							style="width: 300px; float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="date" -->
			<!-- 							ng-model="status.startActualDate"  ng-change="dateformate(status.startActualDate)"> -->

			<!-- 					</div>				 -->
			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Finish Estimated Date :</div> -->
			<!--  						<input name="Date Of Birth" -->
			<!-- 							style="width: 300px; float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="date" -->
			<!-- 							ng-model="status.finishEstimatedDate"   ng-change="dateformate(status.finishEstimatedDate)"> -->

			<!-- 					</div>	 -->

			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Current Module :</div> -->
			<!--  						<input name="Candidate Name" -->
			<!-- 							style="width: 300px;float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="text"  -->
			<!-- 							ng-model="status.currentModule"> -->

			<!-- 					</div> -->







			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Manager Email :</div> -->
			<!--  						<input name="Candidate Name" -->
			<!-- 							style="width: 300px;float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="text"  -->
			<!-- 							ng-model="status.managerEmail"> -->

			<!-- 					</div> -->
			<!-- 					<div style="width: 100%;float: left; text-align: right;margin-top:20px;"> -->
			<!-- 						<div style="width: 33%; float: left;  position: relative; top: 4px;">Manager Phone Number :</div> -->
			<!--  						<input name="Candidate Name" -->
			<!-- 							style="width: 300px;float: left;margin-left: 20px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="text"  -->
			<!-- 							ng-model="status.managerPhoneNumber"> -->

			<!-- 					</div> -->







			<!-- 				</div>	 -->
			<!-- 				<div style="width: 90%; float: left;margin-top:20px;"> -->
			<!-- 				<div style="width: 100%;float: left; text-align: left;margin-top:0px;"> -->
			<!-- 						<div style="width: 33%; float: left; margin-left:80px; position: relative; top: 4px;">Client Details :</div> -->

			<!-- 						<textarea name="description" -->
			<!-- 							style="width: 303px; height:73px;float: left; margin-left:  -190px; position: relative; bottom: 10px;" class="form-control" -->
			<!-- 							autofocus="" required="" type="text" ng-model="status.clientDetails"></textarea> -->
			<!-- 					</div> -->
			<!-- 				</div>	 -->


			<!-- 				<div style="width: 90%; float: left;margin-top:20px;margin-left:90px;"> -->

			<!-- 				<p>Signature:</p> -->
			<!--     			<signature-pad dataurl="storedSignature" accept="accept" clear="clear" height="220" width="568" style=" height: 124px; -->
			<!--   						width: 272px; border: 2px solid black;"></signature-pad> -->
			<!--    				 <button ng-click="clear()">Reset</button> -->





			<!-- 				</div> -->












			<!-- 				<div style="align: center; float:left;margin-left: 450px;margin-top:30px; "> -->

			<!--  				 <input class="btn btn-info" type="button" -->
			<!-- 					style="width: 90px; margin-right: 4px;margin-top:25px;" value="Submit" -->
			<!-- 					ng-click="submit(status)" />  -->

			<!-- 				<input class="btn btn-info" type="button" -->
			<!-- 						style="width: 90px; margin-top:25px; " -->
			<!-- 						value="Cancel" ng-click="cancel(status)" />	 -->

			<!-- 				</div> -->

			<!-- 	test  ....		 -->


			<div
				style="text-align: left; background-color: gray; float: left; width: 1043px; margin-left: 0px; margin-top: 50px; resize: both; padding: 6px; overflow: auto; border: 1px solid rgb(221, 221, 221); position: relative; height: 56px; bottom: 10px;">
				<b
					style="font-size: 14px; top: 5px; margin-top: 0px; margin-left: 330px;"
					class="ng-binding"> N2 IT PMO - Monthly Status Report
					&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<!-- Month Ending : --><!-- 				<input name="Date Of Birth" -->
					<!-- 							style="width: 170px;float:right; " class="form-control" -->
					<!-- 							autofocus="" required="" type="date" --> <!-- 							ng-model="status.statusMonth"  ng-disabled="false" ng-change="dateformate(status.statusMonth)"> -->

					<select ng-model="selectYear2" name="Status2"
					ng-options="x1 for x1 in selectYear1"
					style="float: right; width: 130px; margin-right: 20px;"
					class="form-control">
						<option label="-Select Year-"></option>
				</select> <select ng-model="selectMonth2" name="Status"
					ng-options="x2 for x2 in selectMonth1"
					style="float: right; width: 140px; margin-right: 20px;"
					class="form-control">
						<option label="-Select Month-"></option>
				</select> <!-- 		<select ng-model="status.year" name="priority" 	 --> <!--  							style="width: 100px;float:right; margin-left:0px; margin-top:0px;" class="form-control"> -->

					<!-- 					 		 		<option label="-Year-"></option> --> <!-- 									<option value = "2016" >2016</option> -->
					<!-- 									<option value = "2017"> 2017</option> --> <!-- 									<option value = "2018"> 2018</option> -->
					<!-- 									<option value = "2019" >2019</option> --> <!-- 									<option value = "2020"> 2020</option> -->
					<!-- 									<option value = "2011"> 2011</option> --> <!-- 									<option value = "2012" >2012</option> -->

					<!-- 		</select> --> <!-- 			  <select ng-model="status.month" name="priority" 	 -->
					<!--  							style="width: 140px;float:right; margin-top:0px;" class="form-control"> -->
					<!-- 					 		 		<option label="-Month-"></option> --> <!-- 									<option value = "01" >January</option> -->
					<!-- 									<option value = "02"> February</option> --> <!-- 									<option value = "03"> March</option> -->
					<!-- 									<option value = "04" >April</option> --> <!-- 									<option value = "05"> May</option> -->
					<!-- 									<option value = "06"> June</option> --> <!-- 									<option value = "07" >July</option> -->
					<!-- 									<option value = "08"> August</option> --> <!-- 									<option value = "09"> September</option> -->
					<!-- 									<option value = "10" >October</option> --> <!-- 									<option value = "11"> November</option> -->
					<!-- 									<option value = "12"> December</option> --> <!-- 					</select> -->
				</b>

			</div>

			<table class="table table-bordered" cellspacing="0"
				style="margin-top: 50px; width: 100%;">
				<thead style="background-color: rgba(0, 0, 0, 0.12);">

<!-- 					<tr> -->
					
<!-- 						<th style="width: 1%; color: rgba(51, 51, 51, 0.9);"></th> -->
<!-- 						<th style="width: 50%; color: rgba(51, 51, 51, 0.9);"></th> -->
<!-- 						<th style="width: 6%; color: rgba(51, 51, 51, 0.9);"><b>Start</b></th> -->
<!-- 						<th style="width: 6%; color: rgba(51, 51, 51, 0.9);"></th> -->
<!-- 						<th style="width: 6%; color: rgba(51, 51, 51, 0.9);"><b>Finish</b></th> -->
<!-- 						<th style="width: 6%; color: rgba(51, 51, 51, 0.9);"></th> -->
<!-- 						<th style="width: 25%; color: rgba(51, 51, 51, 0.9);"><b>Status</b></th> -->

<!-- 					</tr> -->
					<tr>
						
<!-- 						<th style="width: 1%; color: rgba(51, 51, 51, 0.9);"></th> -->
						<th style="width: 50%; color: rgba(51, 51, 51, 0.9);"><b>Activity Name</b></th>
						<th style="width: 8%; color: rgba(51, 51, 51, 0.9);"><b>Start Planned</b></th>
						<th style="width: 8%; color: rgba(51, 51, 51, 0.9);"><b> Start Actual</b></th>
						<th style="width: 8%; color: rgba(51, 51, 51, 0.9);"><b>Finish Planned</b></th>
						<th style="width: 8%; color: rgba(51, 51, 51, 0.9);"><b>Finish Current
								Estimated (if Different)</b></th>
						<th style="width: 26%; color: rgba(51, 51, 51, 0.9);">
							<span style="padding: 2px; margin: 2px; background-color: green; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left; margin-top: 4px;"/>
							<b>On Track </b><br/> 
							<span style="padding: 2px; margin: 2px; background-color: yellow; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left; margin-top: 4px;"/>
							<b>Date At Risk </b><br/> 
							<span style="padding: 2px; margin: 2px; background-color: red; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left; margin-top: 4px;"/>
							<b>Impacts End</b><br/> 
							
						</th>

					</tr>
				</thead>
				<tbody>

					<tr ng-repeat="act in ActivityRepeat1" ng-controller="RepeatCtrl">
					
<!-- 						<td style="padding: 0 1px; padding-top: 17px;" > -->
<!-- 							<a ng-click="RemoveSheet($index)"><span class="glyphicon glyphicon-remove-sign"></span></a>											 -->
<!-- 						</td> -->
							
						<td style=""><input name="Date Of Birth" style="width: 100%;"
							class="form-control" autofocus="" required="" type="text"
							placeholder="" ng-model="act.activity"></td>

						<td style=""><input name="Date Of Birth"
							style="width: 127px; padding: 0 2px;" class="form-control"
							autofocus="" required="" type="date" placeholder="MM-dd-yyyy"
							ng-model="act.startPlanDate"></td>

						<td style=""><input name="Date Of Birth"
							style="width: 127px; padding: 0 2px;" class="form-control"
							autofocus="" required="" type="date" placeholder="MM-dd-yyyy"
							ng-model="act.startActualDate"></td>

						<td style=""><input name="Date Of Birth"
							style="width: 127px; padding: 0 2px;" class="form-control"
							autofocus="" required="" type="date" placeholder="MM-dd-yyyy"
							ng-model="act.finishPlannedDate"></td>

						<td style=""><input name="Date Of Birth"
							style="width: 127px; padding: 0 2px;" class="form-control"
							autofocus="" required="" type="date" placeholder="MM-dd-yyyy"
							ng-model="act.finishEstimatedDate"></td>

						<td style=""><select ng-model="act.statuscode"
							name="priority" style="width: 100%;" class="form-control"
							required>

								<option label="-Status-"></option>
								<option value="On-track">On-track</option>
								<option value="Date at Risk">Date at Risk</option>
								<option value="Impacts End Date">Impacts End Date</option>
						</select></td>

					</tr>




				</tbody>

			</table>
			<input class="btn btn-info" type="button"
				style="width: 100px; float: right; margin-right: 4px; margin-top: 0px; background-color: green;"
				value="Add Activity" ng-click="submit1(status)" />


			<div style="width: 70%; float: left; margin-bottom: 20px;">

				<div
					style="width: 100%; float: left; text-align: right; margin-top: 20px;">
					<div style="width: 33%; float: left; position: relative; top: 4px;">Current
						Project :</div>

					<textarea name="currentProject"
						style="width: 413px; height: 63px; float: left; margin-left: 20px; position: relative; bottom: 10px;"
						class="form-control" autofocus="" required="" type="text"
						ng-model="status.currentProject" required /></textarea>

					<!--                   	<span ng-show="userForm.currentProject.$invalid" style=" color:red;float:left;margin-left:550px;margin-top:0px;font-size: 0.86em;" >This is a required field</span> -->

				</div>

				<div
					style="width: 100%; float: left; text-align: right; margin-top: 20px;">
					<div style="width: 33%; float: left; position: relative; top: 4px;">Manager
						Name :</div>
					<input name="Candidate Name"
						style="width: 300px; float: left; margin-left: 20px;"
						class="form-control" autofocus="" required="" type="text"
						ng-model="status.managerName">

				</div>

				<div
					style="width: 100%; float: left; text-align: right; margin-top: 20px;">
					<div style="width: 33%; float: left; position: relative; top: 4px;">Manager
						Email :</div>
					<input name="email"
						style="width: 300px; float: left; margin-left: 20px;"
						class="form-control" autofocus="" required="" type="email"
						ng-model="status.managerEmail">
					<!-- 						<p style=" color:red;float:left;margin-left:10px;margin-top:10px;font-size: 0.86em;" ng-show="userForm.email.$invalid && !userForm.email.$pristine" >Enter a valid email.</p> -->
				</div>
				<div
					style="width: 100%; float: left; text-align: right; margin-top: 20px;">
					<div style="width: 33%; float: left; position: relative; top: 4px;">Manager
						Phone Number :</div>
					<input name="Candidate Name"
						style="width: 300px; float: left; margin-left: 20px;" maxlength="10"
						class="form-control" autofocus="" required="" type="text" numeric-only
						ng-model="status.managerPhoneNumber">

				</div>
				<div
					style="width: 100%; float: left; text-align: right; margin-top: 20px;">
					<div style="width: 33%; float: left; position: relative; top: 4px;">Client
						Details :</div>
					<textarea name="clientDetails"
						style="width: 303px; height: 73px; float: left; margin-left: 20px; position: relative; bottom: 10px;"
						class="form-control" autofocus="" required="" type="text"
						ng-model="status.clientDetails"></textarea>
					<!-- 						<span ng-show="userForm.clientDetails.$invalid" style=" color:red;float:left;margin-left:450px;margin-top:0px;font-size: 0.86em;" >This is a required field</span> -->
				</div>

				</span>

			</div>




			<div
				style="width: 100%; float: left; margin-left: 200px; text-align:right; margin-top: 20px;">

				<p style="float: left; margin-top: 0px;">Signature:</p>
				<signature-pad dataurl="storedSignature" accept="accept"
					clear="clear" height="220" width="568"
					style=" height: 124px;
  						margin-left:70px;margin-top:0px;width: 272px; border: 2px solid black;"></signature-pad>
				<button ng-click="clear()"
					style="float: left; margin-left: 170px; margin-top: 0px;">Reset</button>


				<input class="btn btn-info" type="button"
					style="width: 70px; float: left; margin-left: 0px; margin-top: 85px;"
					value="Save" ng-click="submitActivity()" /> <input
					class="btn btn-info" type="button"
					style="width: 70px; float: left; margin-left: 20px; margin-top: 85px;"
					value="Cancel" ng-click="cancelActivity()" />


			</div>


		</form>

		<div ng-hide="hideStatusTable">

			<select ng-model="statusEmp.month" name="priority"
				style="width: 140px; float: left; margin-top: 30px;"
				class="form-control">

				<option label="-Month-"></option>
				<option value="01">January</option>
				<option value="02">February</option>
				<option value="03">March</option>
				<option value="04">April</option>
				<option value="05">May</option>
				<option value="06">June</option>
				<option value="07">July</option>
				<option value="08">August</option>
				<option value="09">September</option>
				<option value="10">October</option>
				<option value="11">November</option>
				<option value="12">December</option>
			</select> <select ng-model="statusEmp.year" name="priority"
				style="width: 100px; float: left; margin-left: 20px; margin-top: 30px;"
				class="form-control">

				<option label="-Year-"></option>
<!-- 				<option value="2016">2016</option> -->
				<option value="2017">2017</option>
				<option value="2018">2018</option>
				<option value="2019">2019</option>
				<option value="2020">2020</option>
				<option value="2011">2021</option>
				<option value="2012">2022</option>
				<option value="2012">2023</option>
				<option value="2012">2024</option>

			</select>
<!-- 			kkc Start ------working for data format filter--------------------------------------------------------------------- -->
<!-- <input id="exampleInput" type="month" name="input" ng-model="statusEmp.monthYear" -->
<!--      placeholder="yyyy-MM" min="2013-01" max="2023-12" required /> -->
<!--   <div role="alert"> -->
<!--     <span class="error" ng-show="myForm.input.$error.required"> -->
<!--        Required!</span> -->
<!--     <span class="error" ng-show="myForm.input.$error.month"> -->
<!--        Not a valid month!</span> -->
<!-- KKC End ------------------------------------------------------------------------------ -->
			<button class="btn btn-default" id="mybutton"
				style="background-color: #dbdbdb; float: left; margin-left: 20px; margin-top: 30px;"
				ng-click="getStatusReportByMonth()">
				<span style="color: rgba(0, 0, 0, 0.38); font-size: 0.86em;"></span>
				Get
			</button>

		</div>


		<table ng-hide="hideStatusTable1" class="table  table-box table-hover "
			style="font-size: 14px; float: left; margin-left: 0px; margin-top: 40px; width: 100%;"
			cellspacing="0" width="70%">

			<thead style="background-color: rgba(0, 0, 0, 0.12);">



				<tr>
<!-- 					<th style="width: 13%; color: rgba(51, 51, 51, 0.9);">STATUS MONTH</th> -->
					<th style="width: 0%;">
						
						<span style="float: left;margin-left: 320px;font-size: 17px;">MONTHLY STATUS REPORT</span>
						<span style="float: right;"><u><a href="#StatusReport1" ng-hide="StatusReportOn"
											ng-click="getMonthReportView()"> STATUS REPORT ON
											{{StatusMonthRow | date :'MMMM-yyyy'}}</a></u></span>
					</th>
				</tr>

			</thead>

			<tbody style="background-color: white;">
				<tr>
				<!--  kkc start -->
<!-- 					<td> -->
<!-- 						<table style="background-color: white; margin-top: 40px;"> -->
<!-- 							<thead style="background-color: rgba(0, 0, 0, 0.12);"> -->

<!-- 							</thead> -->

<!-- 							<tbody> -->

<!-- 								<tr> -->

<!-- 									<td style="width: 28%; "> -->
<!-- 										<a href="#StatusReport1" ng-hide="StatusReportOn" -->
<!-- 											ng-click="getMonthReportView()"> STATUS REPORT ON -->
<!-- 											{{StatusMonthRow | date :'MMMM-yyyy'}}</a>  -->
											<!--  kkc end -->
<!-- 											<span> -->
<!-- 											<p style="margin-top: 0px;" -->
<!-- 												ng-click="getMonthReport(StatusMonthRow)" -->
<!-- 												ng-show="action_statusRow === 'Pending'"> -->
												
<!-- 												<img -->
<!-- 													src="https://cdn2.iconfinder.com/data/icons/freecns-cumulus/16/519584-081_Pen-16.png" -->
<!-- 													alt=""><a>Edit</a> -->
<!-- 											</p> -->
<!-- 											</span> -->

<!--  kkc start -->
<!-- 									</td> -->



<!-- 								</tr> -->

<!-- 							</tbody> -->



<!-- 						</table> -->


<!-- 					</td> -->
					<!--  kkc end -->
					<td>

						<table>
							<thead>



								<tr style="background-color: #edeff2;">
									
									<th style="width: 0%; border: 1pt solid black; color: rgba(51, 51, 51, 0.9);">S.No</th>

									<th
										style="width: 31%; border: 1pt solid black; color: rgba(51, 51, 51, 0.9);"><center>Activity Name
										</center></th>
									<th
										style="width: 10%; border: 1pt solid black; color: rgba(51, 51, 51, 0.9);"><center>Start
											Planned Date</center></th>
									<th
										style="width: 10%; border: 1pt solid black; color: rgba(51, 51, 51, 0.9);"><center>Start
											Actual Date</center></th>
									<th
										style="width: 11%; border: 1pt solid black; color: rgba(51, 51, 51, 0.9);"><center>Finish
											Planned Date</center></th>
									<th
										style="width: 11%; border: 1pt solid black; color: rgba(51, 51, 51, 0.9);"><center>Finish
											Estimated Date</center></th>
									<th
										style="width: 16%; border: 1pt solid black; color: rgba(51, 51, 51, 0.9);"><center>Status</center></th>
									<th
										style="width: 14%; border: 1pt solid black; color: rgba(51, 51, 51, 0.9);"><center>Action</center></th>

								</tr>

							</thead>

							<tbody>
								<!-- 						<tr style= " height: 37px;"> -->

								<!-- 					<th style=" width: 20%; border-left: solid 1px black; "> </th> -->
								<!-- 					<th style=" width: 12%; border-left: solid 1px black;"></th> -->
								<!-- 					<th style=" width: 12%; border-left: solid 1px black;"></th> -->
								<!-- 					<th style=" width: 12%;  border-left: solid 1px black;"></th> -->
								<!-- 					<th style=" width: 12%;  border-left: solid 1px black;"></th> -->
								<!-- 					<th style=" width: 20%; border-left: solid 1px black; "></th> -->
								<!-- 					<th style=" width:15%; "></th> -->

								<!-- 				</tr> -->
								<tr ng-repeat="contact in contacts " style="height: 37px;">


									<td style="width: 0%; border: 1pt solid black; text-align: center;">{{ $index + 1 }}</td>
									<!-- 					<td style=" width: 20%;"><a href="#StatusReport1"ng-click="viewStatusEmp(contact.statusId)">{{contact.activity}}</a></td> -->
									<td style="width: 31%; border: 1pt solid black; padding-left: 10px;">{{contact.activity}}</td>
									<td style="width: 10%; border: 1pt solid black;text-align: center;">{{contact.startPlannedDate}}</td>
									<td style="width: 10%; border: 1pt solid black;text-align: center;">{{contact.startActualDate}}</td>
									<td style="width: 11%; border: 1pt solid black;text-align: center;">{{contact.finishPlannedDate}}</td>

									<td style="width: 11%; border: 1pt solid black;text-align: center;">
										{{contact.finishEstimatedDate}}</td>
									<td style="width: 16%; border: 1pt solid black;"><span
										ng-show="contact.status === 'On-track'"
										style="padding: 2px; margin: 2px; background-color: green; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left;">
											<p ng-show="contact.status === 'On-track'"
												style="float: right; margin-right: -67px; margin-top: -7px; font-size: 14px;">On-track</p>
									</span> <span ng-show="contact.status === 'Date at Risk'"
										style="padding: 2px; margin: 2px; background-color: yellow; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left; margin-top: 8px;">
									</span>
										<p ng-show="contact.status === 'Date at Risk'"
											style="float: right; margin-right: 60px; font-size: 14px;">Date
											at Risk</p> </span> <span
										ng-show="contact.status === 'Impacts End Date'"
										style="padding: 2px; margin: 2px; background-color: red; width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px; float: left; margin-top: 8px;">
									</span>
										<p ng-show="contact.status === 'Impacts End Date'"
											style="float: right; margin-right: 30px; font-size: 14px;">Impacts
											End Date</p> </span></td>
									<td style="width: 14%; border: 1pt solid black;">
										<!-- 					<p ng-show="contact.action_status === 'Pending'">{{contact.action_status}}</p> -->
										<!-- 					<p ng-show="contact.action_status != 'Pending'">{{contact.action_status}}</p> -->{{contact.action_status}}&nbsp;&nbsp;
<!-- 										<a ng-show="contact.action_status === 'Pending'" -->
<!-- 										href="#StatusReport1" ng-click="edit(contact.statusId)"> -->
<!-- 											Edit</a>  -->
									<br/>
										<a ng-show="contact.action_status === 'Pending'"
												href="#StatusReport1"
												ng-click="singleEdit(StatusMonthRow,contact.statusId)">
												Edit |</a>
											<a ng-show="contact.action_status === 'Pending'"
										href="#StatusReport1"
										ng-click="DeleteStatusReport1(contact.statusId,contact.statusMonth)">
											Delete</a>
											
											


									</td>

								</tr>

							</tbody>



						</table>

					</td>
				</tr>

			</tbody>
		</table>




		<!-- 		<table  ng-hide="hideStatusTable" class="table  table-box table-hover " style="font-size: 14px; float:left;margin-left:-50px;margin-top: 20px;width: 95%;" cellspacing="0" width="70%"> -->

		<!-- 				<thead style="background-color: rgba(0,0,0,0.12);"> -->



		<!-- 							<tr> -->


		<!-- 					<th style=" width: 18%; color: rgba(51, 51, 51, 0.9);">Activity </th> -->
		<!-- 					<th style=" width: 13%; color: rgba(51, 51, 51, 0.9);">Start Planned Date</th> -->
		<!-- 					<th style=" width: 13%; color: rgba(51, 51, 51, 0.9);">Start Actual Date</th> -->
		<!-- 					<th style=" width: 14%; color: rgba(51, 51, 51, 0.9);">Finish Planned Date</th> -->
		<!-- 					<th style=" width: 15%; color: rgba(51, 51, 51, 0.9);">Finish Estimated Date</th> -->
		<!-- 					<th style=" width: 15%; color: rgba(51, 51, 51, 0.9);">Status</th> -->
		<!-- 					<th style=" width:15%; color: rgba(51, 51, 51, 0.9);">Action</th> -->

		<!-- 				</tr> -->

		<!-- 			</thead> -->







		<!-- 			<tbody style="background-color: white;"> -->


		<!-- 				 <tr ng-repeat="contact in contacts " style= " height: 37px;" > -->



		<!-- 					<td style=" width: 18%;"><a href="#StatusReport1"ng-click="viewStatusEmp(contact.statusId)">{{contact.activity}}</a></td> -->

		<!-- 					<td style=" width: 13%;">{{contact.startPlannedDate}}</td> -->
		<!-- 					<td style=" width: 10%;">{{contact.startActualDate}}</td> -->
		<!-- 					<td style=" width: 12%;">{{contact.finishPlannedDate}}</td> -->

		<!-- 					<td style=" width: 12%;"> {{contact.finishEstimatedDate}}</td> -->
		<!-- 					<td style=" width: 15%;">  -->

		<!-- 					<span ng-show="contact.status === 'On-track'" style="padding: 2px; margin: 2px; background-color: green;width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px;float: left;"> <p ng-show="contact.status === 'On-track'" style="float:right;margin-right:-80px;margin-top:-7px;font-size:14px;">On-track</p></span> -->
		<!-- 					<span ng-show="contact.status === 'Date at Risk'" style="padding: 2px; margin: 2px; background-color: yellow;width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px;float: left;margin-top:8px;"> </span> <p ng-show="contact.status === 'Date at Risk'"style="float:right;margin-right:30px;font-size:14px;">Date at Risk</p> </span> -->
		<!-- 					<span ng-show="contact.status === 'Impacts End Date'" style="padding: 2px; margin: 2px; background-color: red;width: 20px; height: 10px; border: 1px solid grey; border-radius: 2px;float: left;margin-top:8px;"> </span> <p ng-show="contact.status === 'Impacts End Date'" style="float:right;margin-right:0px;font-size:14px;">Impacts End Date </p> </span> -->




		<!-- 					</td> -->
		<!-- 					<td style=" width: 15%;">  -->
		<!-- 					<a ng-show="contact.action_status === 'Pending'" href="#StatusReport1" ng-click="edit(contact.statusId)"> Edit</a> -->
		<!-- 					<a ng-show="contact.action_status === 'Pending'" href="#StatusReport1" ng-click="DeleteStatusReport1(contact.statusId,contact.statusMonth)"> Delete</a> -->


		<!-- 					</td> -->

		<!-- 				</tr> -->


		<!-- 		</tbody> -->
		<!-- 	</table> -->







	</div>


	<!--   <div ng-repeat="item in ActivityRepeat1"> -->
	<!--       {{item}} -->
	<!--     </div> -->

</div>