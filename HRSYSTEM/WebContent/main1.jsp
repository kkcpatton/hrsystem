<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html ng-app="mainApp1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<head>
	<title>HR System</title>	

    <link rel="stylesheet" href="https://rawgit.com/dalelotts/angular-bootstrap-datetimepicker/master/src/css/datetimepicker.css" /> 

    <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
		
	 
         
       
     <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
            
     <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    
 	  <link rel="stylesheet" type="text/css"  href="style/main.css"/>
      <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> 
 
      <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.5/angular-material.min.css" />
      
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
      <link rel="stylesheet" href="style/bootsrap.css"/>
      
      
         
</head>

<style type="text/css">



 input:required:invalid, input:focus:invalid {
   	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAeVJREFUeNqkU01oE1EQ/mazSTdRmqSxLVSJVKU9RYoHD8WfHr16kh5EFA8eSy6hXrwUPBSKZ6E9V1CU4tGf0DZWDEQrGkhprRDbCvlpavan3ezu+LLSUnADLZnHwHvzmJlvvpkhZkY7IqFNaTuAfPhhP/8Uo87SGSaDsP27hgYM/lUpy6lHdqsAtM+BPfvqKp3ufYKwcgmWCug6oKmrrG3PoaqngWjdd/922hOBs5C/jJA6x7AiUt8VYVUAVQXXShfIqCYRMZO8/N1N+B8H1sOUwivpSUSVCJ2MAjtVwBAIdv+AQkHQqbOgc+fBvorjyQENDcch16/BtkQdAlC4E6jrYHGgGU18Io3gmhzJuwub6/fQJYNi/YBpCifhbDaAPXFvCBVxXbvfbNGFeN8DkjogWAd8DljV3KRutcEAeHMN/HXZ4p9bhncJHCyhNx52R0Kv/XNuQvYBnM+CP7xddXL5KaJw0TMAF8qjnMvegeK/SLHubhpKDKIrJDlvXoMX3y9xcSMZyBQ+tpyk5hzsa2Ns7LGdfWdbL6fZvHn92d7dgROH/730YBLtiZmEdGPkFnhX4kxmjVe2xgPfCtrRd6GHRtEh9zsL8xVe+pwSzj+OtwvletZZ/wLeKD71L+ZeHHWZ/gowABkp7AwwnEjFAAAAAElFTkSuQmCC);
  	background-position: right top;
    background-repeat: no-repeat;
    background-size: 17px 17px;
    -moz-box-shadow: none;
  }
  input:required:valid {
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAwFBMVEU5tUr///86rkk6rUk5tEo6r0k6sEk6sUk5s0rz8/M5skr39/f8/Pz29vYys0QnrDvB38UnsTwuqz8gsDcts0H//P/x+vLG6MomrDqFzo77/vuc16NwxntFuVUZrzPs+O3U7tdDuVNPtVxPvF7e8uFfwmyR0pl6x4Or3LG24bvu8+/j7eTX69mBxYpwwnrM5tC12bmh2aij06iSzJmz4LhJslbf7eCGx41nwnKN0pWm06vO5NDM6tBZwGZ1yYC95MKobgmfAAAUQElEQVR4nN1d6ULbuhKWQxaTRUHesjkkJglJgJZCWw7cQ8r7v9W1Lcu2ZMmW16QnP4qgY2u+zIxmpBlJQKn7M59vJ8fDYmbvTpul47QdZ7k57ezZ4nCcbOfz2vsH+MdwqAgaQ05Dgtb713qbLPY7R9cRMiCEwPu01bb/0/3dQEjXnd1+Mdlaud6bj02McDgIfh0MFPoPKQ0xrWkq26/ZzkHTAFi73weAaqg9NfiLBhFydrOvJ1OR5iEPmz7CwYg8GTSGI/IK0kiQuA1FQPv0aqsGcrF1O1hg7W4XN9ROD+PqdwKkvY6KRTrWVPuwjXMfvVe+6ySbQw/h4Dr4bRQ0htfkFdfkSdIgJMr1tULTYhJr8tGZjrFGdlpYTu1WgFRtdTGubqtHGhhpu9NqA2jo4OPHY2bXedgcegjD7+g6F8AEramsjraBtABXDGCLAOwEgrsiSK/6DK02hvZxlfjiMroWsumqKogUNp8EWVrz4csGyNXMKwLwqgBAjxYiYH89mFVI0FNeEI45nK8m5WtkSIb/7hGCPtNCCV6FAHs0QF9FKVqojWdP5SXokwTeopyKWoe7KQyYlgAokqBKaPsuCdRPB8ssA3CI5UAQlgC4/VC1UCoSNtgTAGzHAXqCRL1/3spIEDcAi1tGReO0T7YBWwmpFLVBDJDQtjTjz1YpaIOETVDgq4l9B+udDmNMF7JBxqX0yet8Wqjv1oNigz3+iREWG0UHytrWoct9NsBsCaqMikZfBpx+XxcBGNBSHj8fQHNre6NnNQA5KhpJGyJ7WxighzAM1fLY4OBx5nuHfjbTqQAZN5EE2PFoIfpwx9UCACmPn0u5r18Ng2G6KjfBAAzCOqi95nUTQbjqIlRknwy/GuXmDgXc51FRkZtIARgOSK0O2nzmlaD/P8Qf5nly/i0IrTkAq3ETlIqCMJqbzZX87prMgHOo6ETTEkyXCtU6QoCtPvU6A96Y0mwSSCCvBK39NFSlhmwwphDTvZVPggHCHAA/l1rAtJprFJUN1bg2GHudsfzMB9BHmMNN3KNOEmADNhi9DqL7aEYvEZjjGbCsDa42Uw7T+VW0HakoN1TDr+MCdD/ju7d0NimAngzDhZEs2U8gIL3kc/Sl3AT7uv6VBiayEsQeP3SIGQBfUNhL7SqaBtB9HXqRAuhHa0mPL5D93Ebtq6QEq5jRy9gg0zWyH0aZKjpgPH66Da6WRh6pVO4m2O9W+/1IszkUAIw8frqKPjmQp6LN22DYteZs01WUQGI8vgDgBHIBNuomaIA9AOFEankDyKjor1uu2lUTqmWrKMf8fdrbo5lhgwRhlooe9FxSqd0Gu4R2+j9FIMEQSeDx0wG+IG4vDYZqAoCd1vQlQ0WHeAacDvA+BvBSbDDsGi1SJTigPL7ABu/1ojYoDtUqUVH8On2RYoO0x5dQ0QZn9LIAXd//agol6CMhHl80yPBt8DJU1KftjA8CGxxQa94iNxFTURkJ1huqcSTo0gL9mCJBglAAcHLLlUpjM3oZgC7t7SQFYJDlFoRqkGuDZwzVWBUNaOHTQKCiAUIBwFV6LHoBNkhowVUyNU6mTJTHZ6ZLS1hZqJbp6HMBTHbd28z5KsrPcgce1DYKqmgBGyziJugFKpsPkPL44hl92dxE2Rk9DZBvHe6sP6miI8rjs2syqJ3H0aeNogXcRG6ALsSbUQIg5fHZVbWiNthIqMbrWu2+cQGyWW4SxW7ARYdqvK61ExdggDCx8DtNscELchMUrT/PSACks9wkTP9ExSKZCpIvkqEa1zrQJwcgXddGki/LThU2WG+oBpLKA5cPSYDcurb9eZMvIoCpKup/tL3JAKQ9PgE4mV5iqNbtQgNm0aIJBXDI1rXhn3MtmR/Mv+hULvmSBKi13j8+3oEB0rs25nGA/Lo25VuQ4a27TiaXio5/rvwRYqGldO32aMwy69qUm3GVACsK1cY3AZvmDRTS+j2itUkDJAij5ao7YRGCvJuoOlTTAoAe02sD8FUUcwfv2DwGqWsLXjF4Rfwnz+kmYgBd7l2IaRFluGxD1bVFEnwwAu4vKFQbUwBdiDAl095qQysOkK1rM2eG4MnzuYkpA1BRnrUEbYxN4yOulEFdG3nFYIsYps8fqmksQFcO69SKM7SNWR32+OQVI8WGclIpaYPFBpmYk/MgCruG9kBY17ZGoCobrChUY22QLBuuoZhNoP0bKqXC1LV5IryolW0RQM8WE6XlIZuaHaeN17Wt9UsL1YQAB+a/hphNfR2jjWe5d1CqyqIxN5EYRamV7bV4ewfc8evanvT6ZxNV2CBZNnSHG5Ec9C23rs1Osd4zJF9SVDRYNlwLt1jBPyanrm1rXHCoxgOomD+mIja1NwIwVtf2AS/JBrNU1G+Y71DApnaPH4qveVtqXTtfqnUTg3jhyBcSsulFp3SW+6CVs0FCW0GdjJSK+o2VLmITHRJ1bXeZ0UlzoZqkBF3FQyI24Ymta/t3minBi3ETUWOlC9nUn0ZUXZu5r2D/YLM26H2+xkI2tW/BymLg8R9QYYBVJ19kbdD77LpCNlvjeYTQnXH5g1K5hd8m3UTQOI5T2ERf/tMky23TQd7Fuwn/c6O1Be/1uoa2EqtrWwFQ2E00HKqlAWS6XsWy3Ef014RqEcBxBkB0NCOPb0M5N3GmGb0cQJZN+H0UZrkt428K1SRsEL+390DGUmWCmt/5UrOKeu+dTkKEH9rfMpsIJSjDpr9yihGGq1R/Ragmp6Je1w5B+DSVCLabS76UdxOETX2LPb75Os4GeEluQhYgMA6m7w9Nmz0w57JtMC1Uo7uG303sD/ETl5R8KWiDSTaxHW4NLtMlky91hWoybiJk09j6CPFix19igwkVTRsLVW9+4SKcwb8nVJN0E4RNr3AB+Iv5/6FQjWLTW94HiuX8p0I1mk3HchG+oe756mTyjaL5Lcl4cxFOpp3/VKhGsTmeuAgXMI/sL3dGz2FTbWkLF2GwjnjpbiIFYMpQAfcuwh3kc/S3hmoUm+5gCuaO6Mm/NlSLs+nMwVwHeW2w0eQLlqC8Dca+A6+hz8FWv+zki8JzE+JQrcWwqW/BBBUN1ZpzEyUiSjQBR9TudPIpN0QIwU4ZGyznB3N4M3V8BAej381SUSr5Ao33X583/3s3GIBnn9FzIkq1Cw9gAfv5xt/NWlHMkal8OrCJUK2gm8Bdt/rGC5hhhy+rosbJIhytlvDyZhOAGSrgB7BJ1bOUchunecSR9VsTAjxfqEYvPMA/YMc+meYmjE0MoGI+/iZrWM3P6CXztHAHThRH6bKPqajPiGktIajRTZSywaDrE9gAaRuElARxuccSXq4N+p8NWKY9SdXJGEmAri1utAuY0TOhWqzr9gY4Loww/Eq1wRMHoKIQW6xfReVDtajrvroETrvfywLIs0Gyw320WmZuuGo6VItC5q7aBm21L34y0hOODfoAXV694ea8kYxw4aHTBy7CdpYEk26COYTB2sDKAeYvqeMt/rkkKlDFEsy0QbJLzLrqXp6bCGgd4GRIEKso3wbDnZqPvi2ef0YPkrM6x/MWWTFQig36n+trT1EvJVRj5uUb3+N302f0SOK+CXe4OUPyRewmQtqTF5f2k9XBcdl7Cf/ss1tXy+7Z3EQKwPbOnVuEl78IlButJQBGrv8SQrWQtt+2wR72+vST7PiLLJlD2q8Hqw1zSHvtyRdRqBZNW7twBu6hmiF740EG4Mi3xTMkX9IW/zqqcQ8ORpbs0RvLiOjEydVSO2eollwbU4FxAEd6hxQnBtJeJQG6EH9rDMBmZ/QMwL5XoOitl2bIvufIAhyOHn1bbHJGn7EAjyb+mneGcvv1YTIAXRLLW4E7e6hGJOiveVu32cqNFnIA/RU4B54/VIvWxnQLzN0/t7JiIP3nQBJgfAWu+Rl9Yn0aqHOgnNrdbNmPf8qoqP8JV+DKVfyWCtXCxT915+eARaWX8Sf1hSTAwbWJZ/1nmNEn1qc72t7P4wufjPfincKUraLRCtwZZvTJBfgO9PL4eDNJ9s4XfWHKSDBYgatJRfOW86AvFyE+R0Fi/J0GtihzLdHb2d0Efh3aejVRbSA5/k4XMirqfUbh0b6Nzybo9Wm/JkrZQVnl5p+8LJ55nCtUC1+H69qUGZSuk3FH1Bw3mJ0tVAtfZ8z889qOiU184joZ/Sd78nLVAKuzQeBvDPIQPrHOKE3240W9KlptOQ98wju7sparKNn7rr8qgPWEaiFtuxXs7PJ3tUuXcrkQG7DBIqFa4svoflfwjhJvmp+jTgYtzGoAlnMTEvVK2iFA6E4R8yh3m4Th53AT8my635e+DRAqqV8NJwGKw/CyEqwm+ZJazqMQhDNN0gbJkx7Ei3YT/uvgLET4Y5xH9t4P1xYv2k34r/NOwgzOa3uE0k8SkulP0bXSzSdfRFWf0IpOb7Gh5JNR8iUIw2t3E8Urr+Gf2Hltv1D+kmZ9UVyC9dsgYPZyr2CBkmbeCtzZp0vx1z36PQZnm7yDAlX3yRW46kO14ht0oG36PQYIf2j5VBSTsCtwVatooVCNvG78A4+FwektD+MCAK96aFEnwFIqqmoPSoRweB2cTyPhJuj0WXwF7uwzeopN7ZsSIfQ48k4UzGODJDcRrcCdf0ZPezN8XXnsjpINzKuiuDENw/DLCNUC2t4mABid13ZAhUqaO1doUVxF69ugg/zjhKk7SqxeKz0G4gN0GcFheD0z+uIbdJLntZn/aAUk6H/T+kva5W6k8dxIqBbQGvd+10MldivZ6IlJd0upKGZEv0m5+yxoPHTrntHH2fS2qQddx24l88PvYnefaetMgHeiQtv0qs+g65wRpXdGFOk6divZk56v6j72TQNjnRdgDTP6iFbfRl3HbyXbwVwqSqmSwSucKijB0pvk4C7qmrqHdD0toqIBrbE2KwVYXEVdEa5N0jVzD+k7YJ5MdxMUbZucMV1WRcuEaphN+B4CZG4lGz0bqV9NGkC11YXr/AArDtUwrfEcyixxK5k3nBZRUUzrDTccgMLtCrXYoHcqTThtTd5KtuVtKJUF6L470n8pG6w6VMON6RN1nw5zK9kMFrHBkHb8zK7A5bHBIsmXJJvhsZ78W8ksjd0xK2WDhGni+osAzFUnI5ZgT7NoSMytZGZwR0kBFQX+eee+66/KTeS3QdAN7igR30o238CiALFduSNqSTdRxga7nbs5DTB5K9knyqmi7NBvkDBceWw2VPNe1xqvGYCcW8n2RjEbjAzhBVvCj6uG3YT7OpyMEd1KFvx5bhS1QUJrOPbLYf+7qVW1GJt9/86uCCB7K1nQIKfVF1FRgA/RhIahJc71KReqSS086BN6WBlF3iJeCGTuDVmAOZx3E4dJG/s4QMGtZN4UyHL4ALOv56vrzgG5eTl0LNox8Dw+nuNNUFGA/Sza+myw52dEkxWiICFB73M/LmaDEipaSajGB3ifVFGF8fhRncwdu29CygYLqGip5AsFEJ54EmRvJQv3TbzB8MmgFwk3UcIGS4ZqHi1ccQHiW8k4xXj+RpMUgAI3wQVYU6imUgA9I2TvxOXcShYCVJQFKuAHZWmrCNUYgC8cG0zeShYDqCi2lm2Dtd8iKKui3t3qvB2DkbfgFOM9/A6X+XMAPM8hmt5OZdFGAiACOBg9tgU38VRtg6VPR4POimODASQgAug2/GsGqwrV6pnR46HCnZMmbZBAAmKAbmxze/mhmvvv7SRlr0v8VjJOMd6vaZ5B5iyhGgD6r7TNPLFbyXjVhubrlOmlKhusZEbv06JDiopSt5Jxyyk9t1h9qFalirqOMEWC1Jq3oF70Xr9sFV2kAKSz3MKC2HvUqd1N5E6+hBJciN1EgCTy+KKKX+Unuam76hl9BTb4kmKDdJY7BeD14KCLehECbMYG9UOailJZ7jSA7lfz6/YSQzVwm+Ym6Cx3BkDX9RvtamywmuQLDtXgJFtFFTrLzQWIG+srTVoqdYVq9HQJOmmhWoiEyXILAQ4Hjxt2ifG8M3pjs5JR0fBWsiyA7pNzG5WcD1aVfPEnvPY8000obJabV7NNPblAMhJsJFTjz+iTACmPny5B//PVDVfgzhqqeWOMjIoydW3ZAIeDtxOiAZ5n4ffEXVUTAIzXtbEAebK/l71kr7bkC6QWftNskFrzzrTBkGTiaOdNvjgTGTZjaV9A/Vni3IvHPRIBrN8G+/qel3xJA6iAnAAH1+bE8NdvmrfBFjQm6WwmAdIeX1a55zMEaw/VOEUI4xmV4ZVRUW6WW+ar+bzLvEGy6uRLb3y3zssmp64tU0VJ2c31wVfV5kI1TTvMM9hMAuTUtck+6TYsV1VB3cmXsBhv+s1KsKnIsJmoa5NS7oB2a2u9RkI1aHyni/FysclkuWUliP/y/K5TFVT1lDTr9vPALM4myHgyaYOUnqx3Oqx1Rg9vd2uTM9jLy4Gta1NknyR64uqqxjBdXfLFMOytYkpGlHyAybq2nADdcfXpAyDeVRdld770EPjYxngoYIP8ujYpFaV7sQ4bXWtVaoOqNt0cLIUDMJccaI9fGKD3ef42RpALsIgNwrH2janFLQSQqmvLI3sO7cicf71DBCtwExAB+8cDw0MRFR2K6tqKAMS0q+N3bUpfTJv7LAsE/xwfTdGBDQXY5NW1FQXo3bDzMJkB3YCdTgJgpoq2u5oOZhNLSRb8lwAYevywVmjANEi1TdTIpN0evrcMBGHMTcSK3YmZkupM/7Yp6NJ3vh+2ZbuOVQcR2mCOTx6IGkOmMYgambSukm2/ZjsHjTWIj6Mnx2v2o4ZKBk2IkLObHZ/YtxTqOkEbrOqHzkOmIU1ivX0t9jug62gMDR9qO7hbCrTb0DAQ0nV1t198bV23YFbUdYIWxH6LNRRRIw8t/syt7eR4uJ/Zu9Nm6fRV1XE2p509uz8cJ1trXk/XMZL/A2Boqujcj9ZZAAAAAElFTkSuQmCC);
    background-position: right top;
 	background-size: 17px 17px;
    background-repeat: no-repeat;
  }

 textarea:required:invalid, textarea:focus:invalid {
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAeVJREFUeNqkU01oE1EQ/mazSTdRmqSxLVSJVKU9RYoHD8WfHr16kh5EFA8eSy6hXrwUPBSKZ6E9V1CU4tGf0DZWDEQrGkhprRDbCvlpavan3ezu+LLSUnADLZnHwHvzmJlvvpkhZkY7IqFNaTuAfPhhP/8Uo87SGSaDsP27hgYM/lUpy6lHdqsAtM+BPfvqKp3ufYKwcgmWCug6oKmrrG3PoaqngWjdd/922hOBs5C/jJA6x7AiUt8VYVUAVQXXShfIqCYRMZO8/N1N+B8H1sOUwivpSUSVCJ2MAjtVwBAIdv+AQkHQqbOgc+fBvorjyQENDcch16/BtkQdAlC4E6jrYHGgGU18Io3gmhzJuwub6/fQJYNi/YBpCifhbDaAPXFvCBVxXbvfbNGFeN8DkjogWAd8DljV3KRutcEAeHMN/HXZ4p9bhncJHCyhNx52R0Kv/XNuQvYBnM+CP7xddXL5KaJw0TMAF8qjnMvegeK/SLHubhpKDKIrJDlvXoMX3y9xcSMZyBQ+tpyk5hzsa2Ns7LGdfWdbL6fZvHn92d7dgROH/730YBLtiZmEdGPkFnhX4kxmjVe2xgPfCtrRd6GHRtEh9zsL8xVe+pwSzj+OtwvletZZ/wLeKD71L+ZeHHWZ/gowABkp7AwwnEjFAAAAAElFTkSuQmCC);
  	background-position: right top;
    background-repeat: no-repeat;
    background-size: 17px 17px;
    -moz-box-shadow: none;
  }
  textarea:required:valid {
   	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAwFBMVEU5tUr///86rkk6rUk5tEo6r0k6sEk6sUk5s0rz8/M5skr39/f8/Pz29vYys0QnrDvB38UnsTwuqz8gsDcts0H//P/x+vLG6MomrDqFzo77/vuc16NwxntFuVUZrzPs+O3U7tdDuVNPtVxPvF7e8uFfwmyR0pl6x4Or3LG24bvu8+/j7eTX69mBxYpwwnrM5tC12bmh2aij06iSzJmz4LhJslbf7eCGx41nwnKN0pWm06vO5NDM6tBZwGZ1yYC95MKobgmfAAAUQElEQVR4nN1d6ULbuhKWQxaTRUHesjkkJglJgJZCWw7cQ8r7v9W1Lcu2ZMmW16QnP4qgY2u+zIxmpBlJQKn7M59vJ8fDYmbvTpul47QdZ7k57ezZ4nCcbOfz2vsH+MdwqAgaQ05Dgtb713qbLPY7R9cRMiCEwPu01bb/0/3dQEjXnd1+Mdlaud6bj02McDgIfh0MFPoPKQ0xrWkq26/ZzkHTAFi73weAaqg9NfiLBhFydrOvJ1OR5iEPmz7CwYg8GTSGI/IK0kiQuA1FQPv0aqsGcrF1O1hg7W4XN9ROD+PqdwKkvY6KRTrWVPuwjXMfvVe+6ySbQw/h4Dr4bRQ0htfkFdfkSdIgJMr1tULTYhJr8tGZjrFGdlpYTu1WgFRtdTGubqtHGhhpu9NqA2jo4OPHY2bXedgcegjD7+g6F8AEramsjraBtABXDGCLAOwEgrsiSK/6DK02hvZxlfjiMroWsumqKogUNp8EWVrz4csGyNXMKwLwqgBAjxYiYH89mFVI0FNeEI45nK8m5WtkSIb/7hGCPtNCCV6FAHs0QF9FKVqojWdP5SXokwTeopyKWoe7KQyYlgAokqBKaPsuCdRPB8ssA3CI5UAQlgC4/VC1UCoSNtgTAGzHAXqCRL1/3spIEDcAi1tGReO0T7YBWwmpFLVBDJDQtjTjz1YpaIOETVDgq4l9B+udDmNMF7JBxqX0yet8Wqjv1oNigz3+iREWG0UHytrWoct9NsBsCaqMikZfBpx+XxcBGNBSHj8fQHNre6NnNQA5KhpJGyJ7WxighzAM1fLY4OBx5nuHfjbTqQAZN5EE2PFoIfpwx9UCACmPn0u5r18Ng2G6KjfBAAzCOqi95nUTQbjqIlRknwy/GuXmDgXc51FRkZtIARgOSK0O2nzmlaD/P8Qf5nly/i0IrTkAq3ETlIqCMJqbzZX87prMgHOo6ETTEkyXCtU6QoCtPvU6A96Y0mwSSCCvBK39NFSlhmwwphDTvZVPggHCHAA/l1rAtJprFJUN1bg2GHudsfzMB9BHmMNN3KNOEmADNhi9DqL7aEYvEZjjGbCsDa42Uw7T+VW0HakoN1TDr+MCdD/ju7d0NimAngzDhZEs2U8gIL3kc/Sl3AT7uv6VBiayEsQeP3SIGQBfUNhL7SqaBtB9HXqRAuhHa0mPL5D93Ebtq6QEq5jRy9gg0zWyH0aZKjpgPH66Da6WRh6pVO4m2O9W+/1IszkUAIw8frqKPjmQp6LN22DYteZs01WUQGI8vgDgBHIBNuomaIA9AOFEankDyKjor1uu2lUTqmWrKMf8fdrbo5lhgwRhlooe9FxSqd0Gu4R2+j9FIMEQSeDx0wG+IG4vDYZqAoCd1vQlQ0WHeAacDvA+BvBSbDDsGi1SJTigPL7ABu/1ojYoDtUqUVH8On2RYoO0x5dQ0QZn9LIAXd//agol6CMhHl80yPBt8DJU1KftjA8CGxxQa94iNxFTURkJ1huqcSTo0gL9mCJBglAAcHLLlUpjM3oZgC7t7SQFYJDlFoRqkGuDZwzVWBUNaOHTQKCiAUIBwFV6LHoBNkhowVUyNU6mTJTHZ6ZLS1hZqJbp6HMBTHbd28z5KsrPcgce1DYKqmgBGyziJugFKpsPkPL44hl92dxE2Rk9DZBvHe6sP6miI8rjs2syqJ3H0aeNogXcRG6ALsSbUQIg5fHZVbWiNthIqMbrWu2+cQGyWW4SxW7ARYdqvK61ExdggDCx8DtNscELchMUrT/PSACks9wkTP9ExSKZCpIvkqEa1zrQJwcgXddGki/LThU2WG+oBpLKA5cPSYDcurb9eZMvIoCpKup/tL3JAKQ9PgE4mV5iqNbtQgNm0aIJBXDI1rXhn3MtmR/Mv+hULvmSBKi13j8+3oEB0rs25nGA/Lo25VuQ4a27TiaXio5/rvwRYqGldO32aMwy69qUm3GVACsK1cY3AZvmDRTS+j2itUkDJAij5ao7YRGCvJuoOlTTAoAe02sD8FUUcwfv2DwGqWsLXjF4Rfwnz+kmYgBd7l2IaRFluGxD1bVFEnwwAu4vKFQbUwBdiDAl095qQysOkK1rM2eG4MnzuYkpA1BRnrUEbYxN4yOulEFdG3nFYIsYps8fqmksQFcO69SKM7SNWR32+OQVI8WGclIpaYPFBpmYk/MgCruG9kBY17ZGoCobrChUY22QLBuuoZhNoP0bKqXC1LV5IryolW0RQM8WE6XlIZuaHaeN17Wt9UsL1YQAB+a/hphNfR2jjWe5d1CqyqIxN5EYRamV7bV4ewfc8evanvT6ZxNV2CBZNnSHG5Ec9C23rs1Osd4zJF9SVDRYNlwLt1jBPyanrm1rXHCoxgOomD+mIja1NwIwVtf2AS/JBrNU1G+Y71DApnaPH4qveVtqXTtfqnUTg3jhyBcSsulFp3SW+6CVs0FCW0GdjJSK+o2VLmITHRJ1bXeZ0UlzoZqkBF3FQyI24Ymta/t3minBi3ETUWOlC9nUn0ZUXZu5r2D/YLM26H2+xkI2tW/BymLg8R9QYYBVJ19kbdD77LpCNlvjeYTQnXH5g1K5hd8m3UTQOI5T2ERf/tMky23TQd7Fuwn/c6O1Be/1uoa2EqtrWwFQ2E00HKqlAWS6XsWy3Ef014RqEcBxBkB0NCOPb0M5N3GmGb0cQJZN+H0UZrkt428K1SRsEL+390DGUmWCmt/5UrOKeu+dTkKEH9rfMpsIJSjDpr9yihGGq1R/Ragmp6Je1w5B+DSVCLabS76UdxOETX2LPb75Os4GeEluQhYgMA6m7w9Nmz0w57JtMC1Uo7uG303sD/ETl5R8KWiDSTaxHW4NLtMlky91hWoybiJk09j6CPFix19igwkVTRsLVW9+4SKcwb8nVJN0E4RNr3AB+Iv5/6FQjWLTW94HiuX8p0I1mk3HchG+oe756mTyjaL5Lcl4cxFOpp3/VKhGsTmeuAgXMI/sL3dGz2FTbWkLF2GwjnjpbiIFYMpQAfcuwh3kc/S3hmoUm+5gCuaO6Mm/NlSLs+nMwVwHeW2w0eQLlqC8Dca+A6+hz8FWv+zki8JzE+JQrcWwqW/BBBUN1ZpzEyUiSjQBR9TudPIpN0QIwU4ZGyznB3N4M3V8BAej381SUSr5Ao33X583/3s3GIBnn9FzIkq1Cw9gAfv5xt/NWlHMkal8OrCJUK2gm8Bdt/rGC5hhhy+rosbJIhytlvDyZhOAGSrgB7BJ1bOUchunecSR9VsTAjxfqEYvPMA/YMc+meYmjE0MoGI+/iZrWM3P6CXztHAHThRH6bKPqajPiGktIajRTZSywaDrE9gAaRuElARxuccSXq4N+p8NWKY9SdXJGEmAri1utAuY0TOhWqzr9gY4Loww/Eq1wRMHoKIQW6xfReVDtajrvroETrvfywLIs0Gyw320WmZuuGo6VItC5q7aBm21L34y0hOODfoAXV694ea8kYxw4aHTBy7CdpYEk26COYTB2sDKAeYvqeMt/rkkKlDFEsy0QbJLzLrqXp6bCGgd4GRIEKso3wbDnZqPvi2ef0YPkrM6x/MWWTFQig36n+trT1EvJVRj5uUb3+N302f0SOK+CXe4OUPyRewmQtqTF5f2k9XBcdl7Cf/ss1tXy+7Z3EQKwPbOnVuEl78IlButJQBGrv8SQrWQtt+2wR72+vST7PiLLJlD2q8Hqw1zSHvtyRdRqBZNW7twBu6hmiF740EG4Mi3xTMkX9IW/zqqcQ8ORpbs0RvLiOjEydVSO2eollwbU4FxAEd6hxQnBtJeJQG6EH9rDMBmZ/QMwL5XoOitl2bIvufIAhyOHn1bbHJGn7EAjyb+mneGcvv1YTIAXRLLW4E7e6hGJOiveVu32cqNFnIA/RU4B54/VIvWxnQLzN0/t7JiIP3nQBJgfAWu+Rl9Yn0aqHOgnNrdbNmPf8qoqP8JV+DKVfyWCtXCxT915+eARaWX8Sf1hSTAwbWJZ/1nmNEn1qc72t7P4wufjPfincKUraLRCtwZZvTJBfgO9PL4eDNJ9s4XfWHKSDBYgatJRfOW86AvFyE+R0Fi/J0GtihzLdHb2d0Efh3aejVRbSA5/k4XMirqfUbh0b6Nzybo9Wm/JkrZQVnl5p+8LJ55nCtUC1+H69qUGZSuk3FH1Bw3mJ0tVAtfZ8z889qOiU184joZ/Sd78nLVAKuzQeBvDPIQPrHOKE3240W9KlptOQ98wju7sparKNn7rr8qgPWEaiFtuxXs7PJ3tUuXcrkQG7DBIqFa4svoflfwjhJvmp+jTgYtzGoAlnMTEvVK2iFA6E4R8yh3m4Th53AT8my635e+DRAqqV8NJwGKw/CyEqwm+ZJazqMQhDNN0gbJkx7Ei3YT/uvgLET4Y5xH9t4P1xYv2k34r/NOwgzOa3uE0k8SkulP0bXSzSdfRFWf0IpOb7Gh5JNR8iUIw2t3E8Urr+Gf2Hltv1D+kmZ9UVyC9dsgYPZyr2CBkmbeCtzZp0vx1z36PQZnm7yDAlX3yRW46kO14ht0oG36PQYIf2j5VBSTsCtwVatooVCNvG78A4+FwektD+MCAK96aFEnwFIqqmoPSoRweB2cTyPhJuj0WXwF7uwzeopN7ZsSIfQ48k4UzGODJDcRrcCdf0ZPezN8XXnsjpINzKuiuDENw/DLCNUC2t4mABid13ZAhUqaO1doUVxF69ugg/zjhKk7SqxeKz0G4gN0GcFheD0z+uIbdJLntZn/aAUk6H/T+kva5W6k8dxIqBbQGvd+10MldivZ6IlJd0upKGZEv0m5+yxoPHTrntHH2fS2qQddx24l88PvYnefaetMgHeiQtv0qs+g65wRpXdGFOk6divZk56v6j72TQNjnRdgDTP6iFbfRl3HbyXbwVwqSqmSwSucKijB0pvk4C7qmrqHdD0toqIBrbE2KwVYXEVdEa5N0jVzD+k7YJ5MdxMUbZucMV1WRcuEaphN+B4CZG4lGz0bqV9NGkC11YXr/AArDtUwrfEcyixxK5k3nBZRUUzrDTccgMLtCrXYoHcqTThtTd5KtuVtKJUF6L470n8pG6w6VMON6RN1nw5zK9kMFrHBkHb8zK7A5bHBIsmXJJvhsZ78W8ksjd0xK2WDhGni+osAzFUnI5ZgT7NoSMytZGZwR0kBFQX+eee+66/KTeS3QdAN7igR30o238CiALFduSNqSTdRxga7nbs5DTB5K9knyqmi7NBvkDBceWw2VPNe1xqvGYCcW8n2RjEbjAzhBVvCj6uG3YT7OpyMEd1KFvx5bhS1QUJrOPbLYf+7qVW1GJt9/86uCCB7K1nQIKfVF1FRgA/RhIahJc71KReqSS086BN6WBlF3iJeCGTuDVmAOZx3E4dJG/s4QMGtZN4UyHL4ALOv56vrzgG5eTl0LNox8Dw+nuNNUFGA/Sza+myw52dEkxWiICFB73M/LmaDEipaSajGB3ifVFGF8fhRncwdu29CygYLqGip5AsFEJ54EmRvJQv3TbzB8MmgFwk3UcIGS4ZqHi1ccQHiW8k4xXj+RpMUgAI3wQVYU6imUgA9I2TvxOXcShYCVJQFKuAHZWmrCNUYgC8cG0zeShYDqCi2lm2Dtd8iKKui3t3qvB2DkbfgFOM9/A6X+XMAPM8hmt5OZdFGAiACOBg9tgU38VRtg6VPR4POimODASQgAug2/GsGqwrV6pnR46HCnZMmbZBAAmKAbmxze/mhmvvv7SRlr0v8VjJOMd6vaZ5B5iyhGgD6r7TNPLFbyXjVhubrlOmlKhusZEbv06JDiopSt5Jxyyk9t1h9qFalirqOMEWC1Jq3oF70Xr9sFV2kAKSz3MKC2HvUqd1N5E6+hBJciN1EgCTy+KKKX+Unuam76hl9BTb4kmKDdJY7BeD14KCLehECbMYG9UOailJZ7jSA7lfz6/YSQzVwm+Ym6Cx3BkDX9RvtamywmuQLDtXgJFtFFTrLzQWIG+srTVoqdYVq9HQJOmmhWoiEyXILAQ4Hjxt2ifG8M3pjs5JR0fBWsiyA7pNzG5WcD1aVfPEnvPY8000obJabV7NNPblAMhJsJFTjz+iTACmPny5B//PVDVfgzhqqeWOMjIoydW3ZAIeDtxOiAZ5n4ffEXVUTAIzXtbEAebK/l71kr7bkC6QWftNskFrzzrTBkGTiaOdNvjgTGTZjaV9A/Vni3IvHPRIBrN8G+/qel3xJA6iAnAAH1+bE8NdvmrfBFjQm6WwmAdIeX1a55zMEaw/VOEUI4xmV4ZVRUW6WW+ar+bzLvEGy6uRLb3y3zssmp64tU0VJ2c31wVfV5kI1TTvMM9hMAuTUtck+6TYsV1VB3cmXsBhv+s1KsKnIsJmoa5NS7oB2a2u9RkI1aHyni/FysclkuWUliP/y/K5TFVT1lDTr9vPALM4myHgyaYOUnqx3Oqx1Rg9vd2uTM9jLy4Gta1NknyR64uqqxjBdXfLFMOytYkpGlHyAybq2nADdcfXpAyDeVRdld770EPjYxngoYIP8ujYpFaV7sQ4bXWtVaoOqNt0cLIUDMJccaI9fGKD3ef42RpALsIgNwrH2janFLQSQqmvLI3sO7cicf71DBCtwExAB+8cDw0MRFR2K6tqKAMS0q+N3bUpfTJv7LAsE/xwfTdGBDQXY5NW1FQXo3bDzMJkB3YCdTgJgpoq2u5oOZhNLSRb8lwAYevywVmjANEi1TdTIpN0evrcMBGHMTcSK3YmZkupM/7Yp6NJ3vh+2ZbuOVQcR2mCOTx6IGkOmMYgambSukm2/ZjsHjTWIj6Mnx2v2o4ZKBk2IkLObHZ/YtxTqOkEbrOqHzkOmIU1ivX0t9jug62gMDR9qO7hbCrTb0DAQ0nV1t198bV23YFbUdYIWxH6LNRRRIw8t/syt7eR4uJ/Zu9Nm6fRV1XE2p509uz8cJ1trXk/XMZL/A2Boqujcj9ZZAAAAAElFTkSuQmCC);
    background-position: right top;
    background-repeat: no-repeat;
    background-size: 17px 17px;
  }
  
.signature {
  height: 60px;
  width: 250px;
  border: 1px solid black;
}

.signature canvas {
  height: 100%;
  width: 100%;
}

[ng\:cloak],[ng-cloak],.ng-cloak {
	display: none !important;
}

ul {
	list-style-type: none;
}

.mega {
	width: 200px;
}



tbody div{
    overflow:scroll;
    height:480px;
}
table.fixed { table-layout:fixed; }
table.fixed td { overflow: hidden; }

.grid {
	padding: 10px 15px;
	background-color: gray;
	color: white;
	border: 1px solid black;
}

.grid ul {
	display: none;
}

.grid:hover>ul {
	display: inline;
}

.dropdown-submenu {
	position: relative;
}

.dropdown-submenu>.dropdown-menu {
	top: 0;
	left: 100%;
	margin-top: -6px;
	margin-left: -1px;
	-webkit-border-radius: 0 6px 6px 6px;
	-moz-border-radius: 0 6px 6px 6px;
	border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
	display: block;
}

.dropdown-submenu>a:after {
	display: block;
	content: " ";
	float: right;
	width: 0;
	height: 0;
	border-color: transparent;
	border-style: solid;
	border-width: 5px 0 5px 5px;
	border-left-color: #cccccc;
	margin-top: 5px;
	margin-right: -10px;
}

 .dropdown-submenu:hover>a:after { 
	border-left-color: #ffffff; 
 }
.dropdown-submenu.pull-left {
	float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
	left: -100%;
	margin-left: 10px;
	-webkit-border-radius: 6px 0 6px 6px;
	-moz-border-radius: 6px 0 6px 6px;
	border-radius: 6px 0 6px 6px;
}










/* Its tab background color */
 .navbar-inverse{ 

      background-color: rgba(34, 34, 34, 0.83);  
     border-color: #080808; 
     height:50px;

  } 


/* its tab default name colors */
 .navbar-inverse .navbar-nav>li>a{ 
 
	color:rgb(250,250,250);

}

/*  active tab name color */
 .navbar-inverse .navbar-nav>li>a:focus, .navbar-inverse .navbar-nav>li>a:hover { 

 	color: white;
 	font-weight: 700; 
 	font-size :110%;
 	background-color: none; 
 } 
 
 #font {
 
	 font-family: "Trebuchet MS", Helvetica, sans-serif;
	     color: rgb(0, 0, 0);

 }
</style>


<body  id= "font" style="margin:15px;width: 90%;" ng-cloak class="ng-cloak"  >

	<div ng-controller="logoutCtrl"  style="margin-bottom: -10px;width: 96%; margin-left: 75px;">
		<div class="container" style="width:100%;">
			
			<img  alt="image1" ng-show="PayrollName === 'N2 Service Inc/'"src="style/N2-logo.png" width="170px" height="40px" style="float:left;margin-left:0px;"> 
			<img  alt="image1" ng-show="PayrollName !== 'N2 Service Inc/'"src="style/pattonlabs_logo1.jpg" width="70px" height="60px" style="width:50px;float:left;margin-left:0px;"> 
			<div  style="margin-left:60px;margin-top:20px;"><span ng-show="PayrollName !== 'N2 Service Inc/'">{{PayrollName}}</span>
			
			
			<span ng-show="PayrollName !== 'N2 Service Inc/'" >{{}}</span>
			
			</div>
			<div class="pull-right">
<a style=" margin-right: 5px;" href="#profile" >  {{firstname2}}</a>
				<a style=" margin-right: 3px;"  href="#report">Report</a>
				<a style="margin-right: 3px;"href="#user">Settings</a> 
				
				<a href="#logout" ng-click="logout()">{{value}}</a>
			</div>
		</div><br/>
	</div>
	
<nav class="navbar navbar-inverse" style="width: 96%; margin-left: 75px;  background-color:#333;">
  <div class="container" ng-controller="mainCtrl1">
        <ul class="nav navbar-nav" style="font-size: 15px;" >
    
      		<li><a id='{{home1}}' href="#Home1" ng-click="  action();">Home</a></li>
			<li><a id='{{profile}}' href="#profile" ng-click="  action4();">My Profile</a></li>
			<li><a id='{{timesheet}}' href="#timesheet"	ng-click="  action9();">Timesheet</a></li>
			<li><a id='{{statusreport1}}' href="#StatusReport1" ng-click=" action2();">Status Report</a></li>			
			<li><a id='{{performancereview1}}' href="#PerformanceReview1" ng-click="  action3();">Performance Review</a></li>
			<li><a id='{{expenses1}}' href="#Expenses1"	ng-click=" action1();">Expenses</a></li>
			
			<li><a id='{{blog}}' href="#blog" ng-click="  action8();">Blog</a></li>
			<li><a id='{{leave}}' href="#leave" ng-click="  action11();">Leave</a></li>
			
			<li><a id='{{coe}}' href="#coe" ng-click="  action10();">COE</a></li>
			<li><a id='{{support}}' href="#support" ng-click="  action6();">Support</a></li>
			<li><a id='{{help}}' href="#help" ng-click="  action7();">Help</a></li>
      
       </ul>
      
     
  </div>
</nav>
 <div ng-view align="center" >
 
 
 </div>





	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script> 
  	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-route.min.js"></script>
	  	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-animate.js"></script>
	<script src="https://code.angularjs.org/1.5.0/angular-aria.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.0.5/angular-material.min.js"></script>
	
	
    
    	
  
    
    <script src="http://code.jquery.com/jquery-2.0.3.min.js" data-semver="2.0.3" data-require="jquery"></script>
 		
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" data-semver="3.1.1" data-require="bootstrap"></script>
  
    <script src="http://bootboxjs.com/bootbox.js"></script>
        

	<script src="scriptFile/app.js"></script>
 	<script src = "mainApp1.js"></script>
 	<script src="scriptFile/controller_file/HomeController.js"></script>
  
	<!-- 	Login process -->
		<script src="scriptFile/controller_file/login_controller.js"></script>
		<script src="scriptFile/service_file/login_service.js"></script>
		
		
		<!-- 	Expense process -->
		<script src="scriptFile/controller_file/ExpensesController.js"></script>
		<script src="scriptFile/service_file/ExpensesService.js"></script>
		
		
<!-- 			StatusReport process -->
		<script src="scriptFile/controller_file/StatusReportController.js"></script>
		<script src="scriptFile/service_file/StatusReportService.js"></script>
		
<!-- 		<!-- 	PerformanceReview process  -->
		<script src="scriptFile/controller_file/PerformanceReviewController.js"></script>
		<script src="scriptFile/service_file/PerformanceReviewService.js"></script>
		
		<!-- 	Payroll process -->
		<script src="scriptFile/controller_file/ProfileController.js"></script>
		<script src="scriptFile/service_file/ProfileService.js"></script>
		<script src="scriptFile/controller_file/ChartController.js"></script>
		
		<!-- 	Leave process  -->
		<script type="text/javascript" src="scriptFile/controller_file/LeaveController.js"></script>
		<script type="text/javascript" src="scriptFile/service_file/LeaveService.js"></script>
		
		<!-- 	AddEmployee process-->
		<script src="scriptFile/controller_file/AddEmployeeController.js"></script>
		<script src="scriptFile/service_file/AddEmployeeService.js"></script>
		
		<!-- 	Support process -->
		<script src="scriptFile/controller_file/SupportController.js"></script>
			
		
	
	<!-- 	Help process-->
		<script src="scriptFile/controller_file/HelpController.js"></script>
		
			<script src="scriptFile/controller_file/CoeController.js"></script>
		
	<!-- 	Blog process-->	
			<script src="scriptFile/controller_file/BlogController.js"></script>
			<script src="scriptFile/service_file/blogService.js"></script>
	<!-- 	TimeSheet process -->
		
		<script type="text/javascript" src="scriptFile/controller_file/TimesheetController.js"></script>
		<script type="text/javascript" src="scriptFile/service_file/TimesheetService.js"></script>
			
			
 	 <script type="text/javascript" src="https://cdn.rawgit.com/adonespitogo/angular-base64-upload/master/src/angular-base64-upload.js"></script>

 	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/moment.min.js"></script>

  
    <script src="https://rawgit.com/dalelotts/angular-bootstrap-datetimepicker/master/src/js/datetimepicker.js"></script>

   			   <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.js"></script>
	<script type="text/javascript" src="https://cdn.rawgit.com/adonespitogo/angular-base64-upload/master/src/angular-base64-upload.js"></script>
	
	<script src="scriptFile\Blob.js"></script>
		<script src="scriptFile\FileSaver.js"></script>
	<script src="scriptFile\FileSaver.min.js"></script>
	 <script src="scriptFile\signaturepad.js"></script>
	
<!-- 	signature -->
	<script>document.write('<base href="' + document.location + '" />');</script>
    
    <script data-semver="1.4.9" src="https://code.angularjs.org/1.4.9/angular.js" data-require="angular.js@1.4.x"></script>
<!--     <script src="https://rawgit.com/szimek/signature_pad/adca0a1df8dece8419216905fa1926d9b41cb810/signature_pad.js"></script> -->


<!-- 	Document Download -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
 
 
 <!-- Org chart -->
 	 <!-- Org chart -->
  	
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
 <script src="scriptFile\directive_file\angular-fcsa-number.js"></script>
  
  <link rel="stylesheet" href="https://static.formhero.io/formhero/js/material/v1.1.1-fh-build/angular-material.min.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment-with-locales.min.js"></script>
  <script src="https://static.formhero.io/formhero/js/material/v1.1.1-fh-build/angular-material.min.js"></script>
 
  <script src="scriptFile\directive_file\ng-pattern-restrict.js"></script>
  <script src="scriptFile\directive_file\validNumber.js"></script>
   <script data-require="angular-messages@1.4.9" data-semver="1.4.9" src="https://code.angularjs.org/1.4.9/angular-messages.js"></script>
  <script>
  google.load('visualization', '1', {'packages' : ['orgchart']});

  app.factory('google', function(){
    return google;
  });

  
  </script>
  
  
</body>
</html>