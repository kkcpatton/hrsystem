<html ng-app="plunker" >
<head>
  <meta charset="utf-8">
  <title>AngularJS Plunker</title>
  
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.js"></script>
  <script src="app.js"></script>
  
  <script>
  var app = angular.module('plunker', []);
  google.load('visualization', '1', {'packages' : ['orgchart']});

  app.factory('google', function(){
    return google;
  });

  app.controller('ChartCtrl', function($scope, google) {
    $scope.arrData = [
      ['Name', 'Manager'],
      ['Jacob', 'Alex'],
      ['Martin', 'Jacob'],
      ['Martin', 'Pablo'],
      ['Alex', 'Max'],
      ['Pablo', 'Alex']
    ];
    $scope.draw = function() {
      var el = document.getElementById('orgchart');
      var orgChart = new google.visualization.OrgChart(el);
      orgChart.draw(google.visualization.arrayToDataTable($scope.arrData));
    };
    $scope.name = 'test';
  });
  
  </script>
</head>
<body>
<div ng-controller="ChartCtrl">
  <button ng-click="draw()">Render</button>
  <div id="orgchart"></div>
  </div>
</body>
</html>